
--
-- Table structure for table `partners_log`
--

CREATE TABLE IF NOT EXISTS `partners_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_req_id` varchar(50) NOT NULL,
  `partner_id` int(11) NOT NULL,
  -- `vendor_actv_id` int(20) NOT NULL,
  `vendor_actv_id` varchar(20) NULL DEFAULT '0',
  `err_code` varchar(50) NULL,
  `description` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `partner_id` (`partner_id`,`partner_req_id`),
  KEY `vendor_actv_id` (`vendor_actv_id`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Add comment  4-> via API
--
ALTER TABLE `vendors_activations` CHANGE `api_flag` `api_flag` INT( 2 ) NOT NULL DEFAULT '0' COMMENT '0-> via sms, 1-> via api, 2-> via ussd, 4-> via PAY1-API'

ALTER TABLE `partners_log` CHANGE `vendor_actv_id` `vendor_actv_id` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0'
ALTER TABLE `partners_log` CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL 