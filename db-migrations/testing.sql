/* APP Retailers List */
SELECT 
retailers.mobile , retailers.name , distributors.company  
FROM `retailers_logs`
JOIN retailers ON retailers.id = retailer_id
LEFT JOIN distributors ON retailers.parent_id = distributors.id
WHERE 
retailers_logs.date >= '2013-05-17' 
GROUP BY retailer_id 
HAVING (sum(retailers_logs.app_sale) > 0)


/* USSD Retailers List */
SELECT 
retailers.mobile 
FROM `retailers_logs`
JOIN retailers ON retailers.id = retailer_id
LEFT JOIN distributors ON retailers.parent_id = distributors.id
WHERE 
retailers.id = retailer_id AND 
retailers.parent_id = distributors.id AND 
retailers_logs.date >= '2013-05-17' 
GROUP BY retailer_id 
HAVING (sum(retailers_logs.ussd_sale) > 0)


/* SMS Retailers List */
SELECT 
retailers.mobile 
FROM `retailers_logs`
JOIN retailers ON retailers.id = retailer_id
LEFT JOIN distributors ON retailers.parent_id = distributors.id 
WHERE 
retailers.id = retailer_id AND 
retailers.parent_id = distributors.id AND 
retailers_logs.date >= '2013-05-17' 
GROUP BY retailer_id 
HAVING (sum(retailers_logs.sms_sale) > 0)