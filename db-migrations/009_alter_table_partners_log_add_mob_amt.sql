ALTER TABLE `partners_log` 
ADD `mob_dth_no` VARCHAR( 20 ) NOT NULL AFTER `vendor_actv_id` ,
ADD `amount` INT NOT NULL AFTER `mob_dth_no` ,
ADD INDEX ( `mob_dth_no` ) ,
ADD `product_id` INT( 5 ) NOT NULL 