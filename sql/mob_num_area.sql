CREATE TABLE IF NOT EXISTS `mobile_numbering_area` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `area_code` text,
  `area_name` text,
  `oss_code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

--
-- Dumping data for table `mobile_numbering_area`
--

INSERT INTO `mobile_numbering_area` (`id`, `area_code`, `area_name`, `oss_code`) VALUES
(1, 'AP', 'Andhra Pradesh Telecom Circle.', 'AP'),
(2, 'AS', 'Assam Telecom Circle.', 'AS'),
(3, 'BR', 'Bihar &amp; Jharkhand Telecom Circle.', 'BH'),
(4, 'CH', 'Chennai Metro Telecom Circle (includes MEPZ, Mahabalipuram &amp; Minjur)', 'CH'),
(5, 'DL', 'Delhi Metro Telecom Circle (includes NCR, Faridabad, Ghaziabad, Gurgaon &amp; Noida)', 'DH'),
(6, 'GJ', 'Gujarat Telecom Circle (includes Daman &amp; Diu, Dadra &amp; Nagar Haveli)', 'GJ'),
(7, 'HP', 'Himachal Pradesh Telecom Circle.', 'HP'),
(8, 'HR', 'Haryana Telecom Circle (excludes Faridabad, Gurgaon &amp; Panchkula)', 'HR'),
(9, 'JK', 'Jammu &amp; Kashmir Telecom Circle.', 'JK'),
(10, 'KL', 'Kerala Telecom Circle (includes Lakshadweep)', 'KE'),
(11, 'KA', 'Karnataka Telecom Circle.', 'KR'),
(12, 'KO', 'Kolkata Metro Telecom Circle (includes parts of Haora, Hooghly, North &amp; South 24 Parganas and Nadia Districts)', 'CK'),
(13, 'MH', 'Maharashtra Telecom Circle (includes Goa but excludes Mumbai, Navi Mumbai &amp; Kalyan)', 'MH'),
(14, 'MP', 'Madhya Pradesh &amp; Chhattisgarh Telecom Circle.', 'MP'),
(15, 'MU', 'Mumbai Metro Telecom Circle (includes Navi Mumbai &amp; Kalyan)', 'MU'),
(16, 'NE', 'North East India Telecom Circle (excludes Assam)', 'NE'),
(17, 'OR', 'Orissa Telecom Circle.', 'OR'),
(18, 'PB', 'Punjab Telecom Circle (includes Chandigarh &amp; Panchkula)', 'PN'),
(19, 'RJ', 'Rajasthan Telecom Circle.', 'RJ'),
(20, 'TN', 'Tamil Nadu Telecom Circle (includes Pondichery except Yanam, usually excludes Chennai, MEPZ, Mahabalipuram &amp; Minjur)', 'TN'),
(21, 'TC', 'Tamil Nadu + Chennai', '0'),
(22, 'UE', 'Uttar Pradesh (East) Telecom Circle.', 'UPE'),
(23, 'UW', 'Uttar Pradesh (West) &amp; Uttarakhand Telecom Circle (excludes Ghaziabad &amp; Noida)', 'UPW'),
(24, 'WB', 'West Bengal Telecom Circle (includes Andaman &amp; Nicobar, Sikkim, excludes Calcutta Telecom District)', 'WB'),
(25, 'ZZ', 'Customer Care (All Over India)', 'ALI');