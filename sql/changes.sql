ALTER TABLE  `vendors_activations` ADD  `param` VARCHAR( 20 ) NULL DEFAULT NULL AFTER  `mobile`;

ALTER TABLE  `vendors_activations` CHANGE  `cause`  `cause` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
CREATE TABLE IF NOT EXISTS `vendors_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_tran_id` varchar(30) DEFAULT NULL,
  `service_id` int(5) NOT NULL,
  `service_vendor_id` int(11) NOT NULL,
  `internal_error_code` varchar(30) DEFAULT NULL,
  `response` text,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `shops`.`services_disabled` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT( 11 ) NOT NULL ,
`service_id` INT( 5 ) NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE  `vendors_activations` ADD  `extra` VARCHAR( 255 ) NULL ,
ADD  `complaintNo` VARCHAR( 255 ) NULL;
ALTER TABLE  `vendors_activations` ADD  `prevStatus` VARCHAR( 3 ) NULL AFTER  `status`;



CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `retailers_id` int(11) NOT NULL,
  `ref_code` varchar(16) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `comments` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1  ;

CREATE TABLE  `shops`.`oss_rec_codes` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`opr_code` VARCHAR( 10 ) NULL ,
`circle_code` VARCHAR( 10 ) NULL ,
`denomination` VARCHAR( 15 ) NULL ,
`rec_code` VARCHAR( 30 ) NULL
) ENGINE = MYISAM ;

CREATE TABLE IF NOT EXISTS `vendors_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_tran_id` varchar(30) DEFAULT NULL,
  `service_id` int(5) NOT NULL,
  `service_vendor_id` int(11) NOT NULL,
  `internal_error_code` varchar(30) DEFAULT NULL,
  `response` text,
  `status` varchar(10) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

ALTER TABLE `comments`  ADD `mobile` VARCHAR(12) NULL AFTER `comments`;

CREATE TABLE  `shops`.`salesmen` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 100 ) NOT NULL ,
`mobile` VARCHAR( 15 ) NULL ,
`tran_limit` INT( 11 ) NOT NULL ,
`bal` INT( 11 ) NOT NULL ,
`extra` TEXT NULL ,
`created` DATETIME NULL
) ENGINE = MYISAM ;

ALTER TABLE  `salesmen` CHANGE  `bal`  `balance` INT( 11 ) NOT NULL;

ALTER TABLE  `retailers` ADD  `salesman` INT( 5 ) NOT NULL AFTER  `kyc_flag` ,
ADD  `kyc` TEXT NULL AFTER  `salesman` ,
ADD  `mobile_info` TEXT NULL AFTER  `kyc` ,
ADD  `app_type` VARCHAR( 255 ) NOT NULL AFTER  `mobile_info`;

CREATE TABLE  `shops`.`salesman_transactions` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`salesman` INT( 11 ) NOT NULL ,
`amount` VARCHAR( 15 ) NOT NULL ,
`payment_mode` INT( 3 ) NOT NULL ,
`payment_type` INT NOT NULL ,
`details` TEXT NULL ,
`created` DATETIME NOT NULL
) ENGINE = MYISAM ;

ALTER TABLE  `salesman_transactions` CHANGE  `payment_mode`  `payment_mode` INT( 3 ) NOT NULL COMMENT  'MODE_CASH->1,MODE_CHEQUE->2,MODE_NEFT->3,MODE_DD->4',
CHANGE  `payment_type`  `payment_type` INT( 11 ) NOT NULL COMMENT  'SETUP->1,TOPUP->2';

ALTER TABLE  `salesman_transactions` ADD  `shop_tran_id` VARCHAR( 25 ) NOT NULL AFTER  `id`;
ALTER TABLE  `salesman_transactions` DROP  `amount`;
ALTER TABLE  `salesman_transactions` ADD  `collection_date` DATETIME NOT NULL AFTER  `details`;
ALTER TABLE  `salesman_transactions` CHANGE  `collection_date`  `collection_date` DATE NOT NULL;

DROP TABLE `salesmen`;

CREATE TABLE IF NOT EXISTS `salesmen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dist_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `tran_limit` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `extra` text,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MYISAM;

DROP TABLE `salesman_transactions`;

CREATE TABLE IF NOT EXISTS `salesman_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_tran_id` varchar(25) NOT NULL,
  `salesman` int(11) NOT NULL,
  `payment_mode` int(3) NOT NULL COMMENT 'MODE_CASH->1,MODE_CHEQUE->2,MODE_NEFT->3,MODE_DD->4',
  `payment_type` int(11) NOT NULL COMMENT 'SETUP->1,TOPUP->2',
  `details` text,
  `collection_date` date NOT NULL,
  `confirm_flag` int(3) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MYISAM;

CREATE TABLE IF NOT EXISTS `virtual_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(12) NOT NULL,
  `message` text NOT NULL,
  `virtual_num` varchar(12) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE  `shops`.`app_req_log` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`method` VARCHAR( 30 ) NOT NULL ,
`params` TEXT NOT NULL ,
`timesatmp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = MYISAM ;

ALTER TABLE  `app_req_log` ADD  `ret_id` VARCHAR( 15 ) NULL AFTER  `id`;


CREATE TABLE IF NOT EXISTS `user_taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagging_id` int(11) NOT NULL,
  `user_id` varchar(16) DEFAULT NULL,
  `transaction_id` varchar(16) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  ;

CREATE TABLE IF NOT EXISTS `taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  ;


INSERT INTO `taggings` (`id`, `name`) VALUES
(null, 'Drop SMS'),
(null, 'Manual Check'),
(null, 'Manual Reversal'),
(null, 'System Error'),
(null, 'Modem Reversal'),
(null, 'OSS Reversal'),
(null, 'PPI Reversal'),
(null, 'None');

ALTER TABLE `products` ADD `to_show` INT NOT NULL DEFAULT '1' AFTER `price` 

UPDATE `shops`.`products` SET `to_show` = '0' WHERE `products`.`id` =26;


ALTER TABLE `salesmen` ADD `password`  CHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1234', ADD `subarea_id` INT(11) NULL DEFAULT NULL



ALTER TABLE `retailers` ADD `subarea_id` INT(10) NULL DEFAULT NULL, ADD `retailer_type` INT(10) NULL DEFAULT '0'

ALTER TABLE `salesman_transactions` ADD  `billbook_number` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, ADD `cheque_number` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, ADD  `collection_amount` INT(11) NOT NULL DEFAULT '0',ADD`bankname` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL

ALTER TABLE `shop_transactions` ADD `confirm_flag` INT(3) NOT NULL DEFAULT '0'

CREATE TABLE IF NOT EXISTS `subarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM   ;

CREATE TABLE IF NOT EXISTS `salesmen_subarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salesmen_id` int(11) NOT NULL,
  `subarea_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  ;



ALTER TABLE  `distributors` ADD  `level` INT( 2 ) NOT NULL DEFAULT  '1' AFTER  `tds_flag` ,
ADD  `kits` INT( 10 ) NOT NULL DEFAULT  '0' AFTER  `level`;

ALTER TABLE  `distributors` ADD  `retailer_limit` INT( 5 ) NOT NULL DEFAULT  '-1' AFTER  `kits` ,
ADD  `commission_kits_flag` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `retailer_limit`;

ALTER TABLE  `distributors` ADD  `discount_kit` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `commission_kits_flag` ,
ADD  `discounted_money` INT( 1 ) NOT NULL DEFAULT  '0' AFTER  `discount_kit`;


CREATE TABLE IF NOT EXISTS `distributors_kits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `kits` int(5) NOT NULL,
  `amount` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;


CREATE TABLE IF NOT EXISTS `earnings_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(5) NOT NULL,
  `opening` float(10,2) DEFAULT NULL,
  `closing` float(10,2) DEFAULT NULL,
  `sale` float(10,2) DEFAULT NULL,
  `invested` float(10,2) DEFAULT NULL,
  `expected_earning` float(10,2) DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `distributors_logs` ADD `earning` FLOAT( 6, 2 ) NULL DEFAULT NULL AFTER `topup_unique` ;
ALTER TABLE `retailers_logs` ADD `earning` FLOAT( 6, 2 ) NULL DEFAULT NULL AFTER `sale` ;


CREATE TABLE IF NOT EXISTS `ussd_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(10) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sent_xml` text DEFAULT NULL,
  `response` text DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `ussd_logs` ADD  `level` INT( 20 ) NOT NULL DEFAULT  '0' AFTER  `parent`;