/**
 * S7 Admin Template
**/
$(document).ready(function(){
	var rowCount = $('.data-table >tbody >tr').length;
        if(rowCount > 1){
            $('.data-table').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<""l>t<"F"fp>',
                "aaSorting": []
            });
            $("span.icon input:checkbox, th input:checkbox").click(function() {
		var checkedStatus = this.checked;
		var checkbox = $(this).parents('.widget-box').find('tr td:first-child input:checkbox');		
		checkbox.each(function() {
			this.checked = checkedStatus;
			if (checkedStatus == this.checked) {
				$(this).closest('.checker > span').removeClass('checked');
			}
			if (this.checked) {
				$(this).closest('.checker > span').addClass('checked');
			}
		});
            });
        }
		
        $("#change_password_form").validate({
		rules:{
			curr_password:{
				required: true,
				minlength:4,
				maxlength:20
			},
                        new_password:{
				required: true,
				minlength:4,
				maxlength:20
			},
			conf_new_password:{
				required:true,
				minlength:4,
				maxlength:20,
				equalTo:"#new_password"
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});
});
