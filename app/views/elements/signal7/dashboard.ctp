                        <div id="content-header">
                                <h1><? echo $content_heading?></h1>
                                <!--<div style="width: auto;" class="btn-group">
                                        <a data-original-title="Manage Files" class="btn btn-large tip-bottom" title=""><i class="icon-file"></i></a>
                                        <a data-original-title="Manage Users" class="btn btn-large tip-bottom" title=""><i class="icon-user"></i></a>
                                        <a data-original-title="Manage Comments" class="btn btn-large tip-bottom" title=""><i class="icon-comment"></i><span class="label label-important">5</span></a>
                                        <a data-original-title="Manage Orders" class="btn btn-large tip-bottom" title=""><i class="icon-shopping-cart"></i></a>
                                </div>-->
                        </div>
                        <div id="breadcrumb">
                                <a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
                                <a href="#" class="current">Dashboard</a>
                        </div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="alert alert-info">
							Welcome in the <strong>Signal7 Partner Admin Penal</strong>. Don't forget to check all the pages!
							<a href="#" data-dismiss="alert" class="close">�</a>
						</div>
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site Statistics</h5><div class="buttons"><a href="#" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div></div>
							<div class="widget-content">
								<div class="row-fluid">
								<div class="span4">
									<ul class="site-stats">
										<li><i class="icon-user"></i> <strong>1433</strong> <small>Total Users</small></li>
										<li><i class="icon-arrow-right"></i> <strong>16</strong> <small>New Users (last week)</small></li>
										<li class="divider"></li>
										<li><i class="icon-shopping-cart"></i> <strong>259</strong> <small>Total Shop Items</small></li>
										<li><i class="icon-tag"></i> <strong>8650</strong> <small>Total Orders</small></li>
										<li><i class="icon-repeat"></i> <strong>29</strong> <small>Pending Orders</small></li>
									</ul>
								</div>
								<div class="span8">
									<div style="padding: 0px; position: relative;" class="chart"><canvas height="300" width="696" class="base"></canvas><canvas style="position: absolute; left: 0px; top: 0px;" height="300" width="696" class="overlay"></canvas><div class="tickLabels" style="font-size:smaller"><div class="xAxis x1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:center;left:-17px;top:280px;width:87px">0</div><div class="tickLabel" style="position:absolute;text-align:center;left:81px;top:280px;width:87px">2</div><div class="tickLabel" style="position:absolute;text-align:center;left:180px;top:280px;width:87px">4</div><div class="tickLabel" style="position:absolute;text-align:center;left:279px;top:280px;width:87px">6</div><div class="tickLabel" style="position:absolute;text-align:center;left:377px;top:280px;width:87px">8</div><div class="tickLabel" style="position:absolute;text-align:center;left:476px;top:280px;width:87px">10</div><div class="tickLabel" style="position:absolute;text-align:center;left:575px;top:280px;width:87px">12</div></div><div class="yAxis y1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:right;top:255px;right:677px;width:19px">-1.5</div><div class="tickLabel" style="position:absolute;text-align:right;top:213px;right:677px;width:19px">-1.0</div><div class="tickLabel" style="position:absolute;text-align:right;top:171px;right:677px;width:19px">-0.5</div><div class="tickLabel" style="position:absolute;text-align:right;top:129px;right:677px;width:19px">0.0</div><div class="tickLabel" style="position:absolute;text-align:right;top:86px;right:677px;width:19px">0.5</div><div class="tickLabel" style="position:absolute;text-align:right;top:44px;right:677px;width:19px">1.0</div><div class="tickLabel" style="position:absolute;text-align:right;top:2px;right:677px;width:19px">1.5</div></div></div><div class="legend"><div style="position: absolute; width: 54px; height: 44px; top: 9px; right: 9px; background-color: rgb(249, 249, 249); opacity: 0.85;"> </div><table style="position:absolute;top:9px;right:9px;;font-size:smaller;color:#545454"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #BA1E20;overflow:hidden"></div></div></td><td class="legendLabel">sin(x)</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #459D1C;overflow:hidden"></div></div></td><td class="legendLabel">cos(x)</td></tr></tbody></table></div></div>
								</div>	
								</div>							
							</div>
						</div>					
					</div>
				</div>
				
			</div>
	
		