<div id="sidebar"> 
			<a href="/partner/dashboard" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
			<ul style="display: block;">
                            <li <?php if($active_menu == 'dashboard')echo "class='active'";?> ><a href="/partner/dashboard"><i class="icon icon-home"></i> <span>Dashboard</span></a></li>
                            <li <?php if($active_menu == 'change_password')echo "class='active'";?>><a href="/partner/changePassword"><i class="icon icon-lock"></i> <span>Change Password</span></a></li>
                            <li <?php if($active_menu == 'topup_request')echo "class='active'";?>><a href="/partner/topUpRequest"><i class="icon  icon-briefcase"></i> <span>TopUp Request</span></a></li>
                            <li class="submenu <?php if($active_menu == 'reports') echo " active open ";?>">
                                    <a href="#"><i class="icon icon-th-list"></i> <span>Reports</span> <span class="label">3</span></a>
                                    <ul>
                                            <li <?php if($active_submenu == 'recharge_list')echo "class='active'";?> ><a href="/partner/rechargeList">Recharge List</a></li>
                                            <li <?php if($active_submenu == 'transfer_details')echo "class='active'";?> ><a href="/partner/transferDetails">Transfer Details</a></li>
                                            <li <?php if($active_submenu == 'discount_structure')echo "class='active'";?> ><a href="/partner/discountStructure">Discount Structure</a></li>
                                    </ul>
                            </li>

			</ul>
		
		</div>
            