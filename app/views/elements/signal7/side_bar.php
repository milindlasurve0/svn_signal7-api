<div id="content-header">
				<h1>Reports</h1>
<!--				<div style="width: auto;" class="btn-group">
					<a data-original-title="Manage Files" class="btn btn-large tip-bottom" title=""><i class="icon-file"></i></a>
					<a data-original-title="Manage Users" class="btn btn-large tip-bottom" title=""><i class="icon-user"></i></a>
					<a data-original-title="Manage Comments" class="btn btn-large tip-bottom" title=""><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a data-original-title="Manage Orders" class="btn btn-large tip-bottom" title=""><i class="icon-shopping-cart"></i></a>
				</div>-->
			</div>
			<div id="breadcrumb">
				<a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="tip-bottom"><i class="icon icon-th-list"></i>Reports</a>
                                <a href="#" class="current">Recharge List</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
					<div class="row-fluid" >
                                            <div class="span6">
					<div class="input-prepend">
					  <span class="add-on">From</span>
					  <input  id="prependedInput" type="text" placeholder="Date From">
					</div>
					<div class="input-prepend">
					  <span class="add-on">To</span>
					  <input id="prependedInput" type="text" placeholder="Date To">
					</div>
                                                </div>
					<div class="span2">
					<button class="btn">
						<i class="icon-search"></i>
						
					</button>
					</div>
					</div>
<!--						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5>Static table</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover">
									<thead>
										<tr>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
									</tbody>
								</table>							
							</div>
						</div>-->
<!--						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<div id="uniform-title-checkbox" class="checker"><span><input style="opacity: 0;" id="title-checkbox" name="title-checkbox" type="checkbox"></span></div>
								</span>
								<h5>Static table with checkboxes</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover with-check">
									<thead>
										<tr>
											<th><i class="icon-resize-vertical"></i></th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
									</tbody>
								</table>							
							</div>
						</div>-->
						
<!--						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5>Static table with checkboxes in box with padding</h5>
								<span class="label label-info">Featured</span>
							</div>
							<div class="widget-content">
								<table class="table table-bordered table-striped table-hover with-check">
									<thead>
										<tr>
											<th><div id="uniform-title-table-checkbox" class="checker"><span><input style="opacity: 0;" id="title-table-checkbox" name="title-table-checkbox" type="checkbox"></span></div></th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
									</tbody>
								</table>							
							</div>
						</div>-->
						
						<div class="widget-box">
							<div class="widget-title">
							
								<h5>Recharge History ( 18-06-2013 - 18-06-2013 )</h5>
							</div>
							<div class="widget-content nopadding">
								<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<!--                                                                    <div class="">
                                                                        <div class="dataTables_length" id="DataTables_Table_0_length">
                                                                            <label>Show 
                                                                                <div id="s2id_autogen1" class="select2-container">    
                                                                                    <a href="#" onclick="return false;" class="select2-choice">   
                                                                                        <span>10</span>
                                                                                        <abbr class="select2-search-choice-close" style="display:none;">                                                                                            
                                                                                        </abbr>
                                                                                        <div>
                                                                                            <b></b>
                                                                                        </div>
                                                                                    </a>
                                                                                    <div class="select2-drop select2-offscreen">
                                                                                        <div class="select2-search">
                                                                                            <input tabindex="0" autocomplete="off" class="select2-input" type="text"/>   
                                                                                        </div>   
                                                                                        <ul class="select2-results">   
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <select style="display: none;" aria-controls="DataTables_Table_0" size="1" name="DataTables_Table_0_length">
                                                                                    <option selected="selected" value="10">10</option>
                                                                                    <option value="25">25</option>
                                                                                    <option value="50">50</option>
                                                                                    <option value="100">100</option>
                                                                                </select> 
                                                                            entries
                                                                            </label>
                                                                        </div>
                                                                    </div>-->
                                                                    
                                                                    <table id="DataTables_Table_0" class="table table-bordered table-striped table-hover data-table dataTable">
									<thead>
                                                                            
                                                                            <tr role="row">
                                                                                <th aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 192px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Date
                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-triangle-1-n"></span>
                                                                                    </div>
                                                                                </th>
                                                                                <th aria-label="Browser: activate to sort column ascending" style="width: 343px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Trans Id
                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span>
                                                                                    </div>
                                                                                </th>
                                                                                <th aria-label="Platform(s): activate to sort column ascending" style="width: 315px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Number
                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span>
                                                                                    </div>
                                                                                </th>
                                                                                <th aria-label="Platform(s): activate to sort column ascending" style="width: 315px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Operator
                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span>
                                                                                    </div>
                                                                                </th>
                                                                                <th aria-label="Engine version: activate to sort column ascending" style="width: 157px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Amount
                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span>
                                                                                    </div>
                                                                                </th>
                                                                                <th aria-label="Engine version: activate to sort column ascending" style="width: 157px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Opening
                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span>
                                                                                    </div>
                                                                                </th>
                                                                                <th aria-label="Engine version: activate to sort column ascending" style="width: 157px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Closing
                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span>
                                                                                    </div>
                                                                                </th>
                                                                                <th aria-label="Browser: activate to sort column ascending" style="width: 343px;" colspan="1" rowspan="1" aria-controls="DataTables_Table_0" tabindex="0" role="columnheader" class="ui-state-default">
                                                                                    <div class="DataTables_sort_wrapper">
                                                                                        Status
<!--                                                                                        <span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span>-->
                                                                                    </div>
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
									
									<tbody aria-relevant="all" aria-live="polite" role="alert">
                                                                        <tr class="gradeA odd">
                                                                        <td>29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        <td class="center number">9865654578</td>
                                                                        <td class="center number">Airtel</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA even">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        <td class="center number">9865654578</td>
                                                                        <td class="center number">Airtel</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA odd">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        <td class="center number">9865654578</td>
                                                                        <td class="center number">Aircel</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA even">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        <td class="center number">9865654578</td>
                                                                        <td class="center number">Reliance</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA odd">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        <td class="center number">9865654578</td>
                                                                        <td class="center number">Reliance-GSM</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA even">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        <td class="center number">9865654578</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA odd">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        <td class="center number">9865654578</td>
                                                                        <td class="center number">Tata-Docomo</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                            <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search: <input aria-controls="DataTables_Table_0" type="text"></label></div><div id="DataTables_Table_0_paginate" class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers"><a id="DataTables_Table_0_first" tabindex="0" class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled">First</a><a id="DataTables_Table_0_previous" tabindex="0" class="previous fg-button ui-button ui-state-default ui-state-disabled">Previous</a><span><a tabindex="0" class="fg-button ui-button ui-state-default ui-state-disabled">1</a><a tabindex="0" class="fg-button ui-button ui-state-default">2</a><a tabindex="0" class="fg-button ui-button ui-state-default">3</a><a tabindex="0" class="fg-button ui-button ui-state-default">4</a><a tabindex="0" class="fg-button ui-button ui-state-default">5</a></span><a id="DataTables_Table_0_next" tabindex="0" class="next fg-button ui-button ui-state-default">Next</a><a id="DataTables_Table_0_last" tabindex="0" class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default">Last</a></div></div></div>  
							</div>
						</div>
					</div>
				</div>
				
				<div class="row-fluid">
					<div id="footer" class="span12">
						2012 - 2013 © Unicorn Admin. Brought to you by <a href="https://wrapbootstrap.com/user/diablo9983">diablo9983</a>
					</div>
				</div>
			</div>
		