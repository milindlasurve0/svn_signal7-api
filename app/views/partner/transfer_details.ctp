<div id="content-header">
				<h1>Reports</h1>
<!--				<div style="width: auto;" class="btn-group">
					<a data-original-title="Manage Files" class="btn btn-large tip-bottom" title=""><i class="icon-file"></i></a>
					<a data-original-title="Manage Users" class="btn btn-large tip-bottom" title=""><i class="icon-user"></i></a>
					<a data-original-title="Manage Comments" class="btn btn-large tip-bottom" title=""><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a data-original-title="Manage Orders" class="btn btn-large tip-bottom" title=""><i class="icon-shopping-cart"></i></a>
				</div>-->
			</div>
			<div id="breadcrumb">
				<a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="tip-bottom"><i class="icon icon-th-list"></i>Reports</a>
                                <a href="#" class="current"><i class="icon icon-list"></i>Transfer Details</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
					<div class="row-fluid" >
                                            <div class="span6">
					<div class="input-prepend">
					  <span class="add-on">From</span>
					  <input  id="fromDate" name="fromDate" type="text" placeholder="Date From" data-date-format="dd-mm-yyyy" value="<?php if(isset($date_from)) echo date('d-m-Y', strtotime($date_from));?>"/>
					</div>
					<div class="input-prepend">
					  <span class="add-on">To</span>
					  <input id="toDate" name="toDate" type="text" placeholder="Date To" data-date-format="dd-mm-yyyy" value="<?php if(isset($date_to)) echo date('d-m-Y', strtotime($date_to));?>" />
					</div>
                                                </div>
					<div class="span2">
					<button class="btn" onclick="findHistory(<?php echo "'".$this->params['controller']."','".$this->params['action']."'" ?>);">
						<i class="icon-search"></i>
						
					</button>
					</div>
					</div>
<!--						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5>Static table</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover">
									<thead>
										<tr>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
									</tbody>
								</table>							
							</div>
						</div>-->
<!--						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<div id="uniform-title-checkbox" class="checker"><span><input style="opacity: 0;" id="title-checkbox" name="title-checkbox" type="checkbox"></span></div>
								</span>
								<h5>Static table with checkboxes</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped table-hover with-check">
									<thead>
										<tr>
											<th><i class="icon-resize-vertical"></i></th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
									</tbody>
								</table>							
							</div>
						</div>-->
						
<!--						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5>Static table with checkboxes in box with padding</h5>
								<span class="label label-info">Featured</span>
							</div>
							<div class="widget-content">
								<table class="table table-bordered table-striped table-hover with-check">
									<thead>
										<tr>
											<th><div id="uniform-title-table-checkbox" class="checker"><span><input style="opacity: 0;" id="title-table-checkbox" name="title-table-checkbox" type="checkbox"></span></div></th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
											<th>Column name</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
										<tr>
											<td><div id="uniform-undefined" class="checker"><span><input style="opacity: 0;" type="checkbox"></span></div></td>
											<td>Row 1</td>
											<td>Row 2</td>
											<td>Row 3</td>
											<td>Row 4</td>
										</tr>
									</tbody>
								</table>							
							</div>
						</div>-->
						
						<div class="widget-box">
							<div class="widget-title">
							
								<h5>Topup History  <?php if(isset($date_from) && isset($date_to)) echo "(". date('d-m-Y', strtotime($date_from)) . " - " .  date('d-m-Y', strtotime($date_to)) . ")"; ?> </h5>
							</div>
							<div class="widget-content nopadding">
								<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<!--                                                                    <div class="">
                                                                        <div class="dataTables_length" id="DataTables_Table_0_length">
                                                                            <label>Show 
                                                                                <div id="s2id_autogen1" class="select2-container">    
                                                                                    <a href="#" onclick="return false;" class="select2-choice">   
                                                                                        <span>10</span>
                                                                                        <abbr class="select2-search-choice-close" style="display:none;">                                                                                            
                                                                                        </abbr>
                                                                                        <div>
                                                                                            <b></b>
                                                                                        </div>
                                                                                    </a>
                                                                                    <div class="select2-drop select2-offscreen">
                                                                                        <div class="select2-search">
                                                                                            <input tabindex="0" autocomplete="off" class="select2-input" type="text"/>   
                                                                                        </div>   
                                                                                        <ul class="select2-results">   
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <select style="display: none;" aria-controls="DataTables_Table_0" size="1" name="DataTables_Table_0_length">
                                                                                    <option selected="selected" value="10">10</option>
                                                                                    <option value="25">25</option>
                                                                                    <option value="50">50</option>
                                                                                    <option value="100">100</option>
                                                                                </select> 
                                                                            entries
                                                                            </label>
                                                                        </div>
                                                                    </div>-->
                                                                
                                                                    <table id="trans_details_table" class="table table-bordered table-striped table-hover data-table dataTable">
									<thead>
                                                                            
                                                                            <tr role="row">
                                                                                <th >
                                                                                        Date
                                                                                       
                                                                                </th>
                                                                                <th >
                                                                                        Trans Id
                                                                                        
                                                                                </th>
                                                                               
                                                                                <th >
                                                                                        Amount
                                                                                        
                                                                                </th>
                                                                                <th >
                                                                                        Opening
                                                                                </th>
                                                                                <th  >
                                                                                        Closing
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
									
									<tbody aria-relevant="all" aria-live="polite" role="alert">
                                                                            <?php if(empty($transactions) || count($transactions) <= 0 ) { ?>
                    <tr>
                    	<td colspan="4"><span class="success">No Results Found !!</span></td>
                    </tr>
                    
                    <?php } else { ?>
                    <?php $i=0; foreach($transactions as $transaction){ 
                    	if($i%2 == 0)$class = 'even';
                    	else $class = 'odd';
                        $i++;
                    ?>
                      <tr class="<?php echo $class; ?>"> 
<!--                                                                        <tr class="gradeA odd">-->
                                                                       <td><?php echo date('d-m-Y H:i:s', strtotime($transaction['shop_transactions']['timestamp'])); ?></td>
                                    <td ><?php echo $transaction['shop_transactions']['id']; ?></td>
			            <td class="number"><?php echo $transaction['shop_transactions']['amount']; ?></td>
                                    <td class="number"><?php echo empty($transaction['opening_closing']['opening'])? 0 : $transaction['opening_closing']['opening']; ?></td> 
                                    <td class="number"><?php echo empty($transaction['opening_closing']['closing']) ? 0 : $transaction['opening_closing']['closing']; ?></td> 
                                    </tr>
    			    <?php }
    			    $i++; } ?> 	
<!--                                                                        <tr class="gradeA even">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA odd">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA odd">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA even">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA odd">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                       
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>
                                                                        <tr class="gradeA odd">
                                                                        <td class="center number">29-04-2013 15:19:29</td>
                                                                        <td class="center number">2</td>
                                                                        
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class="center number">0</td>
                                                                        <td class=" ">Invalid Number of Parameters. ( )</td>
                                                                        </tr>-->
                                                                        </tbody>
                                                                    </table>
                                                            </div>  
							</div>
						</div>
					</div>
				</div>
				
			</div>
		<script>
               function findHistory(controller,action){
                   
                    var html = $('submit').innerHTML;
                    //showLoader3('submit');
                    var date_from = $('#fromDate').val();
                    var date_to = $('#toDate').val();
                    if(date_from == '' || date_to == ''){
                            $('#date_err').show();
                            $('#submit').html(html);
                    }
                    else {
                            $('#date_err').hide();
                            date_from = date_from.replace(/-/g,"");
                            date_to = date_to.replace(/-/g,"");
                            //$('sub').setAttribute('href', "/partner/rechargeList/"+date_from+"-"+date_to);            
                            $url = "/" +controller+ "/" +action+ "/" ;

                            window.location.href = $url+date_from+"-"+date_to;

                    }
                }    
</script>