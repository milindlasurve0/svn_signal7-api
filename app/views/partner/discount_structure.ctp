<div id="content-header">
        <h1>Reports</h1>
        <!--<div style="width: auto;" class="btn-group">
                <a data-original-title="Manage Files" class="btn btn-large tip-bottom" title=""><i class="icon-file"></i></a>
                <a data-original-title="Manage Users" class="btn btn-large tip-bottom" title=""><i class="icon-user"></i></a>
                <a data-original-title="Manage Comments" class="btn btn-large tip-bottom" title=""><i class="icon-comment"></i><span class="label label-important">5</span></a>
                <a data-original-title="Manage Orders" class="btn btn-large tip-bottom" title=""><i class="icon-shopping-cart"></i></a>
        </div>-->
</div>
<div id="breadcrumb">
        <a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="#" ><i class="icon-th-list"></i>Reports</a>
        <a href="#" class="current"><i class="icon-list-alt"></i>Discount Structure</a>
</div>
<div class="container-fluid">


<div class="row-fluid">
					<div class="span12">

						
						<div class="widget-box">
							<div class="widget-title">
							
								<h5>Operator Wise Commission Slab ( % )</h5>
							</div>
							<div class="widget-content nopadding">
								<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">                                                                
                                                                    <table id="DataTables_Table_0" class="table table-bordered table-striped table-hover data-table dataTable">
									<thead>                                                                            
                                                                            <tr role="row">
                                                                                <th >
                                                                                        Product Name
                                                                                        
                                                                                </th>
                                                                                <th >
                                                                                        Product Code
                                                                                        
                                                                                </th>
                                                                               
                                                                                <th >
                                                                                        Slab ( % )
                                                                                        
                                                                                </th>
                                                                                
                                                                            </tr>
                                                                        </thead>

									<tbody aria-relevant="all" aria-live="polite" role="alert">
                                                                            <?php if(empty($transactions) || count($transactions) <= 0 ) { ?>
                                            <tr>
                                                <td colspan="4"><span class="success">No Results Found !!</span></td>
                                            </tr>
                    
                    <?php } else { ?>
                    <?php $i=0; foreach($transactions as $transaction){ 
                    	if($i%2 == 0)$class = 'even';
                    	else $class = 'odd';
                        
                    ?>
                      <tr class="gradeA <?php echo $class; ?>"> 
                                    <td class="center"><a href="#"><?php echo $transaction['products']['name']; ?></a></td>
                                    <td class="center"><?php echo $transaction['slabs_products']['product_id']; ?></td>
			            <td class="center number"><?php echo $transaction['slabs_products']['percent']; ?></td>
                                    </tr>
    			    <?php  $i++; }
                            
    			    } ?> 			
                                                                       
                        </tbody>
                    </table>

                </div>  
        </div>
</div>
</div>
			

</div>
    </div>

	