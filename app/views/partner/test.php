<div id="content-header">
    <h1>Send Top-Up Request</h1>
</div>
<div id="breadcrumb">
    <a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
    <a href="#" class="current "><i class="icon-edit"></i>Top-Up Request</a>
</div>
<div class="container-fluid">

    <div class="row-fluid">
        <div class="span12">

            <?php if (!empty($err_msg)) { ?>
                <div class="alert alert-error">
                    <?php echo $err_msg; ?>
                    <a href="#" data-dismiss="alert" class="close">×</a>
                </div>
            <?php } elseif (!empty($msg)) { ?>
                <div class="alert alert-success">
                    <?php echo $msg; ?>
                    <a href="#" data-dismiss="alert" class="close">×</a>
                </div>
            <?php } ?>

            <div class="widget-box">
                    <div class="widget-title"><span class="icon"><i class="icon-lock"></i></span><h5></h5><!--<div class="buttons"><a href="#" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div>--></div>
                <div class="widget-content">
                    <div class="row-fluid">


<!--                        <form id="change_password_form" class="form-horizontal" novalidate="novalidate" name="password_validate" action="" method="post">
                            <div class="control-group">
                                <label class="control-label">Current Password</label>
                                <div class="controls">
                                    <input id="curr_password" name="curr_password"  type="password" placeholder="Enter Current Password"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">New Password</label>
                                <div class="controls">
                                    <input id="new_password" name="new_password" type="password" placeholder="Enter New Password"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Confirm password</label>
                                <div class="controls">
                                    <input id="conf_new_password" name="conf_new_password" type="password" placeholder="Re-Enter New Password"/>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input class="btn btn-danger" type="submit" value="Reset" />
                            </div>
                        </form>-->
                        <?php echo $form->create('shop'); ?>
                        <fieldset style="padding:0px;border:0px;margin:0px;">

                            <div class="field" style="padding-top:5px;">
                                <div class="fieldDetail leftFloat" style="width:350px;">
                                    <div class="fieldLabel1 leftFloat"><label for="bank_acc_id" class="compulsory">Bank Acc No. : </label></div>
                                    <div class="fieldLabelSpace1">
                                        <input  tabindex="2" type="hidden" id="mobile" name="mobile" value ="<?php echo $mobile; ?>"/>
                                        <select id="bank_acc_id" name="bank_acc_id" title="Pay1 bank account no in which amount is deposited .">
                                            <option value="">-- Select Bank --</option>
                                            <option value="bom-4079">Bank Of Maharashtra( 4079 )</option>
                                            <option value="icici-1578">ICICI( 1578 )</option>
                                            <option value="icici-0005">ICICI( 0005 )</option>
                                        </select>
                                    </div>                     
                                </div> 
                                <div class="fieldDetail leftFloat" style="margin-left: 20px;width:350px;">
                                    <div class="fieldLabel1 leftFloat"><label for="amount" class="compulsory">Amount :</label></div>
                                    <div class="fieldLabelSpace1">
                                        <input  tabindex="2" type="text" id="mobile" name="amount" value =""/>
                                    </div>                     
                                </div> 
                                <div class="clearLeft">&nbsp;</div>

                            </div>
                            <div class="field" style="padding-top:5px;">
                                <div class="fieldDetail leftFloat" style="width:350px;">
                                    <div class="fieldLabel1 leftFloat"><label for="trans_type_id" class="compulsory">Transfer Type</label></div>
                                    <div class="fieldLabelSpace1">
                                        <select id="trans_type_id" name="trans_type_id">
                                            <option value="">-- Select Type --</option>
                                            <option value="2">NEFT/RTGS</option>
                                            <option value="3">ATM-Transfer</option>
                                            <option value="1">CASH</option>
                                            <option value="4">Cheque</option>
                                        </select>
                                    </div>                  
                                </div> 
                                <div class="fieldDetail leftFloat" style="margin-left: 20px;width:350px;">
                                    <div class="fieldLabel1 leftFloat"><label for="mobile" >Bank Trans ID</label></div>
                                    <div class="fieldLabelSpace1">
                                        <input title="Bank transaciont id ." tabindex="2" type="text" id="bank_trans_id" name="bank_trans_id" value =""/>
                                    </div>                     
                                </div> 
                                <div class="clearLeft">&nbsp;</div>

                            </div>

                            <div class="field">               		
                                <div class="fieldDetail">
                                    <div class="fieldLabel1 ">&nbsp;</div> 
                                    <div class="fieldLabelSpace1" id="sub_butt">
                                        <?php echo $ajax->submit('Send', array('id' => 'send_top_up_req', /* 'tabindex' => '13', */ 'url' => array('controller' => 'apis', 'action' => 'sendTopUpRequest'), 'class' => 'retailBut enabledBut', 'after' => 'showLoader2("pay1_top_request");', 'update' => 'innerDiv')); ?>
                                    </div>                         
                                </div>
                            </div>
                        </fieldset>
                        <?php echo $form->end(); ?>

                    </div>							
                </div>
            </div>					
        </div>
    </div>


</div>