                        
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
 
<script type="text/javascript">
var gData = <?php echo json_encode($saledata);?>;
var gOptions = <?php echo json_encode($optionGraph);?> ;
function refreshData(){
   
	 
     $.ajax({
          type:"POST",
          url:"/partner/graphRetailerData",
          dataType:"json",
          success:function(data){
            if(data.status == 'TRUE'){
                gData = data.saledata;
                drawVisualization() ;
                
                $("#cur_bal").html(data.curBal);
                $("#today_trans").html(data.today.no_trans);
                $("#today_amt").html(data.today.amount);
                $("#today_income").html(data.today.income);
            }else{
                var msg;
                msg = "<strong>Login failed !</strong> &nbsp; &nbsp;"+data.errors.msg;
                //alert(msg);

                $("#err_msg").html(msg);
                $("#err_box").show("slow");
                setTimeout(function(){
                    $("#err_box").hide("slow");
                }, 10000);
            }

          }
        });
}
   
function drawVisualization() {

  // Create and populate the data table.
  var data = google.visualization.arrayToDataTable( gData );

  // Create and draw the visualization.
  var ac = new google.visualization.ComboChart(document.getElementById('chart'));
  ac.draw(data,gOptions );

}
</script>
<div id="content-header">
                                <h1>Dashboard</h1>
                                <!--<div style="width: auto;" class="btn-group">
                                        <a data-original-title="Manage Files" class="btn btn-large tip-bottom" title=""><i class="icon-file"></i></a>
                                        <a data-original-title="Manage Users" class="btn btn-large tip-bottom" title=""><i class="icon-user"></i></a>
                                        <a data-original-title="Manage Comments" class="btn btn-large tip-bottom" title=""><i class="icon-comment"></i><span class="label label-important">5</span></a>
                                        <a data-original-title="Manage Orders" class="btn btn-large tip-bottom" title=""><i class="icon-shopping-cart"></i></a>
                                </div>-->
                        </div>
                        <div id="breadcrumb">
                                <a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
                                <a href="#" class="current">Dashboard</a>
                        </div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="alert alert-info">
							Welcome in the <strong>Signal7 Partner Admin Panel</strong>. Don't forget to check all the pages!
							<a href="#" data-dismiss="alert" class="close">x</a>
						</div>
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Today's Statistics</h5><div class="buttons" ><a onclick="refreshData();" href="#" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div></div>
							<div class="widget-content">
								<div class="row-fluid">
								<div class="span4">
									<ul class="site-stats">
                                                                                <li><i class="icon-shopping-cart"></i> <strong id="cur_bal"><?php echo intval($curBal)?></strong> <small>Rs. ( Current Balance )</small></li>
										<li><i class="icon-user"></i> <strong id="today_trans"><?php echo intval($today->no_trans)?></strong> <small>Total transactions</small></li>
										<li><i class="icon-arrow-right"></i> <strong  id="today_amt"><?php echo intval($today->amount)?></strong> <small>Sale</small></li>
                                                                                <li><i class="icon-arrow-left"></i> <strong  id="today_income"><?php echo empty($today->income)?0:$today->income;?></strong> <small>Income</small></li>
										<li class="divider"></li>
										<!--<li><i class="icon-shopping-cart"></i> <strong>259</strong> <small>Total Shop Items</small></li>
										<li><i class="icon-tag"></i> <strong>8650</strong> <small>Total Orders</small></li>
										<li><i class="icon-repeat"></i> <strong>29</strong> <small>Pending Orders</small></li> -->
									</ul>
								</div>
								<div class="span8">
									<div id="chart" style="padding: 0px; position: relative;" class="chart"></div>
								</div>	
								</div>							
							</div>
						</div>					
					</div>
				</div>
				
			</div>
	
		





<script type="text/javascript">

  // Load the Visualization API and the piechart package.
  google.load('visualization', '1.0', {'packages':['corechart']});
  // Set a callback to run when the Google Visualization API is loaded.
  google.setOnLoadCallback(drawVisualization);

</script>
   
