<script type="text/javascript" src="https://www.google.com/jsapi"></script>
 
<script type="text/javascript">
var gData = <?php echo json_encode($saledata);?>;
var gOptions = <?php echo json_encode($optionGraph);?> ;
function refreshData(){
   

     $.ajax({
          type:"POST",
          url:"/partner/graphRetailerData",
          dataType:"json",
          success:function(data){
            if(data.status == 'TRUE'){
                gData = data.graphData;
                drawVisualization() ;
                $("#today_trans").val(data.today.no_trans);
                $("#today_amt").val(data.today.amount);
                $("#today_income").val(data.today.income);
            }else{
                var msg;
                msg = "<strong>Login failed !</strong> &nbsp; &nbsp;"+data.errors.msg;
                //alert(msg);

                $("#err_msg").html(msg);
                $("#err_box").show("slow");
                setTimeout(function(){
                    $("#err_box").hide("slow");
                }, 10000);
            }

          }
        });
}
   
function drawVisualization() {

  // Create and populate the data table.
  var data = google.visualization.arrayToDataTable( gData );

  // Create and draw the visualization.
  var ac = new google.visualization.ComboChart(document.getElementById('visualization'));
  ac.draw(data,gOptions );

}
</script>
<div><a onclick="refreshData();">Refresh Data</a></div>
<div id='visualization' style="margin-left:220px;">Loading Monthly Sale Graph ...</div>

<script type="text/javascript">

  // Load the Visualization API and the piechart package.
  google.load('visualization', '1.0', {'packages':['corechart']});
  // Set a callback to run when the Google Visualization API is loaded.
  google.setOnLoadCallback(drawVisualization);

</script>
   