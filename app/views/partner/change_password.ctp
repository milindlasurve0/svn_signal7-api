<div id="content-header">
				<h1>Reset Password</h1>
			</div>
			<div id="breadcrumb">
				<a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current "><i class="icon-edit"></i>Change Password</a>
			</div>
			<div class="container-fluid">

				<div class="row-fluid">
					<div class="span12">

						 <?php if(!empty($err_msg)){?>
                                                    <div class="alert alert-error">
							 <?php echo $err_msg ; ?>
                                                         <a href="#" data-dismiss="alert" class="close">×</a>
                                                    </div>
                                                <?php }elseif(!empty($msg)){?>
                                                    <div class="alert alert-success">
							 <?php echo $msg ; ?>
                                                         <a href="#" data-dismiss="alert" class="close">×</a>
                                                    </div>
                                                <?php }?>
                                                
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-lock"></i></span><h5></h5><!--<div class="buttons"><a href="#" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div>--></div>
							<div class="widget-content">
								<div class="row-fluid">
                                                                  

<form id="change_password_form" class="form-horizontal" novalidate="novalidate" name="password_validate" action="" method="post">
<div class="control-group">
<label class="control-label">Current Password</label>
<div class="controls">
<input id="curr_password" name="curr_password"  type="password" placeholder="Enter Current Password"/>
</div>
</div>
<div class="control-group">
<label class="control-label">New Password</label>
<div class="controls">
<input id="new_password" name="new_password" type="password" placeholder="Enter New Password"/>
</div>
</div>
<div class="control-group">
<label class="control-label">Confirm password</label>
<div class="controls">
<input id="conf_new_password" name="conf_new_password" type="password" placeholder="Re-Enter New Password"/>
</div>
</div>
<div class="form-actions">
<input class="btn btn-danger" type="submit" value="Reset" />
</div>
</form>
                                                                   
								</div>							
							</div>
						</div>					
					</div>
				</div>


			</div>