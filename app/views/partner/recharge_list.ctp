<?php if($pageType != 'csv'){?>
<div id="content-header">
				<h1>Reports</h1>
<!--				<div style="width: auto;" class="btn-group">
					<a data-original-title="Manage Files" class="btn btn-large tip-bottom" title=""><i class="icon-file"></i></a>
					<a data-original-title="Manage Users" class="btn btn-large tip-bottom" title=""><i class="icon-user"></i></a>
					<a data-original-title="Manage Comments" class="btn btn-large tip-bottom" title=""><i class="icon-comment"></i><span class="label label-important">5</span></a>
					<a data-original-title="Manage Orders" class="btn btn-large tip-bottom" title=""><i class="icon-shopping-cart"></i></a>
				</div>-->
			</div>
			<div id="breadcrumb">
				<a data-original-title="Go to Home" href="#" title="" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="tip-bottom"><i class="icon icon-th-list"></i>Reports</a>
                                <a href="#" class="current"><i class="icon icon-list"></i>Recharge List</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
					<div class="row-fluid" >
                                            <div class="span6">
					<div class="input-prepend">
					  <span class="add-on">From</span>
					  <input  id="fromDate" name="fromDate" type="text" placeholder="Date From" data-date-format="dd-mm-yyyy" value="<?php if(isset($date_from)) echo date('d-m-Y', strtotime($date_from));?>"/>
					</div>
					<div class="input-prepend">
					  <span class="add-on">To</span>
					  <input id="toDate" name="toDate" type="text" placeholder="Date To" data-date-format="dd-mm-yyyy" value="<?php if(isset($date_to)) echo date('d-m-Y', strtotime($date_to));?>" />
					</div>
                                                </div>
					<div class="span2">
					<button class="btn" title="Search" onclick="findHistory(<?php echo "'".$this->params['controller']."','".$this->params['action']."'" ?>);">
						<i class="icon-search"></i>
                                                
						
					</button>
                                        <button title="Download CSV" class="btn" onclick="saveCsv(<?php echo "'".$this->params['controller']."','".$this->params['action']."'" ?>);">
						<i class="icon-download-alt"></i>
					</button>
                                            
					</div>
                                        <div class="span3">
                                            <span style="background-color:#98FF98" class="badge">Success</span>
                                            <span style="background-color:#FFCBA4" class="badge">Failure</span>
                                            <span style="background-color:#79BAEC" class="badge">Reverse</span>
                                        </div>
					</div>

						
						<div class="widget-box">
							<div class="widget-title">
							
								<h5>Recharge History <?php if(isset($date_from) && isset($date_to)) echo "(". date('d-m-Y', strtotime($date_from)) . " - " .  date('d-m-Y', strtotime($date_to)) . ")"; ?></h5>
							</div>
							<div class="widget-content nopadding">
								<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<!--                                                                    <div class="">
                                                                        <div class="dataTables_length" id="DataTables_Table_0_length">
                                                                            <label>Show 
                                                                                <div id="s2id_autogen1" class="select2-container">    
                                                                                    <a href="#" onclick="return false;" class="select2-choice">   
                                                                                        <span>10</span>
                                                                                        <abbr class="select2-search-choice-close" style="display:none;">                                                                                            
                                                                                        </abbr>
                                                                                        <div>
                                                                                            <b></b>
                                                                                        </div>
                                                                                    </a>
                                                                                    <div class="select2-drop select2-offscreen">
                                                                                        <div class="select2-search">
                                                                                            <input tabindex="0" autocomplete="off" class="select2-input" type="text"/>   
                                                                                        </div>   
                                                                                        <ul class="select2-results">   
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <select style="display: none;" aria-controls="DataTables_Table_0" size="1" name="DataTables_Table_0_length">
                                                                                    <option selected="selected" value="10">10</option>
                                                                                    <option value="25">25</option>
                                                                                    <option value="50">50</option>
                                                                                    <option value="100">100</option>
                                                                                </select> 
                                                                            entries
                                                                            </label>
                                                                        </div>
                                                                    </div>-->
                                                                    
                                                                    <table id="DataTables_Table_0" class="table table-bordered table-striped table-hover data-table dataTable">
									<thead>
                                                                            
                                                                            <tr role="row">
                                                                                <th >Txn Date
                                                                                </th>
                                                                                <th >Txn Id / Signal7 T_ID
                                                                                </th>
                                                                                <th >Number
                                                                                </th>
                                                                                <th >Operator
                                                                                </th>
                                                                                <th >Amount
                                                                                </th>
                                                                                <th >Opening
                                                                                </th>
                                                                                <th >Closing
                                                                                </th>
                                                                                <th >Earning
                                                                                </th>
                                                                                <th >Reversal Date
                                                                                </th>
                                                                                <th >Description
                                                                                </th>
                                                                                <th >Status
                                                                                </th>
                                                                                
                                                                            </tr>
                                                                        </thead>
									
									<tbody aria-relevant="all" aria-live="polite" role="alert">
                                                                        <?php  $sumAmt = 0;
            $sumErn = 0;
            $noTrans = 0;
            if(empty($transactions) || count($transactions) <= 0 ) { ?>
        <tr>
            <td colspan="4"><span class="success">No Results Found !!</span></td>
        </tr>

        <?php } else { ?>
        <?php $i=0; 
           
            foreach($transactions as $transaction){ 
            if($i%2 == 0)$class = 'even';
            else $class = 'odd';
            $amount = empty($transaction[0]['amount'])?$transaction[0]['amount']:$transaction[0]['amount'];
            $i++;
            
            $opn = empty($transaction[0]['opening'])? 0 : $transaction[0]['opening'] ;
            $cls = empty($transaction[0]['closing']) ? 0 : $transaction[0]['closing'] ;
            $ern = 0;
            $bgColor = "";
            $status = "";
            if($opn < $cls){//in case of reversal
                $ern =  $cls - $opn  - $amount ; 
                $bgColor = "#79BAEC";
                $noTrans --;
                $sumAmt = $sumAmt - $amount;
                $status = "Reverse";
                $description = $transaction[0]['description'];
            }else if( $cls< $opn){//in case of successful transaction
                $ern = $amount - ( $opn - $cls );
                $noTrans ++;
                
                $sumAmt = $sumAmt + $amount;
                
                if($transaction[0]['description']!= "Successful transaction ."){ // current staus failed
                    $description = $transaction[0]['description'];
                    $status = "Failure";
                    $bgColor = "#FFCBA4";
                }else{ // current staus stil success
                    $description = $transaction[0]['description'];
                    $status = "Success";
                    $bgColor = "#98FF98";
                }
                
            }else if( $opn == $cls){//in case of instant failure
                $bgColor = "#FFCBA4";
                $status = "Failure";
                $description = $transaction[0]['description'];
            }
            $sumErn = $sumErn +  $ern ;
            
            
            /*if(empty($transaction[0]['err_code'])){
                //success
                $bgColor = "#98FF98";
            }else if($transaction[0]['description'] == 'Transaction Reverse.'){
                //reverse
                $bgColor = "#79BAEC";
            }else{
                //failure
                $bgColor = "#FFCBA4";
            }*/
            
        ?>
          <tr class="gradeA <?php echo $class; ?>"> 
                        <td style="background-color:<?php echo $bgColor; ?>"><?php echo date('d-m-Y H:i:s', strtotime($transaction[0]['created'])); ?></td>
                        <td style="background-color:<?php echo $bgColor; ?>"><?php echo $transaction[0]['id']."/".$transaction[0]['partner_req_id']." / ".$transaction[0]['vendor_actv_id'] ; ?></td>
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo empty($transaction[0]['mob_dth_no'])?(!empty($transaction[0]['param'])? $transaction[0]['param']:$transaction[0]['mobile']):$transaction[0]['mob_dth_no']; ?></td>
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo $transaction[0]['name']; ?></td>
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo $amount; ?></td>
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo empty($transaction[0]['opening'])? 0 : $transaction[0]['opening']; ?></td> 
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo empty($transaction[0]['closing']) ? 0 : $transaction[0]['closing']; ?></td> 
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo round($ern,2);?></td> 
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo empty($transaction[0]['tmp']) ? 0 : $transaction[0]['tmp']; ?></td> 
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo $description; ?></td>
                        <td style="background-color:<?php echo $bgColor; ?>" class="center number"><?php echo $status; ?></td>
                <?php }
                $i++; } ?> 		
                                                                            
                                                                            
                                                                        </tbody>
                                                              </table>
<?php if(!empty($transactions) || count($transactions) > 0 ) { ?>
<div class="widget-title">
    <h5> <?php echo "No of Transactions  = ".$noTrans." &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; Sale = ".$sumAmt."  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; Earning = ".round($sumErn,2); ?></h5>
</div>
<?php } ?>
<!--           <div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
                <div id="DataTables_Table_0_filter" class="dataTables_filter">
                     <label>Search: <input aria-controls="DataTables_Table_0" type="text">
                     </label>
                </div>
          
          <div id="DataTables_Table_0_paginate" class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers">
            <a id="DataTables_Table_0_first" tabindex="0" class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled">
                First
            </a>
            <a id="DataTables_Table_0_previous" tabindex="0" class="previous fg-button ui-button ui-state-default ui-state-disabled">
                Previous
            </a>
            <span>
                <a tabindex="0" class="fg-button ui-button ui-state-default ui-state-disabled">
                    1
                </a>
                <a tabindex="0" class="fg-button ui-button ui-state-default">
                    2
                </a>
                <a tabindex="0" class="fg-button ui-button ui-state-default">
                    3
                </a>
                <a tabindex="0" class="fg-button ui-button ui-state-default">
                    4
                </a>
                <a tabindex="0" class="fg-button ui-button ui-state-default">
                    5
                </a>
            </span>
            <a id="DataTables_Table_0_next" tabindex="0" class="next fg-button ui-button ui-state-default">
                Next
            </a>
            <a id="DataTables_Table_0_last" tabindex="0" class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default">
                Last
            </a>
        </div>
        </div>-->
        </div>  
							</div>
						</div>
					</div>
				</div>
				
			</div>
		<script>
               Date.daysBetween = function( date1, date2 ) {
                  //Get 1 day in milliseconds
                  var one_day=1000*60*60*24;

                  // Convert both dates to milliseconds
                  var date1_ms = date1.getTime();
                  var date2_ms = date2.getTime();
                  
                  // Calculate the difference in milliseconds
                  var difference_ms = date2_ms - date1_ms;
                  
                  // Convert back to days and return
                  return Math.round(difference_ms/one_day); 
                }
               function findHistory(controller,action ){
                   
                    var html = $('submit').innerHTML;
                    //showLoader3('submit');
                    var date_from = $('#fromDate').val();
                    var date_to = $('#toDate').val();
                    
                    
                    if(date_from == '' || date_to == ''){
                            $('#date_err').show();
                            $('#submit').html(html);
                    }
                    else {
                            $('#date_err').hide();
                            
                            var date_from_arr = date_from.split("-");
                            var date_to_arr = date_to.split("-");
                            
                            var dateDiff = Date.daysBetween(new Date(date_from_arr[2], date_from_arr[1], date_from_arr[0], 0, 0, 0, 0),new Date(date_to_arr[2], date_to_arr[1], date_to_arr[0], 0, 0, 0, 0));
                            
                            if(dateDiff > 7){
                                alert("You can not get more than 7 days data !!! ");
                                return false;
                            }
                            
                            date_from = date_from.replace(/-/g,"");
                            date_to = date_to.replace(/-/g,"");
                            //$('sub').setAttribute('href', "/partner/rechargeList/"+date_from+"-"+date_to);            
                            $url = "/" +controller+ "/" +action+ "/" ;

                            window.location.href = $url+date_from+"-"+date_to;

                    }
                }      
                function saveCsv(controller,action ){
                   
                    var html = $('submit').innerHTML;
                    //showLoader3('submit');
                    var date_from = $('#fromDate').val();
                    var date_to = $('#toDate').val();
                    if(date_from == '' || date_to == ''){
                            $('#date_err').show();
                            $('#submit').html(html);
                    }
                    else {
                            $('#date_err').hide();
                            var date_from_arr = date_from.split("-");
                            var date_to_arr = date_to.split("-");
                            
                            var dateDiff = Date.daysBetween(new Date(date_from_arr[2], date_from_arr[1], date_from_arr[0], 0, 0, 0, 0),new Date(date_to_arr[2], date_to_arr[1], date_to_arr[0], 0, 0, 0, 0));
                            
                            if(dateDiff > 7){
                                alert("You can not get more than 7 days data !!! ");
                                return false;
                            }
                            
                            date_from = date_from.replace(/-/g,"");
                            date_to = date_to.replace(/-/g,"");
                            //$('sub').setAttribute('href', "/partner/rechargeList/"+date_from+"-"+date_to);            
                            $url = "/" +controller+ "/" +action+ "/" ;

                            window.location.href = $url+date_from+"-"+date_to+"?res_type=csv";

                    }
                }      

</script>

<?php } ?>