<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Signal7 - ANY TIME | ANY WHERE | ANY PHONE </title>

        <meta name="viewport" content="width=1024">
        <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <?php echo $minify->css(array('signal7/screen'),RETAIL_STYLE_CSS_VERSION); ?>
        <!--<link rel="stylesheet" type="text/css" media="screen" href="signal7/screen.css">-->
        <?php echo $minify->js(array('lib/jquery-1.9.0.min'),MERGE_JS_VERSION); ?>
        <?php echo $minify->js(array('lib/jquery.validate'),MERGE_JS_VERSION); ?>
        
    </head>
    <body>
        <div id="main">
          
            <div id="menu">
                <div class="container">
                    <div style="padding: 3px;">
                        <span class="span3" >
                            <a target="_blank" href="http://www.signal7.in"><img   height="45" title="Signal7" alt="Signal7" src="/img/signal7/logo.gif"></a>
                        </span>
                    </div>
                </div>
           </div>
                <div id="page_signin">
                    <div class="page-header-top">
                        <div class="container">
                            <h1>Innovative Enterprise Mobility Solution</h1>
                        </div>
                    </div>
                    <div class="container">
                        <div class="content">
                            <div class="row">
                                <div class="span16">
                                    <div id="signin">
                                        <div class="title">Already a partner ? Sign in:</div>
                                        <form id="s7login" name="s7login" action="/partner/afterLogin" method="post">
                                            <input name="next" value="/" type="hidden"/>
                                            <fieldset>
                                                <div id="err_box" style="display:none;padding:0 20px 0 20px;">
                                                <div class="alert-message error">
                                                
                                                <p id="err_msg"><strong>Login failed!</strong> Invalid.</p>
                                                </div>
                                                </div>
                                                <div class="clearfix">
                                                    <label for="s7username"><span>Username:</span></label>
                                                    <div class="input">
                                                        <input tabindex="1" id="s7username" name="s7username" label="Username" type="text" placeholder="Enter username"/>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                    <label for="s7password"><span>Password:</span></label>
                                                    <div class="input">
                                                        <input tabindex="2" id="s7password" name="s7password" label="Password" value="" type="password" placeholder="Enter password"/>
                                                    </div>
                                                </div>

                                                <div class="actions">
                                                    <input tabindex="3" class="btn primary large" value="Sign into your account" type="submit">
                                                   <p class="reset"><!-- Recover your <a tabindex="4" href="/partner/forgotPass" title="Recover your username or password">username or password</a>--></p>
                                                </div>
                                            </fieldset>
                                        </form>

                                    </div>







                                    <div id="signup">
                                        <div class="title">Contact - Us !</div>
                                        <form action="#" method="post" class="form-stacked">
                                            <fieldset>
                                                <div class="clearfix">
                                                    <label for="isignup_username"> MindsArray Technologies Pvt. Ltd.</label>
                                                    <div class="input">
                                                        <span class="help-block">726-727, Raheja's Metroplex (IJMIMA),
                                                            Link Road, Malad (W),
                                                            Mumbai - 400 064. India</span>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                    <label for="isignup_email">Email Us:</label>
                                                    <div class="input">
                                                        <span class="help-block"><strong> Sales@signal7.in</strong></span>
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                    <label for="isignup_password">Contact no:</label>
                                                    <div class="input">
                                                        
                                                        <span class="help-block">+91 8879 647 666</span>
                                                    </div>
                                                </div>
                                                <div class="actions"></div>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <script>
                                    $(document).ready(function(){
                                    $("#err_box").hide();
                                    $("#s7username").focus();
                                    $("#s7login").validate({
                                        rules:{
                                                s7username:{
                                                        required: true
                                                },
                                                s7password:{
                                                        required: true,
                                                        minlength:4,
                                                        maxlength:20
                                                }
                                        },
                                        submitHandler: function(form) {
                                             $("#err_box").hide();
                                            var params = "mobile="+$("#s7username").val()+"&password="+$("#s7password").val();
                                            /*$.ajax({
                                              type: "POST",
                                              url: "some.php",
                                              data: { name: "John", location: "Boston" }
                                            }).done(function( msg ) {
                                              alert( "Data Saved: " + msg );
                                            });*/
                                            $.ajax({
                                              type:"POST",
                                              url:"/partner/afterLogin",
                                              data:"mobile="+$("#s7username").val()+"&password="+$("#s7password").val(),
                                              dataType:"json",
                                              success:function(data){
                                                if(data.status == 'TRUE'){
                                                    window.location.href = "/partner/dashboard";
                                                }else{
                                                    var msg;
                                                    msg = "<strong>Login failed !</strong> &nbsp; &nbsp;"+data.errors.msg;
                                                    //alert(msg);
                                                    
                                                    $("#err_msg").html(msg);
                                                    $("#err_box").show("slow");
                                                    setTimeout(function(){
                                                        $("#err_box").hide("slow");
                                                    }, 10000);
                                                }
                                                
                                              }
                                            });
                                        },
                                        errorClass: "help-block",
                                        errorElement: "em",
                                        highlight:function(element, errorClass, validClass) {
                                                $(element).parents('.clearfix').addClass('error');
                                        },
                                        unhighlight: function(element, errorClass, validClass) {
                                                $(element).parents('.clearfix').removeClass('error');
                                                $(element).parents('.clearfix').addClass('success');
                                        }
                                });
});
        </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="push"></div>
            </div>
            <div id="footer">
                <div class="container">
                    <footer>
                        <p>
                            2013 - 2014 © <span style="color:darkgreen">Signal<em style="color:white">7</em></span> &nbsp;  Home . Powered by <a style="color:#0069D6" href="http://www.mindsarray.com/" title="MindsArray Technologies Pvt. Ltd.">MindsArray Technologies Pvt. Ltd.</a>
                            <!--                                                <a href="https://wrapbootstrap.com/support" title="Buyer and seller support">Support</a>&nbsp;&nbsp;·&nbsp;&nbsp;<a href="https://wrapbootstrap.com/help" title="Help topics">Help topics</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/affiliates" title="Affiliates">Affiliates</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/help/buyers/bitcoin" title="Paying with Bitcoin">Bitcoin</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/privacy" title="Privacy policy">Privacy policy</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/terms" title="Terms of use">Terms of use</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/about" title="About us">About us</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/contact" title="Contact us">Contact us</a> <span class="pull-right">© 2013 <a href="http://wrapmarket.com/" title="WrapMarket LLC" target="_blank">WrapMarket LLC</a></span>-->
                        </p>
                    </footer>
                </div>
            </div>
            <div id="subscribe_dialog">
                <div class="shell">
                    <div class="topbar"><a class="close" href="#">Close</a></div>
                    <div class="inner"></div>
                </div>
            </div>
            <div id="mask"></div>
            <script>//$(document).ready(function(){ $(".alert-message").alert(); $("#bsw").twipsy(); $("#bwb").twipsy(); });</script>


    </body></html>