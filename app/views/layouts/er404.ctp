<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Signal7 - ANY TIME | ANY WHERE | ANY PHONE </title>

        <meta name="viewport" content="width=1024">
        <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <?php echo $minify->css(array('signal7/screen'),RETAIL_STYLE_CSS_VERSION); ?>
        <?php //echo $minify->css(array('signal7/screen'),RETAIL_STYLE_CSS_VERSION); ?>
        <!--<link rel="stylesheet" type="text/css" media="screen" href="signal7/screen.css">-->
        <?php //echo $minify->js(array('lib/jquery-1.9.0.min'),MERGE_JS_VERSION); ?>
        <?php //echo $minify->js(array('lib/jquery.validate'),MERGE_JS_VERSION); ?>
        
    </head>
    <body>
        <div id="main">
          
            <div id="menu">
                <div class="container">
                    <div style="padding: 3px;">
                        <span class="span3" >
                            <img   height="45" title="Signal7" alt="Signal7" src="/img/signal7/logo.gif">
                        </span>
                    </div>
                </div>
           </div>
                <div id="page_signin">
                    <div class="page-header-top">
                        <div class="container">
                            <h1>Innovative Enterprise Mobility Solution</h1>
                        </div>
                    </div>
                    <div class="container">
                        <div class="content">
                             <?php echo $content_for_layout; ?>
                        </div>
                    </div>
                </div>

                <div class="push"></div>
            </div>
            <div id="footer">
                <div class="container">
                    <footer>
                        <p>
                            2013 - 2014 © <span style="color:darkgreen">Signal<em style="color:white">7</em></span> &nbsp;  Home . Powered by <a style="color:#0069D6" href="http://www.mindsarray.com/" title="MindsArray Technologies Pvt. Ltd.">MindsArray Technologies Pvt. Ltd.</a>
                            <!--                                                <a href="https://wrapbootstrap.com/support" title="Buyer and seller support">Support</a>&nbsp;&nbsp;·&nbsp;&nbsp;<a href="https://wrapbootstrap.com/help" title="Help topics">Help topics</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/affiliates" title="Affiliates">Affiliates</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/help/buyers/bitcoin" title="Paying with Bitcoin">Bitcoin</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/privacy" title="Privacy policy">Privacy policy</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/terms" title="Terms of use">Terms of use</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/about" title="About us">About us</a>&nbsp;&nbsp;&nbsp;<a href="https://wrapbootstrap.com/contact" title="Contact us">Contact us</a> <span class="pull-right">© 2013 <a href="http://wrapmarket.com/" title="WrapMarket LLC" target="_blank">WrapMarket LLC</a></span>-->
                        </p>
                    </footer>
                </div>
            </div>
            <div id="subscribe_dialog">
                <div class="shell">
                    <div class="topbar"><a class="close" href="#">Close</a></div>
                    <div class="inner"></div>
                </div>
            </div>
            <div id="mask"></div>
            <script>//$(document).ready(function(){ $(".alert-message").alert(); $("#bsw").twipsy(); $("#bwb").twipsy(); });</script>


    </body></html>