
<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
		<title>Signal7 - ANY TIME | ANY WHERE | ANY PHONE </title>
		<meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <?php echo $minify->css(array('/lib/bootstrap/css/bootstrap.min'),RETAIL_STYLE_CSS_VERSION); ?>
                <?php echo $minify->css(array('lib/bootstrap/css/bootstrap-responsive.min'),RETAIL_STYLE_CSS_VERSION); ?>
                <?php echo $minify->css(array('signal7/datepicker'),RETAIL_STYLE_CSS_VERSION); ?>
                <?php echo $minify->css(array('signal7/s7_102'),RETAIL_STYLE_CSS_VERSION); ?>
                <?php echo $minify->css(array('signal7/jquery-ui'),RETAIL_STYLE_CSS_VERSION); ?>
                <link rel="stylesheet" href="/css/signal7/s7.css" class="skin-color">
                
		
	</head>
	<body>
		
            <!-- Header Part Start -->
                <?php echo $this->element('signal7/header');?>
            <!-- Header Part End -->
            
            <!-- Side Bar Part Start -->
		<?php $active_menu ; echo $this->element('signal7/side_bar',array('active_menu'=>$active_menu,'active_submenu'=>$active_submenu));?>
            <!-- Side Bar Part End -->
            
		<div id="style-switcher" >
			<i class="icon-arrow-left icon-white"></i>
			<span>Style:</span>
			<a href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
            <a href="#light-blue" style="background-color: #8399b0;"></a>
			<a href="#blue" style="background-color: #2D2F57;"></a>
			<a href="#red" style="background-color: #673232;"></a>
            <a href="#red-green" style="background-image: url('/img/signal7/red-green.png');background-repeat: no-repeat;"></a>
		</div>
		
                 <!-- Content Part Start -->
		<div id="content">
                    <?php echo $content_for_layout; ?>
			
                    
		</div>
		<!-- Content Part End -->
                <div class="container-fluid">
                        <div class="row-fluid">
					<div id="footer" class="span12">
						2013 - 2014 © Signal7 Admin Panel. Powered by <a href="http://www.mindsarray.com/" title="MindsArray Technologies Pvt. Ltd.">MindsArray Technologies Pvt. Ltd.</a>
					</div>
				</div>
                        </div>

<!--            <script src="web-root/excanvas.js"></script>-->
            
<!--            <script src="web-root/js/jquery_103.js"></script>
            <script src="web-root/js/jquery_102.js"></script>
            <script src="web-root/js/bootstrap.js"></script>
            <script src="web-root/js/jquery.js"></script>
            <script src="web-root/js/jquery_104.js"></script>
            <script src="web-root/js/jquery_105.js"></script>-->
<!--            <script src="web-root/js/bootstrap-colorpicker.js"></script>-->
            
            
<!--            <script src="web-root/fullcalendar.js"></script>-->
<!--            <script src="web-root/js/s7.js"></script>-->
<!--            <script src="web-root/js/s7_102.js"></script>-->


             <?php echo $minify->js(array('signal7/jquery_103'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('signal7/jquery_102'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('lib/bootstrap/js/bootstrap.min'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('signal7/bootstrap-datepicker'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('signal7/jquery'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('signal7/jquery_104'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('signal7/jquery_105'),MERGE_JS_VERSION); ?>
             <?php //echo $minify->js(array('signal7/jquery-ui-1'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('signal7/jquery.dataTables.min'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('lib/jquery.validate'),MERGE_JS_VERSION); ?>
                
             <?php echo $minify->js(array('signal7/s7'),MERGE_JS_VERSION); ?>
             <?php echo $minify->js(array('signal7/s7.tables'),MERGE_JS_VERSION); ?>
          
<!--        <script src="web-root/excanvas.js"></script>
            <script src="web-root/jquery_003.js"></script>
            <script src="web-root/jquery_002.js"></script>
            <script src="web-root/bootstrap.js"></script>
            <script src="web-root/jquery.js"></script>
            <script src="web-root/jquery_004.js"></script>
            <script src="web-root/jquery_005.js"></script>
            <script src="web-root/fullcalendar.js"></script>
            <script src="web-root/s7.js"></script>
            <script src="web-root/s7_002.js"></script>-->
	

</body></html>