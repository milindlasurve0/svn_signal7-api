
<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK REL="SHORTCUT ICON" HREF="/img/favicon.ico?321"/>
<title>Pay1 Channel Partner Panel</title>

<?php echo $minify->css(array('retailer'),RETAIL_STYLE_CSS_VERSION); ?>
<!--[if gt IE 6]>
	<?php echo $minify->css(array('style_ie'),STYLE_CSS_IE_VERSION); ?>
<![endif]-->
<!--[if lt IE 7]>
	<?php echo $minify->css(array('style_ie6'),STYLE_CSS_IE_VERSION); ?>
<![endif]-->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>   
</head>
<body>

<style type="text/css">
.taggLinkBG1 {background-color: #FF8800 !important;}
.success {
background: #FFCC00;
padding: 2px 5px;
}
.error{
font-size: 1em;
background: red;
padding: 2px 5px;
color: white;
}
</style>
<?php echo $minify->js(array('merge'),MERGE_JS_VERSION); ?>
<?php echo $minify->js(array('merge1'),MERGE_1_JS_VERSION); ?>
<?php echo $minify->js(array('script_app'),SCRIPT_APP_JS_VERSION); ?>
    <?php flush(); ?>
		<div class="headerIndex">
            <div class="headerMainCont">
                <div class="headerSpace">                	
	                <div class="logo" style="float:left;position:relative;">
	                	<?php if(isset($_SESSION['Auth']['User']['group_id']) && $_SESSION['Auth']['User']['group_id'] != ADMIN) echo $html->image("logo.png?211", array("url" => SITE_NAME."shops/view/")); 
	                	else echo $html->image("logo.png?211", array("url" => SITE_NAME."users/view/"));
	                	?>
	                	
	                	<span class="slogan fntSz17 strng positionSlogan" style=""><!-- "Bring mobile closer to life" -->&nbsp;</span>
	                </div>
	                <?php if($this->params['controller'] == 'panels' || ($this->params['controller'] == 'cc' && $this->params['action'] == 'panel')) { ?>
	    					<div style="font-size:20px;font-weight:bold;color:red;display:block;position:absolute;left:40%;top:30px; width:300px">
    							<marquee id="notice" behavior="alternate" direction="left"></marquee></div>                
	    					<script>
		    					//setInterval(checkForCalls,10000);
		    					
								function checkForCalls(){
									var params = {};
									var url = '/cc/checkPendingCalls';
									var myAjax = new Ajax.Request(url, {method: 'post', parameters: params,
									onSuccess:function(transport)
											{		
												var response = transport.responseText;
												if($('notice')){
													if(response != 0){
														$('notice').innerHTML = response + " calls pending in call dropped";
													}
													else {
														$('notice').innerHTML = "";
													}
												}
											},
									onFailure:function()
											{		
												alert('Something went wrong...');
											}
									});
								}
								checkForCalls();
							</script>
	               	<?php } ?>
                    <div id="rightHeaderSpace" style="position:relative">                    
                     	<?php  if(isset($_SESSION['Auth']['User']['group_id']))echo $this->element('shop_header'); ?>
	             	</div>
	                <div class="clearBoth">&nbsp;</div>	                
            	</div>
    		</div> 
    	</div>
    		
    	
		<div id="container" class="mainCont">
		<?php echo $this->element('popup_element');?>
		<div id="login_user" style="display:none"> <?php echo $this->element('login_sessionOut');?> </div>
		<div id="content" class="container">
		<?php if($this->params['controller'] == 'panels' || ($this->params['controller'] == 'recharges' && $this->params['action'] == 'ossTest') || ($this->params['controller'] == 'cc' && $this->params['action'] == 'panel')) { ?>
	                <table border='0' cellpadding="2" cellspacing="5">
			 			<tr BGCOLOR="#DBEBD1">
			 			<td><a href="/panels/search" style="width:100%;display:inline-block">Search</a></td>
			 			<td BGCOLOR="#DBEB23"><a href="/cc/panel">Calls Dropped</a></td>
			 			<td BGCOLOR="#DBEB23"><a href="/panels/tranReversal">Reversal Complaints</a></td>
			 			<td BGCOLOR="#DBEB23"><a href="/panels/tranProcess">Transactions in process</a></td>
			 			<td><a href="/panels/retInfo">Retailer info</a></td>
			 			<td><a href="/panels/userInfo">Customer Info</a></td> 
			 			<td><a href="/panels/transaction">Transactions</a></td>
			 			<td><a href="/panels/tranRange">Transactions from-to</a></td>
			 			</tr>
			 			<tr BGCOLOR="#DBEBD1">
			 			<td><a href="/panels/retMsg">Notification</a></td>
			 			<td><a href="/panels/prodVendor">Provider switching</a></td>
			 			<?php if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['id'] == 1){ ?>
			 			<td><a href="/panels/salesmanReport">Salesman Report</a></td>
			 			<td><a href="/panels/retailerSale">Sale Report</a></td>
			 			<?php } ?>
			 			<td><a href="/panels/retColl">All Retailers</a></td>
			 			<td><a href="/recharges/allBalance">Balance Status</a></td>
			 			<td><a href="/recharges/ossTest">OSS Recharges</a></td>
			 			<td><a href="/panels/tags">Tags</a></td>
			 			</tr>
			 		</table>
			 		<?php } ?>
			 		<br/>
				<?php echo $content_for_layout; ?>	
			</div>				    			
		<div id="footer" class="footer">
   		 	<!-- <span class="rightFloat"><a href="http://www.mindsarray.com" target="_blank">About us</a> | <a href="http://blog.smstadka.com" target="_blank">Blog</a> | <a href="<?php echo SITE_NAME; ?>users/dnd">Do Not Disturb Registry</a> | <a href="http://blog.smstadka.com/contact-us" target="_blank" alt="Contact Us opens in new window">Contact Us</a> | <a href="http://blog.smstadka.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://blog.smstadka.com/terms-and-condition" target="_blank" alt="Terms of Services">Terms of Service</a> | <a href="http://blog.smstadka.com/faq" target="_blank">FAQs</a> | <a href="http://blog.smstadka.com/feedback" alt="Feedback opens in new window" target="_blank">Feedback</a><a href="http://www.rapidssl.com/" target="_blank"><img src="/img/spacer.gif"  class="oSPos30 otherSprite" align="absmiddle"></a></span> -->
         A MindsArray Technologies Pvt. Ltd. Product. All Rights Reserved © <?php echo date('Y'); ?> Pay1&trade;
    	</div>
		
</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
<script>
function showLoader3(id){
	$(id).innerHTML = '<span id="loader2" class="loader2" style="display:inline-block; width:50px">&nbsp;</span>';
}

</script>
</html>