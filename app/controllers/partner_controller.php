<?php
class  PartnerController extends AppController {
	var $name = 'Partner';
	var $helpers = array('Html','Ajax','Javascript','Minify','Paginator','GChart','Paginator','Csv');
	var $components = array('RequestHandler','Shop');
	var $uses = array('User','Retailer','ShopTransaction');
	var $paginate = array(
              'Post'	=> array(
		'limit'	=> 2,
		'page'	=> 1,
		'order'	=> array(
			'Post.created'	=> 'asc')
	)
	);

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->layout = "signal7";
		//$this->Auth->loginAction = array('controller' => 'partner', 'action' => 'index');
		//$this->Auth->logoutRedirect = array('controller' => 'partner', 'action' => 'index');
		//$this->Auth->loginRedirect = array('controller' => 'partner', 'action' => 'view');
	}
	function index(){
		//$this->autoRender = false;
		$this->layout = "signal7_index";
		$this->render('index');
	}
	function dashboard(){
		//$this->autoRender = false;
		if(empty ($_SESSION['Auth']['partner_id'])){
			$this->redirect("/");
		}
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		//$data = $vendorActObj->query($query);
                
		$query = "SELECT count(*) as no_trans , sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date = '".date('Y-m-d')."'";
		$today = $vendorActObj->query($query);

		$dataJson = $this->graphRetailerData();
		$data = json_decode($dataJson);
		$this->set('today',$data->today);
		$this->set('saledata',$data->saledata);
		$this->set('optionGraph',$data->optionGraph);
                $this->set('curBal',$data->curBal);
                
		$this->set('active_menu','dashboard');
		$this->set('active_submenu','');
		$this->render('dashboard');
	}
	function rechargeList($dates=null,$pageNo=1){
		//$this->autoRender = false;
                $queryPart = "";
		if(empty ($_SESSION['Auth']['partner_id'])){
			$this->redirect("/");
		}
		$isPartner = isset($_SESSION['Auth']['is_partner'])?$_SESSION['Auth']['is_partner'] : null;
		$partnerId = isset ($_SESSION['Auth']['partner_id']) ? $_SESSION['Auth']['partner_id'] : null;

		$arr = empty ($dates) ? array() : explode("-",$dates);
		$dt_from = isset($arr[0]) ? $arr[0] : "";
		$dt_to = isset($arr[1]) ? $arr[1] : "";
		$partnerLogs = array();
		if (!empty($dt_to) && !empty($dt_from)) {

			$dt_to = substr($dt_to, 4, 4) . "-" . substr($dt_to, 2, 2) . "-" . substr($dt_to, 0, 2);
			$dt_from = substr($dt_from, 4, 4) . "-" . substr($dt_from, 2, 2) . "-" . substr($dt_from, 0, 2);
			$interval = date_diff(date_create($dt_from), date_create($dt_to));
			 if (intval($interval->format('%a')) > 7) {// if request is for more than 7 days then it will show only 7 days before data of  to_date
                             $date = new DateTime($dt_to);
                             $date->modify('-7 day');
                             $dt_from = $date->format('Y-m-d');
			 }
		} else {
			//date ( $format, strtotime ( '-7 day' . $date ) )
			$dt_to = date("Y-m-d");
			$dt_from = date("Y-m-d");
		}
		$this->set('date_from',$dt_from);
		$this->set('date_to',$dt_to);


		$itemsPerPage = PAGE_LIMIT;

		$result = array();
		if($itemsPerPage <= 0 || $pageNo <= 0){
			$queryPart = " limit 0 , ".PAGE_LIMIT;
		}else{
			$ll = $itemsPerPage * ( $pageNo - 1 ) ;//+ 1; // lower limit
			$ul = $itemsPerPage  ;// upper limit//* $pageNo
			$queryPart = " limit $ll , $ul ";

		}
               
		
		$partnerLogs=$this->Retailer->query("SELECT pl . *,va.mobile,va.amount,va.param, oc.opening, oc.closing , pr.name,oc.timestamp as tmp 
                                                    FROM partners_log pl
                                                    LEFT JOIN partners p ON pl.partner_id = p.id
                                                    LEFT JOIN retailers r ON p.retailer_id = r.id 
                                                    LEFT JOIN vendors_activations va ON ( pl.vendor_actv_id = va.ref_code AND r.id =va.retailer_id AND va.date =pl.date )
                                                    LEFT JOIN opening_closing oc ON oc.shop_transaction_id = va.shop_transaction_id and oc.shop_id = ".$this->Session->read('Auth.id')." and oc.group_id = ".$this->Session->read('Auth.User.group_id')."
                                                    LEFT JOIN products pr ON pl.product_id = pr.id 
                                                    WHERE pl.partner_id = $partnerId and pl.date >= '" . $dt_from . "' and pl.date <= '" . $dt_to . "' AND pl.vendor_actv_id != 0  
                                                    UNION
                                                    (SELECT pl . *,null,null,null,null,null,null,pl.created as tmp
                                                    FROM partners_log pl 
                                                    LEFT JOIN partners p ON pl.partner_id = p.id 
                                                    LEFT JOIN products pr ON pl.product_id = pr.id 
                                                    WHERE pl.partner_id = $partnerId and pl.date >= '" . $dt_from . "' and pl.date <= '" . $dt_to . "' AND pl.vendor_actv_id = 0  
               
                                                    ORDER BY tmp asc)");
		/*, sum(pl . amount) as amt ,sum(oc . opening - oc . opening) as earn */
		/*$partnerLogsCount=$this->Retailer->query("SELECT count(*) as cnt
                                                    FROM partners_log pl
                                                    LEFT JOIN partners p ON pl.partner_id = p.id
                                                    LEFT JOIN retailers r ON p.retailer_id = r.id 
                                                    LEFT JOIN vendors_activations va ON pl.vendor_actv_id = va.ref_code
                                                    LEFT JOIN opening_closing oc ON oc.shop_transaction_id = va.shop_transaction_id and oc.shop_id = ".$this->Session->read('Auth.id')." and oc.group_id = ".$this->Session->read('Auth.User.group_id')."
                                                    LEFT JOIN products pr ON pl.product_id = pr.id WHERE pl.partner_id = $partnerId and pl.date >= '" . $dt_from . "' and pl.date <= '" . $dt_to . "' ");
                
               
		/*$total_count = $partnerLogsCount[0][0]['cnt'];
		$this->set('total_count',$total_count);*/
                $pageType = empty($_GET['res_type']) ? "" : $_GET['res_type'];
		if($pageType != 'csv'){
                    
                    $this->set('page',$pageNo);
                    $this->set('pageType',$pageType);
                    $this->set('top_tab','home');
                    $this->set('side_tab','rechargelist');
                    $this->set('transactions',$partnerLogs);
                    $this->set('active_menu','reports');
                    $this->set('active_submenu','recharge_list');
                    $this->render('recharge_list');
                    
                }else{
                     $this->set('page',$pageNo);
                     $this->set('pageType',$pageType);
                    App::import('Helper','csv');          
                    $this->layout = null;
                    $this->autoLayout = false;
                    $csv = new CsvHelper();
                    $line = array("Txn Date","Txn Id","Signal7 T_ID","Number","Operator","Amount","Opening","Closing","Earning","Reversal Date","Description","Status");
                    $csv->addRow($line);
                    $sumAmt = 0;
                    $sumErn = 0;
                    $noTrans = 0;
                    $i=0;
                    foreach($partnerLogs as $transaction){ 
                        if($i%2 == 0)$class = 'even';
                        else $class = 'odd';
                        $amount = empty($transaction[0]['amount'])?$transaction[0]['amount']:$transaction[0]['amount'];
                        $i++;

                        $opn = empty($transaction[0]['opening'])? 0 : $transaction[0]['opening'] ;
                        $cls = empty($transaction[0]['closing']) ? 0 : $transaction[0]['closing'] ;
                        $ern = 0;
                        $bgColor = "";
                        $status = "";
                        if($opn < $cls){//in case of reversal
                            $ern =  $cls - $opn  - $amount ; 
                            $noTrans --;
                            $sumAmt = $sumAmt - $amount;
                            $status = "Reverse";
                            $description = $transaction[0]['description'];
                        }else if( $cls< $opn){//in case of successful transaction
                            $ern = $amount - ( $opn - $cls );
                            $noTrans ++;

                            $sumAmt = $sumAmt + $amount;

                            if($transaction[0]['description']!= "Successful transaction ."){ // current staus failed
                                $description = $transaction[0]['description'];
                                $status = "Failure";
                            }else{ // current staus stil success
                                $description = $transaction[0]['description'];
                                $status = "Success";
                            }

                        }else if( $opn == $cls){//in case of instant failure
                            $status = "Failure";
                            $description = $transaction[0]['description'];
                        }
                            $sumErn = $sumErn +  $ern ;
                            $vendor_actv_id = $transaction[0]['vendor_actv_id'];
                            $line = array(
                                date('d-m-Y H:i:s', strtotime($transaction[0]['created'])),
                                $transaction[0]['partner_req_id'],
                                $vendor_actv_id,                                
                                empty($transaction[0]['mob_dth_no'])?(!empty($transaction[0]['param'])? $transaction[0]['param']:$transaction[0]['mobile']):$transaction[0]['mob_dth_no'],
                                $transaction[0]['name'],
                                $amount,
                                empty($transaction[0]['opening'])? 0 : $transaction[0]['opening'],
                                empty($transaction[0]['closing']) ? 0 : $transaction[0]['closing'],
                                round($ern,2),
                                empty($transaction[0]['tmp']) ? 0 : $transaction[0]['tmp'],
                                $description,
                                $status
                            );
                            $i++; 
                            $csv->addRow($line); 
                    }
                    
                    echo $csv->render('Transactions.csv'); 
                    
                    
                }
                

		
	}
	function transferDetails($date = null,$page=1){
		//$this->autoRender = false;

		if(empty ($_SESSION['Auth']['partner_id'])){
			$this->redirect("/");
		}
		if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
		}
		$grp_id = $_SESSION['Auth']['User']['group_id'];
		$is_partner = $_SESSION['Auth']['is_partner'];
		//if($grp_id != RETAILER || $is_partner != 'true' ) $this->render('/shops/partnerPanel');


		if(empty($date)){
			$date = date('dmY')."-".date('dmY');
		}

		//$limit = " limit " . ($page-1)*PAGE_LIMIT.",".PAGE_LIMIT;

		$dates = explode("-",$date);
		$date_from = $dates[0];
		$date_to = $dates[1];

		$transactions = array();
		if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
		}else{
			$date_from =  date('Y') . "-" . date('m') . "-" . date('d');
			$date_to =  date('Y') . "-" . date('m') . "-" . date('d');
		}

		$params = array();
		$params['date_from'] = $date_from;
		$params['date_to'] = $date_to ;
		$params['page_no'] =  $page;
		$params['items_per_page'] = PAGE_LIMIT ;
		 
		//Instantiation
		App::import('Controller', 'Apis');

		//Instantiation
		$apis = new ApisController;

		//Load model, components...
		//$apis->constructClasses();

		$transactions = array();
		$total_count = 0;
		$results = $apis->getDistToRetlBalTransfer($params,"JSON");
		// print_r($results);
		if($results['status'] == 'success'){
			$transactions = $results['data'];
			$total_count = $results['total_count'];
		}
		$this->set('date_from',$date_from);
		$this->set('date_to',$date_to);
		$this->set('page',$page);
		$this->set('total_count',$total_count);
		$this->set('top_tab','home');
		$this->set('side_tab','transferdetails');//transferdetails discounstructure
		$this->set('transactions',$transactions);

		//$this->render('buy_report_partner');
		$this->set('active_menu','reports');
		$this->set('active_submenu','transfer_details');
		$this->render('transfer_details');
	}
	function discountStructure(){
		//$this->autoRender = false;


		if(empty ($_SESSION['Auth']['partner_id'])){
			$this->redirect("/");
		}
		$slabId = $_SESSION['Auth']['slab_id'];
		$slabProducts = $this->Retailer->query("SELECT
                                                        `slabs_products`.`slab_id`,
                                                        `slabs_products`.`product_id`,
                                                        `slabs_products`.`percent`,
                                                        `products`.`name`,
                                                        `products`.`service_id`
                                                    FROM 
                                                        `slabs_products` , `products`
                                                    WHERE
                                                        `slabs_products`.`product_id` = `products`.`id`  and
                                                        `slabs_products`.`slab_id` = $slabId  ");
		//print_r($slabProducts);discounstructure
		//$this->set('top_tab','home');
		//$this->set('side_tab','discounstructure');
		$this->set('transactions',$slabProducts);

		$this->set('active_menu','reports');
		$this->set('active_submenu','discount_structure');
		$this->render('discount_structure');
	}
	function changePassword($par=null){
		if(empty ($_SESSION['Auth']['partner_id'])){
			$this->redirect("/");
		}

		if(!empty($_POST)){
			if(!empty ($_POST['curr_password']) && !empty ($_POST['new_password']) && !empty ($_POST['conf_new_password']) &&  (  $_POST['new_password'] == $_POST['conf_new_password'] ))
			{
				if(!isset($_SESSION['Auth']['User']['group_id']))
				$this->redirect(array('action' => 'dashboard'));

				$this->User->recursive = -1;

				$password = $this->User->find('first', array('fields' => array('User.password','User.id'), 'conditions' => array('User.mobile' => $this->Session->read('Auth.User.mobile'))));


				if($password['User']['password'] != $this->Auth->password($_POST['curr_password'])){
					$this->set('errFlag','1');
					$this->set('err_msg',"You are not authorized to change the password !");
					if($par != null){
						$this->set('par',$par);
					}
					//$this->render('/elements/change_password','ajax');
				}else{
					if($this->updatePassword($this->Session->read('Auth.User.mobile'),$_POST['new_password'],'change','in')){
						$this->set('msg',"Password changed successfully");
					}else {
						$this->set('errFlag','1');
						$this->set('err_msg',"Password can not be changed successfuly !!!");
					}
				}
			}
		}
		 


		$this->set('active_menu','change_password');
		$this->set('active_submenu','');
		$this->render('change_password');
		//$this->render('setting');
	}
	function updatePassword($mobile,$password,$type,$changeP=null){
		if(empty ($_SESSION['Auth']['partner_id'])){
			$this->redirect("/");
		}

		if($type == "new" || $changeP == null){
			$passFlag = '0';
			$sysPass = $password;
		}
		else if($type == "change"){
			$passFlag = '1';
			$sysPass = 'NULL';
		}

		if( $this->User->updateAll(array('User.password' => "'".$this->Auth->password($password)."'", 'User.passflag' => $passFlag, 'User.syspass' => "$sysPass"), array('User.mobile' => $mobile))){
			if($changeP == null)
			$this->General->sendPassword($mobile,$password,0);

			if($this->Session->read('Auth.User')){
				$_SESSION['Auth']['User']['passflag'] = $passFlag;
			}

			return true;
		}

		return false;
	}
	function afterLogin(){
		$this->autoRender = false;
        
		$response = array();
		$this->data = null;
		$this->data['User']['mobile'] = $_POST['mobile'];
		$this->data['User']['password'] = $this->Auth->password($_POST['password']);
		//$param = $_POST['param'];
		$this->User->recursive = -1;
		$usrData = $this->User->find('first',array('conditions' => array('User.mobile' => $this->data['User']['mobile'],'User.password' => $this->data['User']['password'])));
		if(empty($usrData)){
			//echo '0';
			$response['status'] = "FALSE";
			$response['errors'] = array('code'=>'E000','msg'=>'Invalid LoginID or Password .');
		}
		else {
			if($usrData['User']['group_id'] == RETAILER ){
				$info = $this->Shop->getShopData($usrData['User']['id'],$usrData['User']['group_id']);
				$info['User']['group_id'] = $usrData['User']['group_id'];
				$info['User']['id'] = $usrData['User']['id'];
				$info['User']['mobile'] = $usrData['User']['mobile'];
				$this->Session->write('Auth',$info);
				$response['status'] = "TRUE";
				$response['success'] = array('code'=>'S001','msg'=>'Login Successful.');
				
			}
			else {
				//echo "<script>alert('Wrong username or password');</script>";
				//echo '0';
				$response['status'] = "FALSE";
				$response['errors'] = array('code'=>'E000','msg'=>'Invalid LoginID or Password .');
			}
		}

		echo  json_encode($response);
		//$this->set('json_content', json_encode($response));
	}
	function logout(){
		$this->Auth->logout();
		session_destroy();
		$this->redirect('/partner/index');
	}

	function graphRetailerData($id=null,$type=null){
		$this->autoRender = false;
		$response = array();
		if(empty ($_SESSION['Auth']['partner_id'])){
			return json_encode(array('status'=>"FALSE"));
		}
		$type = empty($type)?"r":$type;
		$id = empty($id)?$_SESSION['Auth']['id']:$id;
		if(empty($_REQUEST['from'])){
			$from = date('Y-m-d',strtotime('-30 days'));
			$to = date('Y-m-d',strtotime('-1 days'));
		}
		else {
			$from = $_REQUEST['from'];
			$to = $_REQUEST['to'];
			if(checkdate(substr($from,2,2), substr($from,0,2), substr($from,4)) && checkdate(substr($to,2,2), substr($to,0,2), substr($to,4))){
				$from =  substr($from,4) . "-" . substr($from,2,2) . "-" . substr($from,0,2);
				$to =  substr($to,4) . "-" . substr($to,2,2) . "-" . substr($to,0,2);
			}
			else {
				$from = date('Y-m-d',strtotime('-30 days'));
				$to = date('Y-m-d',strtotime('-1 days'));
			}
		}

		//if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != RELATIONSHIP_MANAGER)$this->redirect('/shops/view');

		if($type == 'r'){
			if(!empty($id)){
				if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,RETAILER);
					if($data['parent_id'] != $this->info['id'])exit;
				}
				else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,RETAILER);
					$data1 = $this->Shop->getShopDataById($data['parent_id'],DISTRIBUTOR);
					if($data1['parent_id'] != $this->info['id']){
						exit;
					}
				}
			}
			else exit;

			$sale = $this->Retailer->query("SELECT sale,date,topup as min FROM retailers_logs WHERE retailer_id  = $id AND date >= '$from' AND date <= '$to' order by date");
			$from = ($sale['0']['retailers_logs']['date'] > $from)?$sale['0']['retailers_logs']['date']:$from;
			$begin = new DateTime($from);
			$end = new DateTime($to);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);

			$data = array();
			$totSale = 0;
			$i = 0;

			$saleData = array();

			foreach($sale as $sl){
				$saleData[$sl['retailers_logs']['date']]['sale'] = $sl['retailers_logs']['sale'];
				$saleData[$sl['retailers_logs']['date']]['topup'] = isset($sl['retailers_logs']['topup'])?$sl['retailers_logs']['topup']:0;
			}

			foreach ($period as $dt){
				$data1 = array();
				$date = $dt->format("Y-m-d");
				if(isset($saleData[$date])){
					if(isset($saleData[$date]['sale'])){
						$totSale += $saleData[$date]['sale'];
						$sale = intval($saleData[$date]['sale']);
					}
					else {
						$sale = 0;
					}

					if(isset($saleData[$date]['topup'])){
						$topup = intval($saleData[$date]['topup']);
					}
					else {
						$topup = 0;
					}
					$dm = $dt->format("d M");
					$data[] = array($dm,$sale,$topup);
				}
				else {
					$dm = $dt->format("d M");
					$data[] = array($dm,0,0);
				}

				$i++;
			}

			$avg = intval($totSale/$i);
			$i=0;
			foreach($data as $dt){
				$data[$i][3]=$avg;
				$i++;
			}
			$graphData = $data;

		}
		 
		$optionGraph = array(
                        'title' => 'Monthly Report  : '.  $begin->format("d M Y")."    -   ". $end->format("d M Y"),
                        'width' => '700',
                        'height' => '300',
                        'vAxis' => array('title' => "Amount"),
                        'hAxis' => array('title' => "Date"),
                        'seriesType' => "bars",
                        'backgroundColor'=>"#EEEEEE",
                        'series' => array(0 => array("type" => "line",'pointSize' => 3 , 'color'=>'red'),1 => array("type" => "line", 'pointSize' => 3,'color'=>'blue'),2 => array("type" => "line",'pointSize' => 0 , 'color'=>'green'))//1 => array("type" => "line",'curveType'=> "function", 'pointSize' => 3),
	);
		//array("Date","Daily Sale","Daily Topup","Average Sale");
		array_unshift($graphData, array("Date","Daily Sale","Daily Limit","Avg Sale"));
		//echo json_encode($graphData);
		$response['optionGraph'] = $optionGraph;
		$response['saledata'] = $graphData;
		$response['type'] = $type;
		$response['id'] = $id;
		$response['from'] = $from;
		$response['to'] = $to;
		$response['saledata'] = $graphData;

		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		//$data = $vendorActObj->query($query);
		$query = "SELECT count(*) as no_trans , sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date = '".date('Y-m-d')."'";
		$today = $vendorActObj->query($query);
		$response['today'] = $today['0'][0];
		$response['status'] = "TRUE";
                
                $queryRet = "SELECT user_id ,balance,opening_balance ,  mobile FROM `retailers` WHERE `id` =".$_SESSION['Auth']['id'] ;
                $res = $vendorActObj->query($queryRet);
                //print_r($res);
                $response['curBal'] = round($res[0]['retailers']['balance'],0);
                
		return json_encode($response);
		/*$this->set('optionGraph',$optionGraph);
		 $this->set('saledata',$graphData);
		 $this->set('type',$type);
		 $this->set('id',$id);
		 $this->set('from',$from);
		 $this->set('to',$to);
		 $this->set('active_menu','dashboard');
		 $this->set('active_submenu','');
		 $this->render("test");*/
	}
	function topUpRequest(){
		if(empty ($_SESSION['Auth']['partner_id'])){
			$this->redirect("/");
		}
		$dist_user = $this->General->getUserDataFromId($_SESSION['Auth']['user_id']);
		$this->set('mobile',$dist_user['mobile']);
		if(!empty($_POST)){
			if(!empty ($_POST['mobile']) && !empty ($_POST['bank_acc_id']) && !empty ($_POST['amount']))
			{
				$this->autoRender = false;
				$mobile = $_REQUEST['mobile'];
				$transId = str_replace( " ", "", $_REQUEST['bank_acc_id']."_".$_REQUEST['bank_trans_id']."_".$_REQUEST['trans_type_id'] );
				$msg = "bnk ".$_REQUEST['amount']." ".$transId;//'bnk amt bankAcc_bankTransId_bankTransType'

				//Instantiation
				App::import('Controller', 'Apis');

				//Instantiation
				$apis = new ApisController;

				//Load model, components...
				$apis->constructClasses();
				$res = $apis->receiveSMS1($mobile, $msg, "","");
				$this->set('msg',$res['msg']);
				$this->set('errFlag','0');
			}else{
				$this->set('err_msg','Invalid inputs .');
				$this->set('errFlag','1');
			}
		}
		$this->set('active_menu','topup_request');
		$this->set('active_submenu','');
		$this->render('topup_request');
	}
}

?>
