<?php

class ShopComponent extends Object {
	var $components = array('General');
	
	function errors($code){
		$err = array(
			'0'=>'Authentication Failed.',
			'1'=>'Account disabled.',
			'2'=>'Invalid Method.',
			'3'=>'Request cannot be processed. Try again.',
			'4'=>'Invalid Number of Parameters.',
			'5'=>'Invalid Mobile Number.',
			'6'=>'Invalid Amount.',
			'7'=>'Invalid Recharge Type.',
			'8'=>'Invalid Operator Code.',
			'9'=>'Product does not exist.',
			'10'=>'Recharge PPI failed.',
			'11'=>'Getting recharges from OSS failed.',
			'12'=>'Recharge OSS failed.',
			'13'=>'Transaction Successful.',
			'14'=>'Transaction Failed. Try Again.',
			'15'=>'Transaction Pending.',
			'16'=>'DTH Recharge PPI failed.',
			'17'=>'Getting dth recharges from OSS failed.',
			'18'=>'DTH Recharge OSS failed.',
			'19'=>'Custom PPI Message.',
			'20'=>'Custom oss Message.',
			'21'=>'Mobile flexi recharge code not supported from OSS.',
			'22'=>'Mobile recharge code not available from OSS.',
			'23'=>'DTH recharge code not available from OSS.',
			'24'=>'DTH flexi recharge code not supported from OSS.',
			'25'=>'No active vendor for mobile recharge.',
			'26'=>'Insufficient balance. Please recharge.',
			'27'=>'No active vendor for DTH recharge.',
			'28'=>'Login failed. Try Again.',
			'29'=>'Recharge not available.',
			'30'=>'Technical Problem. Please try again.',
			'31'=>'Transaction Processed.',
			'403'=>'Session does not exist.',
			'404'=>'Access denied.',
			'32'=>'Invalid Existing Pin.',
			'33'=>'Minimum recharge error.',
			'34'=>'Maximum recharge error.',
			'35'=>'Telecom Circle not found. Please try after some time.',
			'36'=>'Vendor Server not responding',
			'37'=>'Please try this transaction after 15 minutes.',
			'38'=>'Recharge for this mobile number/subscriber id is already under process.',
			'39'=>'Wrong subscriber id',
			'40'=>'No active sim/no balance',
			'41'=>'Dropped due to lot of pending requests',
			'42'=>'Payment Authorization failed'
			);
			return 	$err[$code];
	}
	function apiErrors($code){
		$err = array(
			'0'=>'Authentication Failed.',
			'1'=>'Account disabled.',
			'2'=>'Invalid Method.',
			'3'=>'Request cannot be processed. Try again.',
			'4'=>'Invalid Number of Parameters.',
			'5'=>'Invalid Mobile Number.',
			'6'=>'Invalid Amount.',
			'7'=>'Invalid Recharge Type.',
			'8'=>'Invalid Operator Code.',
			'9'=>'Product does not exist.',
			'10'=>'Recharge PPI failed.',
			'11'=>'Getting recharges from OSS failed.',
			'12'=>'Recharge OSS failed.',
			'13'=>'Transaction Successful.',
			'14'=>'Transaction Failed. Try Again.',
			'15'=>'Transaction Pending.',
			'16'=>'DTH Recharge PPI failed.',
			'17'=>'Getting dth recharges from OSS failed.',
			'18'=>'DTH Recharge OSS failed.',
			'19'=>'Custom PPI Message.',
			'20'=>'Custom oss Message.',
			'21'=>'Mobile flexi recharge code not supported from OSS.',
			'22'=>'Mobile recharge code not available from OSS.',
			'23'=>'DTH recharge code not available from OSS.',
			'24'=>'DTH flexi recharge code not supported from OSS.',
			'25'=>'No active vendor for mobile recharge.',
			'26'=>'Insufficient balance. Please recharge.',
			'27'=>'No active vendor for DTH recharge.',
			'28'=>'Login failed. Try Again.',
			'29'=>'Recharge not available.',
			'30'=>'Technical Problem. Please try again.',
			'31'=>'Transaction Processed.',
			'403'=>'Session does not exist.',
			'404'=>'Access denied.',
			'32'=>'Invalid Existing Pin.',
			'33'=>'Minimum recharge error.',
			'34'=>'Maximum recharge error.',
			'35'=>'Telecom Circle not found. Please try after some time.',
			'36'=>'Vendor Server not responding',
			'37'=>'Please try this transaction after 15 minutes.',
			'38'=>'Recharge for this mobile number/subscriber id is already under process.',
			'39'=>'Wrong subscriber id',
			'40'=>'No active sim/no balance',
			'41'=>'Dropped due to lot of pending requests',
                        
                        //------API Error Codes-------
                        'E000'=>'Some error occured.',
                        'E001'=>'Invalid Operation Type.',
                        'E002'=>'Account disabled.',
                        'E003'=>'Invalid inputs.',// Empty of insufficient input
                        'E004'=>'Invalid partner ID.',
                        'E005'=>'Authentication Error ( Invalid Access Key ).',
                        'E006'=>'Authentication Error ( Invalid access location ).',
                        'E007'=>'Not enough balance',
                        'E008'=>"Wrong operator code/Wrong Mobile Number.",
						'E009'=>"Wrong operator code/Wrong Subscriber id.",
						'E010'=>"Invalid amount.",
                        'E011'=>"Request already in process",
						'E012'=>"Try after some time",
						'E013'=>"Operator General Error",
						'E014'=>"Duplicate request",
						'E015'=>"Invalid Transaction Id",
                                                'E016'=>"Invalid operator access",
                                                'E017'=>"No of transactions should be less or equal to 10."
                                                
			);
			return 	$err[$code];
	}
	
	function mapApiErrs($code){
		$err = array('1' => 'E002','2' => 'E001','3' => 'E000','4' => 'E003','5' => 'E003','6' => 'E003','7' => 'E003',
		'8' => 'E003', '9' => 'E003','10' => 'E013','11' => 'E013','12' => 'E013','14' => 'E013','16'=>'E013','17'=>'E013','18'=>'E013',
		'19'=>'E013','20'=>'E013','21'=>'E013','22'=>'E013','23'=>'E013','24'=>'E013','25'=>'E013','26'=>'E007','27'=>'E013','29'=>'E013',
		'30'=>'E000','33'=>'E010','34'=>'E010','35'=>'E013','36'=>'E013','37'=>'E012','38'=>'E011','39'=>'E009','40'=>'E013','41'=>'E013','42'=>'E013');
		return 	$err[$code];
	}
	
	function smsProdCodes($code){
		$prod = array(		
				'1'=>array(
					'operator'=>'Aircel',
					'params'=>array('method' => 'mobRecharge','operator'=>'1','type'=>'flexi')
				),
				'2'=>array(
					'operator'=>'Airtel',
					'params'=>array('method' => 'mobRecharge','operator'=>'2','type'=>'flexi')
				),
				'3'=>array(
					'operator'=>'BSNL',
					'params'=>array('method' => 'mobRecharge','operator'=>'3','type'=>'flexi')
				),
				'4'=>array(
					'operator'=>'Idea',
					'params'=>array('method' => 'mobRecharge','operator'=>'4','type'=>'flexi')
				),
				'5'=>array(
					'operator'=>'Loop/BPL',
					'params'=>array('method' => 'mobRecharge','operator'=>'5','type'=>'flexi')
				),
				'6'=>array(
					'operator'=>'MTS',
					'params'=>array('method' => 'mobRecharge','operator'=>'6','type'=>'flexi')
				),
				'7'=>array(
					'operator'=>'Reliance CDMA',
					'params'=>array('method' => 'mobRecharge','operator'=>'7','type'=>'flexi')
				),
				'8'=>array(
					'operator'=>'Reliance GSM',
					'params'=>array('method' => 'mobRecharge','operator'=>'8','type'=>'flexi')
				),
				'9'=>array(
					'operator'=>'Tata Docomo',
					'params'=>array('method' => 'mobRecharge','operator'=>'9','type'=>'flexi')
				),
				'10'=>array(
					'operator'=>'Tata Indicom',
					'params'=>array('method' => 'mobRecharge','operator'=>'10','type'=>'flexi')
				),
				'11'=>array(
					'operator'=>'Uninor',
					'params'=>array('method' => 'mobRecharge','operator'=>'11','type'=>'flexi')
				),
				'12'=>array(
					'operator'=>'Videocon',
					'params'=>array('method' => 'mobRecharge','operator'=>'12','type'=>'flexi')
				),
				'13'=>array(
					'operator'=>'Virgin CDMA',
					'params'=>array('method' => 'mobRecharge','operator'=>'13','type'=>'flexi')
				),
				'14'=>array(
					'operator'=>'Virgin GSM',
					'params'=>array('method' => 'mobRecharge','operator'=>'14','type'=>'flexi')
				),
				'15'=>array(
					'operator'=>'Vodafone',
					'params'=>array('method' => 'mobRecharge','operator'=>'15','type'=>'flexi')
				),
				'27'=>array(
					'operator'=>'Tata SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'9','type'=>'flexi','special'=>1)
				),
				'28'=>array(
					'operator'=>'Videocon SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'12','type'=>'flexi','special'=>1)
				),
				'29'=>array(
					'operator'=>'Uninor SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'11','type'=>'flexi','special'=>1)
				),
				'30'=>array(
					'operator'=>'MTNL',
					'params'=>array('method' => 'mobRecharge','operator'=>'30','type'=>'flexi')
				),
				'31'=>array(
					'operator'=>'MTNL SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'30','type'=>'flexi','special'=>1)
				),
				'34'=>array(
					'operator'=>'BSNL SV',
					'params'=>array('method' => 'mobRecharge','operator'=>'3','type'=>'flexi','special'=>1)
				),
				'16'=>array(
					'operator'=>'Airtel DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'1','type'=>'flexi')
				),
				'17'=>array(
					'operator'=>'Big TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'2','type'=>'flexi')
				),
				'18'=>array(
					'operator'=>'Dish TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'3','type'=>'flexi')
				),
				'19'=>array(
					'operator'=>'Sun TV DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'4','type'=>'flexi')
				),
				'20'=>array(
					'operator'=>'Tata Sky DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'5','type'=>'flexi')
				),
				'21'=>array(
					'operator'=>'Videocon DTH',
					'params'=>array('method' => 'dthRecharge','operator'=>'6','type'=>'flexi')
				),
				'22'=>array(
					'operator'=>'Dil Vil Pyar Vyar',
					'params'=>array('method' => 'vasRecharge','operator'=>'22','type'=>'flexi')
				),
				'23'=>array(
					'operator'=>'Naughty Jokes',
					'params'=>array('method' => 'vasRecharge','operator'=>'23','type'=>'flexi')
				),
				'24'=>array(
					'operator'=>'PNR Alert',
					'params'=>array('method' => 'vasRecharge','operator'=>'24','type'=>'flexi')
				),
				'25'=>array(
					'operator'=>'Instant Cricket',
					'params'=>array('method' => 'vasRecharge','operator'=>'25','type'=>'flexi')
				),
				'32'=>array(
					'operator'=>'Chatpati Baate Mini Pack',
					'params'=>array('method' => 'vasRecharge','operator'=>'32','type'=>'flexi')
				),
				'33'=>array(
					'operator'=>'Chatpati Baate Mega Pack',
					'params'=>array('method' => 'vasRecharge','operator'=>'33','type'=>'flexi')
				)
			);
			return  isset($prod[$code]['params']) ? $prod[$code]['params'] : array() ;
	}
	
	function shopTransactionUpdate($type,$amount,$ref1_id,$ref2_id,$user_id=null,$discount=null){
		$this->data = null;
		$transObj = ClassRegistry::init('ShopTransaction');

		$this->data['ShopTransaction']['ref1_id'] = $ref1_id;
		$this->data['ShopTransaction']['ref2_id'] =  $ref2_id;
		$this->data['ShopTransaction']['amount'] =  $amount;
		$this->data['ShopTransaction']['type'] =  $type;
		$this->data['ShopTransaction']['timestamp'] =  date('Y-m-d H:i:s');
		$this->data['ShopTransaction']['date'] =  date('Y-m-d');
		if($type == RETAILER_ACTIVATION){
			$this->data['ShopTransaction']['confirm_flag'] =  1;
		}
		
		if($user_id != null){
			$this->data['ShopTransaction']['user_id'] =  $user_id;
		}
		if($discount != null){
			$this->data['ShopTransaction']['discount_comission'] =  $discount;
		}
		$transObj->create();
		$transObj->save($this->data);

		return $transObj->id;
	}
	
	function addAppRequest($method,$mobile,$amount,$opr,$type){
		$reqObj = ClassRegistry::init('Request');
		
		//$reqObj->query("DELETE FROM requests WHERE timestamp < now() - 300");
		
		$this->data['Request']['method'] = $method;
		$this->data['Request']['mobile'] = $mobile;
		$this->data['Request']['amount'] =  $amount;
		$this->data['Request']['operator'] =  $opr;
		$this->data['Request']['type'] =  $type;
		$this->data['Request']['timestamp'] =  null;
		$reqObj->create();
		if($reqObj->save($this->data)){
			return $reqObj->id;
		}
		else {
			$reqObj->query("INSERT INTO requests_dropped (retailer_id,method,mobile,amount,operator,timestamp) VALUES (".$_SESSION['Auth']['id'].",'$method','$mobile',$amount,$opr,'".date('Y-m-d H:i:s')."')");
			return null;
		}
	}
	
	function deleteAppRequest($mobile,$amount){
		$reqObj = ClassRegistry::init('Request');
		
		$reqObj->query("DELETE FROM requests WHERE mobile='$mobile' AND amount=$amount");
	}
	
	function addComment($userId,$retId,$transId,$test,$loggedInUser,$name){
		$userObj = ClassRegistry::init('User');
		
		if(!empty($test)){
			$userObj->query("insert into comments(users_id,retailers_id,mobile,ref_code,comments,created) values('$userId','$retId','$loggedInUser','$transId','".addslashes($test)."','".date('Y-m-d H:i:s')."')");	
		
			$user = $this->General->getUserDataFromMobile($loggedInUser);
			$data['user'] = $user['name'];
			if(!empty($transId)){
				$txid = $transId;
				$data['txtype'] = 1;
				
				$extra = $userObj->query("SELECT vendors.shortForm,vendors_activations.amount,products.name FROM vendors_activations,vendors,products WHERE products.id = product_id AND vendors.id = vendor_id AND ref_code = '$transId'");	
				$data['extra'] = $extra['0']['vendors']['shortForm'] . " | " . $extra['0']['products']['name'] . " | " . $extra['0']['vendors_activations']['amount'];
			}
			else if(!empty($retId)){
				$retData = $userObj->query("SELECT mobile FROM retailers WHERE id = $retId");	
		
				$txid = $retData['0']['retailers']['mobile'];
				$data['txtype'] = 2;
			}
			else {
				$user = $this->General->getUserDataFromId($userId);
				$txid = $user['mobile'];
				$data['txtype'] = 3;
			}
			$data['txid'] = $txid;
			$data['process'] = 'cc';
			$data['msg'] = $test;
			$this->General->curl_post_async("http://pay1.in/cclive/server.php",$data);
		}
	}
	
	function shopCreditDebitUpdate($type,$amount,$from,$to,$to_groupId,$desc,$numbering,$api_flag=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		if($api_flag == null)$api_flag = 0;
		if(!empty($from))
			$transObj->query("INSERT INTO shop_creditdebit (from_id,to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($from,$to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");	
		else 
			$transObj->query("INSERT INTO shop_creditdebit (to_id,to_groupid,amount,type,api_flag,description,numbering,timestamp) VALUES ($to,$to_groupId,$amount,$type,$api_flag,'".addslashes($desc)."','$numbering','".date('Y-m-d H:i:s')."')");	
	}
	
	function transactionsOnRetailerActivation($retailer_id,$product,$user_id,$amount){
		$prodObj = ClassRegistry::init('Product');
		$prodObj->recursive = -1;
		//transaction entries
		$percent = $this->getCommissionPercent($_SESSION['Auth']['slab_id'],$product);
		$trans_id = $this->shopTransactionUpdate(RETAILER_ACTIVATION,$amount,$retailer_id,$product,$user_id,$percent);
		$commission_retailer = $this->calculateCommission($retailer_id,RETAILER,$product,$amount,$_SESSION['Auth']['slab_id']);
		
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$distributor_id = $ret_shop['parent_id'];
		
		$dist_shop = $this->getShopDataById($distributor_id,DISTRIBUTOR);
		/*
		$superdistributor_id = $dist_shop['parent_id'];
		$superdist_shop = $this->getShopDataById($superdistributor_id,SUPER_DISTRIBUTOR);
		
		$commission_distributor = $this->calculateCommission($distributor_id,DISTRIBUTOR,$product,$amount);
		
		$commission_superdistributor = $this->calculateCommission($superdistributor_id,SUPER_DISTRIBUTOR,$product,$amount);
		$sd_percent = $this->getCommissionPercent($superdist_shop['slab_id'],$product);
		$this->shopTransactionUpdate(COMMISSION_SUPERDISTRIBUTOR,$commission_superdistributor,$superdistributor_id,$trans_id,null,$sd_percent);
		
		if(TAX_MODEL == 0){
			$tds_superdistributor = $this->calculateTDS($commission_superdistributor);
			$this->shopTransactionUpdate(TDS_SUPERDISTRIBUTOR,$tds_superdistributor,$superdistributor_id,$trans_id);
			$commission_superdistributor -= $tds_superdistributor;
		}
		$d_percent = $this->getCommissionPercent($dist_shop['slab_id'],$product);
		$this->shopTransactionUpdate(COMMISSION_DISTRIBUTOR,$commission_distributor,$distributor_id,$trans_id,null,$d_percent);
		if($superdist_shop['tds_flag'] == 1 && TAX_MODEL == 0){
			$tds_distributor = $this->calculateTDS($commission_distributor);
			$this->shopTransactionUpdate(TDS_DISTRIBUTOR,$tds_distributor,$distributor_id,$trans_id);
			$commission_distributor -= $tds_distributor;
		}*/
		$this->shopTransactionUpdate(COMMISSION_RETAILER,$commission_retailer,$retailer_id,$trans_id,null,$percent);
		if($dist_shop['tds_flag'] == 1 && TAX_MODEL == 0){
			$tds_retailer = $this->calculateTDS($commission_retailer);
			$this->shopTransactionUpdate(TDS_RETAILER,$tds_retailer,$retailer_id,$trans_id);
			$commission_retailer -= $tds_retailer;	
		}
		
		//balance update
		//$amount_distributor = $commission_distributor - $commission_retailer;
		//$amount_superdistributor = $commission_superdistributor - $commission_distributor;
		$amount_retailer = $amount - $commission_retailer;
		
		$bal = $this->shopBalanceUpdate($amount_retailer,'subtract',$retailer_id,RETAILER);
		$this->addOpeningClosing($retailer_id,RETAILER,$trans_id,$bal+$amount_retailer,$bal);
		//$this->shopBalanceUpdate($amount_distributor,'add',$distributor_id,DISTRIBUTOR);
		//$this->shopBalanceUpdate($amount_superdistributor,'add',$superdistributor_id,SUPER_DISTRIBUTOR);
		return array($bal,$trans_id);
	}
	
	function addOpeningClosing($shop_id,$group_id,$shoptrans_id,$opening,$closing){
		$prodObj = ClassRegistry::init('Product');
		$prodObj->query("INSERT INTO opening_closing (shop_id,group_id,shop_transaction_id,opening,closing,timestamp) VALUES ($shop_id,$group_id,$shoptrans_id,$opening,$closing,'".date('Y-m-d H:i:s')."')");
	}
	
	function createVendorActivation($vendor_id,$product_id,$api_flag,$mobile,$amount,$shop_trans_id,$ret_id,$param = null){
		if(empty($api_flag))$api_flag = 0;
		
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		
		$this->data['VendorsActivation']['vendor_id'] = $vendor_id;
		$this->data['VendorsActivation']['product_id'] =  $product_id;
		$this->data['VendorsActivation']['mobile'] =  $mobile;
		$this->data['VendorsActivation']['amount'] =  $amount;
		$this->data['VendorsActivation']['shop_transaction_id'] =  $shop_trans_id;
		$this->data['VendorsActivation']['retailer_id'] =  $ret_id;
		$this->data['VendorsActivation']['timestamp'] =  date('Y-m-d H:i:s');
		$this->data['VendorsActivation']['date'] =  date('Y-m-d');
		$this->data['VendorsActivation']['api_flag'] =  $api_flag;
		if($param != null){
			$this->data['VendorsActivation']['param'] =  $param;
		}
		
		$data = $vendorActObj->query("SELECT discount_commission FROM vendors_commissions WHERE vendor_id=$vendor_id AND product_id=$product_id");
		$this->data['VendorsActivation']['discount_commission'] = $data['0']['vendors_commissions']['discount_commission'];
		
		$vendorActObj->create();
		$vendorActObj->save($this->data);
		$id = $vendorActObj->id;
		
		$ref_code =  "3022" . sprintf('%08d', $id);
		$vendorActObj->updateAll(array('VendorsActivation.ref_code' => $ref_code), array('VendorsActivation.id' => $id));

		return $ref_code;
	}
	
	function updateVendorActivation($ref_code,$trans_id,$status,$code,$cause,$extra,$opr_id){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		
		$vendorActObj->query("update vendors_activations set vendor_refid = '".$trans_id."',code='$code',cause='".addslashes($cause)."',status='".$status."',extra='".addslashes($extra)."',operator_id='$opr_id' where ref_code='".$ref_code."'");
	}
	
	function createTransaction($prodId,$venId,$api_flag,$mobNo,$amt,$custId=null){
		$shopData = $this->getShopDataById($_SESSION['Auth']['id'],$_SESSION['Auth']['User']['group_id']);
		if($shopData['balance'] < $amt){
			return array('status'=>'failure','code'=>'26','description'=>$this->errors(26));	
		}
		else {
			$exists = $this->General->checkIfUserExists($mobNo);
			if(!$exists){
				$user = $this->General->registerUser($mobNo,RETAILER_REG);
				$user = $user['User'];
			}
			else {
				$user = $this->General->getUserDataFromMobile($mobNo);
			}
			$ret = $this->transactionsOnRetailerActivation($_SESSION['Auth']['id'],$prodId,$user['id'],$amt);
			$ref_id = $this->createVendorActivation($venId,$prodId,$api_flag,$mobNo,$amt,$ret[1],$_SESSION['Auth']['id'],$custId);
			return array('status'=>'success','tranId'=>$ref_id,'balance'=>$ret[0]);		
		}
	}
	
	function updateTransaction($tranId,$venTranId,$status,$code,$desc,$extra=null,$opr_id=null){
		if($status == 'success') $status = TRANS_SUCCESS;
		else if($status == 'failure') {
			$status = TRANS_REVERSE;
		}
		else if($status == 'pending') {
			$status = 0;
		}
		if(is_null($extra))
		$extra = '';
		
		$this->updateVendorActivation($tranId,$venTranId,$status,$code,$desc,$extra,$opr_id);
		if($status == TRANS_REVERSE){
			$this->reverseTransaction($tranId,1);
		}
	}
	
	function reverseTransaction($tranId,$update=null,$error=null){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;

		$data = $vendorActObj->query("SELECT vendors_activations.api_flag,vendors_activations.id,vendors_activations.shop_transaction_id,vendors_activations.status,vendors_activations.prevStatus,vendors_activations.cause,vendors_activations.code,vendors_activations.amount,vendors_activations.mobile,vendors_activations.param,vendors_activations.vendor_id,products.service_id,vendors.update_flag,vendors.ip,vendors.port FROM vendors_activations,products,vendors WHERE vendors.id = vendor_id AND products.id = product_id AND ref_code = '$tranId'");
		if($update == null && ($data['0']['vendors_activations']['status'] == TRANS_REVERSE || $data['0']['vendors_activations']['prevStatus'] == TRANS_REVERSE)){
			return;
		}

		/*if(empty($data['0']['vendors_activations']['prevStatus'])){
			$vendorActObj->query("UPDATE vendors_activations SET prevStatus=status,status = '".TRANS_REVERSE."' WHERE ref_code= '".$tranId."'");
		} else {
			$vendorActObj->query("UPDATE vendors_activations SET status = '".TRANS_REVERSE."' WHERE ref_code= '".$tranId."'");
		}*/

		$shop_trans_id = $data['0']['vendors_activations']['shop_transaction_id'];

		$retailer = $vendorActObj->query("SELECT * FROM shop_transactions WHERE id = '$shop_trans_id'");

		$ret_id = $retailer['0']['shop_transactions']['ref1_id'];
		$amount = $retailer['0']['shop_transactions']['amount'];

		$allRecords = $vendorActObj->query("SELECT * FROM shop_transactions WHERE ref2_id = '$shop_trans_id'");
		$ret_amount = 0;
		foreach($allRecords as $record){
			if($record['shop_transactions']['type'] == COMMISSION_RETAILER){
				$ret_amount = $ret_amount + $record['shop_transactions']['amount'];
			}
			else if($record['shop_transactions']['type'] == TDS_RETAILER){
				$ret_amount = $ret_amount - $record['shop_transactions']['amount'];
			}
		}

		$ret_amount = $amount - $ret_amount;

		$this->shopTransactionUpdate(REVERSAL_RETAILER,$ret_amount,$ret_id,$shop_trans_id,null,null);
		$vendorActObj->query("UPDATE shop_transactions SET confirm_flag = 0 WHERE id = $shop_trans_id");

		$bal = $this->shopBalanceUpdate($ret_amount,'add',$ret_id,RETAILER);
		$this->addOpeningClosing($ret_id,RETAILER,$shop_trans_id,$bal-$ret_amount,$bal);

		$bal = round($bal,2);
		//inform retailer by sms
		$shop_data = $this->getShopDataById($ret_id,RETAILER);

		$vendorActObj->query("UPDATE complaints SET resolve_flag=1,closedby='".$_SESSION['Auth']['User']['id']."',resolve_date='".date('Y-m-d')."',resolve_time='".date('H:i:s')."' WHERE vendor_activation_id = " . $data['0']['vendors_activations']['id']);

		if($data['0']['vendors']['update_flag'] == 1){
			$ip = $data['0']['vendors']['ip'];
			if(empty($ip))$ip = '122.170.103.144';
			if(strpos($ip,":") === false){
				if(!empty($data['0']['vendors']['port']))$ip .= ":".$data['0']['vendors']['port'];
				else $ip .= ":8081";
			}
			$url = "http://$ip/start.php";
			$pars['query'] = 'reverse';
			$pars['transId'] = $tranId;
			$this->General->curl_post_async($url,$pars);
		}

		$err_code = "E013";
		if($error==1){
			$err_code = 'E012';
		}else if($error==4){
			if($data['0']['products']['service_id'] == 1)
			$err_code = 'E008';
			else if($data['0']['products']['service_id'] == 2)
			$err_code = 'E009';
		}else if($error==3){
			$err_code = 'E010';
		}  else {
			$err_code = 'E013';
		}
		
		if(empty($data['0']['vendors_activations']['prevStatus'])){
			$vendorActObj->query("UPDATE vendors_activations SET prevStatus=status,status = '".TRANS_REVERSE."',code='$err_code',cause='".addslashes($this->apiErrors($err_code))."' WHERE ref_code= '".$tranId."'");
		} else {
			$vendorActObj->query("UPDATE vendors_activations SET status = '".TRANS_REVERSE."',code='$err_code',cause='".addslashes($this->apiErrors($err_code))."' WHERE ref_code= '".$tranId."'");
		}

		if($data['0']['vendors_activations']['api_flag'] == 4){
			//update partners log entry
			$vendorActObj->query("UPDATE partners_log SET err_code= '$err_code',description = '".$this->apiErrors($err_code)."' WHERE vendor_actv_id= '".$tranId."'");
			 
			// call status_update url of api vendors ( pay1 api client )
			//$partnerRegObj = ClassRegistry::init('Partner');
			$partnerLog = $vendorActObj->query("SELECT partners_log.id,partners_log.partner_req_id,partners.status_update_url,partners.acc_id,partners.password FROM partners,partners_log WHERE partners.id = partners_log.partner_id AND partners_log.vendor_actv_id = '$tranId'");
			$status_update_url = $partnerLog[0]['partners']['status_update_url'];
			if(!empty($status_update_url)){

				$partnerId = $partnerLog['0']['partners']['acc_id'];
				$pay1_trans_id = "2082" . sprintf('%06d', $partnerLog['0']['partners_log']['id']);
				$client_trans_id = $partnerLog['0']['partners_log']['partner_req_id'];
				$open_bal = $bal-$ret_amount;
				$close_bal = $bal;

				$hash = sha1($partnerId.$client_trans_id.$partnerLog['0']['partners']['password']); // order -> acc_id + transId + refCode + password
				
				$sdata = array('open_bal'=>$open_bal,'close_bal'=>$close_bal,'trans_id'=>$pay1_trans_id,'client_req_id'=>$client_trans_id,'status'=>'failure','err_code'=>$err_code,'description'=>$this->apiErrors($err_code),'hash_code'=>$hash);
				$this->General->curl_post_async($status_update_url, $sdata);
				
				//$fh = fopen('/var/www/html/shops/abc.txt','a+');
				//fwrite($fh,$status_update_url . " " . json_encode($sdata));
			}
		}else{
			$tranId = substr($tranId,-5);
			$msg = "";
			if(!empty($error)){
				$msg .= "Reason:".$this->apiErrors($err_code)."\n";
			}
			$msg .= "Reversal of Rs.".$ret_amount." is done.\n";

			$msg .="Trans Id: $tranId\n";
			if($data['0']['products']['service_id'] == 1)
			{
				$msg .="Mobile: " . $data['0']['vendors_activations']['mobile'] . "\n";
				$this->deleteAppRequest($data['0']['vendors_activations']['mobile'],$data['0']['vendors_activations']['amount']);
			}
			else if($data['0']['products']['service_id'] == 2){
				$msg .="Subscriber Id: " . $data['0']['vendors_activations']['param'] . "\n";
				$this->deleteAppRequest($data['0']['vendors_activations']['param'],$data['0']['vendors_activations']['amount']);
			}
			$msg .="Amount: ".$data['0']['vendors_activations']['amount']."\nYour current balance is Rs.".$bal;
			$this->General->sendMessage($shop_data['mobile'],$msg,'shops');

		}

	}
	
	function modemRequest($query,$vendor=4){
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		
		$data = $vendorActObj->query("SELECT ip,port,company FROM vendors WHERE id = $vendor");
		
		$ip = !empty($data['0']['vendors']['ip'])?$data['0']['vendors']['ip']:"122.170.103.144";
		$port = !empty($data['0']['vendors']['port'])?$data['0']['vendors']['port']:"8081";
		
		$fp = fsockopen($ip,$port,$errno, $errstr, 30);
		if(!$fp){
			$this->General->sendMails("Cannot connect to ".$data['0']['vendors']['company'] . " Machine","Error: $errstr",array('tadka@mindsarray.com'));
			return array('status'=>'failure','errno'=>$errno,'error'=>$errstr);
		}
		fclose($fp);
		
		$url = "http://$ip:$port/start.php";
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
		
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_sfetopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 50);
		$Rec_Data = trim(curl_exec($ch));
		curl_close($ch);
		return array('status'=>'success','data'=>$Rec_Data);
	}
	
	function earnings($params){
		$pageNo = empty($params['page_no'])?0:$params['page_no'];
		$itemsPerPage = empty($params['items_per_page'])?0:$params['items_per_page'];

		if(!isset($params['date']) || empty($params['date'])){
			$num = date("w");
			$date_from = date('Y-m-d',strtotime('- ' . $num . ' days'));
			$date_to = date('Y-m-d');
			$next_week = "";
			$prev_week = date('dmY',strtotime('- ' . ($num  + 7). ' days')) .'-'. date('dmY',strtotime('- ' . ($num  + 1). ' days'));
		}
		else {
			$date = $params['date'];
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
			$prev_week = date('dmY',strtotime($date_from . ' - 7 days')) .'-'. date('dmY',strtotime($date_to . ' - 7 days'));
			if(date('Y-m-d',  strtotime($date_to)) < date('Y-m-d')){
				$next_week = date('dmY',strtotime($date_to . ' + 1 days')) .'-'. date('dmY',strtotime($date_to . ' + 7 days'));
			}
			else $next_week = "";
		}
		 
		if($itemsPerPage <= 0 || $pageNo <= 0){
			//$query = "SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.")  WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date";
			$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date >= '$date_from' AND va.date <= '$date_to' group by va.date";
		}else{
			$ll = $itemsPerPage * ( $pageNo - 1 ) ;//+ 1; // lower limit
			$ul = $itemsPerPage * $pageNo ;// upper limit
			//$query = "SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.")  WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date limit $ll , $ul ";
			$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date >= '$date_from' AND va.date <= '$date_to' group by va.date limit $ll , $ul";
		}
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;

		$data = $vendorActObj->query($query);
		$query = "SELECT sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income,trim(va.date) as date FROM vendors_activations as va INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id'] . " AND va.status != 2 AND va.status != 3 AND va.date = '".date('Y-m-d')."'";
		$today = $vendorActObj->query($query);
		//$today = $vendorActObj->query("SELECT sum(st1.amount) as amount, sum(st2.amount) as income,date(st1.timestamp) as date FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_RETAILER.") WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION." AND st1.ref1_id = ".$_SESSION['Auth']['id'] . " AND st1.date = '".date('Y-m-d')."'");
		//print_r($data);
		return array($data,$today['0'],$prev_week,$next_week,$date_from.' to '.$date_to);
	}
	
	function topups($params){
		if(!isset($params['date']) || empty($params['date'])){
			$num = date("w");
			$date_from = date('Y-m-d',strtotime('- ' . $num . ' days'));
			$date_to = date('Y-m-d');
			$next_week = "";
			$prev_week = date('dmY',strtotime('- ' . ($num  + 7). ' days')) .'-'. date('dmY',strtotime('- ' . ($num  + 1). ' days'));
		}
		else {
			$date = $params['date'];
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);	
			$prev_week = date('dmY',strtotime($date_from . ' - 7 days')) .'-'. date('dmY',strtotime($date_to . ' - 7 days'));
			if($date_to < date('Y-m-d')){
				$next_week = date('dmY',strtotime($date_to . ' + 1 days')) .'-'. date('dmY',strtotime($date_to . ' + 7 days'));	
			}
			else $next_week = "";	
		}
		
		$query = "SELECT sum(st1.amount) as amount, date(st1.timestamp) as day FROM shop_transactions as st1 WHERE st1.type = ".DIST_RETL_BALANCE_TRANSFER." AND st1.ref2_id = ".$_SESSION['Auth']['id'] . " AND st1.date >= '$date_from' AND st1.date <= '$date_to' group by st1.date";
		$vendorActObj = ClassRegistry::init('VendorsActivation');
		$vendorActObj->recursive = -1;
		
		$data = $vendorActObj->query($query);
		return array($data,$prev_week,$next_week,$date_from.' to '.$date_to);
	}
	
	function verifyParams($params,$mapping){
		$ret = true;
		$msg = "";
		foreach($mapping['allParams']['param'] as $param){
			$field = trim($param['field']);
			if(!isset($params[$field])){
				$ret = false;
				$msg = $field . " not entered";
				break;
			}
			else if(strlen($params[$field]) >  $param['length'] || empty($params[$field])){
				$msg = $field . ": Enter valid value";
				$ret = false;
				break;
			}
			else if($field == 'Mobile' || $field == 'PNR'){
				if(strlen($params[$field]) != $param['length']){
					$msg = $field . ": Enter valid value";
					$ret = false;
					break;
				}
			}
		}
		if($ret){
			return array('status'=>'success');
		}
		else {
			return array('status'=>'failure','description' => $msg);	
		}
	}
	
	function mailToSuperDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		$superdist_shop = $this->getShopDataById($dist_shop['parent_id'],SUPER_DISTRIBUTOR);
		$this->General->sendMails($subject,$mail_body,array($superdist_shop['email']));
	}
	
	function mailToDistributor($retailer_id,$subject,$mail_body){
		$ret_shop = $this->getShopDataById($retailer_id,RETAILER);
		$dist_shop = $this->getShopDataById($ret_shop['parent_id'],DISTRIBUTOR);
		$this->General->sendMails($subject,$mail_body,array($dist_shop['email']));
	}
	
	function calculateCommission($id,$group_id,$product,$amount,$slab){
		$slabObj = ClassRegistry::init('SlabsUser');
		/*$shop = $this->getShopDataById($id,$group_id);
		$slab = $shop['slab_id'];*/

		$prodData =  $slabObj->query("SELECT percent FROM slabs_products INNER JOIN products ON (product_id = products.id) WHERE slab_id = $slab AND product_id = $product");
		$commission = $amount*$prodData['0']['slabs_products']['percent']/100;
		return $commission;
	}
	
	function getCommissionPercent($slab_id,$product){
		$slabObj = ClassRegistry::init('SlabsUser');
		$prodData =  $slabObj->query("SELECT percent FROM slabs_products WHERE slab_id = $slab_id AND product_id = $product");
		return $prodData['0']['slabs_products']['percent'];
	}
	
	function getAllCommissions($id,$group_id,$slab,$service=null){
		$qryStr = '';
		if(!is_null($service)) $qryStr = ' and products.service_id = '.$service.' ';
		$slabObj = ClassRegistry::init('SlabsUser');
		$table = array();
		/*if($group_id == SUPER_DISTRIBUTOR){
			$slab = SDIST_SLAB;			
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['SD'] = $prodData;
		}
		
		if($group_id <= DISTRIBUTOR){
			$slab = DIST_SLAB;
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['D'] = $prodData;
		}*/
		
		if($group_id <= RETAILER){
			//$slab = RET_SLAB;
			$prodData =  $slabObj->query("SELECT trim(products.name) as prodName, trim(slabs_products.percent) as prodPercent FROM slabs_products,products WHERE products.id = product_id AND slab_id = $slab AND products.to_show = 1 AND products.active = 1 ".$qryStr." ORDER BY products.id");
			$table['R'] = $prodData;
		}
		
		return $table;
	}
	
	function calculateTDS($commission){
		return ($commission*TDS_PERCENT/100);
	}
	
	function shopBalanceUpdate($price,$type,$id,$group_id){
		$userObj = ClassRegistry::init('User');
		
		if($group_id == SUPER_DISTRIBUTOR){
			$table = 'super_distributors';
		}
		else if($group_id == DISTRIBUTOR){
			$table = 'distributors';
		}
		else if($group_id == RETAILER){
			$table = 'retailers';
		}
		
		if($type == 'subtract'){
			$userObj->query("UPDATE $table SET balance = balance - $price WHERE id = $id");	
		}
		else if($type == 'add'){
			$userObj->query("UPDATE $table SET balance = balance + $price WHERE id = $id");
		}
		
		$bal = 	$userObj->query("SELECT balance FROM $table WHERE id = $id");
		return sprintf('%.2f', $bal['0'][$table]['balance']);
	}
	
	function getNewInvoice($shop_id,$from_id,$group_id,$invoice_type){
		$invoiceObj = ClassRegistry::init('Invoice');
		$this->data['Invoice']['ref_id'] = $shop_id;
		$this->data['Invoice']['from_id'] = $from_id;
		$this->data['Invoice']['group_id'] = $group_id;
		$this->data['Invoice']['invoice_type'] =  $invoice_type;
		$this->data['Invoice']['timestamp'] =  date('Y-m-d H:i:s');

		$invoiceObj->create();
		$invoiceObj->save($this->data);

		return $invoiceObj->id;
	}
	
	function addToInvoice($invoice_id,$trans_id){
		$invoiceObj = ClassRegistry::init('Invoice');
		$invoiceObj->query("INSERT INTO invoices_transactions (invoice_id,shoptransaction_id,timestamp) VALUES ($invoice_id,$trans_id,'".date('Y-m-d H:i:s')."')");
	}
	
	function getInvoiceAmount($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');
		
		$invoiceObj->recursive = -1;
		$invoice = $invoiceObj->findById($invoice_id);
		$id = $invoice['Invoice']['ref_id'];
		$group_id = $invoice['Invoice']['group_id'];
		$invoice_type = $invoice['Invoice']['invoice_type'];
		$data['date'] = date('Y-m-d', strtotime($invoice['Invoice']['timestamp']));
		
		$shop = $this->getShopDataById($id,$group_id);
		if($group_id != SUPER_DISTRIBUTOR){
			$parent = $shop['parent_id'];
			if($group_id == DISTRIBUTOR){
				$parentShop = $this->getShopDataById($parent,SUPER_DISTRIBUTOR);
			}
			else if($group_id == RETAILER){
				$parentShop = $this->getShopDataById($parent,DISTRIBUTOR);
			}
			$tds = $parentShop['tds_flag'];
		}
		else {
			$tds = 1;
		}
		
		$Totcommission = 0;
		$Totamount = 0;
		
		$transactions = $invoiceObj->query("SELECT ref2_id,shop_transactions.id FROM invoices_transactions,shop_transactions WHERE invoice_id = $invoice_id AND shop_transactions.id = invoices_transactions.shoptransaction_id");	
		if($invoice_type == DISTRIBUTOR_ACTIVATION){
			$coupons = array();
			foreach($transactions as $transaction){
				$coupons = array_merge($coupons,explode(",",$transaction['shop_transactions']['ref2_id']));
			}
			$couponObj = ClassRegistry::init('Coupon');
			$couponObj->recursive = -1;
			$prodData = $couponObj->find('all',array('fields' => array('GROUP_CONCAT(Coupon.serialNumber) as serials','COUNT(Coupon.id) as quantity','Product.id','Product.name','Product.price'), 'conditions' => array('Coupon.id in (' .implode(",",$coupons). ')' ),
					'joins' => array(
						array(
							'table' => 'products',
							'alias' => 'Product',
							'type' => 'inner',
							'conditions' => array('Coupon.product_id = Product.id')
						)
					),
					'group' => array('Coupon.product_id')
			));
			foreach($prodData as $prod){
				$Totamount += $prod['0']['quantity']*$prod['Product']['price'];
				$product = array();
				$product[$prod['Product']['id']] = $prod['0']['quantity'];
				$Totcommission += $this->calculateCommission($id,$group_id,$product,$date);
			}
		}
		else {
			$products = array();
			$prodids = array();
			foreach($transactions as $transaction){
				$prod_id = $transaction['shop_transactions']['ref2_id'];
				$transObj = ClassRegistry::init('ShopTransaction');
				$transObj->recursive = -1;
			
				if($group_id == RETAILER){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_RETAILER)));
				}
				else if($group_id == DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_DISTRIBUTOR)));
				}
				else if($group_id == SUPER_DISTRIBUTOR){
					$commission = $transObj->find('first',array('fields' => array('amount'), 'conditions' => array('ref2_id' => $transaction['shop_transactions']['id'], 'type' => COMMISSION_SUPERDISTRIBUTOR)));
				}
				$commission = $commission['ShopTransaction']['amount'];
				
				if(!isset($products[$prod_id])){
					$products[$prod_id]['quantity'] = 1;
					$products[$prod_id]['commission'] = $commission;
					$prodids[] = $prod_id;
				}
				else {
					$products[$prod_id]['quantity'] = $products[$prod_id]['quantity'] + 1;
					$products[$prod_id]['commission'] = $products[$prod_id]['commission'] + $commission;
				}
			}
			
			$prodObj = ClassRegistry::init('Product');
			$prodObj->recursive = -1;
			$prodData = $prodObj->find('all',array('fields' => array('Product.id','Product.name','Product.price'), 'conditions' => array('Product.id in (' .implode(",",$prodids). ')' )));	
			foreach($prodData as $prod){
				$Totamount += $products[$prod['Product']['id']]['quantity'] * $prod['Product']['price'];
				$Totcommission += $products[$prod['Product']['id']]['commission'];
			}
		}
		if($tds == 1){
			 $tds = $this->calculateTDS($Totcommission);
			 $Totcommission -= $tds;
		}
		
		$net = sprintf('%.2f',$Totamount - $Totcommission);
		return round($net);
	}
	
	function getOutstandingBalance($id,$type){
		$rcptObj = ClassRegistry::init('Receipt');
		$data = $rcptObj->find('all',array('fields' => array('min(Receipt.os_amount) as os'), 'conditions' => array('Receipt.receipt_ref_id' => $id,'Receipt.receipt_type' => $type)));
			
		if(empty($data['0']['0']['os'])){
			if($type == RECEIPT_INVOICE){
				$invObj = ClassRegistry::init('Invoice');
				$amount = $invObj->findById($id);
				$amount = $amount['Invoice']['amount'];
			}
			else if($type == RECEIPT_TOPUP){
				$transObj = ClassRegistry::init('ShopTransaction');
				$amount = $transObj->findById($id);
				$amount = $amount['ShopTransaction']['amount'];
			}
		}
		else {
			$amount = $data['0']['0']['os'];
		}
		return $amount;
	}
	
	function generateInvoice($id,$group_id,$type,$date=null){
		$transObj = ClassRegistry::init('ShopTransaction');
		$last_trans = $transObj->query("SELECT max(shoptransaction_id) as trans_id FROM invoices_transactions WHERE invoice_id IN (SELECT max(id) FROM invoices WHERE ref_id=$id AND group_id = $group_id AND invoice_type = $type)");
		$trans_id = $last_trans['0']['0']['trans_id'];
		
		if($group_id == RETAILER && $type == RETAILER_ACTIVATION)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == DISTRIBUTOR && $type == RETAILER_ACTIVATION)
		{	
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_DISTRIBUTOR;
		}
		else if($group_id == DISTRIBUTOR && $type == DISTRIBUTOR_ACTIVATION){
			$query = "SELECT st1.id FROM shop_transactions as st1 WHERE st1.type = $type AND st1.ref1_id = $id";
		}
		else if($group_id == SUPER_DISTRIBUTOR)
		{
			$query = "SELECT st1.id FROM shop_transactions as st1, shop_transactions as st2  WHERE st1.type = $type AND st1.id = st2.ref2_id AND st2.ref1_id = $id AND st2.type = " . COMMISSION_SUPERDISTRIBUTOR;
		}
		
		if(!empty($trans_id)){
			$query .= " AND st1.id > $trans_id";
		}
		if($date != null){
			$query .= " AND Date(st1.timestamp) = '$date'";
		}
		$data = $transObj->query($query);
		if(!empty($data)){
			if($group_id == SUPER_DISTRIBUTOR){
				$parent = null;
			}
			else if($group_id == DISTRIBUTOR || $group_id == RETAILER){
				$sData = $this->getShopDataById($id,$group_id);
				$parent = $sData['parent_id'];
			}
			$new_invoice = $this->getNewInvoice($id,$parent,$group_id,$type);
			foreach($data as $trans){
				$this->addToInvoice($new_invoice,$trans['st1']['id']);
			}
			$amount = $this->getInvoiceAmount($new_invoice,date('Y-m-d'));
			$number = $this->getInvoiceNumber($new_invoice,date('Y-m-d'));
			$transObj->query("UPDATE invoices SET amount = $amount,invoice_number='$number' WHERE id = $new_invoice");
		}
	}
	
	
	function getShopDataById($id,$group_id){
		
		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['SuperDistributor'];
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Distributor'];
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->recursive = -1;
			$bal = $userObj->query("select * from retailers where id=$id");
			return $bal['0']['retailers'];
			//$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			//return $bal['Retailer'];
		}else if($group_id == RELATIONSHIP_MANAGER){
			$userObj = ClassRegistry::init('Rm');
			$userObj->recursive = -1;
			$bal = $userObj->find('first',array('conditions' => array('id' => $id)));
			return $bal['Rm'];
		}
	}
	
	function disabledApps($uid){
			$userObj = ClassRegistry::init('Retailer');
			$userObj->recursive = -1;
			$dis = $userObj->query('select service_id from services_disabled where user_id = '.$uid);
			$str = '';
			foreach($dis as $d)
			$str .= $d['services_disabled']['service_id'].",";
			
			if($str == '')return $str;
			else return substr($str,0,-1);
	}
	
	function shortSerials($serials){
		$serials = explode(",",$serials);
		sort($serials);
		$serial_array = array();
		
		$min = $serials[0];
		$last = $serials[0];
		$serial_array[$min] = 1;
		foreach($serials as $serial){
			if($serial == $last + 1){
				$serial_array[$min] = $serial_array[$min] + 1;
				$last = $serial;
			}
			else if($serial != $min){
				$min = $serial;
				$last = $serial;
				$serial_array[$min] = 1;
			}
		}
		$strings = array();
		foreach($serial_array as $key=>$value){
			$string = $key;
			if($value > 1){
				$string .= "-" . ($key + $value - 1);
			}
			$strings[] = $string;
		}
		return implode(", ", $strings);
	}
	
	function getInvoiceNumber($invoice_id,$date){
		$invoiceObj = ClassRegistry::init('Invoice');
		$data = $invoiceObj->findById($invoice_id);
		if($data['Invoice']['group_id'] == SUPER_DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".SUPER_DISTRIBUTOR." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == DISTRIBUTOR){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".DISTRIBUTOR." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		else if($data['Invoice']['group_id'] == RETAILER){
			$shop = $this->getShopDataById($data['Invoice']['ref_id'],$data['Invoice']['group_id']);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
			$ret = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and id < $invoice_id");
			$ret_to = $invoiceObj->query("SELECT count(*) as counts FROM invoices WHERE group_id = ".RETAILER." and from_id = ".$data['Invoice']['from_id']." and ref_id = ". $data['Invoice']['ref_id']." and id < $invoice_id");
		}
		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		$number1 = $ret_to['0']['0']['counts'] + 1;
		return "INV-" . $suffix1 . "/" . $suffix2 . "/" . date('Y',strtotime($date)). "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}
	
	function getTopUpReceiptNumber($transaction_id){
		$transObj = ClassRegistry::init('ShopTransaction');
		$data = $transObj->findById($transaction_id);
		if($data['ShopTransaction']['type'] == ADMIN_TRANSFER){
			$suffix1 = "ST";
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],SUPER_DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);	
		}
		else if($data['ShopTransaction']['type'] == SDIST_DIST_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],DISTRIBUTOR);
			$comp = explode(" ",$shop['company']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['ShopTransaction']['type'] == DIST_RETL_BALANCE_TRANSFER){
			$shop = $this->getShopDataById($data['ShopTransaction']['ref2_id'],RETAILER);
			$comp = explode(" ",$shop['shopname']);
			$parent_shop = $this->getShopDataById($shop['parent_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		$ret = $transObj->query("SELECT count(*) as counts FROM shop_transactions WHERE type = ".$data['ShopTransaction']['type']." and ref1_id = ".$data['ShopTransaction']['ref1_id']." and id < $transaction_id");
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "TP-". $suffix1 . "/". $suffix2 . "/" . sprintf('%06d', $number);
	}
	
	function getCreditDebitNumber($to_id,$from_id,$to_groupid,$type,$id=null){
		$invoiceObj = ClassRegistry::init('Invoice');
		$query = "";
		if($id != null){
			$query = " AND id < $id";
		}
		if(empty($from_id)) $from = " is null";
		else $from = " = $from_id";
		
		$ret = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE to_groupid = $to_groupid AND from_id $from AND to_id = $to_id AND type=$type $query");
		$retAll = $invoiceObj->query("SELECT count(*) as counts FROM shop_creditdebit WHERE from_id $from AND type=$type $query");
		
		$shop = $this->getShopDataById($to_id,$to_groupid);
		if(!empty($from_id)){
			$parent_shop = $this->getShopDataById($from_id,$to_groupid-1);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		
		if(isset($shop['company']))
			$comp = explode(" ",$shop['company']);
		else 
			$comp = explode(" ",$shop['shopname']);

		$suffix1 = "ST";
		$suffix2 = "";
		foreach($comp as $str){
			$suffix2 .= substr($str,0,1);
		}
		$suffix2 = strtoupper($suffix2);
		if(isset($parent_comp)){
			$suffix1 = "";
			foreach($parent_comp as $str){
				$suffix1 .= substr($str,0,1);
			}
			$suffix1 = strtoupper($suffix1);
		}
		$number = $retAll['0']['0']['counts'] + 1;
		$number1 = $ret['0']['0']['counts'] + 1;
		if($type == 0) $suffix0 = "CN";
		else if($type == 1) $suffix0 = "DN";
		return "$suffix0-" . $suffix1 . "/" . $suffix2 . "/" . sprintf('%04d', $number) . "/" . sprintf('%03d', $number1);
	}
	
	function getOtherProds($prodId){
		if($prodId == 2 || $prodId == 26) $prodId1 = "2,26";
		else if($prodId == 3 || $prodId == 34) $prodId1 = "3,34";
		else if($prodId == 9 || $prodId == 27) $prodId1 = "9,27";
		else if($prodId == 10 || $prodId == 27) $prodId1 = "10,27";
		else if($prodId == 11 || $prodId == 29) $prodId1 = "11,29";
		else if($prodId == 12 || $prodId == 28) $prodId1 = "12,28";
		else if($prodId == 30 || $prodId == 31) $prodId1 = "30,31";
		else $prodId1 = $prodId;
		
		return $prodId1;
	}
	
	function getActiveVendor($prodId,$mobile=null,$amount=null){
		$success = true;
		$invoiceObj = ClassRegistry::init('Invoice');
			
		if($mobile != null){
			$success = false;
			$other = false;
			
			$prod = $invoiceObj->query("SELECT product_id FROM mobile_numbering,mobile_numbering_service WHERE mobile_numbering.number='".substr($mobile,0,4)."' AND mobile_numbering.operator = mobile_numbering_service.opr_code");
			if(!empty($prod)){
				$prod = $prod['0']['mobile_numbering_service']['product_id'];
				$prod_multi = explode(",",$this->getOtherProds($prod));
			
				if(!in_array($prodId,$prod_multi)){
					$other = true;
					$mail_body = "Mobile: $mobile, Product: $prodId";
					//$this->General->sendMails("Pay1: Operator not matching",$mail_body,array('ashish@mindsarray.com'));
				}
				else {
					$success = true;
				}
			}
			else {
				$mail_body = "Mobile: $mobile, Product: $prodId";
				$this->General->sendMails("Pay1: Operator not found",$mail_body,array('ashish@mindsarray.com'));
				$other = true;
			}
			
			if($other){
				$id = $invoiceObj->query("SELECT vendors_commissions.vendor_id, vendors.shortForm, products.oprDown, products.name, products.min,products.max,products.invalid FROM vendors_commissions join vendors on (vendors_commissions.vendor_id=vendors.id) join products on (vendors_commissions.product_id=products.id) WHERE vendors_commissions.product_id=$prodId AND vendors_commissions.oprDown = 0 AND vendors.active_flag = 1 AND vendors.update_flag = 1 order by vendors_commissions.discount_commission desc limit 1");
				if(!empty($id)){
					return array('status'=>'success','code'=>'','vendorId'=>$id['0']['vendors_commissions']['vendor_id'],'vendorShortform'=>$id['0']['vendors']['shortForm'],'oprDown'=>$id['0']['products']['oprDown'],'name'=>$id['0']['products']['name'],'min'=>$id['0']['products']['min'],'max'=>$id['0']['products']['max'],'invalid'=>$id['0']['products']['invalid']);
				}
				else {
					$id = $invoiceObj->query("SELECT vendors_commissions.vendor_id, vendors.shortForm, products.oprDown, products.name,products.min,products.max,products.invalid FROM vendors_commissions join vendors on (vendors_commissions.vendor_id=vendors.id) join products on (vendors_commissions.product_id=products.id) WHERE vendors_commissions.product_id=$prodId AND vendors_commissions.oprDown = 0 AND vendors_commissions.vendor_id != 5");
					if(!empty($id)){
						return array('status'=>'success','code'=>'','vendorId'=>$id['0']['vendors_commissions']['vendor_id'],'vendorShortform'=>$id['0']['vendors']['shortForm'],'oprDown'=>$id['0']['products']['oprDown'],'name'=>$id['0']['products']['name'],'min'=>$id['0']['products']['min'],'max'=>$id['0']['products']['max'],'invalid'=>$id['0']['products']['invalid']);
					}
					else {
						$success = true;
					}
				}	
			}
		}
		
		if($success){
			$id = $invoiceObj->query("SELECT vendors_commissions.vendor_id, vendors.shortForm, products.oprDown, products.name,products.min,products.max,products.invalid FROM vendors_commissions join vendors on (vendors_commissions.vendor_id=vendors.id) join products on (vendors_commissions.product_id=products.id) WHERE vendors_commissions.product_id=$prodId AND vendors_commissions.oprDown = 0 ORDER by vendors_commissions.active desc,vendors_commissions.discount_commission desc limit 1");
			
			if(empty($id)) return array('status'=>'failure','code'=>'','description'=>'');
			else return array('status'=>'success','code'=>'','vendorId'=>$id['0']['vendors_commissions']['vendor_id'],'vendorShortform'=>$id['0']['vendors']['shortForm'],'oprDown'=>$id['0']['products']['oprDown'],'name'=>$id['0']['products']['name'],'min'=>$id['0']['products']['min'],'max'=>$id['0']['products']['max'],'invalid'=>$id['0']['products']['invalid']);
		}
	}
	
	function getNextVendor($prodId,$transId){
		$invoiceObj = ClassRegistry::init('Invoice');
		
		$already_done = $invoiceObj->query("SELECT GROUP_CONCAT(service_vendor_id) as vendors FROM vendors_messages WHERE vendors_messages.shop_tran_id = '$transId'");
		$already_done = $already_done['0']['0']['vendors'];
		$already_done = array_unique(explode(",",$already_done));
		
		$id = $invoiceObj->query("SELECT vendor_id,shortForm,discount_commission FROM vendors_commissions,vendors WHERE vendors_commissions.product_id=$prodId AND vendors_commissions.vendor_id=vendors.id AND vendors_commissions.oprDown = 0 AND vendors_commissions.vendor_id NOT IN (".implode(",",$already_done).") order by discount_commission desc");
		
		if(empty($id)) return array('status'=>'failure','code'=>'','description'=>'');
		else return array('status'=>'success','vendors'=>$id);
	}
	
	function checkPossibility($prodId,$mobileNo,$amount,$power,$param=null,$special=null){
		$sms = null;
		if(!empty($power)) return $sms;
		$invoiceObj = ClassRegistry::init('Invoice');
		
		if($amount == ''){
			$prodData = $invoiceObj->query("SELECT price FROM products_info WHERE product_id = $prodId");
			$amount = $prodData['0']['products_info']['price'];
		}
		
		$prodId1 = $this->getOtherProds($prodId);
		
		$mins = TIME_DURATION;
		$later = date('Y-m-d H:i:s',strtotime('- '.$mins.' minutes'));
			
		if($param == null || empty($param))
			$already_done = $invoiceObj->query("SELECT status FROM vendors_activations WHERE product_id in ($prodId1) AND mobile='$mobileNo' AND amount='$amount' AND date = '".date('Y-m-d')."' AND timestamp > '$later' AND status != ".TRANS_REVERSE." ORDER BY id desc LIMIT 1");
		else 
			$already_done = $invoiceObj->query("SELECT status FROM vendors_activations WHERE product_id in ($prodId1) AND param='$param' AND amount='$amount' AND date = '".date('Y-m-d')."' AND timestamp > '$later' AND status != ".TRANS_REVERSE." ORDER BY id desc LIMIT 1");
		
		if(!empty($already_done)){
				
			if($param == null){
				$msg = "*$prodId*$mobileNo*$amount";
				$sms = "hold: Repeat on $mobileNo of Rs$amount within ".($mins/60)." hours";
			}
			else {
				$msg = "*$prodId*$param*$mobileNo*$amount";
				$sms = "hold: Repeat on $param of Rs$amount within ".($mins/60)." hours";
			}
			
			if(!empty($special) && $special == 1) $msg = $msg . "#";
			
			$sender = $_SESSION['Auth']['mobile'];
			$this->addRepeatTransaction($msg,$sender,2);
			$mail_body = "Request is on hold, Retailer $sender has done duplicate transaction within $mins mins";
			$mail_body .= "<br/>Customer Mobile: $mobileNo/$param, Amount: $amount";
			$this->General->sendMails("Pay1: Same Transaction within $mins mins",$mail_body,array('chirutha@mindsarray.com'));
			
			$added = $invoiceObj->query("SELECT id FROM repeated_transactions WHERE msg = '$msg' AND sender='$sender' AND type=2 ORDER BY id desc LIMIT 1");
			if(!empty($added)){
				$id = $added['0']['repeated_transactions']['id'];
				$id = "1" . sprintf('%04d', $id);
				$sms .= "\nTo continue send: #$id via misscall service or SMS to 07666888676";
				$sms .= "\nIgnore if don't want to continue";
			}
		}
		
		return $sms;
	}
	
	function addRepeatTransaction($msg,$sender,$type,$added_by=null){
		$invoiceObj = ClassRegistry::init('Invoice');
		
		if($added_by == null){
			$invoiceObj->query("INSERT INTO repeated_transactions (sender,msg,type,timestamp) VALUES ('$sender','".addslashes($msg)."',$type,'".date('Y-m-d H:i:s')."')");	
		}
		else {
			$invoiceObj->query("INSERT INTO repeated_transactions (sender,msg,send_flag,type,added_by,processed_by,timestamp) VALUES ('$sender','".addslashes($msg)."',1,$type,$added_by,$added_by,'".date('Y-m-d H:i:s')."')");
		}
	}
	
	function getReceiptNumber($id){
		$recObj = ClassRegistry::init('Receipt');
		$data = $recObj->findById($id);
		if($data['Receipt']['receipt_type'] == RECEIPT_INVOICE){
			$suffix1 = "INV";
		}
		else if($data['Receipt']['receipt_type'] == RECEIPT_TOPUP){
			$suffix1 = "TP";
		}
		
		if($data['Receipt']['group_id'] == SUPER_DISTRIBUTOR){
			$suffix2 = "ST";
		}
		else if($data['Receipt']['group_id'] == DISTRIBUTOR){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],SUPER_DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		else if($data['Receipt']['group_id'] == RETAILER){
			$parent_shop = $this->getShopDataById($data['Receipt']['shop_from_id'],DISTRIBUTOR);
			$parent_comp = explode(" ",$parent_shop['company']);
		}
		
		$ret = $recObj->query("SELECT count(*) as counts FROM receipts WHERE shop_from_id = ".$data['Receipt']['shop_from_id']." and group_id = " . $data['Receipt']['group_id'] . " and id < $id");
		if(isset($parent_comp)){
			$suffix2 = "";
			foreach($parent_comp as $str){
				$suffix2 .= substr($str,0,1);
			}
			$suffix2 = strtoupper($suffix2);
		}
		$number = $ret['0']['0']['counts'] + 1;
		return "R/". $suffix1 . "/". $suffix2 . "/" . sprintf('%03d', $number);
	}
	
	function getShopData($user_id,$group_id=null){
		if($group_id == null){
			$userData = $this->General->getUserDataFromId($user_id);
			$group_id = $userData['group_id'];
		}
		if($group_id == SUPER_DISTRIBUTOR){
			$userObj = ClassRegistry::init('SuperDistributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['SuperDistributor'];
		}
		else if($group_id == DISTRIBUTOR){
			$userObj = ClassRegistry::init('Distributor');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
			return $bal['Distributor'];
		}
		else if($group_id == RETAILER){
			$userObj = ClassRegistry::init('Retailer');
			$bal = $userObj->find('first',array('conditions' => array('user_id' => $user_id)));
                        $info = $bal['Retailer'];
                        if(!empty($bal)){
                            $partnerInfo = $userObj->query("SELECT partners.id , partners.acc_id FROM partners WHERE retailer_id = ".$bal['Retailer']['id'] );
                            if(!empty($partnerInfo)){
                                $info = $bal['Retailer'];
                                $info['is_partner'] = 'true';
                                $info['partner_id'] = $partnerInfo[0]['partners']['id'];
                                $info['partner_acc_id'] = $partnerInfo[0]['partners']['acc_id'];
                            }
                        }
			return $info;
		}else if($group_id == RELATIONSHIP_MANAGER){
			$userObj = ClassRegistry::init('Rm');
            $bal = $userObj->find('first',array('conditions' => array('Rm.user_id' => $user_id)));
            return $bal['Rm'];
		}
	}
	
	function getVendor($id){
		$retailObj = ClassRegistry::init('Retailer');
		$ret = $retailObj->query("SELECT company FROM vendors where id = $id");
		return $ret['0']['vendors']['company'];
	}
	
	function getLastTransactions($date,$page=1,$service = null,$date2=null,$itemsPerPage=0,$retailerId=null,$operatorId=0){
		if(empty($date)){
			$date = date('Y-m-d');
		}
		else $date = date('Y-m-d',strtotime($date));

		if(empty($date2)){
			$date2 = $date;
		}
		else $date2 = date('Y-m-d',strtotime($date2));

		$next_day = date('Y-m-d',strtotime($date . ' + 1 day'));
		if($next_day > date('Y-m-d')){
			$next_day = '';
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));

		if($itemsPerPage == 0){
			$itemsPerPage = PAGE_LIMIT;
		}

		$limit = ($page-1)*$itemsPerPage > 0 ? ($page-1)*$itemsPerPage : 0;



		$retailObj = ClassRegistry::init('Retailer');
                if(is_null($retailerId)){
                    $retailer = $_SESSION['Auth']['id'];
                }else{
                    $retailer = $retailerId;
                }
                if(empty($operatorId)){
                    $operatorIdQry = "";
                }else{
                    $operatorIdQry = "AND vendors_activations.product_id = ".$operatorId;
                }
		
		if($service == null){
			
                        $ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations , shop_transactions where shop_transactions.id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' $operatorIdQry  order by vendors_activations.id desc LIMIT $limit," . $itemsPerPage);
                        $ret_cnt_res = $retailObj->query("SELECT count(*) as cnt FROM products,services,vendors_activations , shop_transactions where shop_transactions.id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' $operatorIdQry ");
                        $ret_count = $ret_cnt_res[0][0]['cnt'];
                         
                        
                }else {
			$ret = $retailObj->query("SELECT opening_closing.opening,opening_closing.closing,products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations , opening_closing where opening_closing.shop_transaction_id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' AND vendors_activations.retailer_id =  opening_closing.shop_id AND opening_closing.shop_id = $retailer AND  opening_closing.group_id = ".RETAILER."  $operatorIdQry  order by vendors_activations.id desc,opening_closing.id desc LIMIT $limit," . $itemsPerPage);
			$ret_cnt_res = $retailObj->query("SELECT count(*) as cnt FROM products,services,vendors_activations , opening_closing where opening_closing.shop_transaction_id = vendors_activations.shop_transaction_id AND products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.date>='$date' AND vendors_activations.date<='$date2' AND vendors_activations.retailer_id =  opening_closing.shop_id AND opening_closing.shop_id = $retailer AND  opening_closing.group_id = ".RETAILER."  $operatorIdQry  ");
			$ret_count = $ret_cnt_res[0][0]['cnt'];
		}
		$more = 0;
		if(count($ret) == $itemsPerPage){
			$more = 1;
		}
               
		return array('ret' => $ret,'today' => $date,'prev' => $prev_day,'next' => $next_day,'more' => $more,'total_count' => $ret_count);
	}
	
	function lastten($service){
		$retailObj = ClassRegistry::init('Retailer');
		$retailer = $_SESSION['Auth']['id'];
		$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id =  $retailer AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' order by vendors_activations.id desc LIMIT 0,10");
		return array('ret' => $ret);
	}
	
	function mobileTransactions($mobile,$service=null){
		if($service != '2')$mobile = substr($mobile,-10);
		$retailObj = ClassRegistry::init('Retailer');
		$retailer = $_SESSION['Auth']['id'];
		$limit = 0;
		if(empty($service)){
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.mobile = '$mobile' AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' order by vendors_activations.id desc LIMIT $limit," . PAGE_LIMIT);
		}
		else {
			$q = 'vendors_activations.mobile';
			if($service == '2')
			$q = 'vendors_activations.param';
			
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND ".$q." = '$mobile' AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' order by vendors_activations.id desc LIMIT $limit," . PAGE_LIMIT);
		}
		return array('ret' => $ret);
	}
	
	function searchTransactions($mob_subid,$retailer_id){
		$retailObj = ClassRegistry::init('Retailer');
		$retailer = $_SESSION['Auth']['id'];
		$ret = $retailObj->query("SELECT products.name,services.id,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, vendors_activations.timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer_id AND (vendors_activations.mobile = '$mob_subid' OR vendors_activations.param = '$mob_subid') AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' order by vendors_activations.id desc LIMIT 2");
		
		return $ret;
	}
	
	function reversalTransactions($date,$service = null){
		if(empty($date)){
			$date = date('Y-m-d');
		}
		
		$next_day = date('Y-m-d',strtotime($date.' + 1 day'));
		if($next_day > date('Y-m-d')){
			$next_day = '';
		}
		$prev_day = date('Y-m-d',strtotime($date . ' - 1 day'));
		
		$limit = ($page-1)*PAGE_LIMIT;
		
		$retailObj = ClassRegistry::init('Retailer');
		$retailer = $_SESSION['Auth']['id'];
		$type_sql = " AND (vendors_activations.status = ".TRANS_REVERSE." OR vendors_activations.status = ".TRANS_REVERSE_PENDING." OR vendors_activations.status = ".TRANS_REVERSE_DECLINE.")";
		
		if($service == null || empty($service)){
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id =  $retailer AND vendors_activations.date='$date' $type_sql order by vendors_activations.id desc");
		}
		else {
			$ret = $retailObj->query("SELECT products.name,services.name,vendors_activations.ref_code,vendors_activations.id,vendors_activations.param,vendors_activations.mobile,vendors_activations.amount,vendors_activations.status, trim(vendors_activations.timestamp) as timestamp,vendors_activations.product_id FROM products,services,vendors_activations where products.service_id = services.id AND products.service_id = $service AND products.id = vendors_activations.product_id AND vendors_activations.retailer_id = $retailer AND vendors_activations.date='$date' $type_sql order by vendors_activations.id desc");
		}
		return array('ret' => $ret,'today' => $date,'prev' => $prev_day,'next' => $next_day);
	}
	
	function updateSlab($slab_id,$shop_id,$group_id){
		$slabObj = ClassRegistry::init('SlabsUser');
		$slabObj->create();
		$slabData['SlabsUser']['slab_id'] = $slab_id;
		$slabData['SlabsUser']['shop_id'] = $shop_id;
		$slabData['SlabsUser']['group_id'] = $group_id;
		$slabData['SlabsUser']['timestamp'] = date('Y-m-d H:i:s');
		$slabObj->save($slabData);
	}
	
	function addNewVendorRetailer($vendor_id,$retailer_code){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->query("INSERT INTO vendors_retailers (vendor_id,retailer_code) VALUES ($vendor_id,'$retailer_code')");
	}
	
	function apiAccessHashCheck($params,$partnerModel){
		$pass = $partnerModel['Partner']['password'];
		$hashGen = sha1($params['partner_id'].$params['trans_id'].$pass);//.$params['info_json']
		if(!empty ($params['hash_code']) && $params['hash_code'] == $hashGen){
			return true;
		}else{
			return false;
		}
	}

	function apiAccessIPCheck($partner){
		$result = array();
		$ipAddrsStr = $partner['Partner']['ip_addrs'];// get partener's valid ip list
		$ipArr = explode(",", $ipAddrsStr);
		$ip = $_SERVER['REMOTE_ADDR'];// get client ip
		if(in_array($ip, $ipArr)){//check client ip is in partner's ips list
			$result = array('access'=>true);
		}else{
			$result = array('access'=>false);
		}
		return $result;
	}
	function apiAccessPartnerOperatorCheck($partnerAccno,$operatorId){//check operator is open for a partner (only for recharge api)
		$result = array();
		$retailObj = ClassRegistry::init('Retailer');
		$result = $retailObj->query("select * from  partner_operator_status where partner_acc_no = '".$partnerAccno."' and operator_id = '$operatorId'");
                
		if(empty($result) || $result[0]['partner_operator_status']['status'] == 0){//check client ip is in partner's ips list
			$result = array('access'=>false);
		}else{
			$result = array('access'=>true);
		}
		return $result;
	}
	function partnerSessionInit( $type , $model ){ //$id ,
		// type -> user , partner , retailer , distributor , rm
		// $model -> Model Object of  ( user , partner , retailer , distributor , rm )
		$data = array();
		if($type == "user"){
			$data = $this->User->query("SELECT id FROM users WHERE mobile = '".$params['mobile']."' AND password='$password'");
		}else if($type == "partner"){
			$partnerModel = $model;
			$retailerRegObj = ClassRegistry::init('Retailer');
			$retailerModel = $retailerRegObj->findById($partnerModel['Partner']['retailer_id']);
			if(!empty($retailerModel)){
				$data['user_id'] = $retailerModel['Retailer']['user_id'];
				$data['group_id'] = RETAILER;
			}
		}

		if(empty($data)){// Invalid Partner ( can't find retailer account for partner ).
			return false;
		}else{
			$info = $this->getShopData($data['user_id'],$data['group_id']);
			$info['User']['group_id'] = $data['group_id'];
			$info['User']['id'] = $data['user_id'];
			//$this->SessionComponent->write('Auth',$info);
			$_SESSION['Auth'] = $info;
			if(!empty($_SESSION['Auth']['id'])){
				return true;
			}else{
				return false;
			}
		}
	}
}
?>
