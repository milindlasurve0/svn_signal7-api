<?php

class GeneralComponent extends Object {
	var $components = array('Auth','Shop');
        private static $emailTemplates = array(
            "emailToAdminOnRmRegistration"=>array(  
                                                    "subject" => "New RelationShip Manager ( R M ) Created in Pay1",
                                                    "body" => "New RelationShip Manager ( R M ) Created in Pay1 <br/>
                                                               Mobile No: @rm_mobile@ <br/>
                                                               RM Name: @rm_name@ <br/>
                                                               Distributor: @distributor_company@ <br/>"
                                                 ),
            "emailToAdminOnRmAddWithDistributor"=>array(  
                                                    "subject" => "RM appointed to a distributor",
                                                    "body" => "RM Name: @rm_name@ <br/>
                                                               Super Distributor: @super_distributor_company@ <br/>
                                                               Distributor: @distributor_company@"
                                                 )
        );
        private static $smsTemplates = array(
            "smsToRMOnRmRegistration"=>array(  
                                                    "msg" =>  "Congrats!!\nYou have become RM of Pay1."
                                             ),
            "smsToSuperDistOnRmRegistration"=>array(  
                                                    "msg" => "New RM created\nname: @rm_name@\nmobile: @rm_mobile@"
                                                 ),
            "smsToDistributorOnRmAddWithDistributor"=>array(  
                                                    "msg" => "Pay1 have assigned a new RM @rm_name@ (@rm_mobile@) to you\nPlease contact him for any queries"
                                                 ),
            "smsToDistributorOnOldNoAboutMoblieNoChange"=>array(  
                                                    "msg" => "Dear Distributor,
Your number has been shifted
from @dist_old_mobile@ to @dist_new_mobile@ ")
                                                 
        );
        
        function enCrypt($data = null) {
		if ($data != null) {
			// Make an encryption resource using a cipher
			$td = mcrypt_module_open('cast-256', '', 'ecb', '');
			// Create and encryption vector based on the $td size and random
			$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
			// Initialize the module using the resource, my key and the string vector
			mcrypt_generic_init($td, encKey, $iv);
			// Encrypt the data using the $td resource
			$encrypted_data = mcrypt_generic($td, $data);
			// Encode in base64 for DB storage
			$encoded = base64_encode($encrypted_data);
			// Make sure the encryption modules get un-loaded
			if (!mcrypt_generic_deinit($td) || !mcrypt_module_close($td)) {
				$encoded = false;
			}
		} else {
			$encoded = false;
		}
		return $encoded;
	}
	/**
	 * This function will de-crypt the string that is passed to it
	 *
	 * @param String $data The string to be encrypted.
	 * @return String Returns the encrypted string or false
	 */
	function deCrypt($data = null) {
		if ($data != null) {
			// The reverse of encrypt.  See that function for details
			$data = (string) base64_decode(trim($data));
			$td = mcrypt_module_open('cast-256', '', 'ecb', '');
			$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
			mcrypt_generic_init($td, encKey, $iv);
			$data = (string) trim(mdecrypt_generic($td, $data));
			// Make sure the encryption modules get un-loaded
			if (!mcrypt_generic_deinit($td) || !mcrypt_module_close($td)) {
				$data = false;
			}
		} else {
			$data = false;
		}
		return $data;
	}

	function makeUrl($text){
		$text = preg_replace('/[^a-zA-Z0-9 -]/s', '', $text);
		$text = str_replace('  ', ' ', $text);
		$text = str_replace(' ','-',strtolower($text));
		return $text;
	}

	function makeCamelcase($str)
	{
		$str = trim($str);
		$str = ucwords(strtolower($str));

		return $str;
	}
	function dateFormat($date){
		return date('jS M, Y',strtotime($date));
	}

	function dateTimeFormat($date){
		return date('jS M, Y g:i A',strtotime($date));
	}

	function nameToUrl($name) {
		return $this->makeUrl($name);
	}

	function urlToName($url){
		$name = str_replace('-',' ',$url);
		return $this->makeCamelcase($name);
	}

	function generatePassword($characters,$mobile=null){
		$code = '';

		if($mobile == null){
			$possible = '0123456789';
			$i = 0;
			while ($i < $characters) {
				$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
				$i++;
			}
		}
		else {
			$code =  substr($mobile, -2, 1) .  substr($mobile, -4, 1) . substr($mobile, -6, 1) . substr($mobile, -8, 1);
		}

		return $code;
	}

	function sendPassword($mobile,$password,$flag,$missCall=null){
		if($flag == '1') {
			$alias = "SUBSCRIBE_PASSWORD";
			$vars[] = $password;
		}
		else if($flag == '0'){
			$alias = "FORGOT_PASSWORD";
			$vars[] = $password;
		}
		$message = $this->createMessage($alias,$vars);
		if($missCall == null) {
			$this->sendMessage('',$mobile,$message,'template');
		}
		else if($missCall != null){
			return $message;
		}
	}


	function balanceUpdate($price,$type,$userId=null){
		if($userId == null){
			$userId = $_SESSION['Auth']['User']['id'];
		}
		$bal = $this->getBalance($userId);
		$userObj = ClassRegistry::init('User');

		if($type == 'subtract'){
			$userObj->query("UPDATE users set balance = balance - $price where id = $userId");
			$balance = $bal - $price;
		}
		else if($type == 'add'){
			$userObj->query("UPDATE users set balance = balance + $price where id = $userId");
			$balance = $bal + $price;
		}

		return $balance;
	}

	function getBalance($userId){
		$userObj = ClassRegistry::init('User');
		$userObj->recursive = -1;
		$bal = $userObj->findById($userId);
		return $bal['User']['balance'];
	}

	function checkIfUserExists($mobile){
		$userObj = ClassRegistry::init('User');

		$count = $userObj->find('count',array('conditions' => array('User.mobile' => $mobile)));

		if($count > 0) return true;
		return false;
	}

	function checkIfSalesmanExists($mobile){
		$userObj = ClassRegistry::init('Salesman');
		$count = $userObj->find('count',array('conditions' => array('Salesman.mobile' => $mobile)));

		if($count > 0) return true;
		return false;
	}
        function checkIfRmExists($mobile){
		$userObj = ClassRegistry::init('Rm');
		$count = $userObj->find('count',array('conditions' => array('Rm.mobile' => $mobile)));

		if($count > 0) return true;
		return false;
	}
			
	function getGroupId($name,$mobile=null){
		$groupObj = ClassRegistry::init('Group');
		$groupObj->recursive = -1;
		$groupId = $groupObj->find('first', array('fields' => array('Group.id'),'conditions' => array('Group.name' => $name)));

		$mobileNums = array('9892471157','9892609560','9819852204','9820595052','9004387418','9819032643');
		if($name != 'admin' && in_array($mobile,$mobileNums)){
			return $this->getGroupId('admin');
		}
		return $groupId['Group']['id'];
	}

	function getPrice($price){
		$str = '';
		if($price >= 1)
		$str = '<span><img class="rupee1" src="/img/rs.gif"></span>'.$price;
		else
		$str = 100*$price . ' paise';
		return $str;
	}

	function countChars($message){
		return strlen($message);
	}

	function getSMSNums($charcount){
		//$charcount = $charcount - DEFAULT_MESSAGE_LENGTH + ADSPACE;
		$num = ceil($charcount/DEFAULT_MESSAGE_LENGTH);
		return $num;
	}

	function getCharge($num_messages){
		return (EACH_MESSAGE_COST/100)*$num_messages;
	}


	function getTotalMessageAmount($message,$num){
		$chars = $this->countChars($message);
		$num_messages = $this->getSMSNums($chars);
		$amount = $this->getCharge($num_messages)*$num;
		return $amount;
	}

	function getMessageCharge($id){
		$msgObj = ClassRegistry::init('Message');
		$msgObj->recursive = -1;
		$msgData = $msgObj->find('first', array('fields' => array('Message.charCount','Message.content'),'conditions' => array('Message.id' => $id)));

		if($msgData['Message']['charCount'] == null){
			$chars = $this->countChars($msgData['Message']['content']);
		}
		else {
			$chars = $msgData['Message']['charCount'];
		}

		$num_messages = $this->getSMSNums($chars);
		$amount = $this->getCharge($num_messages);

		return $amount;
	}

	function addAsynchronousCall($random,$controller,$action,$params){
		$userObj = ClassRegistry::init('User');
		$userObj->query("INSERT INTO asynchronous_calls (random_id,controller,action,params) VALUES ($random,'".$controller."','".$action."','".addslashes(json_encode($params))."')");
	}


	function registerUser($mobile_number,$reg_type,$group=null){
		$userObj = ClassRegistry::init('User');
		$this->data = null;
		$this->data['User']['mobile'] = $mobile_number;
		$password = $this->generatePassword(4); //generate 4 character password
		$this->data['User']['password'] = $this->Auth->password($password); //encrypted password using hash salt

		$this->data['User']['balance'] = 0;
		if($group == null)
		$this->data['User']['group_id'] = $this->getGroupId('member',$this->data['User']['mobile']);
		else
		$this->data['User']['group_id'] = $group;
		$this->data['User']['dob'] = '0000-00-00';
		$this->data['User']['gender'] = 0; //0 means male
		$this->data['User']['passflag'] = 0;
		$this->data['User']['login_count'] = 0;

		if($reg_type == ONLINE_REG)
		$this->data['User']['verify'] = 0;
		else if($reg_type == MISSCALL_REG)
		$this->data['User']['verify'] = -1;
		else if($reg_type == RETAILER_REG)
		$this->data['User']['verify'] = -2;
		else if($reg_type == REF_CODE_REG)
		$this->data['User']['verify'] = -3;

		//$dnd = $this->checkDND(substr($mobile_number,-10));
		//$dnd_flag = $dnd['dnd'];
		//$this->data['User']['dnd_flag'] = $dnd_flag;
		$this->data['User']['dnd_flag'] = 0;
		$this->data['User']['syspass'] = $password;
		$this->data['User']['created'] = date("Y-m-d H:i:s");
		$this->data['User']['modified'] = date("Y-m-d H:i:s");
		if(isset($dnd['preference']))
		$this->data['User']['ncpr_pref'] =  $dnd['preference'];
		
		if($this->data['User']['group_id'] == 1){
			$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die (mail('tadka@mindsarray.com','VMN: DB connection refused inside general/registerUser','Reason: '.mysql_error()));
			mysql_select_db(DB_DB);
			
			mysql_query("INSERT INTO users (mobile,password,group_id,syspass,created,modified) VALUES ('$mobile_number','".$this->data['User']['password']."',".$this->data['User']['group_id'].",'$password','".$this->data['User']['created']."','".$this->data['User']['modified']."')");
			$this->data['User']['id'] = mysql_insert_id();
			mysql_close($conn);
			return $this->data;
		}
		else {
			$userObj->create();
			if($userObj->save($this->data)) {
				$this->data['User']['id'] = $userObj->id;
				return $this->data;
			}
		}
	}

	function isDND($mobile){
		if(true){
			$ret = $this->checkDND($mobile);
			return $ret['dnd'];
		}
		else{
			return 0;
		}
	}
	
	function makeOptIn247SMS($mobile){
		$ch = curl_init('http://opted.smsapi.org/v1.0.7/sm_opt.asmx/sm_single_reg_process');
		$adm = "username=optPAYONE&password=optPAYONE&opt_var_numbers=$mobile&opt_var_status=2&opt_var_action_date=".date('Y-m-d')."&opt_trans_unique_id=".time()."&response_type=0";
		
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $adm);
				
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$Rec_Data = $this->xml2array(curl_exec($ch));
		//$this->printArray($Rec_Data);
	}

	function checkTimeSlot($par = null){
		if(DND_FLAG){ //TRAI Changes
			if($par){
				if((intval($par) < (TIME_SLOT_START+30)) || (intval($par) > (TIME_SLOT_END))){
					return false;
				}else{
					return true;
				}
			}else{
				$current = date('Hi');
				if(intval($current) < TIME_SLOT_START || intval($current) > TIME_SLOT_END){
					return false;
				}else{
					return true;
				}
			}
		}
		else {
			return true;
		}
	}

	function addNonSentMessages($sender,$receivers,$message,$type,$app_name){
		$array_flag = 0;
		if(is_array($receivers)) {
			$receivers = implode(",",$receivers);
			$array_flag = 1;
		}
		if(!empty($receivers)){
			$userObj = ClassRegistry::init('User');
			$userObj->query("INSERT INTO log_notsent (sender,receivers,message,type,app_name,array_flag,timestamp) VALUES ('".$sender."','".$receivers."','".addslashes($message)."','".$type."','$app_name',$array_flag,'".date('Y-m-d H:i:s')."')");
		}
	}

	function createMessage($alias,$vars){
		$seperator = "@__123__@";
		$fname = $_SERVER['DOCUMENT_ROOT'] . "/templates.txt";
		$fh = fopen($fname,'r');
		$contents = fread($fh, filesize($fname));
		fclose($fh);
		$templates = json_decode($contents,true);
		//$this->printArray($templates);
		$message = $templates[$alias];

		foreach($vars as $var){
			$message = preg_replace('/@__123__@/', $var, $message, 1);
		}
		$message = preg_replace('/@__123__@/', 'N/A', $message);
		return $message;
	}

	function emailSMSToUsers($sms, $emails){
		$message = nl2br($sms);
		$message .= "<br/><br/><br/>Dear Customer, As your mobile number is in DND registry, You are receiving SMSTadka messages on email. If you wish to receive messages on your mobile, either opt out of DND registry or provide alternate non-DND mobile number.<br/>To know if your number is in DND, visit http://nccptrai.gov.in/nccpregistry/search.misc<br/>For any help, sms HELP to 09223178889.";
		$userObj = ClassRegistry::init('User');
		$str = array();
		foreach($emails as $email){
			$str[] = "('$email','".addslashes($message)."','".date('Y-m-d H:i:s')."')";
		}
		$data = $userObj->query("INSERT INTO log_mails (emailid,body,timestamp) VALUES " . implode(",",$str));
		$this->mailToUsers('Message From SMSTadka - '.date('jS M, Y'),$message,$emails);
	}

		
	function sendMessage($receivers,$message,$type=null,$extra=null){
		if(SMS_FLAG == '1'){
			$this->sendMessageViaOtherServer($receivers,$message,$type,$extra);
		}
	}
	
	function sendMails($subject,$mail_body,$emails=null){
		if(MAIL_FLAG == '1'){
			$this->sendMailsViaOtherServer($subject,$mail_body,$emails);	
		}
	}
	
	function findVar($var){
		$userObj = ClassRegistry::init('User');
		$data = $userObj->query("SELECT value FROM vars WHERE name = '$var'");
		return $data['0']['vars']['value'];
	}

	function sendMessageViaOtherServer($receivers,$message,$type=null,$extra=null){
		if($type != 'ussd'){
			$url = SERVER_BACKUP . 'users/sendMsgMails';
			$data['sender'] = '';
			if(is_array($receivers))
			$data['mobile'] = implode(",",$receivers);
			else $data['mobile'] = $receivers;
			$data['sms'] = $message;
			$data['root'] = $type;
			$data['app_name'] = $extra;
			if(!empty($data)){
				$this->curl_post_async($url,$data);
			}
		}
		else {
			if(!is_array($receivers)){
				$receivers = array($receivers);
			}
			
			$var = $this->findVar('ussd');
			if($var == '1'){
				foreach($receivers as $receiver){
					$data = $this->getUserDataFromMobile($receiver);
					$success = true;
					if(empty($data) || $data['ussd_flag'] == 0){
						$success = false;
						$data = $this->getMobileDetails($receiver);
						if($data[3] == 'TD'){
							$success = true;
						}
					}
					else if($data['ussd_flag'] == -1){
						$success = false;
					}
					
					if($success)$this->startUSSD(3,$receiver,$message);
					else $this->sendMessageViaOtherServer($receiver,$message,'shops');
				}
			}
			else {
				$this->sendMessageViaOtherServer($receivers,$message,'shops');
			}
		}
	}
	
	function getUSSDData($type,$mobile=null,$number=null){
		if($type == 1 || $type == 4){
			$data = "Welcome to pay1!!\n\nEnter your request\ne.g *15*9819032643*10";
		}
		else if($type == 2){
			$ch = curl_init();
			$url = "http://www.24x7sms.com/$number.aspx?MobileNo=$mobile";
			curl_setopt($ch, CURLOPT_URL, $url); 
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 100);
			$data = trim(curl_exec($ch));
			curl_close($ch);
		}
		
		return $data;
	}
	
	function startUSSD($type,$mobile,$data=null,$number=null){
		$mobile = substr($mobile,-10);
		//make an entry logic here
		//retailer check here
		$session_id = $mobile . "-" . time();
		
		if(empty($type)) $type = 1;
		$rnum = rand(0,100);
		$vendor = 3;
		
		if($vendor == 3){
			//$vendor = 3;
			$session_id = "";
		}
		
        $userObj = ClassRegistry::init('User');
		if(in_array(substr($mobile,0,1),array('7','8','9'))){
			if(!empty($data))$extra = $data;
			if(!empty($number))$extra = $number;
			$userObj->query("INSERT INTO ussd_logs (mobile,type,vendor,sessionid,extra,date,time) VALUES ('$mobile',$type,$vendor,'$session_id','".addslashes($extra)."','".date('Y-m-d')."','".date('H:i:s')."')");
			$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
			
			if($vendor == 1){
				
				$url = "http://ussd.sinfini.com/api.php";
				
				$pars = array();
				$pars['method'] = 'ussd';
				$pars['keyword'] = 'MINDSA';
				$pars['mobile'] = $mobile;
				$pars['apikey'] = 'edf9358a1bc98caae15ebff01be585d7';
				$out = $this->curl_post_async($url,$pars);
				
				$out = json_encode($out);
			}
			else if($vendor == 2){
				if(empty($data))$data = $this->getUSSDData($type,$mobile,$number);
				if($type == 1){
					$out = @file_get_contents("http://203.199.114.231/ussdproxy/ussdpush.aspx?msisdn=$mobile&msg=".urlencode($data)."&src=56263&tid=123&session=0&uid=anurag&keyword=Mindsarray",false,$context);	
				}
				else if($type == 2 || $type == 3){
					$out = @file_get_contents("http://203.199.114.231/ussdproxy/ussdpush.aspx?msisdn=$mobile&msg=".urlencode($data)."&src=56263&tid=123&session=1&uid=anurag&keyword=Mindsarray",false,$context);
				}
			}
			else if($vendor == 3){
				$url = "http://59.161.254.85:28088/NETWORK_PUSH/PushAdapter";
				//$url = "http://182.156.191.29:28088/NETWORK_PUSH/PushAdapter";
				
				$pars = array();
				$pars['username'] = 'ussdmar';
				$pars['PASSWORD'] = 'marussd';
				$pars['MSISDN'] = $mobile;
				$pars['MsgType'] = 'USSDyn';
				$pars['UserText'] = '*6694#';
				$pars['taskId'] = '130';
				$pars['circleId'] = '88';
				$pars['OA'] = '4567';
				$out = $this->curl_post_async($url,$pars,'GET');
				
				$out = json_encode($out);
			}
			$arr = json_decode($out,true);
			$userObj->query("UPDATE ussd_logs SET response='".json_encode($out)."',status='".$arr['status']."' WHERE sessionid='$session_id' AND level=0");
			
		}
		else {
			$userObj->query("UPDATE ussd_logs SET response='".json_encode($out)."',status='".$arr['status']."' WHERE sessionid='$session_id' AND level=0");
			$userObj->query("INSERT INTO ussd_logs (mobile,type,vendor,sessionid,date,time) VALUES ('$mobile',$type,$vendor,'$session_id','".date('Y-m-d')."','".date('H:i:s')."')");
		}
	}

	function sendMailsViaOtherServer($subject,$mail_body,$emails=null){
		$url = SERVER_BACKUP . 'users/sendMsgMails';
		$data['mail_subject'] = $subject;
		$data['mail_body'] = $mail_body;
		if(!empty($emails)){
			$data['emails'] = implode(",",$emails);
		}
		if(!empty($data)){
			$this->curl_post_async($url,$data);
		}
	}

	
	function printArray($txt){
		echo  '<pre>';
		print_r($txt);
		echo '</pre>';
	}

	function getUserDataFromMobile($mobile)
	{
		$userObj = ClassRegistry::init('User');
		$query = "SELECT * FROM users WHERE mobile = '$mobile'";
		$res_arr = $userObj->query($query);

		return $res_arr['0']['users'];
	}

	function getUserDataFromId($id)
	{
		$userObj = ClassRegistry::init('User');
		$query = "SELECT * FROM users WHERE id = $id";
		$res_arr = $userObj->query($query);

		return $res_arr['0']['users'];
	}

	function getMobileDetails($mobileNumber)
	{
		$mobNum = substr($mobileNumber,0,4);
		$query = "select mna.area_name, mns.opr_name, mn.area, mns.opr_code from mobile_numbering AS mn LEFT JOIN mobile_numbering_area as mna ON mn.area = mna.area_code LEFT JOIN mobile_numbering_service AS mns ON mn.operator = mns.opr_code WHERE mn.number = ".$mobNum;
		$userObj = ClassRegistry::init('User');

		$data = $userObj->query($query);
		$area_name = $data['0']['mna']['area_name'];
		$opr_name = $data['0']['mns']['opr_name'];

		$ret_arr = array($area_name, $opr_name, $data['0']['mn']['area'], $data['0']['mns']['opr_code']);
		return $ret_arr;
	}

	function xml2array($contents, $get_attributes=1, $priority = 'tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;//Hmm...

		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();

		$current = &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble

			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.

			$result = array();
			$attributes_data = array();

			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}

			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}

			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;

					$current = &$current[$tag];

				} else { //There was another element with the same tag name

					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;

						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}

							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}

		return($xml_array);
	}

	function authenticatedMailToUsers($emails, $subject, $mail_body, $from, $attachments = null){
		$url = SITE_NAME . 'groups/shootMail';
		$params['sub'] = $subject;
		$params['body'] = $mail_body;
		$params['from'] = $from;

		if(!empty($attachments)){
			$params['path'] = implode(",",$attachments);
		}
		foreach($emails as $email){
			$params['email'] = trim($email);
			$this->curl_post_async($url,$params);
		}
	}

	function br2newline( $input ) {
		$out = str_replace( "<br>", "\n", $input );
		$out = str_replace( "<br/>", "\n", $out );
		$out = str_replace( "<br />", "\n", $out );
		$out = str_replace( "<BR>", "\n", $out );
		$out = str_replace( "<BR/>", "\n", $out );
		$out = str_replace( "<BR />", "\n", $out );
		return $out;
	}
	
	function endsWith($haystack,$needle,$case=true)
	{
	  	$expectedPosition = strlen($haystack) - strlen($needle);
	
	  	if($case) return strrpos($haystack, $needle, 0) === $expectedPosition;
	
	  	return strripos($haystack, $needle, 0) === $expectedPosition;
	}

	function getHTMLFromNode($node){
		$domDocument1 = new DOMDocument();
			
		foreach($node->childNodes as $childNode){
			$domDocument1->appendChild($domDocument1->importNode($childNode, true));
		}

		return trim(strip_tags($domDocument1->saveHTML()));
	}

	function RSS_Tags($item, $type)
	{
		$y = array();
		$tnl = $item->getElementsByTagName("title");
		$tnl = $tnl->item(0);
		$title = $tnl->firstChild->textContent;

		$tnl = $item->getElementsByTagName("link");
		$tnl = $tnl->item(0);
		$link = $tnl->firstChild->textContent;

		$tnl = $item->getElementsByTagName("pubDate");
		$tnl = $tnl->item(0);
		$date = $tnl->firstChild->textContent;

		$tnl = $item->getElementsByTagName("description");
		$tnl = $tnl->item(0);
		$description = $tnl->firstChild->textContent;

		$y["title"] = html_entity_decode($title,ENT_QUOTES);
		$y["link"] = $link;
		$y["date"] = date('Y-m-d H:i:s', strtotime($date));
		$y["description"] = html_entity_decode($description,ENT_QUOTES);
		$y["type"] = $type;

		return $y;
	}


	function RSS_Channel($channel)
	{
		$items = $channel->getElementsByTagName("item");

		// Processing channel

		$y = $this->RSS_Tags($channel, 0);		// get description of channel, type 0
		array_push($this->RSS_Content, $y);

		// Processing articles

		foreach($items as $item)
		{
			$y = $this->RSS_Tags($item, 1);	// get description of article, type 1
			array_push($this->RSS_Content, $y);
		}
	}

	function RSS_Retrieve($url)
	{
		$doc  = new DOMDocument();
		$doc->load($url);

		$channels = $doc->getElementsByTagName("channel");

		$this->RSS_Content = array();

		foreach($channels as $channel)
		{
			$this->RSS_Channel($channel);
		}

	}


	function RSS_RetrieveLinks($url)
	{
		$doc  = new DOMDocument();
		$doc->load($url);

		$channels = $doc->getElementsByTagName("channel");

		$this->RSS_Content = array();

		foreach($channels as $channel)
		{
			$items = $channel->getElementsByTagName("item");
			foreach($items as $item)
			{
				$y = $this->RSS_Tags($item, 1);	// get description of article, type 1
				array_push($this->RSS_Content, $y);
			}

		}

	}


	function RSS_Links($url, $size = 15,$mode)
	{
		$page = "";

		$this->RSS_RetrieveLinks($url);
		if($size > 0)
		$recents = array_slice($this->RSS_Content, 0, $size + 1);

		foreach($recents as $article)
		{
			$type = $article["type"];
			if($type == 0) continue;
			$title = $article["title"];
			$link = $article["link"];
			if($mode)
			{
				$page .= "<li><a href=\"$link\">$title</a></li>\n";
			}
			else
			{

				$page .= "# ".$title."<br>\n";
			}
		}

		$page .="\n";

		return $page;

	}

	function RSS_Display($url, $size = 15, $site = 0, $withdate = 0)
	{
		$opened = false;
		$page = "";
		$site = (intval($site) == 0) ? 1 : 0;

		$this->RSS_Retrieve($url);
		if($size > 0)
		$recents = array_slice($this->RSS_Content, $site, $size + 1 - $site);

		return $recents;
	}

	function extractPassword($mobile)
	{
		$userObj = ClassRegistry::init('User');
		$sysPass = $userObj->find('first', array('fields' => array('User.syspass'),'conditions' => array('User.mobile' => $mobile)));
		//$count = $userObj->find('count',array('conditions' => array('User.mobile' => $mobile)));

		if(!empty($sysPass)) return $sysPass['User']['syspass'];
		return false;

	}

	function getBitlyUrl($url){
		$link = 'http://api.bit.ly/v3/shorten?login='.BITLY_USER.'&apiKey='.BITLY_KEY.'&longUrl='.$url.'&format=txt';
		//echo $link; exit;
		$ch = curl_init($link);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$data = curl_exec ($ch);
		curl_close ($ch);
		return $data;
	}

	
	function curl_post_async($url, $params=null,$type='POST')
	{
		foreach ($params as $key => &$val) {
			if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}
		$post_string = implode('&', $post_params);

		$parts=parse_url($url);

		$fp = fsockopen($parts['host'],
		isset($parts['port'])?$parts['port']:80,
		$errno, $errstr, 30);
		
		if(!$fp){
			$this->sendMails("Couldn't open a socket to ".$url." (".$errstr.")","Couldn't open a socket to ".$url." (".$errstr.")",array('tadka@mindsarray.com'));
			return array('status'=>'failure','errno'=>$errno,'error'=>$errstr);
		}
		else {
			if($type == "GET" && !empty($post_string))
				$parts['path'] .= '?'.$post_string;
			$out = "$type ".$parts['path']." HTTP/1.1\r\n";
			$out.= "Host: ".$parts['host']."\r\n";
			$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
			$out.= "Content-Length: ".strlen($post_string)."\r\n";
			$out.= "Connection: Close\r\n\r\n";
			if ($type == 'POST' && isset($post_string)) $out.= $post_string;
	
			fwrite($fp, $out);
			fclose($fp);
			return array('status'=>'success');
		}
	}
	
	function cbzApi($url,$params){
		if(SITE_NAME != 'http://54.235.195.140/'){
			$params['url'] = $url;
			return $this->curl_post('http://54.235.195.140/users/curl',$params);
		}
		else {
			return $this->curl_post($url,$params);
		}
	}
	
	function curl_post($url, $params=null,$type='POST')
	{	
		foreach ($params as $key => &$val) {
			if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}
		$post_string = implode('&', $post_params);
		
		
		$ch = curl_init($url);
		if($type == 'POST'){
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		}
		else {
			curl_setopt($ch, CURLOPT_POST,0);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
		}
		
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		$out = trim(curl_exec($ch));
		curl_close($ch);

		return $out;
	}

	function mobileValidate($phone){
		$err = '';
		if(!ereg("^[7-9]{1}[0-9]{9}$", $phone)) {
			$err = 1;
		}
		return $err;
	}

	function priceValidate($price)
	{
		if(is_numeric($price))
		return sprintf('%01.2f', round($price, 2));
		else
		return '';
	}

	function getRetailerSignature($retailer_id){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->recursive = -1;
		$sign = $retailObj->find('first',array('fields' => array('Retailer.signature', 'Retailer.signature_flag'), 'conditions' => array('Retailer.id' => $retailer_id)));
		return $sign;
	}


	function checkDND($mobile){
		$url = 'http://nccptrai.gov.in/nccpregistry/saveSearchSub.misc';
		$params = "phoneno=$mobile"; //you must know what you want to post
		$user_agent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
		
		$page=curl_exec ($ch);
		if($page)
		{
			$domDocument=new DOMDocument();
			$domDocument->loadHTML($page);
			$xpath = "//td[contains(concat(' ',normalize-space(@class),' '),' GridHeader ')]";
				
			$domXPath = new DOMXPath($domDocument);
			$domNodeList = $domXPath->query($xpath);
			$ret = -1;
			foreach($domNodeList as $node){
				$domDocument1 = new DOMDocument();
				foreach($node->childNodes as $childNode){
					$domDocument1->appendChild($domDocument1->importNode($childNode, true));
					$html = trim(strip_tags($domDocument1->saveHTML()));
					if($html == 'The number is not registered in NCPR'){
						$ret = 0;
					}
					else {
						$ret = 1;
					}
				}
			}
			$return['dnd'] = $ret;
			if($ret == 1){
				$xpath = "//tr[15]/td";

				$domXPath = new DOMXPath($domDocument);
				$domNodeList = $domXPath->query($xpath);
				foreach($domNodeList as $node){
					$domDocument1 = new DOMDocument();
					foreach($node->childNodes as $childNode){
						$domDocument1->appendChild($domDocument1->importNode($childNode, true));
					}
					$html = substr(trim(strip_tags($domDocument1->saveHTML())),-1,1);
					$return['preference'] = $html;
				}
			}
			return $return;
			//return $content;
		}
		else {
			$return['dnd'] = -1;
			return $return;
		}
		curl_close ($ch);
	}

	
	function createTagIfNotExists($tag){
		$tagObj = ClassRegistry::init('Tagging');
		$tagObj->recursive = -1;
		$tagData = $tagObj->find('first',array('conditions' => array('name' => strtoupper($tag))));
		if(empty($tagData)){
			$this->data['Tagging']['name'] = strtoupper($tag);
			$tagObj->create();
			$tagObj->save($this->data);
			return $tagObj->id;
		}
		else return $tagData['Tagging']['id'];
	}

	function tagUser($tag,$user_id,$email = null){
		$tag_id = $this->createTagIfNotExists($tag);
		$tagObj = ClassRegistry::init('UserTagging');
		$this->data['UserTagging']['tagging_id'] = $tag_id;
		$this->data['UserTagging']['user_id'] = $user_id;
		$this->data['UserTagging']['timestamp'] = date('Y-m-d H:i:s');
		$tagObj->create();
		$tagObj->save($this->data);

		if($tag_id == RETAILER_TAG){
			$this->linkRetailerUser($user_id,$email);
		}
	}

	function linkRetailerUser($user_id,$email=null){
		$tagObj = ClassRegistry::init('UserTagging');
		$data = $tagObj->query("SELECT taggings.name,vendors_retailers.id FROM user_taggings,taggings,vendors_retailers WHERE UPPER(vendors_retailers.retailer_code) = taggings.name AND taggings.id = user_taggings.tagging_id AND user_taggings.user_id = $user_id ");
		if(!empty($data)){
			$userData = $this->getUserDataFromId($user_id);
			$mobile = $userData['mobile'];
				
			$eText = "";
			if(trim($email) != '')
			$eText = ",email='$email'";
				
			$tagObj->query("UPDATE vendors_retailers SET user_id = $user_id,mobile='$mobile' $eText WHERE id=" . $data['0']['vendors_retailers']['id']);
		}
	}

	function getTaggedUsers($tag){
		$tagObj = ClassRegistry::init('Tagging');
		$tagObj->recursive = -1;
		$data = $tagObj->find('all',array('fields' => array('mobile_numbering_area.area_name','User.mobile','User.id'),'conditions' => array('Tagging.name like "%' . strtoupper($tag) . '%"'),
			'joins' => array(
		array(
					'type' => 'inner',
					'table' => 'user_taggings',
					'alias' => 'UserTagging',
					'conditions' => array('UserTagging.tagging_id = Tagging.id')
		),
		array(
					'type' => 'inner',
					'table' => 'users',
					'alias' => 'User',
					'conditions' => array('User.id = UserTagging.user_id')
		),
		array(
					'type' => 'left',
					'table' => 'mobile_numbering',
					'conditions' => array('mobile_numbering.number = SUBSTR(User.mobile,1,4)')
		),
		array(
					'type' => 'left',
					'table' => 'mobile_numbering_area',
					'conditions' => array('mobile_numbering.area = mobile_numbering_area.area_code')
		)
		),
			'group' => 'User_id',
			'order' => 'UserTagging.id desc'
			));
			$i = 0;
			foreach($data as $dt){
				$cmntObj = ClassRegistry::init('Comment');
				$cmntObj->recursive = -1;
				$comment = $cmntObj->find('all', array('fields' => array('Comment.comment','Comment.created'),
				'conditions' => array('Comment.itemid' => $dt['User']['id'],'Comment.itemtype' => 0),
				'order' => 'Comment.created desc',
				'limit' => 1
				));
				if(!empty($comment)){
					$data[$i]['Comment']['comment'] = $comment['0']['Comment']['comment'];
					$data[$i]['Comment']['created'] = $comment['0']['Comment']['created'];
				}
				$data[$i]['Tags'] = $this->getUserTags($dt['User']['id']);
				$i++;
			}
			return $data;
	}

	function getUserTags($user_id){
		$tagObj = ClassRegistry::init('UserTagging');
		$tagObj->recursive = 1;
		$data = $tagObj->find('all',array('fields' => array('Tagging.name','Tagging.id'),
			'conditions' => array('UserTagging.user_id' => $user_id)));
		return $data;
	}

	function updateLocation($area_id){
		$retailObj = ClassRegistry::init('Retailer');

		$areaArr = $retailObj->query("SELECT area_id FROM retailers where id = ".$area_id);
		foreach($areaArr as $a){
			$retailObj->query("update locator_area set toShow = 1 where id =".$a['retailers']['area_id']);
			$cityArr = 	$retailObj->query("SELECT city_id FROM locator_area where id = ".$a['retailers']['area_id']);
			foreach($cityArr as $c){
				$retailObj->query("update locator_city set toShow = 1 where id =".$c['locator_area']['city_id']);
				$stateArr = $retailObj->query("SELECT state_id FROM locator_city where id = ".$c['locator_area']['city_id']);
				foreach($stateArr as $s){
					$retailObj->query("update locator_state set toShow = 1 where id =".$s['locator_city']['state_id']);
				}
			}
		}

		$this->autoRender = false;
	}

	function checkRetailerPrizeScheme($vendor_id,$retailer_code,$prod_price){
		$retailObj = ClassRegistry::init('Retailer');
		$retailObj->query("UPDATE scheme_sales SET totalSale= totalSale+$prod_price, dailySale= dailySale+$prod_price");

		$retailObj->query("UPDATE vendors_retailers SET totalSale= totalSale+$prod_price, weeklySale= weeklySale+$prod_price WHERE vendor_id = $vendor_id AND retailer_code = '$retailer_code'");

		$weeklySale_data = $retailObj->query("SELECT vendors_retailers.weeklySale FROM vendors_retailers WHERE vendors_retailers.vendor_id = $vendor_id AND vendors_retailers.retailer_code = '$retailer_code'");
		$weeklySale = $weeklySale_data['0']['vendors_retailers']['weeklySale'];

		$dailySale = $retailObj->query("SELECT scheme_sales.dailySale FROM scheme_sales");
		$dailySale = $dailySale['0']['scheme_sales']['dailySale'];

		$weeklyOffer = false;
		$goaOffer = true;
		$daily = false;
		$message = "";
		if($dailySale >= DAILY_PRIZE_SALE_LIMIT){
			$retailerCheck = $retailObj->query("SELECT max(timestamp) as lastDate FROM retailers_winners WHERE vendor_id = $vendor_id AND retailer_code = '$retailer_code' AND type = 'daily'");
			if(empty($retailerCheck) || $retailerCheck['0']['0']['lastDate'] < date('Y-m-d H:i:s',strtotime('- ' . DAILY_PRIZE_DAYS_WINDOW .' days'))){
				$retailWinObj = ClassRegistry::init('RetailersWinner');
				$this->data['RetailersWinner']['vendor_id'] = $vendor_id;
				$this->data['RetailersWinner']['retailer_code'] = $retailer_code;
				$this->data['RetailersWinner']['prize'] = 1;
				$this->data['RetailersWinner']['type'] = 'daily';
				$this->data['RetailersWinner']['timestamp'] = date('Y-m-d H:i:s');

				$retailWinObj->create();
				$retailWinObj->save($this->data);
				$last_id = $retailWinObj->id;

				App::import('vendor', 'md5Crypt', array('file' => 'md5Crypt.php'));
				$objMd5 = new Md5Crypt;
				$retailer_winner_id = $objMd5->Encrypt($last_id,encKey);

				$retailObj->query("UPDATE scheme_sales SET dailySale=0");
				//$this->sendMails("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code");
				/*if($vendor_id == VENDOR_OSS){
                                    $this->mailToUsers("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code<br/>Please send us the mobile number and address of this retailer to dinesh@mindsarray.com.", array('ashisharya.iitb@gmail.com'));
                                    $this->mailToUsers("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code<br/>Please send us the mobile number and address of this retailer to dinesh@mindsarray.com.", array('navin.shrivastav@onestopshop.in'));
                                    $this->mailToUsers("SMSTadka Retail Daily Winner !!", "Retailer Code: $retailer_code<br/>Please send us the mobile number and address of this retailer to dinesh@mindsarray.com.", array('khushboo.prajapati@onestopshop.in'));
                                    }*/
				$message = "Congratulations, You have just won a Lucky Prize from SMSTadka.<br/>
Your prize is on its way to your home<br/>";
				$daily = true;
			}
		}

		if($goaOffer){
			$retail = $retailObj->query("SELECT SUM(products.price) as totalSaleTill FROM vendors_activations INNER JOIN products_users ON (vendors_activations.productuser_id = products_users.id) INNER JOIN products ON (products.id = products_users.product_id) WHERE vendors_activations.vendor_id = $vendor_id AND vendors_activations.vendor_retail_code = '$retailer_code'");
				
			$message .= "Keep doing transactions. Your total SMSTadka sale till now is Rs " . $retail['0']['0']['totalSaleTill'];
		}

		if($weeklyOffer && !$daily && $weeklySale >= WEEKLY_PRIZE_SALE_LIMIT){
			$message = "Congratulations !!! You have already won your weekly assured prize.<br/>
Your prize is on its way to your home.<br/>
You can keep trying for more lucky prizes on every transactions.";
		}
		else if($weeklyOffer && !$daily){
			$weekly = WEEKLY_PRIZE_SALE_LIMIT - $weeklySale;
			$message = "Keep activating SMSTadka products for your customers and get a chance to win exciting prizes from SMSTadka.<br/>
For an assured prize worth Rs 2499/-, You are just Rs $weekly away.<br/>
<a href='".SITE_NAME."offers'>To know more Click here.</a>";
		}
		return $message;
	}
	
	function maskNumber($n){
		return substr($n,0,6)."XXXX";
	}
	
	function createAppDownloadUrl($type,$app_number){
		App::import('vendor', 'md5Crypt', array('file' => 'md5Crypt.php'));
		$objMd5 = new Md5Crypt;
		if($type == DISTRIBUTOR){
			if($app_number == 1)$fname = DISTRIBUTOR_APP_FILE_1;
			else if($app_number == 2)$fname = DISTRIBUTOR_APP_FILE_2;
		}
		else if($type == RETAILER){
			if($app_number == 1)$fname = RETAILER_APP_FILE_1;
			else if($app_number == 2)$fname = RETAILER_APP_FILE_2;
		}
		$url = 'http://panel.pay1.in/apis/downloadApp/'.$fname;
		//$url = SITE_NAME.'apps/'.$fname;
		//$url = 'www.mindsarray.com/apps/'.$fname;
		return $url;
		//return $this->getBitlyUrl($url);
	}
	
	function getRetailerList($dist_id,$sid=null,$xfer=false){
		$query = 1;
		if($sid != null){
			$query = "Retailer.maint_salesman = $sid";
		}
		$retailObj = ClassRegistry::init('Retailer');
		
		if($xfer){
			$retailers = $retailObj->find('all', array(
			'fields' => array('Retailer.*','users.mobile', 'sum(shop_transactions.amount) as xfer'),
			'conditions' => array('Retailer.parent_id' => $dist_id,'Retailer.toshow' => 1,$query),
			'joins' => array(
				array(
							'table' => 'users',
							'type' => 'left',
							'conditions'=> array('Retailer.user_id = users.id')
				),
				array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('Retailer.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 2')
				)		
				),
			'order' => 'xfer desc, Retailer.shopname asc',
			'group'	=> 'Retailer.id'
			)
			);
		}
		else {
			$retailers = $retailObj->find('all', array(
			'fields' => array('Retailer.*','users.mobile'),
			'conditions' => array('Retailer.parent_id' => $dist_id,'Retailer.toshow' => 1,$query),
			'joins' => array(
				array(
							'table' => 'users',
							'type' => 'left',
							'conditions'=> array('Retailer.user_id = users.id')
				)	
				),
			'order' => 'Retailer.shopname asc',
			'group'	=> 'Retailer.id'
			)
			);
		}
		
		return $retailers;
	}
        function sendTemplateEmailToAdmin($templateId,$varParseArr){
		$emailTemplate = $this->getTemplateEmail($templateId);
                $emailTemplate['body'] = $this->parseMsg($emailTemplate['body'],$varParseArr);
                $this->sendMails($emailTemplate['subject'],$emailTemplate['body']);
                
	}
        function getTemplateEmail($tempId){
                return GeneralComponent::$emailTemplates[$tempId];
        }
        function sendTemplateSMSToMobile($mobNo,$templateId,$varParseArr=array()){
		$smsTemplate = $this->getTemplateSMS($templateId);
                $smsTemplate['msg'] = $this->parseMsg($smsTemplate['msg'],$varParseArr);
                //$this->makeOptIn247SMS($mobNo);
                $this->sendMessage($mobNo,$smsTemplate['msg'],'shops');				
	}
        function getTemplateSMS($tempId){
                return GeneralComponent::$smsTemplates[$tempId];
        }
        function parseMsg($msg , $varArr){
                $outPutStr = $msg;
                foreach($varArr as $key=>$value){
                    $outPutStr = str_replace('@'.$key.'@', $value, $outPutStr);
                }
                return $outPutStr;
        }
        function getTransferTypeName ($type){
            $trans_type = "";
            //$transaction['shop_transactions']['type']
            if($type == ADMIN_TRANSFER) 
                $trans_type =  'Balance Transferred by Company'; 
            else if($type == SDIST_DIST_BALANCE_TRANSFER) 
                $trans_type =  'Balance Transferred by Super Distributor'; 
            else if($type == DIST_RETL_BALANCE_TRANSFER) 
                $trans_type =  'Balance Transferred by Distributor'; 
            else if($type == DISTRIBUTOR_ACTIVATION) 
                $trans_type =  'DISTRIBUTOR ACTIVATION'; 
            else if($type == RETAILER_ACTIVATION) 
                $trans_type =  'RETAILER ACTIVATION'; 
            else if($type == COMMISSION_SUPERDISTRIBUTOR) 
                $trans_type =  'COMMISSION SUPERDISTRIBUTOR'; 
            else if($type == COMMISSION_RETAILER) 
                $trans_type =  'COMMISSION RETAILER'; 
            else if($type == TDS_SUPERDISTRIBUTOR) 
                $trans_type =  'TDS SUPER DISTRIBUTOR'; 
            else if($type == TDS_DISTRIBUTOR) 
                $trans_type =  'TDS DISTRIBUTOR'; 
            else if($type == TDS_RETAILER) 
                $trans_type =  'TDS RETAILER'; 
            else if($type == REVERSAL_RETAILER) 
                $trans_type =  'REVERSAL RETAILER'; 
            else if($type == REVERSAL_DISTRIBUTOR) 
                $trans_type =  'REVERSAL DISTRIBUTOR'; 
            else if($type == REVERSAL_SUPERDISTRIBUTOR) 
                $trans_type =  'REVERSAL SUPERDISTRIBUTOR'; 
            else if($type == DEBIT_NOTE) 
                $trans_type =  'DEBIT NOTE'; 
            else if($type == CREDIT_NOTE) 
                $trans_type =  'CREDIT NOTE'; 
            else if($type == SETUP_FEE) 
                $trans_type =  'TDS DISTRIBUTOR'; 
            else if($type == REFUND) 
                $trans_type =  'REFUND'; 
            else if($type == RENTAL) 
                $trans_type =  'RENTAL'; 


            return $trans_type;
        }
}

?>