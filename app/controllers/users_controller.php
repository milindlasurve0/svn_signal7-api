
<?php
class UsersController extends AppController {

	var $name = 'Users';
	var $helpers = array('Html','Ajax','Javascript','Minify');
	var $uses = array('User','Group');
	var $components = array('RequestHandler','Shop');

	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		/*$this->Auth->allowedActions = array('checkDND','getRecentMsgs','sendMsgMails','paymentResponseOSS','prepaymentPayOSS','iccPaymentDone','paymentResponseICC','prepaymentPayICC','initDB','loginWin','redeemCredits','login','paymentResponse','afterLogin','beforeSend','beforeSubscribe','captcha','captchaValidate',
											'register','forgotPassword','registerCheck','forgotPasswordCheck','updatePassword',
											'twitt','dnd','resenddndcode','senddndcode','dndverification','dndblock','fetchMorePackages','er404','resendPassword','restoreReg','changePassword');
	*/
		$member_acl = array('index','add','edit','delete');
		if(in_array($this->params['action'],$member_acl) && $this->Session->read('Auth.User.group_id') != ADMIN){
			$this->redirect('/');
		}
	}

	function index() {
		$this->User->recursive = 1;
		$this->set('users', $this->paginate());	
	}
	
	function add() {
		$this->set('groups', $this->User->Group->find('list'));
		if (!empty($this->data)) {
			$this->User->create();
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
	}
	
	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('The user has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

	
	//for change of retailer number,pwd sending
	function sendPassword1()
	{
		//echo "In users controller "; 	
		$password = $this->General->generatePassword(4);
		//echo "password is ".$password;
		$this->General->sendPassword($_REQUEST['mobile'],$password,0);
		echo $password;
		$this->autoRender=false;
	
	}
	
	function curl(){
		echo $this->General->curl_post($_REQUEST['url'],$_REQUEST);
		$this->autoRender=false;
	}
	
	function addNewNumber($flag=null){
		if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == CUSTCARE || $_SESSION['Auth']['User']['id'] == 1){
			
		}
		else {
			$this->redirect(array('action' => 'index'));
		}
	    // if retInfo number change then flag=1 
		$oldNum=$_REQUEST['oldNumber'];
		$newNum=$_REQUEST['newNumber'];
		$oldIDForLogsResult=$this->User->query("select users.id,retailers.maint_salesman,salesmen.mobile,retailers.shopname from users,retailers left join salesmen ON (salesmen.id = retailers.maint_salesman) where users.mobile='$oldNum' AND users.id = retailers.user_id");
		$oldIDForLogs=$oldIDForLogsResult['0']['users']['id'];
		
		if(!empty($oldIDForLogs)){
			$result=$this->User->query("Select id,mobile,group_id from users where mobile='$newNum'");
			//for retailers table start
			$newNumRetailerCheck=$this->User->query("Select mobile from retailers where mobile='$newNum'");
			//retailers table end
			
			if((count($result) > 0 && $result['0']['users']['group_id'] == RETAILER) || count($newNumRetailerCheck) > 0) // checking if new number already present in system.
			{
				$msg = "This retailer already exists in the system. ";
				$successs = 0;
			}
			else if(count($result) > 0 && !empty($oldIDForLogs))  // old number and new number get swapped.
			{
				//echo "New Number already present";
				$newId = $result['0']['users']['id'];
				$this->User->query("update users set mobile='temp_str' where id=$newId");//relacing new num by 'temp_str' in users table.
				//if retailer number to b changed start
				$this->General->makeOptIn247SMS($newNum);
					
				$oldId = $oldIDForLogs;
				$this->User->query("update users set mobile='".$newNum."',ussd_flag=0 where id=$oldId");
				$this->User->query("update retailers set mobile='".$newNum."' where user_id=$oldId");
				$this->User->query("update users set mobile='".$oldNum."',ussd_flag=0 where id=$newId");
				//ret end
				$successs = 1;
			}
			else {
				//echo "New Number not present";
				$this->General->makeOptIn247SMS($newNum);
				
				$result=$this->User->query("UPDATE users SET mobile='$newNum',ussd_flag=0 WHERE mobile='$oldNum'");
				$result1=$this->User->query("UPDATE retailers SET mobile='$newNum' WHERE mobile='$oldNum'");
				$successs = 1;
			}
			
			$msg1="Dear Retailer,\nYour number has been shifted from $oldNum to your new number ".$newNum;
			$msg2="Dear Salesman,\nDemo number of retailer (".$oldIDForLogsResult['0']['retailers']['shopname'].") changed from $oldNum to new number ".$newNum;
			
			
			if($successs == 1)
			{
				echo $successs."^^^".$msg1;
				$this->General->sendMessage($newNum,$msg1,'shops');
				if(!empty($oldIDForLogsResult['0']['salesmen']['mobile']))
					$this->General->sendMessage($oldIDForLogsResult['0']['salesmen']['mobile'],$msg2,'shops');
			}else{
				echo $successs."^^^".$msg;
			}
		}
		else {
			echo "0^^^Retailer does not exists";
		}
		$this->autoRender=false;
	}
	
	function forgotPasswordCheck($direct=null,$mobile=null){
		if(!isset($_SESSION['Auth']['User']['group_id']))
			$this->redirect(array('action' => 'index'));
		if($mobile != null)$this->data['User']['mobile'] = $mobile;
		$exists = $this->General->checkIfUserExists($this->data['User']['mobile']);
		$this->Session->write('displayDiv','forgotPassword');
		if($exists){
			if($direct == '1' || $strtolower($this->data['User']['captchaText']) == $this->Session->read('security_code'))	{
				$this->data['User']['mobile'] = $this->data['User']['mobile'];
				$check = $this->User->find('first',array('fields' => array('syspass','dnd_flag'),'conditions' => array('mobile' => $this->data['User']['mobile'])));
				if(empty($check['User']['syspass']) || $check['User']['syspass'] == 'NULL' || SENDFLAG == '0'){
					$password = $this->General->generatePassword(4); //generate 4 character password
				}
				else {
					$password = $check['User']['syspass'];
				}
				
				$this->data['User']['password'] = $this->Auth->password($password); //encrypted password using hash salt
				//echo "<script>showLoader2('messagePopUpDiv');</script>";
				if($this->updatePassword($this->data['User']['mobile'],$password,'change')){
					if($direct == null){
						if(($check['User']['dnd_flag'] == 0 &&  $this->General->checkTimeSlot()) || TRANS_FLAG){
							$this->Session->setFlash(__('The password has been sent to your mobile. <br>Enter your password to sign in<br><br>', true));
							$this->set('mobile',$this->data['User']['mobile']);
							//					 /$this->set('forgot', 0);
							$this->render('/elements/login_user','ajax');
						}
						else if($check['User']['dnd_flag'] == 1){
							echo "Your number is registered in DND. Due to new Telecom regulations we cannot send you SMS. Kindly de-register by calling 1909";
							$this->autoRender = false;
						}
						else {
							echo "Due to New Telecom regulations we cannot send you SMS this time. Kindly re-try " . TIME_SLOT_PERIOD . " only ";
							$this->autoRender = false;
						}
					}
					
				}
			}
			else {
				$this->Session->setFlash(__('You have entered wrong code. Please try again<br><br>', true));
				echo "<script>$('popUpDiv').hide();forgetPassword();</script>";
				$this->autoRender = false;
			}

		}
		else {
			echo 'You are not registered. <a href="javascript:void(0);" onclick="register();">Click here to register.</a>';
			$this->autoRender = false;
		}

		//echo "<script> centerPos('popUpDiv');</script>";
	}


	function changeDetails(){
		if(!isset($_SESSION['Auth']['User']['group_id']))
			$this->redirect(array('action' => 'index'));
		$this->data['User']['id'] = $this->Session->read('Auth.User.id');
		if ($this->User->save($this->data)) {
			$_SESSION['Auth']['User']['name'] = $this->data['User']['name'];
			$_SESSION['Auth']['User']['email'] = $this->data['User']['email'];
			$_SESSION['Auth']['User']['gender'] = $this->data['User']['gender'];
			$_SESSION['Auth']['User']['city'] = $this->data['User']['city'];
			$_SESSION['Auth']['User']['dob'] = $this->data['User']['dob']['year'].'-'.$this->data['User']['dob']['month'].'-'.$this->data['User']['dob']['day'];
			$this->Session->setFlash(__('Details Saved Successfully', true));
			$this->render('/elements/personal_details','ajax');
		}
		else {
			$this->Session->setFlash(__('Please enter proper email id', true));
			$this->render('/elements/personal_details','ajax');
		}

	}


	function userPasswordChange(){
		if(!isset($_SESSION['Auth']['User']['group_id']))
			$this->redirect(array('action' => 'index'));
		$this->Session->write('param','pass');
		$this->redirect("/users/view/");
	}

	function register() {
		$this->render('/users/register','ajax');
	}


	function passwordChange(){
		$this->render('/elements/change_password','ajax');
	}

	function changePassword($par=null){
		if(!isset($_SESSION['Auth']['User']['group_id']))
			$this->redirect(array('action' => 'index'));
		//all other checkings should be from javascript side

		//here i am assuming all other data is correct
		$this->User->recursive = -1;
		$password = $this->User->find('first', array('fields' => array('User.password'), 'conditions' => array('User.mobile' => $this->Session->read('Auth.User.mobile'))));

		if($password['User']['password'] != $this->Auth->password($this->data['User']['pass1']))	{
			$this->set('errFlag','1');
			if($par != null){
				$this->set('par',$par);
			}
			$this->render('/elements/change_password','ajax');
		}
		else{
			if($this->updatePassword($this->Session->read('Auth.User.mobile'),$this->data['User']['pass2'],'change','in')){
				echo "Password changed successfully";
				echo "<script>$('notice').innerHTML = '';</script>";
				echo "<script>$('tabsBgMainNotice').removeClassName('tabsBgMainNotice');</script>";
				echo "<script>$('tabsBgMainNotice').addClassName('tabsBgMain');</script>";
			}
			else {
				echo "Password can not be changed";
			}

			$this->autoRender = false;
		}
	}
	
	function resetPassword($mobile){
		/*if(!in_array($_SESSION['Auth']['User']['group_id'],array(ADMIN,RETAILER,DISTRIBUTOR,SUPER_DISTRIBUTOR)))
			$this->redirect(array('action' => 'index'));*/
			
		$password = $this->General->generatePassword(4);
		$this->User->query("UPDATE users SET password = '".$this->Auth->password($password)."' WHERE mobile = '$mobile'");
		$msg = "Dear Sir,\nYour password has been reset to $password";
		return $msg;
	}

	function updatePassword($mobile,$password,$type,$changeP=null){


		if($type == "new" || $changeP == null){
			$passFlag = '0';
			$sysPass = $password;
		}
		else if($type == "change"){
			$passFlag = '1';
			$sysPass = 'NULL';
		}

		if($this->User->updateAll(array('User.password' => "'".$this->Auth->password($password)."'", 'User.passflag' => $passFlag, 'User.syspass' => "$sysPass"), array('User.mobile' => $mobile))){
			if($changeP == null)
			$this->General->sendPassword($mobile,$password,0);

			if($this->Session->read('Auth.User')){
				$_SESSION['Auth']['User']['passflag'] = $passFlag;
			}

			return true;
		}

		return false;
	}

	function afterLogin(){
		$this->autoRender = false;
                
                $response = array();
                $this->data = null;
		$this->data['User']['mobile'] = $_POST['mobile'];
		$this->data['User']['password'] = $this->Auth->password($_POST['password']);
		$param = $_POST['param'];
		$this->User->recursive = -1;
                $usrData = $this->User->find('first',array('conditions' => array('User.mobile' => $this->data['User']['mobile'],'User.password' => $this->data['User']['password'])));
                if(empty($usrData)){
			//echo '0';
                        $response['status'] = "FALSE";
                        $response['errors'] = array('code'=>'E000','msg'=>'Invalid LoginID or Password .');
		}
		else {
			if($usrData['User']['group_id'] != MEMBER ){
				$info = $this->Shop->getShopData($usrData['User']['id'],$usrData['User']['group_id']);
				$info['User']['group_id'] = $usrData['User']['group_id'];
				$info['User']['id'] = $usrData['User']['id'];
				$info['User']['mobile'] = $usrData['User']['mobile'];
				if($_SERVER['SERVER_NAME'] == 'cc.pay1.in'){
					if(in_array($_SERVER['REMOTE_ADDR'],array('122.170.103.144','59.181.102.142')) || $info['User']['group_id'] == ADMIN || $info['User']['id'] == 1){	
						$this->Session->write('Auth',$info);
						//echo '1';
                                                $response['status'] = "TRUE";
                                                $response['success'] = array('code'=>'S001','msg'=>'Login Successful.');
					}
					else {
						//echo '0';
                                             $response['status'] = "FALSE";
                                             $response['errors'] = array('code'=>'E001','msg'=>'Invalid Access Location .');
                        
					}
				}
				else if($usrData['User']['group_id'] == CUSTCARE && $_SERVER['SERVER_NAME'] != 'cc.pay1.in'){
                                        //echo '0';
                                            $response['status'] = "FALSE";
                                            $response['errors'] = array('code'=>'E002','msg'=>'Invalid Access Location .');
				}
				else {
                                   
					$this->Session->write('Auth',$info);
					
					//echo '1';
                                        $response['status'] = "TRUE";
                                        $response['success'] = array('code'=>'S001','msg'=>'Login Successful.');
				}
			}
			else {
				//echo "<script>alert('Wrong username or password');</script>";
				//echo '0';
                                $response['status'] = "FALSE";
                                $response['errors'] = array('code'=>'E002','msg'=>'Access By Invalid User Type .');
			}
		}
                
		echo  json_encode($response);
                //$this->set('json_content', json_encode($response));
	}

	function rightHeader(){
		$this->render('/elements/right_header','ajax');
	}

	function er404($response){
		//$this->set('response',$response);
                $this->layout = 'er404';
                $this->set('response',$response);
		$this->render('/elements/signal7/err404');
		
	}
		
	function initDB() {
	set_time_limit(0);
	ini_set("memory_limit","-1");
	
	 $group =&$this->User->Group;
	 //Allow admins to everything
	 $group->id = 2;
	 $this->Acl->allow($group, 'controllers');
	  
	 //author permissions
	 $group->id = 3;
	 $this->Acl->deny($group, 'controllers');
	 
	 //user permissions
	 $group->id = 1;
	 $this->Acl->deny($group, 'controllers');
	 
	 $member_acl['Users'] = array('index','add','edit','delete');
	 
	 foreach($member_acl as $controller => $actions){
	 	$this->Acl->allow($group, 'controllers/'.$controller);
	 	foreach($actions as $action){
	 		$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
	 	}
	 }

	 $superdistributor_acl['Shops'] = array('topupReceipts','printRequest','issue','issueReceipt','backReceipt','printReceipt','printInvoice', 'script','view','formDistributor','backDistributor','backDistEdit','createDistributor','allotCards','backAllotment','allotRetailCards','allRetailer','editRetailer','showDetails','editDistValidation','changePassword','accountHistory','cardsAllotted','invoices','transfer','amountTransfer','backTransfer');
	 $distributor_acl['Shops'] = array('topupReceipts','printRequest','issue','issueReceipt','backReceipt','printReceipt','printInvoice', 'script', 'view','activateCards','backActivation','activateRetailCards','allRetailer','editRetailer','showDetails','editRetValidation','formRetailer','backRetailer','createRetailer','changePassword','accountHistory','cardsAllotted','cardsActivated','invoices','transfer','amountTransfer','backTransfer');
	 $retailer_acl['Shops'] = array('topupReceipts','printRequest','issue','issueReceipt','backReceipt','printReceipt','setSignature','saveSignature','printInvoice', 'script', 'view','changePassword','accountHistory','cardsSold','cardsActivated','invoices','retailerProdActivation');
	 
	 //super distributor permissions
	 $group->id = 4;
	 $this->Acl->deny($group, 'controllers');
	 
	 foreach($member_acl as $controller => $actions){
		 $this->Acl->allow($group, 'controllers/'.$controller);
		 foreach($actions as $action){
		 	$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 foreach($superdistributor_acl as $controller => $actions){
		 foreach($actions as $action){
		 	$this->Acl->allow($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 
	//distributor permissions
	 $group->id = 5;
	 $this->Acl->deny($group, 'controllers');
	 
	 foreach($member_acl as $controller => $actions){
		 $this->Acl->allow($group, 'controllers/'.$controller);
		 foreach($actions as $action){
		 	$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 foreach($distributor_acl as $controller => $actions){
		 foreach($actions as $action){
		 	$this->Acl->allow($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 
	//retailer permissions
	 $group->id = 6;
	 $this->Acl->deny($group, 'controllers');
	 
	 foreach($member_acl as $controller => $actions){
		 $this->Acl->allow($group, 'controllers/'.$controller);
		 foreach($actions as $action){
		 	$this->Acl->deny($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 foreach($retailer_acl as $controller => $actions){
		 foreach($actions as $action){
		 	$this->Acl->allow($group, 'controllers/'.$controller.'/'.$action);
		 }
	 }
	 
	 $this->autoRender = false;
}

	function DBbackup($par1,$par2){
		if($par1 == CRON_USERNAME && $par2 == CRON_PASSWORD){
			set_time_limit(0);
			ini_set("memory_limit","-1");
			$host = DB_HOST;
			$user = DB_USER;
			$pass = DB_PASS;
			$name = DB_DB;
			
			exec("mysqldump -h$host -u$user -p$pass $name > /mnt/db/shop-backup.sql");
			exec('tar -zcf /mnt/db/shop-backup.tar.gz /mnt/db/shop-backup.sql');
			exec('scp -i ~/.tadka.pem shop-backup.tar.gz root@10.196.125.190:/mnt/.');
			/*$tables = '*';
			$link = mysql_connect($host,$user,$pass);
			mysql_select_db($name,$link);
			
			//get all of the tables
			if($tables == '*')
			{
				$tables = array();
				$result = mysql_query('SHOW TABLES');
				while($row = mysql_fetch_row($result))
				{
					$tables[] = $row[0];
				}
			}
			else
			{
				$tables = is_array($tables) ? $tables : explode(',',$tables);
			}
			
			//cycle through
			foreach($tables as $table)
			{
				$result = mysql_query('SELECT * FROM '.$table);
				$num_fields = mysql_num_fields($result);
				
				$return.= 'DROP TABLE IF EXISTS '.$table.';';
				$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
				$return.= "\n\n".$row2[1].";\n\n";
				
				for ($i = 0; $i < $num_fields; $i++) 
				{
					while($row = mysql_fetch_row($result))
					{
						$return.= 'INSERT INTO '.$table.' VALUES(';
						for($j=0; $j<$num_fields; $j++) 
						{
							$row[$j] = addslashes($row[$j]);
							$row[$j] = ereg_replace("\n","\\n",$row[$j]);
							if (isset($row[$j]))
							{ 
								$return .= '"'.$row[$j].'"' ; 
							} else {
								$return .= '""'; 
							}
							
							if ($j <($num_fields-1)) { $return.= ','; }
						}
						$return.= ");\n";
					}
				}
				$return.="\n\n\n";
			}
			
			//save file
			//$handle = fopen('/tmp/db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
			$handle = fopen('/mnt/db/shop-backup.sql','w+');
			fwrite($handle,$return);
			fclose($handle);
			exec('tar -zcf /mnt/db/shop-backup.tar.gz /mnt/db/shop-backup.sql');*/
		}
		$this->autoRender = false;
	}
	
	function test(){
		echo "1"; exit;
	}

}