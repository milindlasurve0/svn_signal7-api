<?php
class ShopsController extends AppController {
	var $name = 'Shops';
	var $helpers = array('Html','Ajax','Javascript','Minify','Paginator','GChart','Paginator');
	var $components = array('RequestHandler','Shop');
	var $uses = array('SalesmanTransaction','Salesman','Retailer','Distributor','SuperDistributor','User','SlabsUser','Product','Invoice','ShopTransaction','Rm');
        var $paginate = array(
              'Post'	=> array(
		'limit'	=> 2,
		'page'	=> 1,
		'order'	=> array(
			'Post.created'	=> 'asc')
              )
             );          
        
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
		//$this->Auth->allowedActions = array('retFilter','allDistributor','addInvestedAmount','investmentReport','earningReport','kitsTransfer','transferKits','backKitsTransfer','graphRetailer','pullback','addSalesmanCollection','salesmanTran','backSetup','addSetUpFee','formSetUpFee','backSalesman','createSalesman','formSalesman','saleReport','approve','lastTransactions','createCommissionTemplate','createRetailerApp','cronUpdateBalanceLFRS','printCreditDebitNote','backCreditDebit','createNote','getCreditDebitNotes','createCreditDebitNotes','retailerProdActivation','products','index','initializeOpeningBalance','generateInvoice','logout','test','setSignature','saveSignature','topupReceipts','printRequest','script','issue','backReceipt','issueReceipt','printReceipt','printInvoice','retailerListing','PNRListing');

		$states = $this->Retailer->query("SELECT id,name FROM locator_state ORDER BY name asc");
		$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $states['0']['locator_state']['id']." ORDER BY name asc");

		$this->set('objShop',$this->Shop);
		if($this->Session->check('Auth.User')){
			$info = $this->Shop->getShopData($this->Session->read('Auth.User.id'),$this->Session->read('Auth.User.group_id'));
			$this->info = $info;
			$this->set('info',$info);
		}

		if($this->Session->read('Auth.User.group_id') == ADMIN){
			//$distributors = $this->Distributor->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));
			$this->SuperDistributor->recursive = -1;
			$super_distributors = $this->SuperDistributor->find('all', array(
			'fields' => array('SuperDistributor.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount) as xfer'),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('SuperDistributor.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('SuperDistributor.user_id = users.id')
			),
			array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('SuperDistributor.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 0')
			)

			),
			'order' => 'SuperDistributor.company asc',
			'group'	=> 'SuperDistributor.id'
			)
			);

			$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE group_id = " . RETAILER);

			$this->set('slabs',$slabs);
			$this->set('super_distributors',$super_distributors);
			$this->set('records',$super_distributors);
			$this->set('modelName','SuperDistributor');
			$this->sds = $super_distributors;
		}
		else if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
			//$distributors = $this->Distributor->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));
			$this->Distributor->recursive = -1;
			$distributors = $this->Distributor->find('all', array(
			'fields' => array('Distributor.*', 'slabs.name','users.mobile','rm.name', 'sum(shop_transactions.amount) as xfer'),
			'conditions' => array('Distributor.parent_id' => $this->info['id'],'Distributor.toshow' => 1),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Distributor.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Distributor.user_id = users.id')
			),
			array(
								'table' => 'rm',
								'type' => 'left',
								'conditions'=> array('Distributor.rm_id = rm.id')
			),
			array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('Distributor.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 1')
			)

			),
			'order' => 'Distributor.company asc',
			'group'	=> 'Distributor.id'
			)
			);

			if($this->info['id'] == 3){
				$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE group_id = " . RETAILER);
			}
			else {
				$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE Slab.id = " . $this->info['slab_id']);
			}
			$this->set('slabs',$slabs);
			$this->set('distributors',$distributors);
			$this->set('records',$distributors);
			$this->set('modelName','Distributor');
			$this->ds = $distributors;
		}
		else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			$retailers = $this->General->getRetailerList($this->info['id']);
			if($this->info['id'] == 1){
				$slabs = $this->Retailer->query("SELECT * FROM slabs as Slab WHERE group_id = " . RETAILER);
			}
			else {
				$slabs = $this->Retailer->query("SELECT * FROM slabs as Slab WHERE Slab.id = " . $this->info['slab_id']);
			}
			$this->set('slabs',$slabs);
			$this->set('retailers',$retailers);
			$this->set('records',$retailers);
			$this->set('modelName','Retailer');

			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $cities['0']['locator_city']['id']." ORDER BY name asc");
			$this->set('areas',$areas);
			$this->retailers = $retailers;
		}else if($this->Session->read('Auth.User.group_id') == RELATIONSHIP_MANAGER){
			$this->Distributor->recursive = -1;
			$distributors = $this->Distributor->find('all', array(
			'fields' => array('Distributor.*', 'slabs.name','users.mobile','rm.name', 'sum(shop_transactions.amount) as xfer'),
			'conditions' => array('Distributor.rm_id' => $this->info['id'],'Distributor.toshow' => 1),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Distributor.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Distributor.user_id = users.id')
			),
			array(
								'table' => 'rm',
								'type' => 'left',
								'conditions'=> array('Distributor.rm_id = rm.id')
			),
			array(
							'table' => 'shop_transactions',
							'type' => 'left',
							'conditions'=> array('Distributor.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 1')
			)

			),
			'order' => 'Distributor.company asc',
			'group'	=> 'Distributor.id'
			)
			);
				
			/*if($this->info['id'] == 3){
				$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE group_id = " . RETAILER);
				}
				else {

				$slabs = $this->Distributor->query("SELECT * FROM slabs as Slab WHERE Slab.id = " . $this->info['slab_id']);
				}*/
			//$this->set('slabs',$slabs);
			$this->set('distributors',$distributors);// this var is used in retailers_list view
			$this->set('records',$distributors);
			$this->set('modelName','Distributor');
			$this->ds = $distributors;;// this var is used in sale reports view
		}

		$this->set('states',$states);
		$this->set('cities',$cities);
		/*$this->Auth->loginAction = array('controller' => 'shops', 'action' => 'index');
		$this->Auth->logoutRedirect = array('controller' => 'shops', 'action' => 'index');
		$this->Auth->loginRedirect = array('controller' => 'shops', 'action' => 'view');*/
	}

	function index(){
		if($this->Session->check('Auth.User')){
			if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
				$this->redirect(array('action' => 'view'));
			}
			else if($_SESSION['Auth']['User']['group_id'] == CUSTCARE){
				$this->redirect(array('controller' => 'panels','action' => 'index'));
			}
		}
		else if($_SERVER['SERVER_NAME'] == 'apis.signal7.in'){
			echo "Work going on";
			$this->autoRender = false;
		}
		else {
			$this->render('index');
		}
	}

	function view(){
		if(!isset($_SESSION['Auth']['User']))$this->redirect('/');
		else if($_SESSION['Auth']['User']['group_id'] == ADMIN || $_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR || $_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR )
                        $this->render('transfer');
		else if($_SESSION['Auth']['User']['group_id'] == RETAILER  && isset ($_SESSION['Auth']['is_partner']) && $_SESSION['Auth']['is_partner'] == 'true' ){//if user is a api-partner
			$this->redirect(array('controller' => 'shops','action' => 'partnerTrans'));
		}else if($_SESSION['Auth']['User']['group_id'] == RETAILER ){//if user is a pay1 retailer
			$this->redirect('http://rx.pay1.in');                       
		}else if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
			$this->redirect(array('controller' => 'shops','action' => 'allDistributor'));
		}
		else{
			$this->redirect(array('controller' => 'panels','action' => 'index'));
		}
	}

	function autoCompleteSubarea(){
		$name = $this->data['Salesmen']['subarea'];

		$this->Shop->recursive = -1;
		$data = $this->User->query("SELECT * from subarea where name like '".$name."%'");

		$this->set('subarea',$data);
		$this->render('auto_complete_subarea');
	}




	function formDistributor(){
		if($_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR)$this->redirect('/');
		$this->render('form_distributor');
	}

	function backDistributor(){
		if($_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR)$this->redirect('/');
		$this->set('data',$this->data);
		$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
		$this->set('cities',$cities);
		$this->render('/elements/shop_form_distributor');
	}

	function backDistEdit($type){
		/*echo "<pre>";
		 print_r($this->data);
		 echo "</pre>";**/
		if($type == 'r'){
			if($_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR)$this->redirect('/');

			$modName = 'Retailer';
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
			$this->set('areas',$areas);

			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Retailer']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Retailer']['city']);
			$areaName = $this->Retailer->query("SELECT name FROM locator_area WHERE id = ". $this->data['Retailer']['area_id']);

			$this->data['Retailer']['state'] = $stateName['0']['locator_state']['name'];
			$this->data['Retailer']['city'] = $cityName['0']['locator_city']['name'];
			$this->data['Retailer']['area'] = $areaName['0']['locator_area']['name'];

			$sMen = $this->Retailer->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
			$this->set('sMen',$sMen);

			//$fees = $this->Retailer->query("SELECT sum(shop_transactions.amount) as fee FROM shop_transactions join salesman_transactions on (salesman_transactions.shop_tran_id = shop_transactions.id) where salesman_transactions.payment_type = ".TYPE_SETUP." and shop_transactions.ref2_id = ".$this->data['Retailer']['id']." group by salesman_transactions.payment_type");
			//$this->set('fees',$fees);
		}
		if($type == 'd'){
			if($_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR)$this->redirect('/');

			$modName = 'Distributor';
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Distributor']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Distributor']['city']);

			$this->data['Distributor']['state'] = $stateName['0']['locator_state']['name'];
			$this->data['Distributor']['city'] = $cityName['0']['locator_city']['name'];;
		}
		$tmparr[0] = $this->data;
		$this->set('editData',$tmparr);
		$this->set('type',$type);
		$this->render('/elements/edit_form_ele_retailer');
	}


	function createDistributor(){
		if($_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR)$this->redirect('/');

		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['Distributor']['tds_flag']) && $this->data['Distributor']['tds_flag'] == 'on')
		$this->data['Distributor']['tds_flag'] = 1;
		else
		$this->data['Distributor']['tds_flag'] = 0;
			
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);
		if(empty($this->data['Distributor']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['company'])){
			$empty[] = 'Company Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Distributor']['mobile'] = trim($this->data['Distributor']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Distributor']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}
		if($this->data['Distributor']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Distributor']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['area_range'])){
			$empty[] = 'Area Range';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['pan_number'])){
			$empty[] = 'PAN Number';
			$empty_flag = true;
			$to_save = false;
		}

		if($to_save){
			$exists = $this->General->checkIfUserExists($this->data['Distributor']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Distributor']['mobile']);
				if($user['group_id'] == SUPER_DISTRIBUTOR || $user['group_id'] == DISTRIBUTOR || $user['group_id'] == RETAILER){
					$to_save = false;
					$msg = "You cannot make this mobile as your distributor";
				}
			}
		}

		if(!$to_save){
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_distributor','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.' </div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_distributor','ajax');
			}
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Distributor']['slab_id']);

			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			$this->render('confirm_distributor','ajax');
		}
		else {

			$this->data['Distributor']['created'] = date('Y-m-d H:i:s');
			$this->data['Distributor']['modified'] = date('Y-m-d H:i:s');
			$this->data['Distributor']['balance'] = 0;
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);
			$this->data['Distributor']['state'] = $state['0']['locator_state']['name'];

			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);
			$this->data['Distributor']['city'] = $city['0']['locator_city']['name'];

			if(!$exists){
				$user = $this->General->registerUser($this->data['Distributor']['mobile'],RETAILER_REG,DISTRIBUTOR);
				$user = $user['User'];
				$new_user = 1;
			}
			else if($user['group_id'] == MEMBER){
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = DISTRIBUTOR;
				$this->User->save($userData);//make already user to a distributor
			}
			$this->data['Distributor']['user_id'] = $user['id'];
			$this->data['Distributor']['parent_id'] = $this->info['id'];
			$this->data['Distributor']['target_amount'] = 25000;

			$this->Distributor->create();
			if ($this->Distributor->save($this->data)) {
				$mail_subject = "New Distributor Created";
				$this->General->makeOptIn247SMS($user['mobile']);

				$mail_body = "SuperDistributor: " . $this->info['company'] . "<br/>";
				$mail_body .= "Distributor: " . $this->data['Distributor']['company'];
				$mail_body .= "Address: " . $this->data['Distributor']['address'];
				$this->General->sendMails($mail_subject, $mail_body);
				$this->Shop->updateSlab($this->data['Distributor']['slab_id'],$this->Distributor->id,DISTRIBUTOR);

				$distributor = $this->data;

				/*$ret_user = $this->General->registerUser('1'.$distributor['Distributor']['mobile'],RETAILER_REG,RETAILER);
				 $ret_user = $ret_user['User'];

				 $this->data = null;
				 $this->data['Retailer']['user_id'] = $ret_user['id'];
				 $this->data['Retailer']['mobile'] = $distributor['Distributor']['mobile'];
				 $this->data['Retailer']['parent_id'] = $this->Distributor->id;
				 $this->data['Retailer']['toshow'] = 1;
				 $this->data['Retailer']['email'] = $distributor['Distributor']['email'];
				 $this->data['Retailer']['name'] = $distributor['Distributor']['name'];
				 $this->data['Retailer']['slab_id'] = RET_SLAB;
				 $this->data['Retailer']['shopname'] = $distributor['Distributor']['company'];
				 $this->data['Retailer']['address'] = $distributor['Distributor']['address'];
				 $this->data['Retailer']['created'] = date('Y-m-d H:i:s');
				 $this->data['Retailer']['modified'] = date('Y-m-d H:i:s');
				 $this->Retailer->create();
				 $this->Retailer->save($this->data);
				 $this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);
				 */
				//if($this->data['login'] == 'on'){
				$sms = "Congrats!!\nYou have become Distributor of Pay1. Your login details are below\nOnline Url: http://panel.pay1.in\nUserName: ".$distributor['Distributor']['mobile']."\nPassword: ".$user['syspass'];
				//$sms .= "\nFind below url to download distributor app: " . $this->General->createAppDownloadUrl(DISTRIBUTOR,1);
				//$sms .= "\n\nDefault Retailer credentials for you are below: \nUserName: 1".$distributor['Distributor']['mobile']."\nPassword: ".$ret_user['syspass'];
				//$sms .= "\nFind below url to download retailer app: " . $this->General->createAppDownloadUrl(RETAILER,1);
					
				$this->General->sendMessage($user['mobile'],$sms,'shops');
				//}

				//create default salesman with distributors phone number
				if(!empty($this->data['Distributor']['name'])){
					$sales = $distributor['Distributor']['name'];
				}
				else {
					$sales = "Default";
				}
				$this->Retailer->query("INSERT INTO salesmen (dist_id,name,mobile,tran_limit,balance,extra,created) VALUES ('".$this->Distributor->id."','".addslashes($sales)."','".$distributor['Distributor']['mobile']."','1000000','1000000','','".date('Y-m-d H:i:s')."')");

				$this->set('data',null);
				$this->render('/elements/shop_form_distributor','ajax');
			} else {
				$err_msg = '<div class="error_class">The Distributor could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_distributor','ajax');
			}
		}
	}

	function allotCards(){
		$this->render('allotcards');
	}

	function backAllotment(){
		$this->set('data',$this->data);
		$this->render('/elements/shop_allot_cards');
	}


	function allDistributor(){
		if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != RELATIONSHIP_MANAGER){
			$this->redirect('/shops/view');
		}

		if($this->Session->read('Auth.User.group_id') == ADMIN){
			$data=$this->Retailer->query("SELECT sum(st.amount) as amts,ref1_id from shop_transactions as st where st.type='".COMMISSION_SUPERDISTRIBUTOR."' AND st.date= '".date('Y-m-d')."' group by st.ref1_id");
			//}else if($this->Session->read('Auth.User.group_id') == RELATIONSHIP_MANAGER ){
			//$distributors = $this->Distributor->find('all',array('conditions' => array('parent_id' => $this->info['id']), 'order' => 'name asc'));
				
			//      $data=$this->Retailer->query("SELECT sum(st.amount) as amts,ref1_id from shop_transactions as st where st.type='".COMMISSION_DISTRIBUTOR."' AND st.date= '".date('Y-m-d')."' group by st.ref1_id");
		}else {
			$data=$this->Retailer->query("SELECT sum(st.amount) as amts,ref1_id from shop_transactions as st where st.type='".COMMISSION_DISTRIBUTOR."' AND st.date= '".date('Y-m-d')."' group by st.ref1_id");
		}
		$datas = array();
		foreach($data as $dt){
			$datas[$dt['st']['ref1_id']] = $dt['0']['amts'];
		}
		$this->set('datas',$datas);
		$this->render('alldistributor');
	}

	function retFilter(){
		if(!$this->Session->check('Auth.User')){
			$this->redirect('/shops/view');
		}

		$filter = $_REQUEST['filter'];
		$id = $_REQUEST['id'];
		$query = "";
		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			if($id == 0)$id = null;
			else if($id != null)$query = " AND retailers.maint_salesman = $id";
			$dist = $this->info['id'];
		}
		else {
			$dist = $id;
		}
			
		if($filter == 1){//top transacting last 7 days
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-8 days'))."' $query AND retailers.parent_id=$dist group by retailer_id having (avg(sale) > 1000)");
		}
		else if($filter == 2){//avg transacting last 7 days
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-8 days'))."' $query AND retailers.parent_id=$dist group by retailer_id having (avg(sale) <= 1000 AND avg(sale) > 500)");
		}
		else if($filter == 3){//low transacting last 7 days
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '".date('Y-m-d',strtotime('-8 days'))."' $query AND retailers.parent_id=$dist group by retailer_id having (avg(sale) <= 500)");
		}
		else if($filter == 4){//dropped in last 2 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-2 days'))."' group by retailer_id having (max(vendors_activations.date) = '".date('Y-m-d',strtotime('-2 days'))."')");
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-2 days'))."' group by retailer_id having (max(retailers_logs.date) = '".date('Y-m-d',strtotime('-2 days'))."')");
		}
		else if($filter == 5){//dropped in last 7 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-7 days'))."' group by retailer_id having (max(vendors_activations.date) >= '".date('Y-m-d',strtotime('-7 days'))."' AND max(vendors_activations.date) <= '".date('Y-m-d',strtotime('-2 days'))."')");
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-7 days'))."' group by retailer_id having (max(retailers_logs.date) >= '".date('Y-m-d',strtotime('-7 days'))."' AND max(retailers_logs.date) < '".date('Y-m-d',strtotime('-2 days'))."')");
		}
		else if($filter == 6){//dropped between last 7-14 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-14 days'))."' group by retailer_id having (max(vendors_activations.date) >= '".date('Y-m-d',strtotime('-14 days'))."' AND max(vendors_activations.date) < '".date('Y-m-d',strtotime('-7 days'))."')");
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-14 days'))."' group by retailer_id having (max(retailers_logs.date) >= '".date('Y-m-d',strtotime('-14 days'))."' AND max(retailers_logs.date) < '".date('Y-m-d',strtotime('-7 days'))."')");
		}
		else if($filter == 7){//dropped between last 14-30 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailer_id having (max(vendors_activations.date) >= '".date('Y-m-d',strtotime('-30 days'))."' AND max(vendors_activations.date) < '".date('Y-m-d',strtotime('-14 days'))."')");
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailer_id having (max(retailers_logs.date) >= '".date('Y-m-d',strtotime('-30 days'))."' AND max(retailers_logs.date) < '".date('Y-m-d',strtotime('-14 days'))."')");
		}
		else if($filter == 8){//dropped before 30 days
			//$ids = $this->Retailer->query("SELECT retailers.id FROM vendors_activations,retailers WHERE retailers.id = vendors_activations.retailer_id $query AND retailers.parent_id=$dist AND vendors_activations.date >= '".date('Y-m-d',strtotime('-2 days'))."' group by retailer_id having (max(vendors_activations.date) < '".date('Y-m-d',strtotime('-30 days'))."')");
			$ids = $this->Retailer->query("SELECT retailers.id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id $query AND retailers.parent_id=$dist AND retailers_logs.date >= '".date('Y-m-d',strtotime('-120 days'))."' group by retailer_id having (max(retailers_logs.date) < '".date('Y-m-d',strtotime('-30 days'))."')");
		}

		$idrs = array();
		foreach($ids as $idr){
			$idrs[] = $idr['retailers']['id'];
		}

		echo implode(",",$idrs);
		$this->autoRender = false;
	}

	function allRetailer($id = null){
		if(!$this->Session->check('Auth.User')){
			$this->redirect('/');
		}
			
		$query = "";
		$this->set('retailer_type','non-deleted');
		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			if($id == 0)$id = null;
			else if($id != null)$query = " AND retailers.maint_salesman = $id";
			$dist = $this->info['id'];
			$salesmen=$this->User->query("select name,id,mobile from salesmen WHERE dist_id = $dist AND mobile != '".$_SESSION['Auth']['User']['mobile']."' AND active_flag = 1 order by id");
			$this->set('salesmen',$salesmen);
			$this->set('sid',$id);
			$sid = $id;
		}else {
			$this->set('dist',$id);
			if($id != null){
				//$retailers = $this->General->getRetailerList($id,null,true);
				$distributors = $this->viewVars['distributors'];// already fetched in before controller so used used thos record
				//$this->printArray($distributors);
				$this->set('modelName','Retailer');
				//$this->set('records',$retailers);
				$distributor=array();
				foreach ($distributors as $d){//check given id is in rm's distribtors list
					if($d['Distributor']['id'] == $id ){
						$distributor = $d;
					}
				}
				if(empty($distributor)){// if rm is not belongs to the distribtors( given id )
					$this->redirect('/shops/view');
				}
				$dist = $id;
			}
		}

		$amountCollected = array();
		$averageResult=$this->Retailer->query("SELECT avg(sale) as avg_ret, retailer_id
                                                                           from 
                                                                                retailers_logs,retailers 
                                                                           WHERE 
                                                                                retailers.id = retailers_logs.retailer_id AND 
                                                                                retailers.parent_id = ".( empty($dist)? 0 : $dist )." AND 
                                                                                retailers_logs.date > '".date('Y-m-d',strtotime('-30 days'))."' 
                                                                                group by retailer_id");

		foreach($averageResult as $avg){
			$amountCollected[$avg['retailers_logs']['retailer_id']]['average'] = $avg['0']['avg_ret'];
		}
		$successToday=$this->Retailer->query("SELECT sum(va.amount) as amts,retailers.id from vendors_activations as va join retailers on(va.retailer_id=retailers.id AND retailers.parent_id = ".( isset($dist)? $dist : 0 ).") where retailers.toshow = 1 and  va.status != 2 and va.status != 3 AND va.date= '".date('Y-m-d')."' $query group by retailers.id");
		//$successToday=$this->Retailer->query("SELECT sum(st.amount) as amts,retailers.id from shop_transactions as st join retailers on(st.ref1_id=retailers.id AND retailers.parent_id = ".( isset($dist)? $dist : 0 ).") where retailers.toshow = 1 and  st.type='".RETAILER_ACTIVATION."' and st.confirm_flag = 1 AND st.date= '".date('Y-m-d')."' $query group by retailers.id");
		foreach($successToday as $ac){
			$amountCollected[$ac['retailers']['id']]['sale'] = $ac['0']['amts'];
		}

		$retailers = $this->General->getRetailerList(isset($dist)? $dist : 0,isset($sid)? $sid : 0,true);
		$transactions = array();
		$lastTrans=$this->Retailer->query("SELECT retailer_id FROM vendors_activations,retailers WHERE vendors_activations.date = '".date('Y-m-d')."' AND retailers.id = vendors_activations.retailer_id AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by vendors_activations.retailer_id");
		foreach($lastTrans as $ac){
			$transactions[$ac['vendors_activations']['retailer_id']] = date('Y-m-d');
		}
		$lastTrans=$this->Retailer->query("SELECT max(date) as maxDate,retailer_id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.sale > 0 AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by retailers_logs.retailer_id");
		foreach($lastTrans as $ac){
			if(!isset($transactions[$ac['retailers_logs']['retailer_id']]))
			$transactions[$ac['retailers_logs']['retailer_id']] = $ac['0']['maxDate'];
		}
		$this->set('lastTrans',$transactions);
		$this->set('records',$retailers);
		$this->set('amounts',$amountCollected);
		//print_r($amountCollected);
		$this->render('allretailer');
	}
        
        function deletedRetailer($id = null){
		if(!$this->Session->check('Auth.User')){
			$this->redirect('/');
		}
			
		$query = "";
                $this->set('retailer_type','deleted');
		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			if($id == 0)$id = null;
			else if($id != null)$query = " AND retailers.maint_salesman = $id";
			$dist = $this->info['id'];
			$salesmen=$this->User->query("select name,id,mobile from salesmen WHERE dist_id = $dist AND mobile != '".$_SESSION['Auth']['User']['mobile']."' AND active_flag = 1 order by id");
			$this->set('salesmen',$salesmen);
			$this->set('sid',$id);
			
		}else {
			$this->set('dist',$id);
			if($id != null){
				$retailers = $this->General->getRetailerList($id);
				$distributors = $this->viewVars['distributors'];// already fetched in before controller so used used thos record
				$this->set('modelName','Retailer');
				$this->set('records',$retailers);
				
				$distributor=array();
				foreach ($distributors as $d){//check given id is in rm's distribtors list
					if($d['Distributor']['id'] == $id ){
						$distributor = $d;
					}
				}
				if(empty($distributor)){// if rm is not belongs to the distribtors( given id )
					$this->redirect('/shops/view');
				}
				$dist = $id;
			}
		}

		$amountCollected = array();
		$averageResult=$this->Retailer->query("SELECT avg(sale) as avg_ret, retailer_id
                                                                           from 
                                                                                retailers_logs,retailers 
                                                                           WHERE 
                                                                                retailers.id = retailers_logs.retailer_id AND 
                                                                                retailers.parent_id = ".( empty($dist)? 0 : $dist )." AND 
                                                                                 
                                                                                retailers.toShow = 0
                                                                                group by retailer_id");//retailers_logs.date > '".date('Y-m-d',strtotime('-30 days'))."'

		foreach($averageResult as $avg){
			$amountCollected[$avg['retailers_logs']['retailer_id']]['average'] = $avg['0']['avg_ret'];
		}
		
		$successToday=$this->Retailer->query("SELECT sum(va.amount) as amts,retailers.id from vendors_activations as va join retailers on(va.retailer_id=retailers.id AND retailers.parent_id = ".( isset($dist)? $dist : 0 ).") where retailers.toshow = 1 and  va.status != 2 and va.status != 3 AND va.date= '".date('Y-m-d')."' $query group by retailers.id");
		foreach($successToday as $ac){
			$amountCollected[$ac['retailers']['id']]['sale'] = $ac['0']['amts'];
		}
                
                $qry = 1;
		if($id != null){
			$qry = "Retailer.maint_salesman = $id";
		}
                $retailObj = ClassRegistry::init('Retailer');
		$retailers =  $retailObj->find('all', array(
                                                'fields' => array('Retailer.*','users.mobile', 'sum(shop_transactions.amount) as xfer'),
                                                'conditions' => array('Retailer.parent_id' => $dist,'Retailer.toshow' => 0,$qry),
                                                'joins' => array(
                                                        array(
                                                                                'table' => 'users',
                                                                                'type' => 'left',
                                                                                'conditions'=> array('Retailer.user_id = users.id')
                                                        ),
                                                        array(
                                                                                'table' => 'shop_transactions',
                                                                                'type' => 'left',
                                                                                'conditions'=> array('Retailer.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 2')
                                                        )		
                                                        ),
                                                'order' => 'xfer desc, Retailer.shopname asc',
                                                'group'	=> 'Retailer.id'
                                                )
                                                );
                ////$this->General->getRetailerList(isset($dist)? $dist : 0 ,isset($sid)? $sid : 0);
		$transactions = array();
		$lastTrans=$this->Retailer->query("SELECT retailer_id FROM vendors_activations,retailers WHERE vendors_activations.date = '".date('Y-m-d')."' AND retailers.id = vendors_activations.retailer_id AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by vendors_activations.retailer_id");
		foreach($lastTrans as $ac){
			$transactions[$ac['vendors_activations']['retailer_id']] = date('Y-m-d');
		}
		$lastTrans=$this->Retailer->query("SELECT max(date) as maxDate,retailer_id FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers_logs.sale > 0 AND retailers.parent_id = ".( isset($dist)? $dist : 0 )." $query group by retailers_logs.retailer_id");
		foreach($lastTrans as $ac){
			if(!isset($transactions[$ac['retailers_logs']['retailer_id']]))
			$transactions[$ac['retailers_logs']['retailer_id']] = $ac['0']['maxDate'];
		}
		$this->set('lastTrans',$transactions);
		$this->set('records',$retailers);
		$this->set('amounts',$amountCollected);
		$this->render('allretailer');
	}
        
	function salesmanListing($id = null)
	{
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$salesmanListResult=$this->User->query("select sm.*,group_concat(sub.name) as subs from salesmen sm left join salesmen_subarea ssa on(ssa.salesmen_id=sm.id) left join subarea sub on(sub.id=ssa.subarea_id) WHERE sm.dist_id = ".$this->info['id']." AND sm.mobile != '".$_SESSION['Auth']['User']['mobile']."' AND active_flag = 1 group by sm.id order by sm.id asc  ");
		$topups = $this->User->query("SELECT salesman_transactions.salesman,sum(shop_transactions.amount) as amt FROM salesman_transactions inner join shop_transactions ON (shop_transactions.id = salesman_transactions.shop_tran_id) WHERE shop_transactions.ref1_id = ".$this->info['id']." AND collection_date = '".date('Y-m-d')."' AND salesman_transactions.payment_type = 2 group by salesman");
		$colls = $this->User->query("SELECT collection_amount,salesman FROM salesman_collections WHERE date = '".date('Y-m-d')."' AND payment_type = 2 AND distributor_id = ". $this->info['id']);

		$tops = array();
		foreach($topups as $top){
			$tops[$top['salesman_transactions']['salesman']] = $top['0']['amt'];
		}

		foreach($colls as $col){
			$tops[$col['salesman_collections']['salesman']] = $tops[$col['salesman_collections']['salesman']] - $col['salesman_collections']['collection_amount'];
		}
		$this->set('salesman',$salesmanListResult);
		$this->set('topups',$tops);
	}


	function deleteSubarea($subareaId)
	{
		$success=0;
		$smId=$_REQUEST['smId'];

		//exit;
		$this->User->query("delete from salesmen_subarea where salesmen_id=$smId and subarea_id=$subareaId");
		$success=1;
		echo $success;
		$this->autoRender=false;
	}


	function saveEditSm()
	{
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$smId=$_REQUEST['smId'];
		$limit = $_REQUEST['smLimit'];
		$newSmMobile=$_REQUEST['smMobile'];

		$smMobileResult=$this->User->query("select sm.mobile,sm.tran_limit,balance from salesmen sm where sm.id=$smId");
		$smMobile=$smMobileResult['0']['sm']['mobile'];
		$subareas=$_REQUEST['subAreaList'];
		$balance = $smMobileResult['0']['sm']['balance'];

		$chkDuplicateSmMobile=$this->User->query("select sm.mobile from salesmen sm where sm.mobile=$newSmMobile and sm.id!=$smId");
		if(empty($chkDuplicateSmMobile))
		{
			$success=1;
		}
		else
		{
			$success=0;
		}

		$diff = $limit - $smMobileResult['0']['sm']['tran_limit'];
		$balance = $balance + $diff;
		/*if($diff > 0)
			$balance = $balance + $diff;
			else
			$balance = $balance - abs($diff);*/
			
		$update=$this->User->query("update salesmen set name='".$_REQUEST['smName']."', balance=".$balance.",tran_limit=$limit,mobile='$newSmMobile' where id=$smId");
		$count=$this->mapSalesmanToSubarea($smId,$subareas);


		echo $success;

		$this->autoRender=false;
	}


	function editSalesman($mobile)
	{
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$salesmanResult=$this->User->query("select sm.* from salesmen sm where sm.mobile='$mobile' AND sm.dist_id = " . $this->info['id']);
		if(empty($salesmanResult)){
			$this->redirect(array('action' => 'salesmanListing'));
		}
		$this->set('smR',$salesmanResult);

		$salesmanExistingSubareaResult=$this->User->query("select sm.name,sm.id,sm.mobile,sm.balance,sub.name,sub.id from salesmen sm left join salesmen_subarea ssa on(ssa.salesmen_id=sm.id) left join subarea sub on(sub.id=ssa.subarea_id) where sm.mobile='$mobile'");
		$this->set('existingSA',$salesmanExistingSubareaResult);
		$this->set('smEditDeatils',$smDetails);
	}

	/*function retailerListing($id = null){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();

		$retailers = array();
		if($id == null)$this->set('empty',1);
		else {
		$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
		$this->set('distributor',$shop['company']);
		$this->set('distributor_id',$id);
		if($shop['parent_id'] == $this->info['id']){
		$this->Retailer->recursive = -1;
		$retailers = $this->Retailer->find('all', array(
		'fields' => array('Retailer.*', 'users.mobile', 'sum(shop_transactions.amount) as xfer'),
		'conditions' => array('Retailer.parent_id' => $id),
		'joins' => array(
		array(
		'table' => 'users',
		'type' => 'left',
		'conditions'=> array('Retailer.user_id = users.id')
		),
		array(
		'table' => 'shop_transactions',
		'type' => 'left',
		'conditions'=> array('Retailer.id = shop_transactions.ref2_id','shop_transactions.date = "'.date('Y-m-d').'"', 'shop_transactions.type = 2')
		)
		),
		'order' => 'Retailer.shopname asc',
		'group'	=> 'Retailer.id'
		)
		);
		}
		else {
		$retailers = array();
		}
		}
		$this->set('retailers',$retailers);
		$this->render('retailer_listing');
		}*/

	function editRetailer($type,$id){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR){
			$this->redirect('/');
		}
		if(!in_array($type, array('d','r')))
		$this->redirect(array('action' => 'allRetailer'));

		$cityName = "";
		$stateName = "";

		if($type == 'r'){
			$tableName = 'retailers';
			$modName = 'Retailer';
			$editData = $this->Retailer->find('all', array(
			'fields' => array('Retailer.*', 'slabs.name','users.mobile'),
			'conditions' => array('Retailer.id' => $id),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Retailer.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Retailer.user_id = users.id')
			)
			))
			);


			$subareaList=$this->Retailer->query("select id,name from subarea where area_id= " .  $editData['0']['Retailer']['area_id']);


			$this->set('subareaList',$subareaList);




			$city = $this->Retailer->query("SELECT locator_city.name,locator_city.id FROM locator_city,locator_area WHERE locator_area.id = " . $editData['0']['Retailer']['area_id'] . " AND locator_area.city_id = locator_city.id");
			$cityName = "";
			if(!empty($city)){
				$cityName = trim($city['0']['locator_city']['name']);
				$this->set('retCity',$city['0']['locator_city']['id']);
				$state = $this->Retailer->query("SELECT locator_state.name,locator_state.id FROM locator_state,locator_city WHERE locator_city.id = " . $city['0']['locator_city']['id'] . " AND locator_city.state_id = locator_state.id");
			}
			$stateName = "";
			if(!empty($state)){
				$stateName = trim($state['0']['locator_state']['name']);
				$this->set('retState',$state['0']['locator_state']['id']);
			}
		}

		if($type == 'd'){
			$tableName = 'distributors';
			$modName = 'Distributor';
			$editData = $this->Distributor->find('all', array(
			'fields' => array('Distributor.*', 'slabs.name','users.mobile'),
			'conditions' => array('Distributor.id' => $id),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Distributor.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Distributor.user_id = users.id')
			)

			))
			);
			$stateName = trim($editData['0']['Distributor']['state']);
		}


		if(!$this->Retailer->query("SELECT id FROM ".$tableName." WHERE parent_id = ".$this->info['id']." and id=".$id))
		$this->redirect(array('action' => 'allRetailer'));


		$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = (select id from locator_state where name='".$stateName."') ORDER BY name asc");
		$this->set('cities',$cities);

		$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = (select id from locator_city where name='".$cityName."') ORDER BY name asc");
		$this->set('areas',$areas);

		$sMen = $this->Retailer->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
		$this->set('sMen',$sMen);

		$rm_list = $this->Rm->query("SELECT id,name FROM rm WHERE super_dist_id = ".$this->info['id']." ORDER BY name asc");
		$this->set('rm_list',$rm_list);
		/*$fees = $this->Retailer->query("SELECT sum(salesman_transactions.collection_amount) as fee FROM salesman_transactions inner join shop_transactions on (salesman_transactions.shop_tran_id = shop_transactions.id) where salesman_transactions.payment_type = ".TYPE_SETUP." and shop_transactions.ref2_id = ".$id);
		 $this->set('fees',$fees);*/


		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('editData',$editData);
		$this->set('modName',$modName);

		$this->render('edit_form_retailer');
		//$this->autoRender = false;
	}

        function deleteRetailer(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR){
			$this->redirect('/');
		}
                
                $type = $_REQUEST['type'] ; 
                $id = $_REQUEST['id'] ; 
                $toShow = $_REQUEST['toShow'] ; 
                $block = $_REQUEST['block'] ; 
                
		if(!in_array($type, array('d','r')))
		$this->redirect(array('action' => 'allRetailer'));

               
                
		if($type == 'r'){
			$subareaList=$this->Retailer->query("UPDATE retailers SET toshow = $toShow ,  block_flag = $block where id= " . $id);
		}

		if($type == 'd'){
			$subareaList=$this->Retailer->query("UPDATE retailers SET toshow = $toShow ,  block_flag = $block where id= " . $id);
		}

		$this->autoRender = false;
                $res = array('status'=>'success','msg'=>'record deleted .');
                return(json_encode($res));
	}
        /*
         ({request:{options:{method:"post", asynchronous:true, contentType:"application/x-www-form-urlencoded", encoding:"UTF-8", parameters:{id:43, type:"r"}, evalJSON:true, evalJS:true, onSuccess:(function (transport)
                            {
                                    alert(transport.toSource());
                                    $("ret_"+rid).hide();
                            })}, transport:{}, url:"/shops/deleteRetailer", method:"post", parameters:{id:43, type:"r"}, body:"id=43&type=r", _complete:true}, transport:{}, readyState:4, status:200, statusText:"OK", responseText:"{\"status\":\"success\",\"msg\":\"record deleted .\"}", headerJSON:null, responseXML:null, responseJSON:null})
         */
	function showDetails($type,$id,$dist=null){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != RELATIONSHIP_MANAGER){
			$this->redirect('/');
		}
		if(!in_array($type, array('d','r')) || empty($id)){
			if($dist == null)
			$this->redirect(array('action' => 'allRetailer'));
			else
			$this->redirect(array('action' => 'retailerListing'));
		}

		if($type == 'r'){
			$tableName = 'retailers';
			$modName = 'Retailer';
			$this->Retailer->recursive = -1;
			$editData = $this->Retailer->find('all', array(
			'fields' => array('Retailer.*', 'slabs.name','users.mobile'),
			'conditions' => array('Retailer.id' => $id),
			'joins' => array(
			array(
							'table' => 'slabs',
							'type' => 'inner',
							'conditions'=> array('Retailer.slab_id = slabs.id')
			),
			array(
							'table' => 'users',
							'type' => 'inner',
							'conditions'=> array('Retailer.user_id = users.id')
			)
			))
			);

			if($dist != null){
				$parent = $editData['0']['Retailer']['parent_id'];
				$shop = $this->Shop->getShopDataById($parent,DISTRIBUTOR);
				if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR && ($this->$parent != $dist || $shop['parent_id'] != $this->info['id']))
				$this->redirect(array('action' => 'retailerListing'));
				else
				$this->redirect(array('action' => 'allRetailer'));
				$this->set('dist',$dist);
			}
				
			$state = isset ($editData['0']['Retailer']['state'])?$editData['0']['Retailer']['state']:"";
			$city = isset($editData['0']['Retailer']['city'])?$editData['0']['Retailer']['city']:"";
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $editData['0']['Retailer']['slab_id']);
			$area = $this->Retailer->query("SELECT name FROM locator_area WHERE id = " . $editData['0']['Retailer']['area_id']);

			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city);
			$this->set('state',$state);
			$this->set('area',$area['0']['locator_area']['name']);
		}

		if($type == 'd'){
			$tableName = 'distributors';
			$modName = 'Distributor';
			$this->Distributor->recursive = -1;
			$editData = $this->Distributor->find('all', array(
				'fields' => array('Distributor.*', 'slabs.name','users.mobile'),
				'conditions' => array('Distributor.id' => $id),
				'joins' => array(
			array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Distributor.slab_id = slabs.id')
			),
			array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Distributor.user_id = users.id')
			)

			))
			);
			$state = $editData['0']['Distributor']['state'];
			$city = $editData['0']['Distributor']['city'];
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $editData['0']['Distributor']['slab_id']);

			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city);
			$this->set('state',$state);
		}

		if(!$this->Retailer->query("SELECT id FROM ".$tableName." WHERE parent_id = ".$this->info['id']." and id=".$id) && $dist == null)
		$this->redirect(array('action' => 'allRetailer'));

		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('editData',$editData);
		$this->set('modName',$modName);
		//$this->render('showDetails');
	}

	function editRetValidation(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);
		if(empty($this->data['Retailer']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['pan_number'])){
			$empty[] = 'Pan Number';
			$empty_flag = true;
			$to_save = false;
		}

		if($this->data['Retailer']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['area_id'] == 0){
			$empty[] = 'Area';
			$empty_flag = true;
			$to_save = false;
		}

		if(empty($this->data['Retailer']['pin'])){
			$empty[] = 'Pin Code';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['shopname'])){
			$empty[] = 'Shop Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Retailer']['salesman'] == 0){
			$empty[] = 'Salesman';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['mobile_info'])){
			$empty[] = 'Mobile Info';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['app_type'])){
			$empty[] = 'App Type';
			$empty_flag = true;
			$to_save = false;
		}

		$sMen = $this->Retailer->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
		$this->set('sMen',$sMen);

		/*$fees = $this->Retailer->query("SELECT sum(shop_transactions.amount) as fee FROM shop_transactions join salesman_transactions on (salesman_transactions.shop_tran_id = shop_transactions.id) where salesman_transactions.payment_type = ".TYPE_SETUP." and shop_transactions.ref2_id = ".$this->data['Retailer']['id']." group by salesman_transactions.payment_type");
		 $this->set('fees',$fees);*/

		$this->set('appType',implode(",",$this->data['Retailer']['app_type']));

		if(!$to_save){
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
			$this->set('cities',$cities);

			$areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
			$this->set('areas',$areas);
			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Retailer']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Retailer']['city']);
			$areaName = $this->Retailer->query("SELECT name FROM locator_area WHERE id = ". $this->data['Retailer']['area_id']);
			if($empty_flag){
				$this->data['Retailer']['state'] = $stateName['0']['locator_state']['name'];
				$this->data['Retailer']['city'] = $cityName['0']['locator_city']['name'];
				$this->data['Retailer']['area_id'] = $this->data['Retailer']['area_id'];
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','r');
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/edit_form_ele_retailer','ajax');

			}
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Retailer']['slab_id']);
			$area = $this->Retailer->query("SELECT name FROM locator_area WHERE id = " . $this->data['Retailer']['area_id']);
			$subarea=$this->Retailer->query("SELECT name FROM subarea WHERE id = " . $this->data['Retailer']['subarea']);
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			$this->set('area',$area['0']['locator_area']['name']);
			$this->set('subarea',$subarea['0']['subarea']['name']);

			$sman = $this->Retailer->query("SELECT name FROM salesmen WHERE id = " . $this->data['Retailer']['salesman']);
			$this->set('salesman',$sman['0']['salesmen']['name']);

			$this->render('confirm_ret_edit','ajax');
		}
		else {
			$id = $this->data['Retailer']['id'];
			$name = $this->data['Retailer']['name'];
			$panNumber = $this->data['Retailer']['pan_number'];

			$email = $this->data['Retailer']['email'];
			$subareaId=$this->data['Retailer']['subarea'];

			$areaId = $this->data['Retailer']['area_id'];
			$pin = $this->data['Retailer']['pin'];
			$shopName = $this->data['Retailer']['shopname'];
			$Address = $this->data['Retailer']['address'];
			$slab = $this->data['Retailer']['slab_id'];
			$salesman = $this->data['Retailer']['salesman'];
			$kyc = addslashes($this->data['Retailer']['kyc']);
			$mInfo = addslashes($this->data['Retailer']['mobile_info']);
			$appType = addslashes($this->data['Retailer']['app_type']);

			//old slab id
			$oldSlabQ = $this->Distributor->query("select slab_id from retailers where id=".$id);
			$oldSlabId = $oldSlabQ['0']['retailers']['slab_id'];
			//

			$modified = date('Y-m-d H:i:s');

			$go = 0;
			if($this->Retailer->query("SELECT id FROM retailers WHERE parent_id = ".$this->info['id']." and id=".$id)){
				if ($this->Distributor->query("update retailers set app_type= '".$appType."',mobile_info= '".$mInfo."', kyc= '".$kyc."',salesman= '".$salesman."',name='".$name."',pan_number='".$panNumber."', email='".$email."', area_id='".$areaId."', pin='".$pin."', shopname='".$shopName."', address='".$Address."', slab_id='".$slab."', modified='".$modified."',subarea_id='".$subareaId."' where id=".$id)) {
					if($oldSlabId != $slab)
					$this->Shop->updateSlab($slab,$id,RETAILER);
					$go = 1;
				}else{
					$err = 'The Retailer could not be saved. Please, try again.';
				}
			}else{
				$err = "You don't have permission to edit this retailer.";
			}

			if ($go == 1) {
				$this->set('data',null);
				$retailer = $this->Retailer->find('all', array(
				'fields' => array('Retailer.*', 'slabs.name','users.mobile', 'sum(shop_transactions.amount)'),
				'conditions' => array('Retailer.parent_id' => $this->info['id']),
				'joins' => array(
				array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Retailer.slab_id = slabs.id')
				),
				array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Retailer.user_id = users.id')
				),
				array(
								'table' => 'shop_transactions',
								'type' => 'left',
								'conditions'=> array('Retailer.id = shop_transactions.ref2_id','datediff(now(),shop_transactions.timestamp) < 1 ', 'shop_transactions.type = 2')
				)
					
				),
				'order' => 'Retailer.name asc',
				'group'	=> 'Retailer.id')
				);

				$this->set('records',$retailer);
				$this->render('/elements/allRetailers','ajax');
			} else {
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','r');
				$err_msg = '<div class="error_class">'.$err.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}
	}

	function editDistValidation(){
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR){
			$this->redirect('/');
		}
		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);
		if(empty($this->data['Distributor']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}

		if(empty($this->data['Distributor']['company'])){
			$empty[] = 'Company Name';
			$empty_flag = true;
			$to_save = false;
		}



		if($this->data['Distributor']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['Distributor']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
		}



		if(empty($this->data['Distributor']['area_range'])){
			$empty[] = 'Area Range';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Distributor']['pan_number'])){
			$empty[] = 'PAN Number';
			$empty_flag = true;
			$to_save = false;
		}
		if(!$to_save){
			$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Distributor']['state']." ORDER BY name asc");
			$this->set('cities',$cities);
			$stateName = $this->Retailer->query("SELECT name FROM locator_state WHERE id = ". $this->data['Distributor']['state']);
			$cityName = $this->Retailer->query("SELECT name FROM locator_city WHERE id = ". $this->data['Distributor']['city']);
			//$data['Retailer']['state'] = $stateName['0']['locator_state']['name'];

			if($empty_flag){
				$this->data['Distributor']['state'] = $stateName['0']['locator_state']['name'];
				$this->data['Distributor']['city'] = $cityName['0']['locator_city']['name'];
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','d');
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}
		else if($confirm == 0 && $to_save){
			$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);
			$city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);
			$slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Distributor']['slab_id']);
			$rm = $this->Rm->query("SELECT name FROM rm WHERE id = " . $this->data['Distributor']['rm_id']);
			$this->set('slab',$slab['0']['slabs']['name']);
			$this->set('city',$city['0']['locator_city']['name']);
			$this->set('state',$state['0']['locator_state']['name']);
			if(empty($rm)){
				$this->set('rm_name',"");
			}else{
				$this->set('rm_name',$rm['0']['rm']['name']);
			}
			$this->set('rm_id',$this->data['Distributor']['rm_id']);
			$this->set('type','d');
			$this->render('confirm_dist_edit','ajax');
		}
		else {
			$id = $this->data['Distributor']['id'];
			$name = $this->data['Distributor']['name'];
			$panNumber = $this->data['Distributor']['pan_number'];
			$companyName = $this->data['Distributor']['company'];
			$email = $this->data['Distributor']['email'];


			$stateQ = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Distributor']['state']);
			$state = $stateQ['0']['locator_state']['name'];

			$cityQ = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Distributor']['city']);
			$city = $cityQ['0']['locator_city']['name'];

			$rmQ = $this->Retailer->query("SELECT name FROM rm WHERE id = " . $this->data['Distributor']['rm_id']);
			if(empty($rmQ)){
				$rm_name = "";
			}else{
				$rm_name = $rmQ['0']['rm']['name'];
			}

				

			$areaRange = $this->data['Distributor']['area_range'];
			$compAddress = $this->data['Distributor']['address'];
			$slab = $this->data['Distributor']['slab_id'];
				

			//old slab id
			$oldSlabQ = $this->Distributor->query("SELECT distributors.slab_id,distributors.rm_id,users.mobile FROM distributors,users WHERE users.id = distributors.user_id AND distributors.id=".$id);
			$oldSlabId = $oldSlabQ['0']['distributors']['slab_id'];
			$oldRmId = $oldSlabQ['0']['distributors']['rm_id'];
			$distMobile = $oldSlabQ['0']['users']['mobile'];
			//
			$modified = date('Y-m-d H:i:s');
			$go = 0;
			if($this->Retailer->query("SELECT distributors.id,distributors.company,users.mobile FROM distributors,users WHERE users.id = distributors.user_id AND distributors.parent_id = ".$this->info['id']." and distributors.id=".$id)){
				if ($this->Distributor->query("update distributors set name='".$name."',pan_number='".$panNumber."', company='".$companyName."', email='".$email."', state='".$state."', city='".$city."', area_range='".$areaRange."', address='".$compAddress."', slab_id='".$slab."', rm_id=".$this->data['Distributor']['rm_id'].", modified='".$modified."' where id=".$id)) {
					if($oldSlabId != $slab){
						$this->Shop->updateSlab($slab,$id,DISTRIBUTOR);
						$retailers = $this->Retailer->query("SELECT id FROM retailers WHERE parent_id = $id");
						foreach($retailers as $ret){
							$this->Shop->updateSlab($slab,$ret['retailers']['id'],RETAILER);
						}
						$this->Retailer->query("UPDATE retailers SET slab_id = $slab WHERE parent_id = $id");
					}
					$go = 1;
					if(!empty($this->data['Distributor']['rm_id']) && $oldRmId != $this->data['Distributor']['rm_id'] ){// execute this block only if rm_id updated
						$rm_details = $this->Retailer->query("SELECT * FROM rm WHERE id=".$this->data['Distributor']['rm_id']);
						if(!empty($rm_details)){
							//-------------- Send email to admin when rm added with distributor ----------
							// variable array contains the values which is to be parsed in email_body
							$varParseArr = array (
                                                     'rm_name'             =>  $rm_details[0]['rm']['name'],
                                                     'super_distributor_company'  =>  $this->info['name'],
													 'distributor_company'  => $companyName,
							);
							$this->General->sendTemplateEmailToAdmin("emailToAdminOnRmAddWithDistributor",$varParseArr);
							//----------------------------------------------------------------------------

							//---- Send SMS ( welcome msg ) to distributor(who became RM) on RM Add with distributor ------
							// variable array contains the values which is to be parsed in sms_body
							$varParseArr = array (
                                                     'rm_mobile'           =>  $rm_details[0]['rm']['mobile'],
                                                     'rm_name'             =>  $rm_details[0]['rm']['name'],                                                
							);
							$this->General->sendTemplateSMSToMobile($distMobile,"smsToDistributorOnRmAddWithDistributor",$varParseArr);
							//----------------------------------------------------------------------------
						}
					}
					 
				}else{
					$err = 'The Distributor could not be saved. Please, try again.';
				}
			}else{
				$err = "You don't have permission to edit this distributor.";
			}

			if ($go == 1) {
				$this->set('data',null);
				$distributors = $this->Distributor->find('all', array(
				'fields' => array('Distributor.*', 'slabs.name','users.mobile','rm.name', 'sum(shop_transactions.amount) as xfer'),
				'conditions' => array('Distributor.parent_id' => $this->info['id']),
				'joins' => array(
				array(
								'table' => 'slabs',
								'type' => 'inner',
								'conditions'=> array('Distributor.slab_id = slabs.id')
				),
				array(
								'table' => 'users',
								'type' => 'inner',
								'conditions'=> array('Distributor.user_id = users.id')
				),
				array(
								'table' => 'rm',
								'type' => 'left',
								'conditions'=> array('Distributor.rm_id = rm.id')
				),
				array(
								'table' => 'shop_transactions',
								'type' => 'left',
								'conditions'=> array('Distributor.id = shop_transactions.ref2_id','datediff(now(),shop_transactions.timestamp) < 1 ', 'shop_transactions.type = 1')
				)
					
				),
				'order' => 'Distributor.name asc',
				'group'	=> 'Distributor.id'
				)
				);

				$this->set('records',$distributors);
				if($this->data['trans_type'] == 'd'){
					$this->render('/elements/allDistributors','ajax');
				}else{
					$this->render('/elements/allRetailers','ajax');
				}
				//$this->render('/elements/allRetailers','ajax');

			} else {
				$tArr[0] = $this->data;
				$this->set('editData',$tArr);
				$this->set('type','d');
				$err_msg = '<div class="error_class">'.$err.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/edit_form_ele_retailer','ajax');
			}
		}
	}

	function formRetailer(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$this->render('form_retailer');
	}

	function formSalesman(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$states = $this->User->query("SELECT id,name FROM locator_state ORDER BY name asc");
		$this->render('form_salesman');
		$this->set('states',$states);
	}
	function formRm(){
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR){
			$this->redirect('/');
		}
		$states = $this->User->query("SELECT id,name FROM locator_state ORDER BY name asc");
		$this->render('form_rm');
		$this->set('states',$states);
	}

	function formSetUpFee(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$sMen = $this->Retailer->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");

		$fee = $this->Retailer->query("SELECT sum(amount) as fee,ref2_id FROM shop_transactions where type = ".SETUP_FEE." group by ref2_id");
		$fees = array();
		foreach($fee as $f){
			$fees[$f['shop_transactions']['ref2_id']] = $f['0']['fee'];
		}

		$this->set('fees',$fees);
		$this->set('sMen',$sMen);
		$this->render('form_setupfee');
	}

	function backRetailer(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}

		$this->set('data',$this->data);
		/*$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
		 $this->set('cities',$cities);
		 $areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
		 $this->set('areas',$areas);*/
		$this->render('/elements/shop_form_retailer');
	}

	function backSalesman(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}

		$this->set('data',$this->data);
		$this->render('/elements/shop_form_salesman');
	}

	function backSetup(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}

		$sMen = $this->Retailer->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
		$this->set('sMen',$sMen);
		$this->set('data',$this->data);
		$this->render('/elements/shop_form_setup');
	}

	function addSetUpFee(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}

		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];

		$this->set('data',$this->data);
		if($this->data['SalesmanTransaction']['retailer'] == 0){
			$empty[] = 'Retailer';
			$empty_flag = true;
			$to_save = false;
		}
		if($this->data['SalesmanTransaction']['salesman'] == 0){
			$empty[] = 'Salesman';
			$empty_flag = true;
			$to_save = false;
		}


		if(empty($this->data['SalesmanTransaction']['amount'])){
			$empty[] = 'Amount';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['SalesmanTransaction']['amount'] = trim($this->data['SalesmanTransaction']['amount']);
			preg_match('/^[0-9]{1,}$/',$this->data['SalesmanTransaction']['amount'],$matches,0);
			if(empty($matches)){
				$msg = "Enter valid Amount";
				$to_save = false;
			}
		}

		if($this->data['SalesmanTransaction']['payment_mode'] == 0){
			$empty[] = 'Payment Mode';
			$empty_flag = true;
			$to_save = false;
		}

		if(empty($this->data['SalesmanTransaction']['collection_date'])){
			$empty[] = 'Collection Date';
			$empty_flag = true;
			$to_save = false;
		}

		if(!$to_save){
			$sMen = $this->Retailer->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
			$this->set('sMen',$sMen);
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_setup','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_setup','ajax');
			}
		}else if($confirm == 0 && $to_save){
			$ret = $this->Retailer->query("SELECT mobile,name FROM retailers WHERE id = " . $this->data['SalesmanTransaction']['retailer']);
			$sman = $this->Retailer->query("SELECT name FROM salesmen WHERE id = " . $this->data['SalesmanTransaction']['salesman']);

			$this->set('retailer',$ret['0']['retailers']['name']." (".$ret['0']['retailers']['mobile'].")");
			$this->set('salesman',$sman['0']['salesmen']['name']);
			$this->render('/shops/confirm_setup','ajax');
		}else {
			$shpTranId = $this->Shop->shopTransactionUpdate(SETUP_FEE,$this->data['SalesmanTransaction']['amount'],$this->info['id'],$this->data['SalesmanTransaction']['retailer']);
			$this->data['SalesmanTransaction']['shop_tran_id'] = $shpTranId;

			$colldate = explode("-",$this->data['SalesmanTransaction']['collection_date']);
			$this->data['SalesmanTransaction']['collection_date'] = $colldate['2']."-".$colldate['1']."-".$colldate['0'];
			$this->data['SalesmanTransaction']['created'] = date('Y-m-d H:i:s');
			$this->data['SalesmanTransaction']['payment_type'] = TYPE_SETUP;
			$this->data['SalesmanTransaction']['confirm_flag'] = 1;
			$this->SalesmanTransaction->create();
			if ($this->SalesmanTransaction->save($this->data)) {
				$this->set('data',null);
				$sMen = $this->Retailer->query("SELECT id,name,mobile FROM salesmen where dist_id = ".$this->info['id']."");
				$this->set('sMen',$sMen);
				$this->render('/elements/shop_form_setup','ajax');
			} else {
				$err_msg = '<div class="error_class">The transaction could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_setup','ajax');
			}
		}
	}


	function createSalesman(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}

		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];

		$subareas=$this->data['subArea1'];
		$this->set('data',$this->data);
		if(empty($this->data['Salesman']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Salesman']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Salesman']['mobile'] = trim($this->data['Salesman']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Salesman']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}

		if(empty($this->data['Salesman']['tran_limit'])){
			$empty[] = 'Transaction Limit';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Salesman']['tran_limit'] = trim($this->data['Salesman']['tran_limit']);
			preg_match('/^[0-9]{1,}$/',$this->data['Salesman']['tran_limit'],$matches,0);
			if(empty($matches)){
				$msg = "Enter valid Transaction Limit";
				$to_save = false;
			}
		}

		if($to_save){
			$exists = $this->General->checkIfSalesmanExists($this->data['Salesman']['mobile']);

			$retCheck = $this->User->query("SELECT * FROM retailers WHERE mobile='".$this->data['Salesman']['mobile']."'");

			if($exists){
				$msg = "Mobile number already exists.";
				$to_save = false;
			}
			else if(!empty($retCheck)){
				$msg = "You cannot make this mobile as your salesman";
				$to_save = false;
			}
		}

		if(!$to_save){
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_salesman','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_salesman','ajax');
			}
		}else if($confirm == 0 && $to_save){

			$this->render('/shops/confirm_salesman','ajax');
		}else {

			$this->data['Salesman']['created'] = date('Y-m-d H:i:s');
			$this->data['Salesman']['dist_id'] = $this->info['id'];
			$this->data['Salesman']['balance'] = $this->data['Salesman']['tran_limit'];
			$this->Salesman->create();

			if ($this->Salesman->save($this->data)) {
				//echo "Count from function is ".$count;
				$this->General->makeOptIn247SMS($this->data['Salesman']['mobile']);
				$sms = "Congrats!!\nYou have become Salesman of Pay1.";

				$this->General->sendMessage($this->data['Salesman']['mobile'],$sms,'shops');
				$count=$this->mapSalesmanToSubarea($this->data['Salesman']['id'],$subareas);
				$this->set('data',null);
				$this->render('/elements/shop_form_salesman','ajax');
			} else {
				$err_msg = '<div class="error_class">The Salesman could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_salesman','ajax');
			}
		}
	}
	function createRm(){
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR){
			$this->redirect('/');
		}

		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];

		$this->set('data',$this->data);
		if(empty($this->data['Rm']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Rm']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}else {
			$this->data['Rm']['mobile'] = trim($this->data['Rm']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Rm']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}


		if($to_save){
			$exists = $this->General->checkIfUserExists($this->data['Rm']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Rm']['mobile']);
				if(($user['group_id'] != MEMBER )){//|| $user['group_id'] == RELATIONSHIP_MANAGER
					$to_save = false;
					$msg = "You cannot make this mobile as your relationship manager ( RM ).";
				}
			}
		}

		if(!$to_save){
			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_rm','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_rm','ajax');
			}
		}else if($confirm == 0 && $to_save){
			$this->render('/shops/confirm_rm','ajax');
		}else {
				
			// check if user already exist exist
			// $exists = $this->General->checkIfUserExists($this->data['Rm']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Rm']['mobile']);
				if(($user['group_id'] != MEMBER )){//|| $user['group_id'] == RELATIONSHIP_MANAGER
					$to_save = false;
					$msg = "You cannot make this mobile as your relationship manager ( RM ).";
				} else {
					$userData['User']['id'] = $user['id'];
					$userData['User']['group_id'] = RELATIONSHIP_MANAGER;
					$this->User->save($userData);//make existing user to a RelationShip Manager

				}
			}else{
				$user = $this->General->registerUser($this->data['Rm']['mobile'],ONLINE_REG,RELATIONSHIP_MANAGER);

				$user = $user['User'];
			}

			//-------------- Send email to admin on new rm registration ----------------
			// variable array contains the values which is to be parsed in email_body
			$varParseArr = array (
                             'rm_mobile'           =>  $this->data['Rm']['mobile'],
                             'rm_name'             =>  $this->data['Rm']['name'],
                             'distributor_company' =>  "None"
                             );
                             $this->General->sendTemplateEmailToAdmin("emailToAdminOnRmRegistration",$varParseArr);
                             //----------------------------------------------------------------------------

                             $this->data['Rm']['created'] = date('Y-m-d H:i:s');
                             $this->data['Rm']['super_dist_id'] = $this->info['id'];
                             $this->data['Rm']['user_id'] = $user['id'];
                             $this->Rm->create();
                             	
                             if ($this->Rm->save($this->data)) {

                             	//--------- Send SMS ( welcome msg ) to rm on rm registration ----------------
                             	// variable array contains the values which is to be parsed in sms_body
                             	$varParseArr = array (
                                     'rm_mobile'           =>  $this->data['Rm']['mobile'],
                                     'rm_name'             =>  $this->data['Rm']['name'],
                                     'distributor_company' =>  "None"
                                     );
                                     $this->General->sendTemplateSMSToMobile($this->data['Rm']['mobile'],"smsToRMOnRmRegistration",$varParseArr);
                                     //----------------------------------------------------------------------------

                                     //----- Send SMS ( rm registered ) to super distributors on rm registration --
                                     // variable array contains the values which is to be parsed in sms_body
                                     $varParseArr = array (
                                     'rm_mobile'           =>  $this->data['Rm']['mobile'],
                                     'rm_name'             =>  $this->data['Rm']['name'],
                                     'distributor_company' =>  "None"
                                     );
                                     $this->General->sendTemplateSMSToMobile($_SESSION['Auth']['User']['mobile'],"smsToSuperDistOnRmRegistration",$varParseArr);
                                     //----------------------------------------------------------------------------

                                     //$count=$this->mapSalesmanToSubarea($this->data['Salesman']['id'],$subareas);
                                     $this->set('data',null);
                                     $this->render('/elements/shop_form_rm','ajax');
                             } else {
                             	$err_msg = '<div class="error_class">The rm could not be saved. Please, try again.</div>';
                             	$this->Session->setFlash(__($err_msg, true));
                             	$this->render('/elements/shop_form_rm','ajax');
                             }
		}
	}
	function mapSalesmanToSubarea($sId,$subArea)
	{
		/*$sIdResult=$this->User->query("select id from salesmen where mobile='$sMobile'");
		 $sId=$sIdResult['0']['salesmen']['id'];*/

		$subareaList =split(" ",$subArea);

		$count=0;
		foreach($subareaList as $s)
		{

			$subareaIdResult=$this->User->query("select id from subarea where name='$subareaList[$count]'");
			$subAreaId=$subareaIdResult['0']['subarea']['id'];

			$chkIfExistResult=$this->User->query("select id from salesmen_subarea where  salesmen_id=$sId and subarea_id=$subAreaId ");
			if(empty($chkIfExistResult))
			{
				$this->User->query("insert into salesmen_subarea (salesmen_id,subarea_id) values($sId,$subAreaId)");
				$count++;
			}
		}

		return $count;
	}



	function salesmanTran($frm=null,$to=null,$id=null){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		if(is_null($frm)){
			$frm = date('d-m-Y');
		}
		if(is_null($to)){
			$to = date('d-m-Y');
		}
		$query = "";
		if($id != null && $id != 0){
			$query = " AND salesman = $id";
		}

		$frmArr = date('Y-m-d',strtotime($frm));
		$toArr = date('Y-m-d',strtotime($to));

		if($id != 0){
			$salesman = $this->Retailer->query("SELECT * FROM salesmen where dist_id = ".$this->info['id']." AND id = $id");
			$this->set('sinfo',$salesman);
		}
		$salesmans = $this->Retailer->query("SELECT * FROM salesmen where dist_id = ".$this->info['id']." AND active_flag = 1 order by id");

		$data = array();
		$collections = $this->Retailer->query("SELECT salesman_collections.* FROM salesman_collections WHERE salesman_collections.distributor_id = ".$this->info['id']." AND salesman_collections.date >= '$frmArr' AND salesman_collections.date <= '$toArr' $query order by salesman_collections.date");
                $topups = $this->Retailer->query("SELECT salesman_transactions.salesman,salesman_transactions.collection_date,salesman_transactions.payment_type,if(salesman_transactions.payment_type = 1,sum(salesman_transactions.collection_amount),sum(shop_transactions.amount)) as amt FROM salesman_transactions inner join salesmen ON (salesman_transactions.salesman = salesmen.id) inner join shop_transactions ON (shop_transactions.id = salesman_transactions.shop_tran_id) WHERE shop_transactions.ref1_id = ".$this->info['id']." AND collection_date >= '$frmArr' AND collection_date <= '$toArr' $query group by salesman,collection_date,payment_type");
                foreach($collections as $coll){
			if(empty($coll['salesman_collections']['collection_amount'])){
				$coll['salesman_collections']['collection_amount'] = 0;
			}
			$data[$coll['salesman_collections']['date']][$coll['salesman_collections']['salesman']]['collection'][$coll['salesman_collections']['payment_type']] = $coll['salesman_collections']['collection_amount'];
		}
		foreach($topups as $topup){
			if(empty($topup['0']['amt'])){
				$topup['0']['amt'] = 0;
			}
			$data[$topup['salesman_transactions']['collection_date']][$topup['salesman_transactions']['salesman']]['topup'][$topup['salesman_transactions']['payment_type']] = $topup['0']['amt'];
		}
		$this->set('data',$data);
		$this->set('salesmans',$salesmans);
		$this->set('from',$frm);
		$this->set('to',$to);
		$this->set('id',$id);
		$this->render('form_salesman_tran');
	}

	function addSalesmanCollection(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$id = trim($_REQUEST['id']);
		$date = trim($_REQUEST['date']);

		$topup = trim($_REQUEST['topup']);
		$setup = trim($_REQUEST['setup']);
		$cash = trim($_REQUEST['cash']);
		$cheque = trim($_REQUEST['cheque']);

		if(empty($topup))$topup = 0;
		if(empty($setup))$setup = 0;
		if(empty($cash))$cash = 0;
		if(empty($cheque))$cheque = 0;

		$data = $this->Retailer->query("SELECT * FROM salesman_collections WHERE date = '$date' AND salesman = $id order by payment_type");
		$salesman = $this->Retailer->query("SELECT salesmen.dist_id FROM salesmen WHERE id = $id");

		if($cash + $cheque == $topup + $setup){
			if(empty($data) && $salesman['0']['salesmen']['dist_id'] == $this->info['id']){
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',1,$setup,'".date('Y-m-d H:i:s')."')");
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',2,$topup,'".date('Y-m-d H:i:s')."')");
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',3,$cash,'".date('Y-m-d H:i:s')."')");
				$this->Retailer->query("INSERT INTO salesman_collections (salesman,distributor_id,date,payment_type,collection_amount,created) VALUES ($id,".$this->info['id'].",'$date',4,$cheque,'".date('Y-m-d H:i:s')."')");

				$diff_top = $topup;
				$diff_set = $setup;
				$this->Retailer->query("UPDATE salesmen SET balance = balance + $diff_top, setup_pending = setup_pending - $diff_set WHERE id = $id");
				echo "done";
			}
			else if(!empty($data) && $data[0]['salesman_collections']['distributor_id'] == $this->info['id']){
				$diff_top = $topup - $data[1]['salesman_collections']['collection_amount'];
				$diff_set = $setup - $data[0]['salesman_collections']['collection_amount'];
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$topup WHERE id = " . $data[1]['salesman_collections']['id']);
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$setup WHERE id = " . $data[0]['salesman_collections']['id']);
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$cash WHERE id = " . $data[2]['salesman_collections']['id']);
				$this->Retailer->query("UPDATE salesman_collections SET collection_amount=$cheque WHERE id = " . $data[3]['salesman_collections']['id']);

				$this->Retailer->query("UPDATE salesmen SET balance = balance + $diff_top, setup_pending = setup_pending - $diff_set WHERE id = $id");
				echo "done";
			}
			else {
				echo "Sorry, you cannot edit this entry";
			}
		}
		else {
			echo "Sum of cash & cheque should match with your total topup & setup collections";
		}

		$this->autoRender = false;
	}

	function createRetailer(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}

		$msg = "";
		$empty = array();
		$empty_flag = false;
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
			
		$this->set('data',$this->data);
		if(empty($this->data['Retailer']['name'])){
			$empty[] = 'Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['mobile'])){
			$empty[] = 'Mobile';
			$empty_flag = true;
			$to_save = false;
		}
		else {
			$this->data['Retailer']['mobile'] = trim($this->data['Retailer']['mobile']);
			preg_match('/^[7-9][0-9]{9}$/',$this->data['Retailer']['mobile'],$matches,0);
			if(empty($matches)){
				$msg = "Mobile Number is not valid";
				$to_save = false;
			}
		}
		/*if($this->data['Retailer']['state'] == 0){
			$empty[] = 'State';
			$empty_flag = true;
			$to_save = false;
			}
			if($this->data['Retailer']['city'] == 0){
			$empty[] = 'City';
			$empty_flag = true;
			$to_save = false;
			}
			if($this->data['Retailer']['area_id'] == 0){
			$empty[] = 'Area';
			$empty_flag = true;
			$to_save = false;
			}

			if(empty($this->data['Retailer']['pin'])){
			$empty[] = 'Pin Code';
			$empty_flag = true;
			$to_save = false;
			}*/
		if(empty($this->data['Retailer']['shopname'])){
			$empty[] = 'Shop Name';
			$empty_flag = true;
			$to_save = false;
		}
		if(empty($this->data['Retailer']['address'])){
			$empty[] = 'Address';
			$empty_flag = true;
			$to_save = false;
		}

		if($to_save){
			if(!isset($this->data['Retailer']['rental_flag'])){
				$this->data['Retailer']['rental_flag'] = 0;
			}

			$exists = $this->General->checkIfUserExists($this->data['Retailer']['mobile']);
			if($exists){
				$user = $this->General->getUserDataFromMobile($this->data['Retailer']['mobile']);
				if($user['group_id'] == SUPER_DISTRIBUTOR || $user['group_id'] == DISTRIBUTOR || $user['group_id'] == RETAILER || $user['group_id'] == ADMIN){
					$to_save = false;
					$msg = "You cannot make this mobile as your retailer";
				}
			}

			if($this->info['retailer_creation'] == 0){
				$msg = "You cannot create a retailer, contact pay1";
				$to_save = false;
			}

			$count = count($this->retailers);
			if($this->info['retailer_limit'] > 0 && $count == $this->info['retailer_limit']){
				$msg = "You have reached your distributor limit. You cannot create retailer now";
				$to_save = false;
			}

			if($this->data['Retailer']['rental_flag'] == 0 && $this->info['kits'] == 0){
				$msg = "You have 0 kits left. Buy more retailer kits to enjoy this benefit";
				$to_save = false;
			}
		}

		if(!$to_save){
			/*$cities = $this->Retailer->query("SELECT id,name FROM locator_city WHERE state_id = ". $this->data['Retailer']['state']." ORDER BY name asc");
			 $this->set('cities',$cities);
			 	
			 $areas = $this->Retailer->query("SELECT id,name FROM locator_area WHERE city_id = ". $this->data['Retailer']['city']." ORDER BY name asc");
			 $this->set('areas',$areas);
			 	
			 $subareas=$this->Retailer->query("select id,name from subarea where area_id=".$this->data['Retailer']['area_id']);
			 $this->set('subareas',$subareas);*/

			if($empty_flag){
				$err_msg = '<div class="error_class">'.implode(", ",$empty).' cannot be set empty</div>';
				$this->Session->setFlash($err_msg, true);
				$this->render('/elements/shop_form_retailer','ajax');
			}
			else {
				$err_msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_retailer','ajax');
			}
		}
		else if($confirm == 0 && $to_save){
			/*$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);
			 $city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);
			 $slab = $this->Retailer->query("SELECT name FROM slabs WHERE id = " . $this->data['Retailer']['slab_id']);
			 $area = $this->Retailer->query("SELECT name FROM locator_area WHERE id = " . $this->data['Retailer']['area_id']);
			 	
			 	
			 $this->set('slab',$slab['0']['slabs']['name']);
			 $this->set('city',$city['0']['locator_city']['name']);
			 $this->set('state',$state['0']['locator_state']['name']);
			 $this->set('area',$area['0']['locator_area']['name']);*/

			$this->render('/shops/confirm_retailer','ajax');
		}
		else {

			$this->data['Retailer']['created'] = date('Y-m-d H:i:s');
			$this->data['Retailer']['modified'] = date('Y-m-d H:i:s');
			$this->data['Retailer']['balance'] = 0;
			/*$state = $this->Retailer->query("SELECT name FROM locator_state WHERE id = " . $this->data['Retailer']['state']);
			 $this->data['Retailer']['state'] = $state['0']['locator_state']['name'];
			 	
			 $city = $this->Retailer->query("SELECT name FROM locator_city WHERE id = " . $this->data['Retailer']['city']);
			 $this->data['Retailer']['city'] = $city['0']['locator_city']['name'];*/

			$default = $this->Retailer->query("SELECT id FROM salesmen WHERE mobile = '" . $this->Session->read('Auth.User.mobile')."'");

			if(!empty($default)){
				$this->data['Retailer']['salesman'] = $default['0']['salesmen']['id'];
				$this->data['Retailer']['maint_salesman'] = $default['0']['salesmen']['id'];
			}

			if(!$exists){
				$user = $this->General->registerUser($this->data['Retailer']['mobile'],RETAILER_REG,RETAILER);
				$user = $user['User'];
				$new_user = 1;
			}
			else if($user['group_id'] == MEMBER){
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = RETAILER;
				$this->User->save($userData);//make already user to a retailer
			}
			$this->data['Retailer']['user_id'] = $user['id'];
			$this->data['Retailer']['parent_id'] = $this->info['id'];
			$this->data['Retailer']['slab_id'] = $this->info['slab_id'];

			$this->Retailer->create();
			if ($this->Retailer->save($this->data)) {
				//$this->General->updateLocation($this->data['Retailer']['area_id']);
				$this->General->makeOptIn247SMS($this->data['Retailer']['mobile']);

				//if($this->data['login'] == 'on'){
				$sms = "Welcome to Pay1!\nUserName: ".$this->data['Retailer']['mobile']."\nPassword: ".$user['syspass'];
				$sms .= "\nDownload app:";
				$sms .= "\nBasic: " . $this->General->createAppDownloadUrl(RETAILER,2);
				$sms .= "\nAndroid: " . $this->General->createAppDownloadUrl(RETAILER,1);
				$sms .= "\nWebsite: http://rx.pay1.in ";
				$sms .= "\nMisscall recharges: Dial 02261512234";
					
				$sms .= $this->getRentalSMS($this->data['Retailer']['rental_flag'],$this->info['rental_amount'],$this->info['target_amount']);
					
				$this->General->sendMessage($user['mobile'],$sms,'payone');
				if($this->data['Retailer']['rental_flag'] == 1){
					$mail_subject = "New Retailer Created On Rental";
				}else {
					$mail_subject = "New Retailer Created Via Kit";
				}
				$mail_body = "Distributor: " . $this->info['company'] . "<br/>";
				$mail_body .= "Retailer: " . $this->data['Retailer']['shopname'] . "<br/>";
				$mail_body .= "Address: " . $this->data['Retailer']['address'];
				$this->General->sendMails($mail_subject, $mail_body);

				$this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);

				if($this->data['Retailer']['rental_flag'] == 0){
					if($this->info['commission_kits_flag'] == 1){
						if($this->info['kits'] == -1){
							$this->Retailer->query("UPDATE distributors SET discounted_money=discounted_money+".$this->info['discount_kit']." WHERE id = ".$this->info['id']);
						}
						else {
							$this->Retailer->query("UPDATE distributors SET kits=kits-1,discounted_money=discounted_money+".$this->info['discount_kit']." WHERE id = ".$this->info['id']);
						}
					}
					else if($this->info['kits'] > 0){
						$this->Retailer->query("UPDATE distributors SET kits=kits-1 WHERE id = ".$this->info['id']);
					}
				}

				/*if(!empty($default)){
					$date=Date("Y-m-d");
					$date_created=Date("Y-m-d H:i:s");
					$st_id = $this->Shop->shopTransactionUpdate(SETUP_FEE,SETUP_FEE_AMT,$this->info['id'],$this->Retailer->id);
					//for trial put insert amount=0 in sst but in st put 1500
					$this->Retailer->query("INSERT INTO salesman_transactions(shop_tran_id,salesman,payment_mode,payment_type,collection_amount,collection_date,created) VALUES ($st_id,".$default['0']['salesmen']['id'].",1,1,0,'$date','$date_created')");
					}*/

				$this->set('data',null);
				$this->render('/elements/shop_form_retailer','ajax');
			} else {
				$err_msg = '<div class="error_class">The Retailer could not be saved. Please, try again.</div>';
				$this->Session->setFlash(__($err_msg, true));
				$this->render('/elements/shop_form_retailer','ajax');
			}
		}
	}

	function getRentalSMS($rental_flag,$rental_amount,$target_amount){
		$msg = "";
		if($rental_flag == 1){
			if($rental_amount > 0 && $target_amount > 0){
				$msg = "\nDo minimum sale of Rs $target_amount per month to avoid rental of Rs $rental_amount";
			}
			else if($rental_amount > 0 && $target_amount == -1){
				$msg = "\nRs $rental_amount will be charged as monthly rental";
			}
		}
		else {
			$msg = "\nKindly pay setup fee of Rs 500 to your distributor";
		}
		return $msg;
	}

	function createRetailerApp($params,$format){
		$to_save = true;
		$this->data['Retailer']['mobile'] = $params['mobile'];
		preg_match('/^[7-9][0-9]{9}$/',$this->data['Retailer']['mobile'],$matches,0);
		if(empty($matches)){
			$msg = "Invalid demo mobile number";
			return array('status' => 'failure','description' => $msg);
		}
		$exists = $this->General->checkIfUserExists($this->data['Retailer']['mobile']);
		if($exists){
			$user = $this->General->getUserDataFromMobile($this->data['Retailer']['mobile']);
			if($user['group_id'] != MEMBER){
				$to_save = false;
				$msg = "You cannot make this mobile as your retailer.";
			}
			else {
				$userData['User']['id'] = $user['id'];
				$userData['User']['group_id'] = RETAILER;
				$this->User->save($userData);//make already user to a retailer
			}
		}
		else{
			$user = $this->General->registerUser($this->data['Retailer']['mobile'],RETAILER_REG,RETAILER);
			$user = $user['User'];
		}

		if($to_save){
			$this->data['Retailer']['user_id'] = $user['id'];
			$this->data['Retailer']['parent_id'] = $_SESSION['Auth']['id'];
			$this->data['Retailer']['slab_id'] = $_SESSION['Auth']['slab_id'];
			$this->data['Retailer']['rental_flag'] = $params['rental_flag'];
			if(isset($params['name'])){
				$this->data['Retailer']['name'] = $params['name'];
				$this->data['Retailer']['pin'] = $params['pincode'];
			}

			if(isset($params['shopname']))
			{
				$this->data['Retailer']['shopname'] = $params['shopname'];
			}

			if(isset($params['subArea']))
			{
				$this->data['Retailer']['subarea_id'] = $params['subArea'];
			}

			if(isset($params['type']))
			{
				$this->data['Retailer']['retailer_type'] = $params['type'];
			}


			if(isset($params['salesmanId'])){
				$this->data['Retailer']['salesman'] = $params['salesmanId'];
				$this->data['Retailer']['maint_salesman'] = $params['salesmanId'];
			}

			$this->Retailer->create();
			if ($this->Retailer->save($this->data)) {
				$this->General->makeOptIn247SMS($this->data['Retailer']['mobile']);
				$sms = "Welcome to Pay1!\nUserName: ".$this->data['Retailer']['mobile']."\nPassword: ".$user['syspass'];
				$sms .= "\nDownload app:";
				$sms .= "\nBasic: " . $this->General->createAppDownloadUrl(RETAILER,2);
				$sms .= "\nAndroid: " . $this->General->createAppDownloadUrl(RETAILER,1);
				$sms .= "\nWebsite: http://rx.pay1.in";
				$sms .= "\nMisscall recharges: Dial 02261512234";
				
				$sms .= $this->getRentalSMS($params['rental_flag'],$_SESSION['Auth']['rental_amount'],$_SESSION['Auth']['target_amount']);

				$this->General->sendMessage($this->data['Retailer']['mobile'],$sms,'payone');

				$sName = '';
				$msg = 'Retailer created successfully.';

				if($params['salesmanId']){
					$sQ = $this->User->query("SELECT name,mobile FROM salesmen where id=".$params['salesmanId']);
					$sName = $sQ['0']['salesmen']['name'];
				}
				if($params['rental_flag'] == 1){
					$mail_subject = "New Retailer Created On Rental";
				}
				else {
					$mail_subject = "New Retailer Created Via Kit";
				}
				$mail_body = "Salesman: ".$sName."<br/>";
				$mail_body .= "Distributor: " .$_SESSION['Auth']['company'] . "<br/>";
				if(isset($this->data['Retailer']['name'])){
					$mail_body .= "Retailer: " . $this->data['Retailer']['name'] . "<br/>";
				}
				$mail_body .= "Retailer Mobile: " . $this->data['Retailer']['mobile'];
				$this->General->sendMails($mail_subject, $mail_body);

				$this->Shop->updateSlab($this->data['Retailer']['slab_id'],$this->Retailer->id,RETAILER);
					
				if($params['rental_flag'] == 0){
					if($_SESSION['Auth']['commission_kits_flag'] == 1){
						if($_SESSION['Auth']['kits'] == -1){
							$this->User->query("UPDATE distributors SET discounted_money=discounted_money+".$_SESSION['Auth']['discount_kit']." WHERE id = ".$_SESSION['Auth']['id']);
						}
						else {
							$this->User->query("UPDATE distributors SET kits=kits-1,discounted_money=discounted_money+".$_SESSION['Auth']['discount_kit']." WHERE id = ".$_SESSION['Auth']['id']);
						}
					}
					else if($_SESSION['Auth']['kits'] > 0){
						$this->Retailer->query("UPDATE distributors SET kits=kits-1 WHERE id = ".$_SESSION['Auth']['id']);
						if($params['salesmanId'] && $sQ['0']['salesmen']['mobile'] == $_SESSION['Auth']['User']['mobile']){
							$msg .= "\nKits left: " . $_SESSION['Auth']['kits'] - 1;
						}
					}
				}

				/*if(isset($params['salesmanId'])){
					$date=Date("Y-m-d");
					$date_created=Date("Y-m-d H:i:s");
					$st_id = $this->Shop->shopTransactionUpdate(SETUP_FEE,SETUP_FEE_AMT,$_SESSION['Auth']['id'],$this->Retailer->id);
					//for trial put insert amount=0 in sst but in st put 1500
					$this->User->query("insert into salesman_transactions(shop_tran_id,salesman,payment_mode,payment_type,collection_amount,collection_date,created) values($st_id,".$params['salesmanId'].",1,1,0,'$date','$date_created')");
					}*/
			}
			return array('status' => 'success','description' => $msg);
		}
		else {
			return array('status' => 'failure','description' => $msg);
		}
	}

	function changePassword(){
		if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != RELATIONSHIP_MANAGER){
			$this->redirect('/');
		}

		$this->render('setting');
	}

	function commissions(){
		$data = $this->Shop->getAllCommissions($this->info['id'],$this->Session->read('Auth.User.group_id'),$this->info['slab_id']);
		//$this->printArray($data);
		$this->set('data',$data);
		$this->render('discount_table');
	}

	function setSignature(){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		if($this->info['signature_flag'] == 1){
			$data['signature'] = 'on';
		}
		$data['text'] = $this->info['signature'];
		$this->set('data',$data);
		$this->render('signature');
	}


	function saveSignature(){
		if(!$this->Session->check('Auth.User.group_id'))$this->logout();
		$signature = 0;
		if(isset($this->data['signature']) && $this->data['signature'] == 'on'){
			$signature = 1;
		}
		$this->set('data',$this->data);
		if($signature == 1 && empty($this->data['text'])){
			$err_msg = '<div class="error_class">Please enter your 60 character signature.</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_signature','ajax');
		}
		else {
			$this->Retailer->updateAll(array('Retailer.signature_flag' => $signature, 'Retailer.signature' => '"'.$this->data['text'].'"'),array('Retailer.id' => $this->info['id']));
			if($signature == 0) $msg = 'Signature Removed Successfully';
			else $msg = 'Signature Saved Successfully';
			$err_msg = '<div class="success">'.$msg.'</div>';
			$this->Session->setFlash(__($err_msg, true));
			$this->render('/elements/shop_signature','ajax');
		}
	}

	function accountHistory($date=null,$page=null){
		if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect('/');
		}
		$api = false;
		if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
		}
		$grp_id = $_SESSION['Auth']['User']['group_id'];
		if($page == null)$page = 1;
		if($date != null){
			$limit = " limit " . ($page-1)*PAGE_LIMIT.",".PAGE_LIMIT;

			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];

			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
				if($grp_id == SUPER_DISTRIBUTOR){
					/*$query = "(
						(SELECT shop_transactions.id, shop_transactions.amount,'Free Credit' as name,'' as refid, shop_transactions.type, shop_transactions.timestamp FROM shop_transactions WHERE ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . ADMIN_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,distributors.company as name,distributors.id as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN distributors ON (distributors.id = ref2_id) WHERE ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . SDIST_DIST_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,st1.type as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type in (" . TDS_SUPERDISTRIBUTOR . "," . COMMISSION_SUPERDISTRIBUTOR . "))
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,distributors.company as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) INNER JOIN distributors ON (distributors.id = shop_transactions.ref1_id AND distributors.parent_id = ".$this->info['id'].") WHERE shop_transactions.type in (" . TDS_DISTRIBUTOR . "," . COMMISSION_DISTRIBUTOR . "))
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,'Reversal' as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type = ".REVERSAL_SUPERDISTRIBUTOR.")
						UNION
						(SELECT 'TOCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,'' as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit WHERE shop_creditdebit.to_id =  ".$_SESSION['Auth']['id'] . " AND shop_creditdebit.to_groupid = ".SUPER_DISTRIBUTOR." AND shop_creditdebit.from_id is null)
						UNION
						(SELECT 'FROMCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,distributors.company as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit INNER JOIN distributors ON (distributors.id = shop_creditdebit.to_id) WHERE shop_creditdebit.api_flag = 0 AND shop_creditdebit.from_id =  ".$_SESSION['Auth']['id'] . "  AND shop_creditdebit.to_groupid = ".DISTRIBUTOR.")
						)";*/
					$query = "(
						(SELECT shop_transactions.id, shop_transactions.amount,distributors.company as name,distributors.id as refid,shop_transactions.type, shop_transactions.date, shop_transactions.timestamp FROM shop_transactions,distributors WHERE ((distributors.id = ref2_id AND type = " . SDIST_DIST_BALANCE_TRANSFER . ") OR (distributors.id = ref1_id AND type = " . COMMISSION_DISTRIBUTOR . ")))
					)";	
				}
				else if($grp_id == DISTRIBUTOR){
					/*$query = "(
						(SELECT shop_transactions.id,shop_transactions.amount,retailers.shopname as name,retailers.id as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN retailers ON (retailers.id = ref2_id) WHERE ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . DIST_RETL_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,super_distributors.company as name,super_distributors.id as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN super_distributors ON (super_distributors.id = ref1_id) WHERE ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . SDIST_DIST_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,st1.type as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type in (" . TDS_DISTRIBUTOR . "," . COMMISSION_DISTRIBUTOR . "))
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,retailers.shopname as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) INNER JOIN retailers ON (retailers.id = shop_transactions.ref1_id AND retailers.parent_id = ".$_SESSION['Auth']['id'].") WHERE shop_transactions.type in (" . TDS_RETAILER . "," . COMMISSION_RETAILER . "))
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,'Reversal' as name,'' as refid,shop_transactions.type, shop_transactions.timestamp FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type = ".REVERSAL_DISTRIBUTOR.")
						UNION
						(SELECT 'TOCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,super_distributors.company as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit LEFT JOIN super_distributors ON (super_distributors.id = shop_creditdebit.from_id) WHERE shop_creditdebit.to_id =  ".$_SESSION['Auth']['id'] . " AND shop_creditdebit.to_groupid = ".DISTRIBUTOR.")
						UNION
						(SELECT 'FROMCREDITDEBIT' as id, shop_creditdebit.amount,shop_creditdebit.description as name,retailers.shopname as refid,shop_creditdebit.type, shop_creditdebit.timestamp FROM shop_creditdebit INNER JOIN retailers ON (retailers.id = shop_creditdebit.to_id) WHERE shop_creditdebit.from_id =  ".$_SESSION['Auth']['id'] . " AND shop_creditdebit.to_groupid = ".RETAILER.")
						)";*/
					$query = "(
						(SELECT shop_transactions.id,shop_transactions.amount,retailers.shopname as name,retailers.id as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN retailers ON (retailers.id = ref2_id) WHERE ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . DIST_RETL_BALANCE_TRANSFER . ") 
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,super_distributors.company as name,super_distributors.id as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN super_distributors ON (super_distributors.id = ref1_id) WHERE ((ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . SDIST_DIST_BALANCE_TRANSFER . ") OR (ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . COMMISSION_DISTRIBUTOR . ")))
					)";
				}
				else if($grp_id == RETAILER){
					$query = "(
						(SELECT shop_transactions.id,shop_transactions.amount,distributors.company as name,distributors.id as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN distributors ON (distributors.id = ref1_id) WHERE ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . DIST_RETL_BALANCE_TRANSFER . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,products.name as name,services.name as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN products ON (ref2_id = products.id) INNER JOIN services ON (products.service_id = services.id) WHERE ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . RETAILER_ACTIVATION . ")
						UNION
						(SELECT shop_transactions.id,shop_transactions.amount,st1.type as name,'' as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type in (" . TDS_RETAILER . "," . COMMISSION_RETAILER . "))
						UNION
						(SELECT shop_transactions.id, shop_transactions.amount,'Reversal' as name,products.name as refid,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date FROM shop_transactions INNER JOIN shop_transactions as st1 ON (st1.id = shop_transactions.ref2_id) INNER JOIN products ON (st1.ref2_id = products.id) WHERE shop_transactions.ref1_id = ".$_SESSION['Auth']['id'] . " AND shop_transactions.type = ".REVERSAL_RETAILER.")
					)";
				}
				$transactions = $this->Retailer->query("SELECT * FROM $query as transactions where transactions.date >= '$date_from' AND  transactions.date <= '$date_to' order by transactions.timestamp desc,transactions.id desc $limit");
				$trans_count = $this->Retailer->query("SELECT * FROM $query as transactions where transactions.date >= '$date_from' AND  transactions.date <= '$date_to' order by transactions.timestamp desc,transactions.id desc");
				$trans_count = count($trans_count);
				$this->set(compact('trans_count','date_from','date_to'));
				if($api){
					$result['trans_count'] = $trans_count;
					$result['date_from'] = $date_from;
					$result['date_to'] = $date_to;
				}
			}
			else {
				$transactions = array();
			}

		}
		else {
			$transactions = array();
		}
		$this->set('page',$page);
		$this->set('transactions',$transactions);
		if($api){
			if($date == null) $result['empty'] = 0;
			$result['page'] = $page+1;
			$result['transactions'] = $transactions;
			return $result;
		}
		else {
			if($date == null) $this->set('empty',0);
			$this->render('account_history');
		}
	}

	function topup($date = null,$page=null){
		if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
		}
		$grp_id = $_SESSION['Auth']['User']['group_id'];
		if($grp_id != DISTRIBUTOR && $grp_id != SUPER_DISTRIBUTOR) $this->render('/shops/view');
		if($page == null)$page = 1;
		if(empty($date)){
                    $date = date('dmY')."-".date('dmY');
                }
			$limit = " limit " . ($page-1)*PAGE_LIMIT.",".PAGE_LIMIT;

			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];

			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);

			if($grp_id == DISTRIBUTOR){
				$query = "SELECT shop_transactions.id,shop_transactions.ref1_id,shop_transactions.ref2_id,shop_transactions.amount,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND shop_id = ".$_SESSION['Auth']['id']." AND group_id = ".DISTRIBUTOR.") WHERE ((ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . SDIST_DIST_BALANCE_TRANSFER . ") OR (ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . COMMISSION_DISTRIBUTOR . ") OR (ref1_id = ".$_SESSION['Auth']['id'] . "  AND ref2_id = ".DISTRIBUTOR. " AND type = " . REFUND . ") OR (ref1_id = ".$_SESSION['Auth']['id'] ."  AND ref2_id = ".DISTRIBUTOR. " AND type = " . RENTAL . ")) AND date >= '$date_from' AND date <= '$date_to' order by id desc";
			}
			else if($grp_id == SUPER_DISTRIBUTOR){
				$query = "SELECT shop_transactions.id,shop_transactions.ref1_id,shop_transactions.ref2_id,shop_transactions.amount,shop_transactions.type, shop_transactions.timestamp,shop_transactions.date , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND shop_id = ".$_SESSION['Auth']['id']." AND group_id = ".SUPER_DISTRIBUTOR.") WHERE ((ref2_id = ".$_SESSION['Auth']['id'] . " AND type = " . ADMIN_TRANSFER . ") OR (ref1_id = ".$_SESSION['Auth']['id'] . " AND type = " . COMMISSION_SUPERDISTRIBUTOR . ") OR (ref1_id = ".$_SESSION['Auth']['id'] . "  AND ref2_id = ".SUPER_DISTRIBUTOR. " AND type = " . REFUND . ") OR (ref1_id = ".$_SESSION['Auth']['id'] ."  AND ref2_id = ".SUPER_DISTRIBUTOR. " AND type = " . RENTAL . ")) AND date >= '$date_from' AND date <= '$date_to' order by id desc";
			}
				$transactions = $this->Retailer->query($query . " $limit");
				$transArr = array();
                                foreach ($transactions as $transaction){
				if($transaction['shop_transactions']['type'] == COMMISSION_DISTRIBUTOR || $transaction['shop_transactions']['type'] == COMMISSION_SUPERDISTRIBUTOR){
                                        $transArr[$transaction['shop_transactions']['ref2_id']]['amount'] =  ( isset($transArr[$transaction['shop_transactions']['ref2_id']]['amount']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['amount'] : 0 ) + $transaction['shop_transactions']['amount'];
                                        $transArr[$transaction['shop_transactions']['ref2_id']]['opening'] =  isset($transArr[$transaction['shop_transactions']['ref2_id']]['opening']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['opening'] : 0  ;
                                        $transArr[$transaction['shop_transactions']['ref2_id']]['closing'] = isset($transArr[$transaction['shop_transactions']['ref2_id']]['closing']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['closing'] : 0  ;
                                        $transArr[$transaction['shop_transactions']['ref2_id']]['timestamp'] = isset($transArr[$transaction['shop_transactions']['ref2_id']]['timestamp']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['timestamp'] : 0  ;
                                        $transArr[$transaction['shop_transactions']['ref2_id']]['type'] = isset($transArr[$transaction['shop_transactions']['ref2_id']]['type']) ? $transArr[$transaction['shop_transactions']['ref2_id']]['type'] : $transaction['shop_transactions']['type']  ;
                                    }else{
                                        $transArr[$transaction['shop_transactions']['id']]['amount'] = ( isset($transArr[$transaction['shop_transactions']['id']]['amount']) ? $transArr[$transaction['shop_transactions']['id']]['amount'] : 0 ) +  $transaction['shop_transactions']['amount'];
                                        $transArr[$transaction['shop_transactions']['id']]['opening'] = $transaction['opening_closing']['opening'];
                                        $transArr[$transaction['shop_transactions']['id']]['closing'] = $transaction['opening_closing']['closing'];
                                        $transArr[$transaction['shop_transactions']['id']]['timestamp'] = $transaction['shop_transactions']['timestamp'];
                                        $transArr[$transaction['shop_transactions']['id']]['type'] = $transaction['shop_transactions']['type'];
                                       
                                    }
                                }
                                $transactions = $transArr ;
                                $trans_count = $this->Retailer->query($query);
				$trans_count = count($trans_count);
				$this->set(compact('trans_count','date_from','date_to'));
				if(!empty ($api)){
					$result['trans_count'] = $trans_count;
					$result['date_from'] = $date_from;
					$result['date_to'] = $date_to;
				}
			}
			else {
				$transactions = array();
			}

		/*}
		else {
			$transactions = array();
		}*/
		$this->set('page',$page);
		$this->set('transactions',$transactions);

		if($date == null) $this->set('empty',0);
		$this->render('buy_report');
	}

	function topupDist($date = null,$dist=null){
		$grp_id = $_SESSION['Auth']['User']['group_id'];

		if($grp_id != ADMIN && $grp_id != SUPER_DISTRIBUTOR && $grp_id != RELATIONSHIP_MANAGER) $this->render('/shops/view');

		if(is_array($date)){
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
		}
		if($dist == null)$dist = 0;

		if($date == null){
			$date = date('dmY') . '-' . date('dmY');
		}

		$this->set('dist',$dist);
		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];

			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);

				if($grp_id == ADMIN){
					if($dist == 0){
						$query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.note,shop_transactions.type_flag,st.amount as commission,shop_transactions.timestamp,trim(super_distributors.company) as company ,opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join super_distributors  ON (super_distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_SUPERDISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id ) WHERE shop_transactions.type =" . ADMIN_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to' order by shop_transactions.id desc";
					}
					else {
						$query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.note,shop_transactions.type_flag,st.amount as commission,shop_transactions.timestamp,trim(super_distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join super_distributors  ON (super_distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_SUPERDISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id ) WHERE shop_transactions.ref2_id = $dist AND shop_transactions.type =" . ADMIN_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to' order by shop_transactions.id desc";
					}
				}else {
					$r1 = 0;
					if($grp_id == RELATIONSHIP_MANAGER){
						$r1 = $this->info['super_dist_id'];
						$extra = " AND distributors.rm_id = " . $this->info['id'];
					}else{
						$r1 = $this->info['id'];
						$extra = "";
					}
					if($dist == 0){
						$query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.type_flag,shop_transactions.note,st.amount as commission,shop_transactions.timestamp,trim(distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join distributors  ON (distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_DISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND shop_id = distributors.id AND group_id = ".DISTRIBUTOR.") WHERE shop_transactions.ref1_id = $r1 AND shop_transactions.type =" . SDIST_DIST_BALANCE_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to'  $extra order by shop_transactions.id desc";
					}
					else {
						$query = "SELECT shop_transactions.id,shop_transactions.amount,shop_transactions.type,shop_transactions.type_flag,shop_transactions.note,st.amount as commission,shop_transactions.timestamp,trim(distributors.company) as company , opening_closing.opening AS opening, opening_closing.closing AS closing FROM shop_transactions inner join distributors  ON (distributors.id = shop_transactions.ref2_id) left join shop_transactions as st ON (st.ref2_id = shop_transactions.id AND st.type = " . COMMISSION_DISTRIBUTOR . ") LEFT JOIN opening_closing ON ( opening_closing.shop_transaction_id = shop_transactions.id AND shop_id = distributors.id AND group_id = ".DISTRIBUTOR.") WHERE shop_transactions.ref1_id = $r1 AND shop_transactions.ref2_id = $dist AND shop_transactions.type =" . SDIST_DIST_BALANCE_TRANSFER . " AND shop_transactions.date >= '$date_from' AND shop_transactions.date <= '$date_to' $extra order by shop_transactions.id desc";
					}
				}

				$transactions = $this->Retailer->query($query);
				$this->set(compact('date_from','date_to'));
			}
			else {
				$transactions = array();
			}
		}
		else {
			$transactions = array();
		}
		$this->set('transactions',$transactions);

		if($date == null) $this->set('empty',0);
		$this->render('buy_dist_report');
	}

	function saleReport($date = null,$id=null){
		$api = false;
		$service_id = null;
		if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			if(isset($params['id']))$id = $params['id'];
			if(isset($params['service']))$service_id = $params['service'];
		}
		if(!isset($_SESSION['Auth']['User']))$this->redirect(array('action' => 'index'));
		$show = true;

		if($date != null){
			$dates = explode("-",$date);
			$date_from = $dates[0];
			$date_to = $dates[1];
			if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
				$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
				$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
			}
		}
		else {
			$date_from = date('Y-m-d');
			$date_to = date('Y-m-d');
		}
		$this->set('date_from',$date_from);
		$this->set('date_to',$date_to);

		$cond_id = '';
		if($id != null){
			$cond_id = " AND st3.ref1_id = $id";
			$this->set('id',$id);
		}
		if($show){
			$cond = 'AND st1.date >= "'.$date_from.'" AND st1.date <= "' .$date_to . '"';

			if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
				$query = "SELECT products.name,products.id,count(st1.id) as counts,sum(st1.amount) as amount, sum(st2.amount-st3.amount) as income FROM shop_transactions as st1 INNER JOIN shop_transactions as st2 ON (st2.ref2_id = st1.id AND st2.type = ".COMMISSION_SUPERDISTRIBUTOR." AND st2.ref1_id = ".$_SESSION['Auth']['id'].") INNER JOIN shop_transactions as st3 ON (st3.ref2_id = st1.id $cond_id AND st3.type = ".COMMISSION_DISTRIBUTOR.") INNER JOIN products ON (products.id = st1.ref2_id) WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION;
			}
			else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
				$query = "SELECT products.name,products.id,count(st1.id) as counts,sum(st1.amount) as amount FROM shop_transactions as st1 INNER JOIN products ON (products.id = st1.ref2_id) INNER JOIN retailers ON (retailers.id = st1.ref1_id AND retailers.parent_id = ".$this->info['id'].") WHERE st1.confirm_flag = 1 AND st1.type = ".RETAILER_ACTIVATION;
			}
			else if($this->Session->read('Auth.User.group_id') == RETAILER){
				//$query = "SELECT products.name,products.id,count(vd.id) as counts,sum(vd.amount) as amount, sum(st1.amount) as income FROM vendors_activations as vd INNER JOIN shop_transactions as st1 ON (st1.ref2_id = vd.shop_transaction_id AND st1.type = ".COMMISSION_RETAILER.") INNER JOIN products ON (products.id = vd.product_id) WHERE  vd.retailer_id = ".$_SESSION['Auth']['id']." AND vd.status != 2 AND vd.status != 3";
				$query = "SELECT products.name,products.id,count(va.id) as counts,sum(va.amount) as amount, sum(va.amount+oc.closing-oc.opening) as income FROM vendors_activations as va INNER JOIN products ON (products.id = va.product_id) INNER JOIN opening_closing as oc ON (oc.shop_transaction_id=va.shop_transaction_id AND oc.shop_id=va.retailer_id AND oc.group_id = ".RETAILER.") WHERE va.retailer_id = ".$_SESSION['Auth']['id']." AND va.status != 2 AND va.status != 3";
				$cond = 'AND va.date >= "'.$date_from.'" AND va.date <= "' .$date_to . '"';
			}
			$products = array();
			if($service_id != null || !empty($service_id)){
				$services = $this->Retailer->query("SELECT services.id,services.name FROM services WHERE services.id = $service_id");
			}
			else {
				$services = $this->Retailer->query("SELECT services.id,services.name FROM services order by id");
			}

			foreach($services as $service){
				$service_id = $service['services']['id'];
				$query1 = $query . " AND service_id = $service_id $cond group by products.id";
				$data = $this->Retailer->query($query1);
				if(!empty($data)){
					$products[$service_id]['data'] = $data;
					$products[$service_id]['name'] = $service['services']['name'];
				}
			}
		}
		else {
			$products = array();
		}
		if(!$api)
		$this->set('products',$products);
		else return $products;
	}

	function investmentReport($from=null,$to=null,$vendorId=null){
		if($this->Session->read('Auth.User.group_id') != ADMIN){
			$this->redirect("/");
		}
		if(!isset($from))
		$from=date('d-m-Y',strtotime('-7 days'));
		if(!isset($to))
		$to=date('d-m-Y');
			
		if(empty($vendorId))$vendorId = 0;
			
		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);
			
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
		$this->set('from',$from);
		$this->set('to',$to);
			
		//for vendors dropdown
		$vendorResult=$this->User->query("select * from vendors where active_flag = 1");
		$this->set('vendors',$vendorResult);
		$this->set('id',$vendorId);

		if($vendorId==0){
			$result=$this->User->query("SELECT earnings_logs.*,vendors.company FROM earnings_logs,vendors WHERE vendors.id = vendor_id AND date >= '".$fd."' AND date <= '".$ft."' order by date desc,vendor_id");
		}else{
			$result=$this->User->query("SELECT earnings_logs.*,vendors.company FROM earnings_logs,vendors WHERE vendors.id = vendor_id AND date >= '".$fd."' AND date <= '".$ft."' AND vendor_id=$vendorId order by date desc,vendor_id");
		}
			
		$data = array();
		foreach($result as $res){
			$data[$res['earnings_logs']['date']][] = $res;
		}
			
		$this->set('data',$data);
	}

	function addInvestedAmount(){
		if($this->Session->read('Auth.User.group_id') != ADMIN){
			$this->redirect(array('action' => 'index'));
		}
		$id = trim($_REQUEST['id']);
		$amount = trim($_REQUEST['amount']);

		if(empty($amount))$amount = 0;
		$this->Retailer->query("UPDATE earnings_logs SET invested=$amount WHERE id = $id");

		echo 'done';
		$this->autoRender = false;
	}

	function earningReport($from=null,$to=null){
		if($this->Session->read('Auth.User.group_id') != ADMIN){
			$this->redirect("/");
		}
		if(!isset($from))
		$from=date('d-m-Y',strtotime('-7 days'));
		if(!isset($to))
		$to=date('d-m-Y');
			
		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);
			
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
		$this->set('from',$from);
		$this->set('to',$to);
			
		$result=$this->User->query("SELECT sum(sale) as sale,sum(invested) as invested,sum(expected_earning) as expected_earning,sum(sale+closing-opening-invested) as earning,date FROM earnings_logs WHERE closing is not null AND date >= '".$fd."' AND date <= '".$ft."' group by date order by date desc");
			
		$data = array();
		foreach($result as $res){
			$data[$res['earnings_logs']['date']]['sale'] = $res['0']['sale'];
			$data[$res['earnings_logs']['date']]['invested'] = $res['0']['invested'];
			$data[$res['earnings_logs']['date']]['earning'] = $res['0']['earning'];
			$data[$res['earnings_logs']['date']]['expected_earning'] = $res['0']['expected_earning'];
		}
			
		$extra_ret=$this->User->query("SELECT sum(earning) as earning,date FROM retailers_logs WHERE date >= '".$fd."' AND date <= '".$ft."' group by date");
		$extra_dist=$this->User->query("SELECT sum(distributors_logs.earning) as earning,distributors_logs.date FROM distributors_logs,distributors WHERE distributors.id = distributors_logs.distributor_id AND distributors.parent_id = 3 AND distributors_logs.date >= '".$fd."' AND distributors_logs.date <= '".$ft."' group by distributors_logs.date");
		$extra_superdist=$this->User->query("SELECT sum(shop_transactions.amount) as earning,shop_transactions.date FROM shop_transactions WHERE shop_transactions.type = ".COMMISSION_SUPERDISTRIBUTOR." AND shop_transactions.date >= '".$fd."' AND shop_transactions.date <= '".$ft."' group by shop_transactions.date");
			
		foreach($extra_ret as $ext){
			if(isset($data[$ext['retailers_logs']['date']]))
			$data[$ext['retailers_logs']['date']]['retailer_earning'] = $ext['0']['earning'];
		}
		foreach($extra_dist as $ext){
			if(isset($data[$ext['distributors_logs']['date']]))
			$data[$ext['distributors_logs']['date']]['distributor_earning'] = $ext['0']['earning'];
		}
		foreach($extra_superdist as $ext){
			if(isset($data[$ext['shop_transactions']['date']]))
			$data[$ext['shop_transactions']['date']]['sdistributor_earning'] = $ext['0']['earning'];
		}

		$this->set('data',$data);
	}

	function salesmanReport($from=null,$to=null,$salesmanId=null)
	{
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect(array('action' => 'index'));
		}
		//echo "Start SSM".$salesmanMobile;
		if(!isset($from))
		$from=date('d-m-Y');
		if(!isset($to))
		$to=date('d-m-Y');
			
		$fdarr = explode("-",$from);
		$tdarr = explode("-",$to);
			
		$fd = $fdarr[2]."-".$fdarr[1]."-".$fdarr[0];
		$ft = $tdarr[2]."-".$tdarr[1]."-".$tdarr[0];
			
		$this->set('from',$from);
		$this->set('to',$to);
			
		//for salesman dropdown
		$salesmanResult=$this->User->query("select * from salesmen where dist_id = ".$this->info['id']." AND active_flag = 1");
		$this->set('salesmans',$salesmanResult);
			
		//for table in salesman Reports
		//	echo "Sales Mobile= ".$salesmanMobile;
		$this->set('id',$salesmanId);
			

		if($salesmanId!=0){
			$salesResult=$this->User->query("
                                select r.name,r.mobile,r.shopname,st.amount,sst.id,st.id,st.note,st.type_flag,sst.created,salesmen.name,oc.opening,oc.closing
                                from 
                                salesmen 
                                join salesman_transactions sst on (salesmen.id=sst.salesman) 
                                join shop_transactions st on(sst.shop_tran_id=st.id)
                                left join opening_closing oc on(oc.shop_transaction_id=st.id AND oc.shop_id = st.ref2_id
AND oc.group_id = ".RETAILER.")
                                join retailers r on(r.id=st.ref2_id AND r.parent_id=".$this->info['id'].")  
                                where 
                                salesmen.id = $salesmanId AND 
                                salesmen.dist_id = ".$this->info['id']." AND 
                                st.type = ".DIST_RETL_BALANCE_TRANSFER." AND 
                                st.date between '".$fd."' and '".$ft."' 
                                group by sst.shop_tran_id 
                                order by sst.created desc");
                        
		}else{
			$salesResult=$this->User->query("
                                select r.name,r.mobile,r.shopname,st.amount,sst.id,st.id,st.note,st.type_flag,sst.created,salesmen.name,oc.opening,oc.closing
                                from 
                                salesmen 
                                join  salesman_transactions sst on (salesmen.id=sst.salesman) 
                                join shop_transactions st on(sst.shop_tran_id=st.id) 
                                left join opening_closing oc on(oc.shop_transaction_id=st.id  AND oc.shop_id = st.ref2_id
AND oc.group_id = ".RETAILER.")  
                                join retailers r on(r.id=st.ref2_id)  
                                where 
                                salesmen.dist_id = ".$this->info['id']." AND 
                                st.type = ".DIST_RETL_BALANCE_TRANSFER." AND 
                                st.date between '".$fd."' and '".$ft."' 
                                group by sst.shop_tran_id 
                                order by sst.created desc");
		}
		$this->set('salesResult',$salesResult);
	}

	function pullback(){
		if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != DISTRIBUTOR){
			$this->redirect(array('action' => 'index'));
		}

		if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
			$salesman_trans_id = $_REQUEST['salesman_transid'];
			$salesmanResult=$this->User->query("SELECT shop_transactions.ref2_id,salesman_transactions.id,salesman_transactions.shop_tran_id,salesman_transactions.created,salesman_transactions.salesman FROM salesman_transactions,shop_transactions,salesmen WHERE salesmen.dist_id = ".$this->info['id']." AND salesman_transactions.salesman = salesmen.id AND payment_type = 2 AND salesman_transactions.id = $salesman_trans_id AND salesman_transactions.shop_tran_id = shop_transactions.id");

			$success = false;
			$msg = "";
			if(!empty($salesmanResult)){
				$retid = $salesmanResult['0']['shop_transactions']['ref2_id'];
				$salesmanid = $salesmanResult['0']['salesman_transactions']['salesman'];
				$shopid = $salesmanResult['0']['salesman_transactions']['shop_tran_id'];
				$shopResult=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref1_id = ".$this->info['id']." AND ref2_id = $retid AND type = ".DIST_RETL_BALANCE_TRANSFER." ORDER BY id desc limit 1");

				if ($shopResult['0']['shop_transactions']['id'] == $shopid){
					$amt = $shopResult['0']['shop_transactions']['amount'];

					$retResult=$this->User->query("SELECT balance,mobile,mobile,shopname FROM retailers WHERE id = $retid AND parent_id = " . $this->info['id']);

					if(!empty($retResult)){
						if($retResult['0']['retailers']['balance'] >= $amt){
							$success = true;
							$this->User->query("UPDATE retailers SET balance = balance - $amt WHERE id = $retid");
							$this->User->query("UPDATE distributors SET balance = balance + $amt WHERE id = " . $this->info['id']);
							$this->User->query("UPDATE salesmen SET balance = balance + $amt WHERE id = $salesmanid");

							$this->User->query("DELETE FROM shop_transactions WHERE id = $shopid");
							$this->User->query("DELETE FROM salesman_transactions WHERE id = $salesman_trans_id");

							$this->User->query("INSERT INTO pullbacks (salesman_id,retailer_id,distributor_id,amount,topup_time,pullback_time) VALUES ($salesmanid,$retid,".$this->info['id'].",$amt,'".$shopResult['0']['shop_transactions']['timestamp']."','".date('Y-m-d H:i:s')."')");
							$salesData = $this->User->query("SELECT mobile FROM salesmen WHERE id = $salesmanid");

							$this->General->sendMessage($retResult['0']['retailers']['mobile'],"Dear Retailer, Rs $amt is pulled back from your account. Your balance is now Rs " . ($retResult['0']['retailers']['balance'] - $amt),'shops');
							$this->General->sendMessage($salesData['0']['salesmen']['mobile'],"Dear Salesman, Rs $amt is pulled back from retailer ".$retResult['0']['retailers']['shopname']." (".$retResult['0']['retailers']['mobile'].")",'shops');

							echo "success";
						}
						else{
							$msg = "Retailer balance is less than $amt";
							echo $msg;
						}
					}
				}
				else {
					$msg = "Only last topup can be pulled back";
					echo $msg;
				}
			}
		}
		else if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
			$shop_trans_id = $_REQUEST['shop_transid'];
			$shopResult=$this->User->query("SELECT shop_transactions.* FROM shop_transactions WHERE id = $shop_trans_id AND type = " .SDIST_DIST_BALANCE_TRANSFER);

			if(!empty($shopResult)){
				$amt = $shopResult['0']['shop_transactions']['amount'];
				$distid = $shopResult['0']['shop_transactions']['ref2_id'];
					
				$shopResult1=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref1_id = ".$this->info['id']." AND ref2_id = $distid AND type = ".SDIST_DIST_BALANCE_TRANSFER." ORDER BY id desc limit 1");
				if ($shopResult1['0']['shop_transactions']['id'] == $shop_trans_id){
					$comm=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_DISTRIBUTOR);
					if(!empty($comm)){
						$amt += $comm['0']['shop_transactions']['amount'];
					}
					$distResult=$this->User->query("SELECT distributors.balance,users.mobile,distributors.company FROM distributors,users WHERE distributors.user_id = users.id AND distributors.id = $distid AND parent_id = " . $this->info['id']);

					if(!empty($distResult)){
						if($distResult['0']['distributors']['balance'] >= $amt){
							$success = true;
							$this->User->query("UPDATE distributors SET balance = balance - $amt WHERE id = $distid");
							$this->User->query("UPDATE super_distributors SET balance = balance + $amt WHERE id = " . $this->info['id']);

							$this->User->query("DELETE FROM shop_transactions WHERE id = $shop_trans_id");
							$this->User->query("DELETE FROM shop_transactions WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_DISTRIBUTOR);

							//$this->User->query("INSERT INTO pullbacks (salesman_id,retailer_id,distributor_id,amount,topup_time,pullback_time) VALUES ($salesmanid,$retid,".$this->info['id'].",$amt,'".$shopResult['0']['shop_transactions']['timestamp']."','".date('Y-m-d H:i:s')."')");

							$this->General->sendMessage($distResult['0']['users']['mobile'],"Dear Distributor, Rs $amt is pulled back from your account. Your balance is now Rs " . ($distResult['0']['distributors']['balance'] - $amt),'shops');

							echo "success";
						}
						else{
							$msg = "Distributor balance is less than $amt";
							echo $msg;
						}
					}
				}
				else {
					$msg = "Only last topup can be pulled back";
					echo $msg;
				}
			}
		}
		else if($this->Session->read('Auth.User.group_id') == ADMIN){
			$shop_trans_id = $_REQUEST['shop_transid'];
			$shopResult=$this->User->query("SELECT shop_transactions.* FROM shop_transactions WHERE id = $shop_trans_id AND type = " .ADMIN_TRANSFER);

			if(!empty($shopResult)){
				$amt = $shopResult['0']['shop_transactions']['amount'];
				$sdistid = $shopResult['0']['shop_transactions']['ref2_id'];
					
				$shopResult1=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref2_id = $sdistid AND type = ".ADMIN_TRANSFER." ORDER BY id desc limit 1");
				if ($shopResult1['0']['shop_transactions']['id'] == $shop_trans_id){
					$comm=$this->User->query("SELECT id,amount FROM shop_transactions WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_SUPERDISTRIBUTOR);
					if(!empty($comm)){
						$amt += $comm['0']['shop_transactions']['amount'];
					}
					$distResult=$this->User->query("SELECT super_distributors.balance,users.mobile,super_distributors.company FROM super_distributors,users WHERE super_distributors.user_id = users.id AND super_distributors.id = $sdistid");

					if(!empty($distResult)){
						if($distResult['0']['super_distributors']['balance'] >= $amt){
							$success = true;
							$this->User->query("UPDATE super_distributors SET balance = balance - $amt WHERE id = $sdistid");

							$this->User->query("DELETE FROM shop_transactions WHERE id = $shop_trans_id");
							$this->User->query("DELETE FROM shop_transactions WHERE ref2_id = $shop_trans_id AND type = ".COMMISSION_SUPERDISTRIBUTOR);

							//$this->User->query("INSERT INTO pullbacks (salesman_id,retailer_id,distributor_id,amount,topup_time,pullback_time) VALUES ($salesmanid,$retid,".$this->info['id'].",$amt,'".$shopResult['0']['shop_transactions']['timestamp']."','".date('Y-m-d H:i:s')."')");

							$this->General->sendMessage($distResult['0']['users']['mobile'],"Dear Sir, Rs $amt is pulled back from your account. Your balance is now Rs " . ($distResult['0']['super_distributors']['balance'] - $amt),'shops');

							echo "success";
						}
						else{
							$msg = "Super Distributor balance is less than $amt";
							echo $msg;
						}
					}
				}
				else {
					$msg = "Only last topup can be pulled back";
					echo $msg;
				}
			}
		}

		if(!$success && empty($msg)){
			echo "Cannot be pulled back";
		}
		$this->autoRender = false;
	}

	function topupRequests(){//for distributor
		$data = $this->Retailer->query("SELECT topup_request.*,retailers.name,retailers.id FROM topup_request,retailers where retailers.user_id =  topup_request.user_id AND retailers.parent_id = " . $_SESSION['Auth']['id']);
		$this->set('data',$data);
	}

	function approve($id){
		$data = $this->Retailer->query("SELECT topup_request.*,retailers.name,retailers.mobile,retailers.id,retailers.shopname,retailers.kyc_flag FROM topup_request,retailers where retailers.user_id =  topup_request.user_id AND retailers.parent_id = " . $_SESSION['Auth']['id'] . " AND topup_request.id=$id");
		$bal = $this->Shop->getShopDataById($_SESSION['Auth']['id'],DISTRIBUTOR);
		$to_save = true;
		if($data['0']['topup_request']['amount'] > $bal['balance']){
			$message = "Your amount cannot be greater than your account balance";
			$to_save = false;
		}
		else {
			if($data['0']['retailers']['kyc_flag'] == 0 && ($data['0']['topup_request']['amount'] + $bal['balance']) > KYC_AMOUNT){
				$message = "Please collect KYC of the retailer. Retailer balance cannot be greater than Rs." . KYC_AMOUNT;
				$to_save = false;
			}
		}
		if($to_save){
			$this->Shop->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$data['0']['topup_request']['amount'],$_SESSION['Auth']['id'],$data['0']['retailers']['id']);
			$bal = $this->Shop->shopBalanceUpdate($data['0']['topup_request']['amount'],'subtract',$_SESSION['Auth']['id'],DISTRIBUTOR);
			$bal1 = $this->Shop->shopBalanceUpdate($data['0']['topup_request']['amount'],'add',$data['0']['retailers']['id'],RETAILER);
			$mail_subject = "Retailer Top-Up request approved";
			$mail_body = "Distributor: " . $_SESSION['Auth']['company']. " approved top-up of Rs. " . $data['0']['topup_request']['amount'] . " of retailer: " . $data['0']['retailers']['shopname'];

			$msg = "Dear Retailer,\nYour account is successfully credited with Rs." . $data['0']['topup_request']['amount']. "\nYour current balance is Rs.$bal1";
			$this->General->sendMessage($data['0']['retailers']['mobile'],$msg,'shops');
			//$this->General->sendMails($mail_subject, $mail_body);

			$this->Retailer->query("UPDATE topup_request SET approveStatus = 1 WHERE id = $id");
			echo "<script> reloadShopBalance(".$bal.");  </script>";
			echo "Approved";
		}
		else {
			echo $message;
		}
		$this->autoRender = false;
	}

	function transfer(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != ADMIN){
			$this->redirect(array('action' => 'index'));
		}
		$this->render('transfer');
	}

	function amountTransfer($params=null){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != ADMIN){
			$this->redirect(array('action' => 'index'));
		}
		$app_flag = false;

		if($params != null){
			$this->data['confirm'] = 1;
			$this->data['amount'] = $params['amount'];
			$this->data['shop'] = $params['retailer'];
			$this->info = $this->Session->read('Auth');
			$app_flag = true;
		}
		$to_save = true;
		$confirm = 0;
		$this->data['amount'] = trim($this->data['amount']);
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
		if($this->data['shop'] == 0)
		{
			if($this->Session->read('Auth.User.group_id') == ADMIN)
			$msg = "Please select super distributor";
			else if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR)
			$msg = "Please select distributor";
			else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR)
			{
				if($app_flag)
				{
					$msg = "Invalid SMS format.\nCorrect format: PAY1 TB mobile amount";
				}
				else
				{
					$msg = "Please select retailer";
				}
			}

			$to_save = false;
		}
		else if(empty($this->data['amount']))
		{
			if($app_flag)
			{
				$msg = "Invalid SMS format.\nCorrect format: PAY1 TB mobile amount";
			}
			else $msg = "Please enter some amount";
			$to_save = false;
		}
		else if($this->data['amount'] <= 0 || !preg_match('/^\d+$/',$this->data['amount'])){
			if($app_flag){
				$msg = "Invalid SMS format.\nCorrect format: PAY1 TB mobile amount";
			}
			else $msg = "Amount entered is not valid";
			$to_save = false;
		}
		else if(isset($this->data['commission']) && !empty($this->data['commission']) && !is_numeric($this->data['commission'])){
			$msg = "Commission entered is not valid";
			$to_save = false;
		}
		else {
			if($this->Session->read('Auth.User.group_id') != ADMIN){
				$shop = $this->Shop->getShopData($this->Session->read('Auth.User.id'),$this->Session->read('Auth.User.group_id'));
				$bal = $shop['balance'];
			}
			$amt_bal = $this->data['amount'];
			if($this->Session->read('Auth.User.group_id') == ADMIN || $this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR)$amt_bal = $amt_bal + $this->data['commission'];

			if($this->Session->read('Auth.User.group_id') != ADMIN && $amt_bal > $bal){
				if($app_flag){
					$msg = "Contact your distributor, he doesn't have enough balance";
				}
				else $msg = "Your amount cannot be greater than your account balance";
				$to_save = false;
			}
			else {
				if($this->Session->read('Auth.User.group_id') != ADMIN){
					$shop = $this->Shop->getShopDataById($this->data['shop'],$this->Session->read('Auth.User.group_id') + 1);
				}
				else {
					$shop = $this->Shop->getShopDataById($this->data['shop'],SUPER_DISTRIBUTOR);
				}
				if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR && $shop['kyc_flag'] == 0 && ($this->data['amount'] + $shop['balance']) > KYC_AMOUNT){
					$msg = "Please collect KYC of the retailer. Retailer balance cannot be greater than Rs." . KYC_AMOUNT;
					$to_save = false;
				}
				else {
					$this->data['shopData'] = $shop;
					if($confirm == 0){
						if($this->Session->read('Auth.User.group_id') != ADMIN)$this->set('balance',$bal - $amt_bal);
						$this->set('data',$this->data);
						$this->render('confirm_transfer','ajax');
					}
					else {
						$mail_subject = "Retail Panel: Amount Transferred";
						$bal1 = 0;
						if($this->Session->read('Auth.User.group_id') == ADMIN){
							$trans_id = $this->Shop->shopTransactionUpdate(ADMIN_TRANSFER,$this->data['amount'],null,$shop['id']);
							if(!empty($this->data['commission'])){
								$this->Shop->shopTransactionUpdate(COMMISSION_SUPERDISTRIBUTOR,$this->data['commission'],$shop['id'],$trans_id);
							}

							if(isset($this->data['typeRadio'])){
								$this->Retailer->query("UPDATE shop_transactions SET type_flag = ".$this->data['typeRadio'].",note = '".addslashes($this->data['description'])."' where id = $trans_id");
							}

							$bal1 = $this->Shop->shopBalanceUpdate($this->data['amount']+$this->data['commission'],'add',$shop['id'],SUPER_DISTRIBUTOR);
							$this->Shop->addOpeningClosing($shop['id'],SUPER_DISTRIBUTOR,$trans_id,$bal1-($this->data['amount']+$this->data['commission']),$bal1);

							$mail_body = "Admin: " . $this->info['company'] . " transferred Rs. " . ($this->data['amount'] + $this->data['commission']) . " to SuperDistributor: " . $shop['company'];
							$name = "SuperDistributor";
							$dist_data = $this->General->getUserDataFromId($shop['user_id']);
							$shop['mobile'] = $dist_data['mobile'];
							$msg = "Dear $name,\nYour account is successfully credited with Rs." . ($this->data['amount'] + $this->data['commission']) . "\nYour current balance is Rs.$bal1";

							if(!empty($this->data['description'])){
								$mail_body .= "<br/>".$this->data['description'];
							}

							if(!in_array($shop['id'],explode(",",SDISTS))){
								$this->General->sendMails($mail_subject, $mail_body);
								$data1 = array();
								$data1['sender'] =  "TFR";
								$data1['process'] =  "limits";
								$data1['type'] = "SD";
								$data1['name'] = $shop['company'];
								$data1['mobile'] = $shop['mobile'];
								$data1['amount'] = $this->data['amount'];
								$data1['commission'] = $this->data['commission'];
								$data1['transid'] = $trans_id;
								$this->General->curl_post_async($this->General->findVar('limit_url'),$data1);
							}
						}
						else if($this->Session->read('Auth.User.group_id') == SUPER_DISTRIBUTOR){
							$trans_id = $this->Shop->shopTransactionUpdate(SDIST_DIST_BALANCE_TRANSFER,$this->data['amount'],$this->info['id'],$shop['id']);
							if(!empty($this->data['commission'])){
								$this->Shop->shopTransactionUpdate(COMMISSION_DISTRIBUTOR,$this->data['commission'],$shop['id'],$trans_id);
							}

							if(isset($this->data['typeRadio'])){
								$this->Retailer->query("UPDATE shop_transactions SET type_flag = ".$this->data['typeRadio'].",note = '".addslashes($this->data['description'])."' where id = $trans_id");
							}

							$bal = $this->Shop->shopBalanceUpdate($this->data['amount']+$this->data['commission'],'subtract',$this->info['id'],SUPER_DISTRIBUTOR);
							$bal1 = $this->Shop->shopBalanceUpdate($this->data['amount']+$this->data['commission'],'add',$shop['id'],DISTRIBUTOR);
							$this->Shop->addOpeningClosing($this->info['id'],SUPER_DISTRIBUTOR,$trans_id,$bal+$this->data['amount']+$this->data['commission'],$bal);
							$this->Shop->addOpeningClosing($shop['id'],DISTRIBUTOR,$trans_id,$bal1-($this->data['amount']+$this->data['commission']),$bal1);

							$mail_body = "SuperDistributor: " . $this->info['company'] . " transferred Rs. " . ($this->data['amount'] + $this->data['commission']) . " to Distributor: " . $shop['company'];
							$name = "Distributor";
							$dist_data = $this->General->getUserDataFromId($shop['user_id']);
							$shop['mobile'] = $dist_data['mobile'];
							$msg = "Dear $name,\nYour account is successfully credited with Rs." . ($this->data['amount'] + $this->data['commission']) . "\nYour current balance is Rs.$bal1";

							if(!empty($this->data['description'])){
								$mail_body .= "<br/>".$this->data['description'];
							}

							if(in_array($this->info['id'],explode(",",SDISTS)) && !in_array($shop['id'],explode(",",DISTS))){
								$this->General->sendMails($mail_subject, $mail_body);

								$data1 = array();
								$data1['sender'] =  "TFR";
								$data1['process'] =  "limits";
								$data1['type'] = "D";
								$data1['name'] = $shop['company'];
								$data1['mobile'] = $shop['mobile'];
								$data1['amount'] = $this->data['amount'];
								$data1['commission'] = $this->data['commission'];
								$data1['transid'] = $trans_id;
								$this->General->curl_post_async($this->General->findVar('limit_url'),$data1);
							}
						}
						else if($this->Session->read('Auth.User.group_id') == DISTRIBUTOR){
							$recId = $this->Shop->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$this->data['amount'],$this->info['id'],$shop['id']);
							if(!isset($params['salesmanId']) || $params['salesmanId'] == ''){
								$dstMob = $this->Retailer->query("SELECT mobile from users where id = '".$this->Session->read('Auth.User.id')."'");
								$sm = $this->Retailer->query("SELECT id,name from salesmen where mobile = '".$dstMob['0']['users']['mobile']."'");
								$params['salesmanId'] = $sm['0']['salesmen']['id'];
								$params['salesmanName'] = $sm['0']['salesmen']['name'];
								$params['distId'] = 0;
							}

							if(isset($this->data['typeRadio'])){
								$this->Retailer->query("UPDATE shop_transactions SET type_flag = ".$this->data['typeRadio'].",note = '".addslashes($this->data['description'])."' where id = $recId");
							}

							$this->Retailer->query("INSERT INTO salesman_transactions (shop_tran_id,salesman,payment_mode,payment_type,details,collection_date,created) VALUES ('".$recId."','".$params['salesmanId']."','".MODE_CASH."','".TYPE_TOPUP."','','".date('Y-m-d')."','".date('Y-m-d H:i:s')."')");
							$bal = $this->Shop->shopBalanceUpdate($this->data['amount'],'subtract',$this->info['id'],DISTRIBUTOR);
							$bal1 = $this->Shop->shopBalanceUpdate($this->data['amount'],'add',$shop['id'],RETAILER);

							$this->Shop->addOpeningClosing($shop['id'],RETAILER,$recId,$bal1-$this->data['amount'],$bal1);
							$this->Shop->addOpeningClosing($this->info['id'],DISTRIBUTOR,$recId,$bal+$this->data['amount'],$bal);

							if(!empty($shop['shopname'])){
								$shop_name = substr($shop['shopname'],0,15);
								if($shop_name != $shop['shopname'])$shop_name = $shop_name . "..";
							}
							else $shop_name = $shop['mobile'];
							$mail_body = "Distributor: " . $this->info['company'] . " (Salesman: ".$params['salesmanName']." ) transferred Rs. " . $this->data['amount'] . " to Retailer: " . $shop_name;
							$name = "Retailer";
							$msg = "Dear $name,\nYour account is successfully credited with Rs." . $this->data['amount']. "\nYour current balance is Rs.$bal1";

							if(!empty($this->data['description'])){
								$mail_body .= "<br/>".$this->data['description'];
							}

							if(in_array($this->info['id'],explode(",",DISTS))){
								$this->General->sendMails($mail_subject, $mail_body);

								$data1 = array();
								$data1['sender'] =  "TFR";
								$data1['process'] =  "limits";
								$data1['type'] = "R";
								$data1['name'] = $shop['shopname'];
								$data1['mobile'] = $shop['mobile'];
								$data1['amount'] = $this->data['amount'];
								$data1['transid'] = $recId;
								$this->General->curl_post_async($this->General->findVar('limit_url'),$data1);
							}
						}
						$this->General->sendMessage($shop['mobile'],$msg,'shops');
						if(isset($params['distId'])){
							$sm = $this->Retailer->query("SELECT mobile,balance,tran_limit from salesmen where id = ".$params['salesmanId']);

							$data = $this->Retailer->query("SELECT sum(shop_transactions.amount) as topups FROM salesman_transactions inner join shop_transactions ON (shop_transactions.id=salesman_transactions.shop_tran_id) WHERE salesman_transactions.salesman=".$params['salesmanId']." AND shop_transactions.ref2_id is not null AND salesman_transactions.payment_type=2 AND collection_date='".date('Y-m-d')."'");

							$message = "Amount Rs ".$this->data['amount']." transferred to retailer $shop_name successfully.";
							$message .= "\nYour balance now: $bal";
							$message .= "\nYour today's topups: " . $data['0']['0']['topups'];
							if(!$app_flag){
								//$this->General->sendMessage($sm['0']['salesmen']['mobile'],$message,'ussd');
							}
							else {
								$sms_send = $message;
							}
						}
						else if(isset($params['salesmanId'])){
							$sm = $this->Retailer->query("SELECT mobile,balance,tran_limit from salesmen where id = ".$params['salesmanId']);

							$this->Retailer->query("UPDATE salesmen SET balance = ".($sm['0']['salesmen']['balance'] - $this->data['amount'])." WHERE id=".$params['salesmanId']);
							$data = $this->Retailer->query("SELECT sum(shop_transactions.amount) as topups FROM salesman_transactions inner join shop_transactions ON (shop_transactions.id=salesman_transactions.shop_tran_id) WHERE salesman_transactions.salesman=".$params['salesmanId']." AND salesman_transactions.payment_type=2 AND collection_date='".date('Y-m-d')."'");

							$message = "Amount Rs ".$this->data['amount']." transferred to retailer $shop_name successfully.";
							$message .= "\nYour balance now: " . ($sm['0']['salesmen']['balance'] - $this->data['amount']) . " (" . $sm['0']['salesmen']['tran_limit'] . ")";
							$message .= "\nYour today's topups: " . $data['0']['0']['topups'];
							if(!$app_flag){
								$this->General->sendMessage($sm['0']['salesmen']['mobile'],$message,'shops');
							}
							else {
								$sms_send = $message;
							}
						}

						//$this->General->sendMails($mail_subject, $mail_body);
						if(!$app_flag){
							echo "<script> reloadShopBalance(".$bal.");  </script>";
							$this->render('/elements/shop_transfer','ajax');
						}
						else {
							return array('status' => 'success','balance' => $bal1,'description' => $sms_send);
						}
					}
				}
			}
		}

		if(!$to_save){
			if(!$app_flag){
				$this->set('data',$this->data);
				$msg = '<div class="error_class">'.$msg.'</div>';
				$this->Session->setFlash(__($msg, true));
				$this->render('/elements/shop_transfer','ajax');
			}
			else {
				return array('status' => 'failure','description' => $msg);
			}
		}
	}


	function backTransfer(){
		if($this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != ADMIN){
			$this->redirect(array('action' => 'index'));
		}
		$this->set('data',$this->data);
		$this->render('/elements/shop_transfer');
	}

	function kitsTransfer(){

		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR){
			$this->redirect('/shops/view');

		}

	}

	function transferKits(){
		//echo "======".isset($this->data['commission_flag'])?"Y":"N"."======";
		$this->data['commission_flag'] = isset($this->data['commission_flag'])?$this->data['commission_flag']:"";
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR)$this->redirect('/shops/view');
		$to_save = true;
		$confirm = 0;
		if(isset($this->data['confirm']))
		$confirm = $this->data['confirm'];
		preg_match('/^[0-9]/',$this->data['amount'],$matches,0);
		preg_match('/^[0-9]/',$this->data['kit_commission'],$matches1,0);
		if($this->data['shop'] == 0)
		{
			$msg = "Please select distributor";
			$to_save = false;
		}
		else if(empty($this->data['amount']))
		{
			$msg = "Please enter amount";
			$to_save = false;
		}
		else if($this->data['amount'] <= 0 || empty($matches)){
			$msg = "Amount entered is not valid";
			$to_save = false;
		}
		else if($this->data['commission_flag'] == 'on' && (empty($matches1) || empty($this->data['kit_commission']))){
			$msg = "Commission amount entered is not valid";
			$to_save = false;
		}
		else {
			$shop = $this->Shop->getShopDataById($this->data['shop'],DISTRIBUTOR);
			$this->data['shopData'] = $shop;

			if($confirm == 0){
				$this->set('data',$this->data);
				$this->render('confirm_kits_transfer','ajax');
			} else {

				$mail_subject = "Retail Panel: Kits Transferred";
				$kits = $shop['kits'];
				if($kits == -1)$kits = 0;
				if(empty($this->data['kit']))
				$kits = -1;
				else
				$kits = $kits + $this->data['kit'];

				if($this->data['commission_flag'] == 'on'){
					$this->Retailer->query("UPDATE distributors SET kits=$kits,commission_kits_flag=1,retailer_creation=1,discount_kit=".$this->data['kit_commission']." WHERE id = " . $shop['id']);
				}
				else {
					$this->Retailer->query("UPDATE distributors SET kits=$kits,commission_kits_flag=0,retailer_creation=1,discount_kit=null WHERE id = " . $shop['id']);
				}

				if(!empty($this->data['kit'])){
					$this->Retailer->query("INSERT INTO distributors_kits (distributor_id,kits,amount,note,timestamp) VALUES (".$shop['id'].",".$this->data['kit'].",".$this->data['amount'].",'".addslashes($this->data['note'])."','".date('Y-m-d H:i:s')."')");
				}
				else {
					$this->Retailer->query("INSERT INTO distributors_kits (distributor_id,amount,note,timestamp) VALUES (".$shop['id'].",".$this->data['amount'].",'".addslashes($this->data['note'])."','".date('Y-m-d H:i:s')."')");
				}

				$mail_body = "SuperDistributor: " . $this->info['company'] . " transferred kits " . $this->data['kit'] . " to Distributor: " . $shop['company'] . " in Rs. " . $this->data['amount'];
				if(!empty($this->data['note'])){
					$mail_body .= "<br/>Note: ".$this->data['note'];
				}

				$dist_data = $this->General->getUserDataFromId($shop['user_id']);
				$shop['mobile'] = $dist_data['mobile'];
				$msg = "Dear Distributor,\nYour account is successfully credited with " . $this->data['kit']. "kits";
				if($kits != -1){
					$msg .= "\nYou have total $kits now";
				}
				$this->General->sendMessage($shop['mobile'],$msg,'shops');
				$this->General->sendMails($mail_subject, $mail_body);

				$this->render('/elements/shop_kits_transfer','ajax');
			}
		}

		if(!$to_save){
			$this->set('data',$this->data);
			$msg = '<div class="error_class">'.$msg.'</div>';
			$this->Session->setFlash(__($msg, true));
			$this->render('/elements/shop_kits_transfer','ajax');
		}
	}


	function backKitTransfer(){
		if($this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR)
		$this->redirect('/');
		$this->set('data',$this->data);
		$this->render('/elements/shop_kits_transfer');
	}


	function logout(){
		$this->Auth->logout();
                session_destroy();
		$this->redirect('/shops');
	}

	function updateTransactionPP(){
		$TransactionId = $_REQUEST['TransactionId'];
		$TransactionType = $_REQUEST['TransactionType'];
		$MobileNo = $_REQUEST['MobileNo'];
		$CustomerNo = $_REQUEST['CustomerNo'];
		$Amount = $_REQUEST['Amount'];
	}

	function lastTransactions($page = null){
		if($page == null)$page = 1;
		$ret = $this->Shop->getLastTransactions(null,$page);
		//$this->printArray($ret);
	}

	function initializeOpeningBalance(){//initialize balance once everyday at 11:30PM
		$this->SuperDistributor->updateAll(array('SuperDistributor.opening_balance' => 'SuperDistributor.balance'));
		$this->Distributor->updateAll(array('Distributor.opening_balance' => 'Distributor.balance'));
		$this->Retailer->updateAll(array('Retailer.opening_balance' => 'Retailer.balance'));
		$this->autoRender = false;
	}


	function products(){
		$this->render('products','xml/default');
	}

	function createCommissionTemplate(){
		$slab_id = 3;
		$percents = array(3,1.9,3.5,2,3.5,4.5,3,3,2.5,2.5,3.5,4,3.5,3.5,2,2,3.3,2.7,3.5,3.3,4.2);
		$products = $this->Retailer->query("SELECT id from products where active = 1");
		$i = 0;
		foreach($products as $prod){

			$this->Retailer->query("INSERT INTO slabs_products (slab_id,product_id,percent) VALUES (".$slab_id.",".$prod['products']['id'].",".$percents[$i].")");
			$i++;
		}
		$this->autoRender = false;
	}


	function genaric_match($template,$string,$varStart="{{",$varEnd="}}"){


		$template = str_replace($varStart,"|~|`",$template);
		$template = str_replace($varEnd,"`|~|",$template);

		$t=explode("|~|",$template);

		$temp="";
		$i=0;
		foreach ($t as $n=>$v){
			$i++;
			if (($i==count($t)||($i==(count($t)-1)&&$t[$n+1]==""))&&substr($v,0,1)=="`"&&substr($v,-1)=="`"){
				//Last Item
				$temp.="(?P<".substr($v,1,-1).">.++)";

			}elseif(substr($v,0,1)=="`"&&substr($v,-1)=="`"){
				//Search Item
				$temp.="(?P<".substr($v,1,-1).">[^".$t[$n+1]."]++)";

			}else{
				$temp.=$v;
			}

		}
		$temp="~^".$temp."$~";
		echo $temp . "<br/>";
		echo $string . "<br/>";

		preg_match($temp, $string, $matches);

		return $matches;

	}

	function test(){
		echo "1";
		$this->autoRender = false;
		exit;
		/*$rets = array(24,73,124,196,245,301,507,527,580,592,658,662,672,711,793,835,943,25,61,73,124,580,666,771,933,943,955,984,873,821,597,225,597,821,908,244);
		 $bals = array(48.35,87.6,29.25,145,97.5,68.25,194,121.87,29.1,19.3,29.4,97,368.6,48.25,57.6,29.39,58.5,108.23,48.75,87.6,29.25,29.1,121.87,108.23,300.7,58.5,58.02,9.72,145.89,107.67,106.92,108.22,106.92,107.67,58.32,53.46);

		 $i = 0;
		 foreach($rets as $retailer){
			$trans_id = $this->Shop->shopTransactionUpdate(DIST_RETL_BALANCE_TRANSFER,$bals[$i],1,$retailer);

			$bal = $this->Shop->shopBalanceUpdate($bals[$i],'subtract',1,DISTRIBUTOR);
			$bal1 = $this->Shop->shopBalanceUpdate($bals[$i],'add',$retailer,RETAILER);
			$this->Shop->addOpeningClosing(1,DISTRIBUTOR,$trans_id,$bal+$bals[$i],$bal);
			$this->Shop->addOpeningClosing($retailer,RETAILER,$trans_id,$bal1-$bals[$i],$bal1);

			$msg = "Dear Retailer,\nYour account is successfully credited with Rs." . $bals[$i]. "\nYour current balance is Rs. $bal1";
			$msg .= "\nKal hui asubidha ke liye maafi chahte hai";

			$shop = $this->Shop->getShopDataById($retailer,RETAILER);
			$this->General->sendMessage($shop['mobile'],$msg,'shops');
			$i++;
			}*/

		echo "1";exit;
		$this->General->sendMessageViaInfobip('',array('9819032643','9833032643'),'hello');exit;
		$this->General->mailToUsers('test','test',array('ashish@mindsarray.com'));
		exit;
		$this->General->sendMessage('','9892609560,9819852204,9004387418','hello');
		exit;
	}

	function script(){
		$this->Invoice->recursive = -1;
		$invoices = $this->Invoice->find('all');
		foreach($invoices as $invoice){
			if($invoice['Invoice']['group_id'] == SUPER_DISTRIBUTOR){
				$parent = null;
			}
			else if($invoice['Invoice']['group_id'] == DISTRIBUTOR || $invoice['Invoice']['group_id'] == RETAILER){
				$shop = $this->Shop->getShopDataById($invoice['Invoice']['ref_id'],$invoice['Invoice']['group_id']);
				$parent = $shop['parent_id'];
			}
			$this->Invoice->updateAll(array('Invoice.from_id' => $parent),array('Invoice.id' => $invoice['Invoice']['id']));
		}
		$this->autoRender = false;
	}

	function issue(){
		$id = $_REQUEST['id'];
		$type = $_REQUEST['type'];
		$child = $_REQUEST['child'];

		if($type == RECEIPT_TOPUP){
			$number = $this->Shop->getTopUpReceiptNumber($id);
		}
		else if($type == RECEIPT_INVOICE){
			$invoice = $this->Invoice->find('first',array('fields' => array('Invoice.timestamp','Invoice.invoice_number'), 'conditions' => array('Invoice.id' => $id)));
			$number = $invoice['Invoice']['invoice_number'];
		}
		$this->set('number',$number);
		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('child',$child);
		$this->render('receipt_form','ajax');
	}
        
	function graphRetailer(){
		$type = $_REQUEST['type'];
		$id = $_REQUEST['id'];
		if(empty($_REQUEST['from'])){
			$from = date('Y-m-d',strtotime('-30 days'));
			$to = date('Y-m-d');
		}
		else {
			$from = $_REQUEST['from'];
			$to = $_REQUEST['to'];
			if(checkdate(substr($from,2,2), substr($from,0,2), substr($from,4)) && checkdate(substr($to,2,2), substr($to,0,2), substr($to,4))){
				$from =  substr($from,4) . "-" . substr($from,2,2) . "-" . substr($from,0,2);
				$to =  substr($to,4) . "-" . substr($to,2,2) . "-" . substr($to,0,2);
			}
			else {
				$from = date('Y-m-d',strtotime('-30 days'));
				$to = date('Y-m-d');
			}
		}

		if($this->Session->read('Auth.User.group_id') != ADMIN && $this->Session->read('Auth.User.group_id') != SUPER_DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != DISTRIBUTOR && $this->Session->read('Auth.User.group_id') != RELATIONSHIP_MANAGER)$this->redirect('/shops/view');

		if($type == 'r'){
			if(!empty($id)){
				if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,RETAILER);
					if($data['parent_id'] != $this->info['id'])exit;
				}
				else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,RETAILER);
					$data1 = $this->Shop->getShopDataById($data['parent_id'],DISTRIBUTOR);
					if($data1['parent_id'] != $this->info['id']){
						exit;
					}
				}
			}
			else exit;

			$sale = $this->Retailer->query("SELECT sale,date,topup FROM retailers_logs WHERE retailer_id  = $id AND date >= '$from' AND date <= '$to'");
			$begin = new DateTime($from);
			$end = new DateTime($to);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);

			$data = array();
			$totSale = 0;
			$i = 0;

			$saleData = array();

			foreach($sale as $sl){
				$saleData[$sl['retailers_logs']['date']]['sale'] = $sl['retailers_logs']['sale'];
				$saleData[$sl['retailers_logs']['date']]['topup'] = $sl['retailers_logs']['topup'];
			}

			foreach ($period as $dt){
				$data1 = array();
				$date = $dt->format("Y-m-d");
				if(isset($saleData[$date])){
					if(isset($saleData[$date]['sale'])){
						$totSale += $saleData[$date]['sale'];
						$sale = intval($saleData[$date]['sale']);
					}
					else {
						$sale = 0;
					}

					if(isset($saleData[$date]['topup'])){
						$topup = intval($saleData[$date]['topup']);
					}
					else {
						$topup = 0;
					}
					$data[] = array($date,$sale,$topup);
				}
				else {
					$data[] = array($date,0,0);
				}

				$i++;
			}

			$avg = intval($totSale/$i);
			$i=0;
			foreach($data as $dt){
				$data[$i][3]=$avg;
				$i++;
			}
			$graphData = array(
			   'labels' => array(
			array('string' => 'Sample'),
			array('number' => 'Daily Sale'),
			array('number' => 'Daily Topup'),
			array('number' => 'Average Sale')
			),
			   'data' => $data,
			   'title' => 'Retailer Sale Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
		}
		else if($type == 'd'){
			if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
				$id = $this->info['id'];
			}

			if(!empty($id)){
				if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
					$data = $this->Shop->getShopDataById($id,DISTRIBUTOR);
					if($data['parent_id'] != $this->info['id']){
						exit;
					}
				}

				$sale = $this->Retailer->query("SELECT sum(sale) as amts,date FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id  = $id AND date >= '$from' AND date <= '$to' GROUP by date order by date");
				$dist_data = $this->Retailer->query("SELECT sum(topup_buy) as topup_buy,sum(topup_sold) as topup_sold,sum(topup_unique) as topup_unique,sum(retailers) as retailers,sum(transacting) as transacting,date FROM distributors_logs WHERE distributor_id = $id AND date >= '$from' AND date <= '$to' group by date order by date");
				$averageResult=$this->Retailer->query("SELECT retailers_logs.sale,retailers_logs.date,retailers_logs.retailer_id from retailers_logs,retailers WHERE retailers.parent_id = $id AND retailers.id = retailers_logs.retailer_id AND retailers_logs.date >= '$from' AND retailers_logs.date <= '$to'");
			}
			else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
				$sale = $this->Retailer->query("SELECT sum(sale) as amts,date FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND date >= '$from' AND date <= '$to' GROUP by date order by date");
				$dist_data = $this->Retailer->query("SELECT sum(topup_buy) as topup_buy,sum(topup_sold) as topup_sold,sum(topup_unique) as topup_unique,sum(retailers) as retailers,sum(transacting) as transacting,date FROM distributors_logs WHERE date >= '$from' AND date <= '$to' group by date order by date");
				$averageResult=$this->Retailer->query("SELECT retailers_logs.sale,retailers_logs.date,retailers_logs.retailer_id from retailers_logs WHERE retailers_logs.date >= '$from' AND retailers_logs.date <= '$to'");
			}

			$begin = new DateTime($from);
			$end = new DateTime($to);

			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);

			$data = array();
			$data0 = array();
			$data1 = array();
			$data2 = array();

			$totSale = 0;
			$totSale_sec = 0;
			$i = 0;
			$avgSale = 0;
			$avgSale_sec = 0;

			$saleData = array();

			foreach($sale as $sl){
				$saleData[$sl['retailers_logs']['date']]['sale'] = $sl['0']['amts'];
				$totSale += $sl['0']['amts'];
				$i++;
			}

			if($i>0)$avgSale = intval($totSale/$i);
			$i = 0;
			$retData = array();

			foreach($dist_data as $tp){
				$saleData[$tp['distributors_logs']['date']]['topup'] = $tp['0']['topup_buy'];
				$saleData[$tp['distributors_logs']['date']]['topup_sec'] = $tp['0']['topup_sold'];
				$totSale_sec += $tp['0']['topup_sold'];
				$i++;

				$retData[$tp['distributors_logs']['date']]['retailers'] = $tp['0']['retailers'];
				$retData[$tp['distributors_logs']['date']]['transacting'] = $tp['0']['transacting'];
				$retData[$tp['distributors_logs']['date']]['topups'] = $tp['0']['topup_unique'];
			}

			if($i>0)$avgSale_sec = intval($totSale_sec/$i);


			foreach($averageResult as $sl){
				if($sl['retailers_logs']['sale'] >= 1000){
					if(isset($retData[$sl['retailers_logs']['date']]['plus1000'])){
						$retData[$sl['retailers_logs']['date']]['plus1000'] += 1;
					}
					else {
						$retData[$sl['retailers_logs']['date']]['plus1000'] = 1;
					}
				}
				else if($sl['retailers_logs']['sale'] >= 500 && $sl['retailers_logs']['sale'] < 1000){
					if(isset($retData[$sl['retailers_logs']['date']]['plus500'])){
						$retData[$sl['retailers_logs']['date']]['plus500'] += 1;
					}
					else {
						$retData[$sl['retailers_logs']['date']]['plus500'] = 1;
					}
				}
				else {
					if(isset($retData[$sl['retailers_logs']['date']]['less500'])){
						$retData[$sl['retailers_logs']['date']]['less500'] += 1;
					}
					else {
						$retData[$sl['retailers_logs']['date']]['less500'] = 1;
					}
				}
			}

			foreach ($period as $dt){
				$date = $dt->format("Y-m-d");
				if(isset($saleData[$date])){
					if(isset($saleData[$date]['sale'])){
						$sale = intval($saleData[$date]['sale']);
					}
					else {
						$sale = 0;
					}

					if(isset($saleData[$date]['topup'])){
						$topup = intval($saleData[$date]['topup']);
					}
					else {
						$topup = 0;
					}

					if(isset($saleData[$date]['topup_sec'])){
						$topup_s = intval($saleData[$date]['topup_sec']);
					}
					else {
						$topup_s = 0;
					}
					$data[] = array($date,$topup,$topup_s,$avgSale_sec);
					$data0[] = array($date,$sale,$avgSale);
				}
				else {
					$data[] = array($date,0,0,0);
					$data0[] = array($date,0,0);
				}

				$retBefore = 0;
				if(isset($retData[$date])){
					if(isset($retData[$date]['retailers'])){
						$retBefore = $retData[$date]['retailers'];
					}
					$ret = $retBefore;

					if(isset($retData[$date]['transacting'])){
						$trans = $retData[$date]['transacting'];
					}
					else {
						$trans = 0;
					}

					if(isset($retData[$date]['topups'])){
						$topups = $retData[$date]['topups'];
					}
					else {
						$topups = 0;
					}

					if(isset($retData[$date]['plus1000'])){
						$plus1000 = $retData[$date]['plus1000'];
					}
					else {
						$plus1000 = 0;
					}

					if(isset($retData[$date]['plus500'])){
						$plus500 = $retData[$date]['plus500'];
					}
					else {
						$plus500 = 0;
					}

					if(isset($retData[$date]['less500'])){
						$less500 = $retData[$date]['less500'];
					}
					else {
						$less500 = 0;
					}

					$data1[] = array($date,$ret,$trans,$topups);
					$data2[] = array($date,$plus1000,$plus500,$less500);
				}
				else {
					$data1[] = array($date,$retBefore,0,0);
					$data2[] = array($date,0,0,0);
				}
				$i++;
			}

			$graphData = array(
			   'labels' => array(
			array('string' => 'Sample'),
			array('number' => 'Daily Topup (Primary)'),
			array('number' => 'Daily Sale (Secondary)'),
			array('number' => 'Average Sale (Secondary)')
			),
			   'data' => $data,
			   'title' => 'Distributor Sale Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $graphData0 = array(
			   'labels' => array(
			   array('string' => 'Sample'),
			   array('number' => 'Daily Sale (Tertiary)'),
			   array('number' => 'Average Sale (Tertiary)')
			   ),
			   'data' => $data0,
			   'title' => 'Retailers Sale Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $graphData1 = array(
			   'labels' => array(
			   array('string' => 'Sample'),
			   array('number' => 'Total Retailers'),
			   array('number' => 'Transacting Retailers'),
			   array('number' => 'Daily Unique Retailer topups')
			   ),
			   'data' => $data1,
			   'title' => 'Distributor Retailers Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $graphData2 = array(
			   'labels' => array(
			   array('string' => 'Sample'),
			   array('number' => 'Retailers (Sale >= 1000)'),
			   array('number' => 'Retailers (500 <= Sale < 1000)'),
			   array('number' => 'Retailers (Sale < 500)')
			   ),
			   'data' => $data2,
			   'title' => 'Retailer Performance Report',
			   'type' => 'line',
			   'width' => '1200',
			   'height'=>'500'
			   );
			    
			   $this->set('data0',$graphData0);
			   $this->set('data1',$graphData1);
			   $this->set('data2',$graphData2);
		}
                //echo json_encode($graphData);
		$this->set('data',$graphData);
		$this->set('type',$type);
		$this->set('id',$id);
		$this->set('from',$from);
		$this->set('to',$to);
	}

	function mainReport($id = null)
	{
		if($_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR && $_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR && $_SESSION['Auth']['User']['group_id'] != ADMIN && $_SESSION['Auth']['User']['group_id'] != RELATIONSHIP_MANAGER){
			$this->redirect('/');
		}
		$show = false;
		if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
			$distid = $this->info['id'];
			$show = true;
			$this->set('name',$this->info['company']);
		}
		else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
			$sdistid = $this->info['id'];
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
				if($shop['parent_id'] == $this->info['id'] || $shop['parent_id'] == $this->info['super_dist_id']){
					$show = true;
					$this->set('name',$shop['company']);
					$distid = $id;
					$this->set('dist',$distid);
				}
			}
			else {
				$this->set('name','All Distributors');
				$show = true;
			}
		}
		else if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
			$rmid = $this->info['id'];
			$sdistid = $this->info['super_dist_id'];
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,DISTRIBUTOR);
				if($shop['rm_id'] == $this->info['id'] && $shop['parent_id'] == $this->info['super_dist_id']){
					$show = true;
					$this->set('name',$shop['company']);
					$distid = $id;
					$this->set('dist',$distid);
				}
			}
			else {
				$this->set('name','All Distributors');
				$show = true;
			}
		}
		else if($_SESSION['Auth']['User']['group_id'] == ADMIN){
			if(!empty($id)){
				$shop = $this->Shop->getShopDataById($id,SUPER_DISTRIBUTOR);
				if(!empty($shop)){
					$sdistid = $id;
					$this->set('name',$shop['company']);
					$show = true;
					$this->set('dist',$sdistid);
				}
			}
			else {
				$this->set('name','All MasterDistributors');
				$show = true;
			}
		}

		if($show){
			$today = date('Y-m-d');
			$datas = array();

			if(!isset($sdistid) && !isset($distid)){//Admin with all SD's
				$data_buy = $this->User->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions WHERE ((shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER.") OR (shop_transactions.type = ".COMMISSION_DISTRIBUTOR.")) AND shop_transactions.date = '$today'");
				$data_sold = $this->User->query("SELECT sum(amount) as amts,count(distinct ref2_id) as cts FROM shop_transactions WHERE type = ".DIST_RETL_BALANCE_TRANSFER." AND date = '$today'");
				$data_ret = $this->User->query("SELECT count(retailers.id) as cts FROM retailers");
				$data_trans = $this->User->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va WHERE va.status != 2 AND va.status !=3 AND va.date = '$today'");
				
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '2013-06-10' AND retailers.parent_id=64
				$data_bef = $this->Retailer->query("SELECT sum(topup_buy) as topup_buy,sum(topup_sold) as topup_sold,sum(topup_unique) as topup_unique,sum(retailers) as retailers,sum(transacting) as transacting,date FROM distributors_logs WHERE date >= '".date('Y-m-d',strtotime('-30 days'))."' group by date order by date");
				$data_bef_ret = $this->Retailer->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs WHERE retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}
			else if(isset($sdistid) && !isset($distid)){//SD with all distributors or RM with all distributors or Admin with a SD
				if(isset($rmid))$extra = " AND distributors.rm_id = $rmid";
				else $extra = " AND distributors.parent_id = $sdistid";
				
				$data_buy = $this->User->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions,distributors WHERE ((shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER." AND shop_transactions.ref1_id = distributors.parent_id $extra) OR (shop_transactions.type = ".COMMISSION_DISTRIBUTOR." AND distributors.id = shop_transactions.ref1_id $extra)) AND shop_transactions.date = '$today'");
				$data_sold = $this->User->query("SELECT sum(shop_transactions.amount) as amts,count(distinct shop_transactions.ref2_id) as cts FROM shop_transactions,distributors WHERE shop_transactions.type = ".DIST_RETL_BALANCE_TRANSFER." AND shop_transactions.date = '$today' AND shop_transactions.ref1_id = distributors.id $extra");
				$data_ret = $this->User->query("SELECT count(retailers.id) as cts FROM retailers,distributors WHERE retailers.parent_id = distributors.id $extra");
				$data_trans = $this->User->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers,distributors WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND retailers.parent_id = distributors.id AND va.date = '$today' $extra");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers,distributors WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND retailers.parent_id = distributors.id AND va.date = '$today' $extra
				$data_bef = $this->Retailer->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs,distributors WHERE distributor_id  = distributors.id $extra AND distributors_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by distributors_logs.date order by distributors_logs.date");
				$data_bef_ret = $this->Retailer->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs,retailers,distributors WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id=distributors.id $extra AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}
			else if(isset($sdistid) && isset($distid)){//SD with a distributor OR RM with a distributor
				$extra = "";
				if(isset($rmid))$extra = " AND distributors.rm_id = $rmid";
				$data_buy = $this->User->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions,distributors WHERE ((shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER." AND shop_transactions.ref1_id = $sdistid AND distributors.parent_id = shop_transactions.ref1_id $extra) OR (shop_transactions.type = ".COMMISSION_DISTRIBUTOR." AND shop_transactions.ref1_id = $distid AND distributors.id = shop_transactions.ref1_id AND distributors.parent_id = $sdistid $extra)) AND shop_transactions.date = '$today'");
				$data_sold = $this->User->query("SELECT sum(shop_transactions.amount) as amts,count(distinct shop_transactions.ref2_id) as cts FROM shop_transactions WHERE shop_transactions.type = ".DIST_RETL_BALANCE_TRANSFER." AND shop_transactions.date = '$today' AND shop_transactions.ref1_id = $distid");
				$data_ret = $this->User->query("SELECT count(retailers.id) as cts FROM retailers WHERE retailers.parent_id = $distid");
				$data_trans = $this->User->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id=$distid");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '2013-06-10' AND retailers.parent_id=$distid
				
				$data_bef = $this->Retailer->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by distributors_logs.date order by distributors_logs.date");
				$data_bef_ret = $this->Retailer->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}
			else if(!isset($sdistid) && isset($distid)){//Distributor
				$data_buy = $this->User->query("SELECT sum(shop_transactions.amount) as amts FROM shop_transactions WHERE ((shop_transactions.type = ".SDIST_DIST_BALANCE_TRANSFER." AND shop_transactions.ref2_id = $distid) OR (shop_transactions.type = ".COMMISSION_DISTRIBUTOR." AND shop_transactions.ref1_id = $distid)) AND shop_transactions.date = '$today'");
				$data_sold = $this->User->query("SELECT sum(shop_transactions.amount) as amts,count(distinct shop_transactions.ref2_id) as cts FROM shop_transactions WHERE shop_transactions.type = ".DIST_RETL_BALANCE_TRANSFER." AND shop_transactions.date = '$today' AND shop_transactions.ref1_id = $distid");
				$data_ret = $this->User->query("SELECT count(retailers.id) as cts FROM retailers WHERE retailers.parent_id = $distid");
				$data_trans = $this->User->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id=$distid");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id=$distid
					
				$data_bef = $this->Retailer->query("SELECT sum(distributors_logs.topup_buy) as topup_buy,sum(distributors_logs.topup_sold) as topup_sold,sum(distributors_logs.topup_unique) as topup_unique,sum(distributors_logs.retailers) as retailers,sum(distributors_logs.transacting) as transacting,distributors_logs.date FROM distributors_logs WHERE distributor_id = $distid AND distributors_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by distributors_logs.date order by distributors_logs.date");
				$data_bef_ret = $this->Retailer->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
			}

			foreach($data_buy as $dt){
				$datas['buy'] = $dt['0']['amts'];
			}

			foreach($data_sold as $dt){
				$datas['sold'] = $dt['0']['amts'];
				$datas['unique'] = $dt['0']['cts'];
			}

			foreach($data_ret as $dt){
				$datas['retailers'] = $dt['0']['cts'];
			}

			foreach($data_trans as $dt){
				$datas['transacting'] = $dt['0']['cts'];
				$datas['sale'] = $dt['0']['sale'];
				$datas['percent_trans'] = $datas['retailers']==0 ? 0 : intval($datas['transacting']*100/$datas['retailers']);
				$datas['sale_avg_ret'] = empty($datas['transacting'])? 0 : intval($datas['sale']/$datas['transacting']);
				$datas['sale_avg'] = intval($datas['sale']);
			}

			$yest_date = date('Y-m-d',strtotime('-1 days'));
			$week_date = date('Y-m-d',strtotime('-7 days'));
			$month_date = date('Y-m-d',strtotime('-30 days'));

			$datas_before = array();
			$ret_before=0;
			$ret_tot = 0;
			$datas_before['week']['new'] = 0;
			$datas_before['month']['new'] = 0;
			$i=0;
			$j=0;
			foreach($data_bef as $dt){
				if($dt['distributors_logs']['date'] == $yest_date){
					$datas_before['yesterday']['buy'] = $dt['0']['topup_buy'];
					$datas_before['yesterday']['sold'] = $dt['0']['topup_sold'];
					$datas_before['yesterday']['unique'] = $dt['0']['topup_unique'];
					$datas_before['yesterday']['retailers'] = $dt['0']['retailers'];
					$datas_before['yesterday']['transacting'] = $dt['0']['transacting'];
					$datas_before['yesterday']['new'] = $dt['0']['retailers'] - $ret_before;
					$datas_before['yesterday']['percent_trans'] = intval($dt['0']['transacting']*100/$dt['0']['retailers']);
				}

				if($dt['distributors_logs']['date'] >= $week_date){
					$datas_before['week']['percent_trans'] += $dt['0']['transacting']/$dt['0']['retailers'];
					$datas_before['week']['new'] = $datas_before['week']['new'] + $dt['0']['retailers'] - $ret_before;
					$datas_before['week']['buy'] +=  $dt['0']['topup_buy'];
					$datas_before['week']['sold'] +=  $dt['0']['topup_sold'];
					$datas_before['week']['unique'] +=  $dt['0']['topup_unique'];
					$i++;
				}
				$datas_before['month']['percent_trans'] += $dt['0']['transacting']/$dt['0']['retailers'];
				if($ret_before > 0)$datas_before['month']['new'] = $datas_before['month']['new'] + $dt['0']['retailers'] - $ret_before;
				$datas_before['month']['buy'] +=  $dt['0']['topup_buy'];
				$datas_before['month']['sold'] +=  $dt['0']['topup_sold'];
				$datas_before['month']['unique'] +=  $dt['0']['topup_unique'];
				$ret_before = $dt['0']['retailers'];
				$j++;
			}

			$datas_before['week']['percent_trans'] = empty($datas_before['week']['percent_trans'])? 0 : intval($datas_before['week']['percent_trans']*100/$i);
			$datas_before['week']['buy'] = empty($datas_before['week']['buy']) ? 0 : intval($datas_before['week']['buy']/$i);
			$datas_before['week']['sold']= empty($datas_before['week']['sold'])? 0 : intval($datas_before['week']['sold']/$i);
			$datas_before['week']['unique'] = empty($datas_before['week']['unique']) ? 0 : intval($datas_before['week']['unique']/$i);

			$datas_before['month']['percent_trans'] = empty($datas_before['month']['percent_trans'])? 0 : intval($datas_before['month']['percent_trans']*100/$j);
			$datas_before['month']['buy'] = empty($datas_before['month']['buy']) ? 0 : intval($datas_before['month']['buy']/$j);
			$datas_before['month']['sold'] = empty($datas_before['month']['sold']) ? 0 : intval($datas_before['month']['sold']/$j);
			$datas_before['month']['unique'] = empty($datas_before['month']['unique']) ? 0 :intval($datas_before['month']['unique']/$j);

			if(isset($datas_before['yesterday']['retailers'])){
				$datas['new'] = $datas['retailers'] - $datas_before['yesterday']['retailers'];
			}
			$i = 0;
			$j = 0;
			foreach($data_bef_ret as $dt){
				if($dt['retailers_logs']['date'] == $yest_date){
					$datas_before['yesterday']['sale'] = $dt['0']['sale'];
					$datas_before['yesterday']['sale_avg_ret'] = intval($datas_before['yesterday']['sale']/$datas_before['yesterday']['transacting']);
				}
				if($dt['retailers_logs']['date'] >= $week_date){
					$datas_before['week']['sale'] += $dt['0']['sale'];
					$datas_before['week']['sale_avg_ret'] += $dt['0']['sale']/$dt['0']['transacting'];
					$i++;
				}
				$datas_before['month']['sale'] += $dt['0']['sale'];
				$datas_before['month']['sale_avg_ret'] += $dt['0']['sale']/$dt['0']['transacting'];
				$j++;
			}

			$datas_before['yesterday']['sale_avg'] = isset($datas_before['yesterday']['sale']) ? $datas_before['yesterday']['sale'] : 0;
			$datas_before['week']['sale_avg'] = isset($datas_before['week']['sale']) ? intval($datas_before['week']['sale']/$i) : 0;
			$datas_before['month']['sale_avg'] = isset($datas_before['month']['sale'])? intval($datas_before['month']['sale']/$j) : 0;
			$datas_before['week']['sale_avg_ret'] = isset($datas_before['week']['sale_avg_ret']) && $i != 0 ? intval($datas_before['week']['sale_avg_ret']/$i) : 0;
			$datas_before['month']['sale_avg_ret'] = isset($datas_before['month']['sale_avg_ret']) && $i != 0 ? intval($datas_before['month']['sale_avg_ret']/$j) : 0;

			$this->set('data_today',$datas);
			$this->set('data_before',$datas_before);

		}

		if(!empty($id))$this->set('id',$id);
	}


	function overallReport($date=null){
		if($_SESSION['Auth']['User']['group_id'] != RELATIONSHIP_MANAGER && $_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR && $_SESSION['Auth']['User']['group_id'] != ADMIN && $_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR){
			$this->redirect('/');
		}

		if($date == null){
			$date = date('dmY',strtotime('-30 days')) . '-' . date('dmY',strtotime('-1 days'));
		}
             
		$dates = explode("-",$date);
		$date_from = $dates[0];
		$date_to = $dates[1];

		if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
			$date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
			$date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);

			if($date_to == date('Y-m-d')){
				$date_to = date('Y-m-d',strtotime('-1 days'));
			}

			if($_SESSION['Auth']['User']['group_id'] == RELATIONSHIP_MANAGER){
				$data = $this->Retailer->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp FROM `distributors_logs`,distributors WHERE distributors.rm_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND distributors.id = distributor_id AND date >= '$date_from' AND date <= '$date_to' group by distributor_id");
				$data_ter = $this->Retailer->query("SELECT sum(sale) as sale,trim(distributors.id) as rid,trim(distributors.company) as comp FROM `retailers_logs`,retailers,distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.rm_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND retailers_logs.date >= '$date_from' AND retailers_logs.date <= '$date_to' group by distributors.id");
			}
			else if($_SESSION['Auth']['User']['group_id'] == SUPER_DISTRIBUTOR){
				$data = $this->Retailer->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp FROM `distributors_logs`,distributors WHERE distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND distributors.id = distributor_id AND date >= '$date_from' AND date <= '$date_to' group by distributor_id");
				$data_ter = $this->Retailer->query("SELECT sum(sale) as sale,trim(distributors.id) as rid,trim(distributors.company) as comp FROM `retailers_logs`,retailers,distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND retailers_logs.date >= '$date_from' AND retailers_logs.date <= '$date_to' group by distributors.id");
			}
			else if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
				$data = array();//$this->Retailer->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(distributors.id) as rid,trim(distributors.company) as comp FROM `distributors_logs`,distributors WHERE distributors.parent_id = ".$this->info['id']." AND distributors.id NOT IN (".DISTS.") AND distributors.id = distributor_id AND date >= '$date_from' AND date <= '$date_to' group by distributor_id");
				$data_ter = $this->Retailer->query("SELECT retailers.id as rid , retailers.name as rname , retailers.mobile as rmobile , sum(sale) as sum_sale , sum(app_sale) as sum_app_sale , sum(ussd_sale) as sum_ussd_sale , sum(sms_sale) as sum_sms_sale ,  sum(topup) as sum_topup  FROM `retailers_logs`,retailers WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = ".$this->info['id']." AND date >= '$date_from' AND date <= '$date_to'  group by retailers.id order by sum_sale desc");
				$this->set('datas',$data_ter);
				$this->set('date_from',$date_from);
				$this->set('date_to',$date_to);
				$this->render('retailer_overall_report');
			}
			else {
				$data = $this->Retailer->query("SELECT sum(topup_sold) as second,sum(topup_buy) as prim,max(retailers)-min(retailers) as newr,trim(super_distributors.id) as rid,trim(super_distributors.company) as comp FROM `distributors_logs`,distributors,super_distributors WHERE distributors.parent_id = super_distributors.id AND super_distributors.id NOT IN (".SDISTS.") AND distributors.id = distributor_id AND date >= '$date_from' AND date <= '$date_to' group by super_distributors.id");
				$data_ter = $this->Retailer->query("SELECT sum(sale) as sale,trim(super_distributors.id) as rid,trim(super_distributors.company) as comp FROM `retailers_logs`,retailers,distributors,super_distributors WHERE retailers_logs.retailer_id = retailers.id AND retailers.parent_id = distributors.id AND distributors.parent_id = super_distributors.id AND super_distributors.id NOT IN (".SDISTS.") AND retailers_logs.date >= '$date_from' AND retailers_logs.date <= '$date_to' group by super_distributors.id");
			}

			$list = array();
			foreach($data as $dt){
				$list[$dt['0']['rid']]['primary'] = $dt['0']['prim'];
				$list[$dt['0']['rid']]['secondary'] = $dt['0']['second'];
				$list[$dt['0']['rid']]['newr'] = $dt['0']['newr'];
				$list[$dt['0']['rid']]['company'] = $dt['0']['comp'];
				$list[$dt['0']['rid']]['tertiary'] = 0;
			}

			foreach($data_ter as $dt){
				if($_SESSION['Auth']['User']['group_id'] != DISTRIBUTOR){
					$list[$dt['0']['rid']]['tertiary'] = $dt['0']['sale'];
					$list[$dt['0']['rid']]['company'] = $dt['0']['comp'];
				}
			}

			$list = Set::sort($list, '{[0-9]}.tertiary', 'desc');
			$this->set('datas',$list);
			$this->set('date_from',$date_from);
			$this->set('date_to',$date_to);
		}
	}


	function sReport()
	{
		if($_SESSION['Auth']['User']['group_id'] != RELATIONSHIP_MANAGER && $_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR && $_SESSION['Auth']['User']['group_id'] != ADMIN){
			$this->redirect('/');
		}

		$today = date('Y-m-d');
		$yest_date = date('Y-m-d',strtotime('-1 days'));
		$week_date = date('Y-m-d',strtotime('-7 days'));
		$month_date = date('Y-m-d',strtotime('-30 days'));
		$datas = array();
			
		if($_SESSION['Auth']['User']['group_id'] == ADMIN){

			foreach($this->sds as $sdist){
				$sdistid = $sdist['SuperDistributor']['id'];
				$data_trans = $this->User->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id = distributors.id AND distributors.parent_id=$sdistid");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id = distributors.id AND distributors.parent_id=$sdistid
				$data_bef_ret = $this->Retailer->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs,retailers,distributors WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id=distributors.id AND distributors.parent_id = $sdistid AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
				$i = 0;
				$j = 0;
				$datas_before = array();
				$datas_before['yesterday'] = 0;
				$datas_before['name'] = $sdist['SuperDistributor']['company'];
				$datas_before['today'] = $data_trans['0']['0']['sale'];
				foreach($data_bef_ret as $dt){
					if($dt['retailers_logs']['date'] == $yest_date){
						$datas_before['yesterday'] = $dt['0']['sale'];
					}
					if($dt['retailers_logs']['date'] >= $week_date){
						$datas_before['week'] += $dt['0']['sale'];
						$i++;
					}
					$datas_before['month'] += $dt['0']['sale'];
					$j++;
				}

				$datas_before['week'] = isset($datas_before['week'])&& !empty($i) ? intval($datas_before['week']/$i):0;
				$datas_before['month'] = isset($datas_before['month']) && !empty($j) ? intval($datas_before['month']/$j) : 0;

				$datas[] = $datas_before;
			}
		}
		else {
			foreach($this->ds as $dist){
				$distid = $dist['Distributor']['id'];
				$data_trans = $this->User->query("SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id = $distid");
				//SELECT count(distinct va.retailer_id) as cts,sum(va.amount) as sale FROM vendors_activations as va,retailers WHERE va.status != 2 AND va.status !=3 AND retailers.id = va.retailer_id AND va.date = '$today' AND retailers.parent_id = $distid
				$data_bef_ret = $this->Retailer->query("SELECT sum(retailers_logs.sale) as sale,count(retailers_logs.id) as transacting,retailers_logs.date FROM retailers_logs,retailers WHERE retailers.id = retailers_logs.retailer_id AND retailers.parent_id= $distid AND retailers_logs.date >= '".date('Y-m-d',strtotime('-30 days'))."' group by retailers_logs.date order by retailers_logs.date");
				$i = 0;
				$j = 0;
				$datas_before = array();

				$datas_before['name'] = $dist['Distributor']['company'];
				$datas_before['today'] = $data_trans['0']['0']['sale'];
				foreach($data_bef_ret as $dt){
					$datas_before['yesterday'] = 0;
					if($dt['retailers_logs']['date'] == $yest_date){
						$datas_before['yesterday'] = $dt['0']['sale'];
					}
					if($dt['retailers_logs']['date'] >= $week_date){
						$datas_before['week'] += $dt['0']['sale'];
						$i++;
					}
					$datas_before['month'] += $dt['0']['sale'];
					$j++;
				}
				$datas_before['week'] = empty($i)? 0 : intval( (isset($datas_before['week'])?$datas_before['week']:0 )/$i);
				$datas_before['month'] = empty($i)? 0 : intval((isset($datas_before['month'])?$datas_before['month']:0 )/$j);
				$datas[] = $datas_before;
			}
		}

		$this->set('datas',$datas);
	}

	function refundRetailer($ret_id,$amt){
		if($_SESSION['Auth']['User']['group_id'] != ADMIN){
			$this->redirect('/');
		}
		$shop = $this->Shop->getShopDataById($ret_id,RETAILER);

		$trans_id = $this->Shop->shopTransactionUpdate(REFUND,$amt,$ret_id,RETAILER);
		$bal = $this->Shop->shopBalanceUpdate($amt,'add',$ret_id,RETAILER);
		$this->Shop->addOpeningClosing($ret_id,RETAILER,$trans_id,$bal-$amt,$bal);

		$sms = "Dear Retailer,\nYou have got refund of Rs $amt from Pay1 company";
		$sms .= "\nYour current balance is now: Rs. " .$bal;

		$this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date) VALUES (".RETAILER.",$trans_id,$amt,2,'$sms','".date('Y-m-d')."')");
		$this->General->sendMessage($shop['mobile'],$sms,'payone');
		$this->autoRender = false;
	}

	function incentiveDistributor($id,$amt){
		if($_SESSION['Auth']['User']['group_id'] != ADMIN){
			$this->redirect('/');
		}
		$shop = $this->Retailer->query("SELECT mobile FROM users,distributors WHERE distributors.user_id = users.id AND distributors.id = $id");

		$trans_id = $this->Shop->shopTransactionUpdate(REFUND,$amt,$id,DISTRIBUTOR);
		$bal = $this->Shop->shopBalanceUpdate($amt,'add',$id,DISTRIBUTOR);
		$this->Shop->addOpeningClosing($id,DISTRIBUTOR,$trans_id,$bal-$amt,$bal);

		$sms = "Dear Distributor,\nYou have got incentive of Rs $amt from Pay1 company";
		$sms .= "\nYour current balance is now: Rs. " .$bal;

		$this->Retailer->query("INSERT INTO refunds (group_id,shoptrans_id,amount,type,note,date) VALUES (".DISTRIBUTOR.",$trans_id,$amt,1,'$sms','".date('Y-m-d')."')");
		$this->General->sendMessage($shop['0']['users']['mobile'],$sms,'payone');
		$this->autoRender = false;
	}

	function generic_match($template,$string,$varStart="#@",$varEnd="@#"){
		echo $templateStr = "RC SUCCESS.Cust:#@mobile@#.Amt:#@amount@#. on #@oth1@#.Comm:#@incentive@#CurrBal:#@balance@# (#@oth2@#).TxID #@transid@#.Dial#@oth3@#";
		$template = preg_quote($templateStr);

		echo "<br/>" , $string = preg_quote("RC SUCCESS.Cust:9892252205.Amt:200.00. on 2-04-2013 at 09:57PM.Comm:0CurrBal:39,533 (Pre=39,533,Post=0).TxID 417595258.Dial*122*#")/**/;
		// preg_match_all('/\d+([\.,\,]\d+)?/', $string, $arr);
		$template = str_replace($varStart,"|~|`",$template);
		$template = str_replace($varEnd,"`|~|",$template);

		$t=explode("|~|",$template);

		//$this->printArray($t);
		$tempStr = $string;
		$varStartIndx = 0;

		if( isset($t[0][0]) && $t[0][0]=="`" && $t[0][strlen($t[0])-1]=="`"){
			$valIndx = "even";
		}else{
			$valIndx = "odd";
		}

		$output = array();
		foreach($t as $key=>$val){
			$temp = array();
			if($key%2 == 0){//even key
				if($valIndx == "even"){
					//variable is value  [var - even]
					 
					//extract variable value from given input
					if($t[$key+1]==''){
						$var_val = '';
					}else{
						$var_val = strstr($tempStr, $t[$key+1], true);
					}
					if(isset($t[$key+1])){
						$var_val = strstr($tempStr, $t[$key+1], true);
						$temp['var_val'] = $var_val;
					}else{
						$temp['var_val'] = $tempStr;//if variable value  = remaing string
					}
					 
					// ----- replace first occuerence of $temp['var_val'] in $tempStr -----
					if($temp['var_val']==''){
						$pos = strlen($tempStr)+2;
					}else{
						$pos = strpos($tempStr,$temp['var_val']);
					}
					 
					if ($pos !== false) {
						$tempStr = substr_replace($tempStr,"",$pos,strlen($temp['var_val']));
					}
					//---------------------------------------------------------------------
					 
					$temp['var_val'] = stripslashes($temp['var_val']);// reverse preg_quote in variable value
					array_push($output, $temp);
					 
				}else{
					//variable is template part [var - odd]
					 
					// ----- replace first occuerence of $val in $tempStr -----
					if($val==''){//check because strpos does not support empty params
						$pos = strlen($tempStr)+2;
					}else{
						$pos = strpos($tempStr,$val);
					}
					if ($pos !== false) {
						$tempStr = substr_replace($tempStr,"",$pos,strlen($val));
					}
					//----------------------------------------------------------

				}
			}else{//odd key
				if($valIndx == "odd"){
					//variable is value  [var - odd]
					$temp['var_name'] = substr($val,1,strlen($val)-2);
					//extract variable value from given input
					if(isset($t[$key+1])){
						if($t[$key+1]==''){
							$var_val = '';
						}else{
							$var_val = strstr($tempStr, $t[$key+1], true);
						}
						 
						$temp['var_val'] = $var_val;
					}else{
						$temp['var_val'] = $tempStr;//if variable value  = remaing string
					}
					// ----- replace first occuerence of $temp['var_val'] in $tempStr -----
					if($temp['var_val']==''){
						$pos = strlen($tempStr)+2;
					}else{
						$pos = strpos($tempStr,$temp['var_val']);
					}
					if ($pos !== false) {
						$tempStr = substr_replace($tempStr,"",$pos,strlen($temp['var_val']));
					}
					//---------------------------------------------------------------------
					$temp['var_val'] = stripslashes($temp['var_val']);// reverse preg_quote in variable value
					array_push($output, $temp);
					 
				}else{
					//variable is template part  [var - even]
					// ----- replace first occuerence of $val in $tempStr -----
					if($val==''){
						$pos = strlen($tempStr)+2;
					}else{
						$pos = strpos($tempStr,$val);
					}
					if ($pos !== false) {
						$tempStr = substr_replace($tempStr,"",$pos,strlen($val));
					}
					//---------------------------------------------------------
					 
				}
			}


		}

		echo "<pre>";
		print_r($output);
		echo "</pre>";
		$this->autoRender = false;
	}
        
	function changeDistributorMobileNo($oldNo,$newNo){

		$response = array();
		$this->autoRender = FALSE;
		if(empty($_SESSION['Auth']) || ($_SESSION['Auth']['User']['group_id'] != ADMIN && $_SESSION['Auth']['User']['group_id'] != SUPER_DISTRIBUTOR)){
			$response = array(
                                    'status' => FALSE ,
                                    'errors' => array('code'=>'E000','msg'=>'UnAuthorized Access.')
			) ;



		}else if(empty ($oldNo) || empty ($newNo)){
			$response = array(
                                    'status' => FALSE ,
                                    'errors' => array('code'=>'E001','msg'=>'Input Nos should not be empty .')
			) ;


		}else{


			 
			$oldData = $this->User->query("SELECT * FROM users WHERE mobile = '".$oldNo."' and group_id = ".DISTRIBUTOR);
			if(empty ($oldData)){
				//error user-distributor does'nt exist
				$response = array(
                                            'status' => FALSE ,
                                            'errors' => array('code'=>'E002','msg'=>'Distributor(user) not exist for Given mobile number .')
				) ;

			}else{
				$oldDistData = $this->User->query("SELECT id,user_id,company FROM distributors WHERE user_id = '".$oldData[0]['users']['id']."'");
				$newData = $this->User->query("SELECT * FROM users WHERE mobile = '".$newNo."'");

				if(empty ($newData)){//check if new_no not already exist )
					$oldSalesMen = $this->User->query("SELECT id FROM salesmen WHERE mobile = '".$oldNo."' and dist_id=".$oldDistData[0]['distributors']['id']);
					if(empty($oldSalesMen)){// if salesmen entry not exist for given user
						$response = array(
                                                    'status' => FALSE ,
                                                    'errors' => array('code'=>'E003','msg'=>'Salesmen entry not exist for given user.')
						) ;
					}else{
						$this->User->query("update users set mobile = '".$newNo."'"." where id = ".$oldData[0]['users']['id']);
						//update numbers in salesman table
						$newSalesMen = $this->User->query("SELECT id FROM salesmen WHERE mobile = '".$newNo."' and dist_id=".$oldDistData[0]['distributors']['id']);
						if(empty($newSalesMen)){//ONLY UPDATE SALESMEN NUMBER
							$newData = $this->User->query("update salesmen set mobile = '".$newNo."'"." where id = ".$oldSalesMen[0]['salesmen']['id']);
						}else{//SWAP SALESMEN NUMBER
							$dumyNo = '1111111111';// because of unique constraint
							$this->User->query("update salesmen set mobile = '".$dumyNo."'"." where id = ".$oldSalesMen[0]['salesmen']['id']);
							$this->User->query("update salesmen set mobile = '".$oldNo."'"." where id = ".$newSalesMen[0]['salesmen']['id']);
							$this->User->query("update salesmen set mobile = '".$newNo."'"." where id = ".$oldSalesMen[0]['salesmen']['id']);
						}
						$response = array(
                                            'status' => TRUE ,
                                            'success' => array('code'=>'S001','msg'=>'Number changed successfully .')
						) ;
					}
				}else{//( if  existing number )
					if($newData[0]['users']['group_id'] != MEMBER){// if existing user is not a member then error

						$response = array(
                                            'status' => FALSE ,
                                            'errors' => array('code'=>'E004','msg'=>'Given Mobile No is already exist in users table and goup_id is not a member .  .')
						) ;
					}else{// existing user is a member
						//update numbers in salesman table
						$oldSalesMen = $this->User->query("SELECT id FROM salesmen WHERE mobile = '".$oldNo."' and dist_id=".$oldDistData[0]['distributors']['id']);
						$newSalesMen = $this->User->query("SELECT id FROM salesmen WHERE mobile = '".$newNo."' and dist_id=".$oldDistData[0]['distributors']['id']);


						if(empty($oldSalesMen)){// if salesmen entry not exist for given user
							$response = array(
                                            'status' => FALSE ,
                                            'errors' => array('code'=>'E005','msg'=>'Salesmen entry not exist for given user.')
							) ;
						}else{
							//swap numbers in user table
							$dumyNo = '1111111111';// because of unique constraint
							$rn = $this->User->query("update users set mobile = '".$dumyNo."'"." where id = ".$oldData[0]['users']['id']);
							$ro = $this->User->query("update users set mobile = '".$oldNo."'"." where id = ".$newData[0]['users']['id']);//" , group_id = ".MEMBER.
							$rn = $this->User->query("update users set mobile = '".$newNo."'"." where id = ".$oldData[0]['users']['id']);//" , group_id = ".DISTRIBUTOR.

							if(empty($newSalesMen)){//ONLY UPDATE SALESMEN NUMBER
								$newData = $this->User->query("update salesmen set mobile = '".$newNo."'"." where id = ".$oldSalesMen[0]['salesmen']['id']);
							}else{//SWAP SALESMEN NUMBER
								$dumyNo = '1111111111';// because of unique constraint
								$rso = $this->User->query("update salesmen set mobile = '".$dumyNo."'"." where id = ".$oldSalesMen[0]['salesmen']['id']);
								$rsn = $this->User->query("update salesmen set mobile = '".$oldNo."'"." where id = ".$newSalesMen[0]['salesmen']['id']);
								$rso = $this->User->query("update salesmen set mobile = '".$newNo."'"." where id = ".$oldSalesMen[0]['salesmen']['id']);
							}

							$response = array(
                                        'status' => TRUE ,
                                        'success' => array('code'=>'S002','msg'=>'Number changed successfully .')
							) ;
                                                        //Send SMS to old and new no 
                                                        //---- Send SMS ( mobile no changed msg ) to distributor  ------
							// variable array contains the values which is to be parsed in sms_body
                                                            $varParseArrOldNo = array (
                                                         'dist_old_mobile'           =>  $oldNo,
                                                         'dist_new_mobile'           =>  $newNo,
                                                         'dist_name'             =>  $oldDistData[0]['distributors']['company'],                                                
                                                            );
                                                            $this->General->sendTemplateSMSToMobile($oldNo,"smsToDistributorOnOldNoAboutMoblieNoChange",$varParseArrOldNo);
                                                            
                                                           /* $varParseArrNewNo = array (
                                                         'dist_old_mobile'           =>  $oldNo,
                                                         'dist_new_mobile'           =>  $newNo,
                                                         'dist_name'             =>  $oldDistData[0]['distributors']['company'],                                                
                                                            );
                                                            $this->General->sendTemplateSMSToMobile($newNo,"smsToDistributorOnNewNoAboutMoblieNoChange",$varParseArrNewNo);
							//---------------------------------------------------------------------------- */
						}



					}
				}


			}
		}
		echo "<pre>";
		print_r($response);
		echo "</pre>";

	}

	function floatGraph(){ 
            if (empty($_SESSION['Auth']) || ($_SESSION['Auth']['User']['group_id'] != ADMIN )) {
                $this->redirect('/');
            }
            $dt_to = isset($_REQUEST['from']) ? $_REQUEST['to'] : "";
            $dt_from = isset($_REQUEST['to']) ? $_REQUEST['from'] : "";
            $type = empty($_REQUEST['type']) ? "hourly" : $_REQUEST['type'];
            $qp = "";
            $to = "";
            $from = "";

            $qp = " where 1";
            if (!empty($dt_to) && !empty($dt_from)) {
                $qp = $qp . " and ( date >= '" . substr($dt_from, 4, 4) . "-" . substr($dt_from, 2, 2) . "-" . substr($dt_from, 0, 2) . "' and date <= '" . substr($dt_to, 4, 4) . "-" . substr($dt_to, 2, 2) . "-" . substr($dt_to, 0, 2) . "' ) or `date` = '".date("Y-m-d")."'";

                $to = substr($dt_to, 4, 4) . "-" . substr($dt_to, 2, 2) . "-" . substr($dt_to, 0, 2);
                $from = substr($dt_from, 4, 4) . "-" . substr($dt_from, 2, 2) . "-" . substr($dt_from, 0, 2);
                $interval = date_diff(date_create($from), date_create($to));
                if (intval($interval->format('%a')) > 15) {// if request is for more than 15 days then it will show only 15 days before data of  to_date
                    //$to = date("Y-m-d");
                    $date = new DateTime($to);
                    $date->modify('-15 day');
                    $from = $date->format('Y-m-d');
                }
            } else {
                //date ( $format, strtotime ( '-7 day' . $date ) )
                $to = date("Y-m-d");
                $date = new DateTime($to);
                $date->modify('-7 day');
                $from = $date->format('Y-m-d');
                $qp = $qp . " and date >= '" . $from . "' and date <= '" . $to . "'";
            }


            $optionSales = array(
                'title' => 'Hourly Sale Report',
                'width' => 1200,
                'height' => 600,
                'vAxis' => array('title' => "Amount"),
                'hAxis' => array('title' => "Hours"),
                'seriesType' => "bars",
                'series' => array(1 => array("type" => "line", 'curveType'=> "function",'pointSize' => 3 , 'color'=>'red'),2 => array("type" => "line",'curveType'=> "function", 'pointSize' => 3,'color'=>'green'))//1 => array("type" => "line",'curveType'=> "function", 'pointSize' => 3),
             );



            $datas = $this->Retailer->query("SELECT id , sale , float_logs.float , `date` , hour FROM float_logs $qp order by `date` , `hour` "); //and hour = 24 

            $dateWiseFloatData = array();

            $datewiseSaleGraphData = array();
            $hourlyData = array();
            $hourlyTodayData = array(); // , 1 =>634.5 ,2 =>172 ,  1 =>634.5 , 2 =>172
            $hourlyTillTodayData = array();
            $prevHrSale = 0;
            $prevDate = "";
            $tillHourData = array();
            $totalTillHourData = array();
            $avgTillHourData = 0;
            $countDayEndHourData = 0;
            $sumDayEndHourData = 0;
            foreach ($datas as $data) {
                $d = $data['float_logs'];
                $hrSale = 0;

                if ($d['hour'] == 1) {
                    $hrSale = $d['sale'];
                } else {
                    $hrSale = $d['sale'] - $prevHrSale;
                }
                $prevHrSale = $d['sale'];
                $prevDate = $d['date'];
                if (!isset($hourlyData[$d['hour']])) {
                    $hourlyData[$d['hour']] = array();
                }
                if ($d['date'] != date('Y-m-d')) {
                     array_push($hourlyData[$d['hour']], $hrSale);
                }
                
                if (!isset($tillHourData[$d['hour']])) {
                    $tillHourData[$d['hour']] = array();
                }
                if ($d['date'] != date('Y-m-d')) {
                    array_push($tillHourData[$d['hour']], $d['sale']);
                }
                if ($d['date'] == date('Y-m-d')) {
                    $hourlyTodayData[$d['hour']] = $hrSale;
                    $hourlyTillTodayData[$d['hour']] = $d['sale'];
                }
                

                $dtStr = date("d-M-Y", strtotime($data['float_logs']['date']));
                if ($data['float_logs']['hour'] == 24) {
                    array_push($totalTillHourData , $data['float_logs']['sale']);
                    
                    $sumDayEndHourData = $sumDayEndHourData + $data['float_logs']['sale'];
                    $countDayEndHourData++;
                    array_push($datewiseSaleGraphData, array($dtStr, intval($data['float_logs']['sale'])));
                    
                    $dateWiseFloatData[$dtStr]['day_end'] = $data['float_logs']['float'];
                }
                //$dateWiseFloatData[$dtStr]['min'] = 
                if (isset($dateWiseFloatData[$dtStr]['min'])) {
                    if ($dateWiseFloatData[$dtStr]['min'] >= $d['float']) {
                        $dateWiseFloatData[$dtStr]['min'] = $d['float'];
                        $dateWiseFloatData[$dtStr]['min_hour'] = $d['hour'];
                    }

                    if ($dateWiseFloatData[$dtStr]['max'] <= $d['float']) {
                        $dateWiseFloatData[$dtStr]['max'] = $d['float'];
                        $dateWiseFloatData[$dtStr]['max_hour'] = $d['hour'];
                    }
                } else {
                    $dateWiseFloatData[$dtStr]['min'] = $d['float'];
                    $dateWiseFloatData[$dtStr]['min_hour'] = $d['hour'];

                    $dateWiseFloatData[$dtStr]['max'] = $d['float'];
                    $dateWiseFloatData[$dtStr]['max_hour'] = $d['hour'];
                }
                //array_push($graphData3['data'],array($dtStr,$data['float_logs']['float']));
            }

            $hourlyAvgData = array();
            foreach ($hourlyData as $key => $data) {
                $hourlyAvgData[$key] = array_sum($data) / count($data);
            }
            
            $datewiseSaleGraphDataWithAvg = array();
            $avgDayEndHourData = $countDayEndHourData==0 ? 0 : $sumDayEndHourData / $countDayEndHourData ;
            foreach ($datewiseSaleGraphData as $key => $data) {
                array_push( $datewiseSaleGraphDataWithAvg , array($data[0], $data[1] ,$avgDayEndHourData) );
            }
                        
            array_unshift($datewiseSaleGraphDataWithAvg, array('Date', 'Sale' ,'Average'));
            $this->set('datewisesaledata', $datewiseSaleGraphDataWithAvg);
            

            $tillHourAvgData = array();
            foreach ($tillHourData as $key => $data) {
                $tillHourAvgData[$key] = array_sum($data) / count($data);
            }
            $avgTillHourData = count($totalTillHourData) == 0 ? 0 : array_sum($totalTillHourData) / count($totalTillHourData);
            


            $saleData = array();
            $sumAvg = 0;
            $sumCurr = 0;
            $perDiff = 0;
            $hdAvg = 0;
            $hdCurr = 0;
            $prevAvg = 0;
            $prevCurr = 0;
            $sumDip = 0;
            $countDip = 0;
            $finalSale = 0;
            $calPerNumSum = 0;
            $calPerDenSum = 0;
            $calPer = 0;
            // main logic part
            foreach ($hourlyAvgData as $h => $data) {
                if (isset($hourlyTodayData[$h])) {
                                       
                    $diffTill = $hourlyTillTodayData[$h] - $tillHourAvgData[$h];
                    $perDiffTill = $tillHourAvgData[$h] * 100 / $tillHourAvgData[24];
                    
                    $calPerNum = ( $diffTill / $tillHourAvgData[$h] ) * $perDiffTill; // numerator single part
                    $calPerDen = $perDiffTill;
                    $calPerNumSum = $calPerNumSum*0.5 + $calPerNum;//numerator part
                    $calPerDenSum = $calPerDenSum*0.5 + $calPerDen;//denominator part
                    $calPer = $calPerNumSum / $calPerDenSum * 100;// final weighted mean (nuemerator part / denominator part)
                    
                    $temp = array($h . "", intval($data),intval($tillHourAvgData[$h]),intval($hourlyTillTodayData[$h]));//, intval($hourlyTodayData[$h])

                    //$finalSale = $finalSale + $prevCurr;
                } else { // here we will calculate exacted data
                    
                	//echo $calPer . "<br/>";
                    $expDiffTillSale = $tillHourAvgData[$h] * $calPer / 100;
                    $expTillSale = $tillHourAvgData[$h] + $expDiffTillSale;
                    
                    $temp = array($h . "", intval($data),intval($tillHourAvgData[$h]),intval($expTillSale));//, intval($expSale)

                    //$finalSale = $finalSale + $prevCurr;
                }
                array_push($saleData, $temp);
            }
            array_unshift($saleData, array('Hours', 'Average',"Avg Sale Trend","Today's Sale Trend"));//,"Today's Sale Trend" //, 'Today' . " = " . round($finalSale, 0) . " \n  [ " . round($perDiff, 2) . " % ]"
            //echo json_encode($saleData);
            $this->set('saledata', $saleData);
            $this->set('type', $type);
            $this->set('to', $to);
            $this->set('from', $from);

            $this->set('optionSales', $optionSales);

            $floatdata = array(
                    // array('Date', 'Min','Max','Day End'),
                    // array('01-May-2013',  165, 551.5 ,938      ),
                    // array('29-May-2013', 135,  627.5 ,  1120   )
            );
            foreach ($dateWiseFloatData as $key => $data) {
                array_push($floatdata, array($key, "", intval($data['min']), intval($data['max']), intval(isset($data['day_end']) ? $data['day_end'] : 0)));
            }


            $optionFloat = array(
                'title' => 'Daily Float Report',
                'width' => 1200,
                'height' => 600,
                'vAxis' => array('title' => "Amount"),
                'hAxis' => array('title' => "Date"),
                'seriesType' => "bars",
                'tooltip' => array('isHtml' => true),
                'series' => array(0 => array('type' => 'line', 'pointSize' => 5), 1 => array('type' => 'line', 'pointSize' => 5), 2 => array('type' => 'line', 'pointSize' => 5)),
                'focusTarget' => 'category',
            );
            $this->set('floatdata', $floatdata);
            $this->set('optionFloat', $optionFloat);


            $optionDateWiseSale = array(
                'title' => 'Daily Sale Report',
                'width' => 1200,
                'height' => 600,
                'vAxis' => array('title' => "Amount"),
                'hAxis' => array('title' => "Date"),
                'seriesType' => "bars",
                'series' => array(0 => array('type' => 'line', 'pointSize' => 5),1 => array('type' => 'line', 'pointSize' => 5))//
            );

            $this->set('optionDateWiseSale', $optionDateWiseSale);
            $this->render('float_graph');
	}
        
	function modemRequest(){
		$response = array();
		$this->autoRender = FALSE;
		if(empty($_SESSION['Auth'])){
			$response = array('status'=>'failure','errno'=>'0','data'=>  $this->Shop->errors(0));
		}else{
			$type   =  isset($_REQUEST['type']) ? $_REQUEST['type'] : "" ;
			$vendor   =  isset($_REQUEST['vendor']) ? $_REQUEST['vendor'] : "" ;
			$device   =  isset($_REQUEST['device']) ? $_REQUEST['device'] : "" ;
			$ret = "";
			if($type == 1){//to send SMS
				$mobile   =  isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : "" ;
				$msg   =  isset($_REQUEST['msg']) ? $_REQUEST['msg'] : "" ;
				$adm = "query=command&type=1&device=$device&mobile=$mobile&msg=".urlencode($msg);// create query to send SMS // @TODO
				$ret = $this->Shop->modemRequest($adm,$vendor);// @TODO

			}else if($type == 2){//to execute Command
				$cmd   =  isset($_REQUEST['cmd']) ? $_REQUEST['cmd'] : "" ;
				$command   =  isset($_REQUEST['command']) ? $_REQUEST['command'] : "" ;
				$wait   =  isset($_REQUEST['wait']) ? $_REQUEST['wait'] : "" ;
				$adm = "query=command&type=2&device=$device&command=$command&time=$wait";// create query to run AT Command // @TODO
				$ret = $this->Shop->modemRequest($adm,$vendor);

			}else if($type == 3){//to USSD Command
				$cmd   =  isset($_REQUEST['ussd']) ? $_REQUEST['ussd'] : "" ;
				$command   =  isset($_REQUEST['command']) ? $_REQUEST['command'] : "" ;
				$wait   =  isset($_REQUEST['wait']) ? $_REQUEST['wait'] : "" ;
				$adm = "query=command&type=3&device=$device&command=$command&time=$wait";// create query to run AT Command // @TODO
				$ret = $this->Shop->modemRequest($adm,$vendor);

			}else {//wrong type
				$response = array('status'=>'failure','errno'=>'2','data'=>  $this->Shop->errors(2));
			}
			
			if(!isset($response['status'])){
				if(empty($ret))$response = array('status'=>'failure','errno'=>'2','data'=>  'Device Busy');
				else $response = array('status'=>'success','data'=>  $ret);
			}
		}

		return json_encode($response);
	}
//        function partnerPanel(){ //$trans=null,$par=null
//                
//            if(empty ($_SESSION['Auth']['partner_id'])){
//                    $this->redirect("/");
//                }
//                $isPartner = $_SESSION['Auth']['is_partner'] ;
//        	$partnerId = $_SESSION['Auth']['partner_id'] ;
//                
//                $dt_to = isset($_REQUEST['from']) ? $_REQUEST['to'] : "";
//                $dt_from = isset($_REQUEST['to']) ? $_REQUEST['from'] : "";
//                $partnerLogs = array();
//                $qp = " where 1";
//                if (!empty($dt_to) && !empty($dt_from)) {
//                    $qp = $qp . " and created >= '" . $dt_from. "'". " and created <= '" . $dt_to. "'";
//
//                    $interval = date_diff(date_create($dt_from), date_create($dt_to));
//                    if (intval($interval->format('%a')) > 15) {// if request is for more than 15 days then it will show only 15 days before data of  to_date
//                        $date = new DateTime($to);
//                        $date->modify('-15 day');
//                        $dt_from = $date->format('Y-m-d');
//                    }
//                } else {
//                    //date ( $format, strtotime ( '-7 day' . $date ) )
//                    $dt_to = date("Y-m-d");
//                    $dt_from = date("Y-m-d");
//                }
//                
//        	
//                   $partnerLogs=$this->Retailer->query("SELECT pl . * , oc.opening, oc.opening
//                                                            FROM partners_log pl
//                                                            LEFT JOIN partners p ON pl.partner_id = p.id
//                                                            LEFT JOIN retailers r ON p.retailer_id = r.id
//                                                            LEFT JOIN vendors_activations va ON pl.vendor_actv_id = va.ref_code
//                                                            LEFT JOIN opening_closing oc ON oc.shop_transaction_id = va.shop_transaction_id and oc.shop_id = ".$this->Session->read('Auth.User.id')." and oc.group_id = ".$this->Session->read('Auth.User.group_id')." 
//                                                        WHERE pl.partner_id = $partnerId and "." pl.created >= '" . $dt_from . "' and pl.created <= '" . $dt_to . "'");
//
//               
//                //print_r($partnerLogs);
//                $this->set('partnerLogs',$partnerLogs);
//                $this->set('side_tab','rechargelist');//transferdetails discounstructure
//            $this->render('partner_panel');
//        }
        function partnerTrans($dates=null,$pageNo=1){ //$trans=null,$par=null
                
                if(empty ($_SESSION['Auth']['partner_id'])){
                   $this->redirect("/");
                }
                $isPartner = isset($_SESSION['Auth']['is_partner'])?$_SESSION['Auth']['is_partner'] : null;
        	$partnerId = isset ($_SESSION['Auth']['partner_id']) ? $_SESSION['Auth']['partner_id'] : null;
                                
                $arr = empty ($dates) ? array() : explode("-",$dates);
                $dt_from = isset($arr[0]) ? $arr[0] : "";
                $dt_to = isset($arr[1]) ? $arr[1] : "";
                $partnerLogs = array();
                if (!empty($dt_to) && !empty($dt_from)) {
                    
                    $dt_to = substr($dt_to, 4, 4) . "-" . substr($dt_to, 2, 2) . "-" . substr($dt_to, 0, 2);
                    $dt_from = substr($dt_from, 4, 4) . "-" . substr($dt_from, 2, 2) . "-" . substr($dt_from, 0, 2);
                    /*$interval = date_diff(date_create($dt_from), date_create($dt_to));
                    if (intval($interval->format('%a')) > 15) {// if request is for more than 15 days then it will show only 15 days before data of  to_date
                        $date = new DateTime($to);
                        $date->modify('-15 day');
                        $dt_from = $date->format('Y-m-d');
                    }*/
                } else {
                    //date ( $format, strtotime ( '-7 day' . $date ) )
                    $dt_to = date("Y-m-d");
                    $dt_from = date("Y-m-d");
                }
                $this->set('date_from',$dt_from);
                $this->set('date_to',$dt_to);
        	
               
		$itemsPerPage = PAGE_LIMIT;

		$result = array();
		if($itemsPerPage <= 0 || $pageNo <= 0){
			$queryPart = " limit 0 , ".PAGE_LIMIT;
		}else{
			$ll = $itemsPerPage * ( $pageNo - 1 ) ;//+ 1; // lower limit
			$ul = $itemsPerPage  ;// upper limit//* $pageNo
			$queryPart = " limit $ll , $ul ";

		}
                
               $partnerLogs=$this->Retailer->query("SELECT pl . * , oc.opening, oc.opening
                                                        FROM partners_log pl
                                                        LEFT JOIN partners p ON pl.partner_id = p.id
                                                        LEFT JOIN retailers r ON p.retailer_id = r.id
                                                        LEFT JOIN vendors_activations va ON pl.vendor_actv_id = va.ref_code
                                                        LEFT JOIN opening_closing oc ON oc.shop_transaction_id = va.shop_transaction_id and oc.shop_id = ".$this->Session->read('Auth.User.id')." and oc.group_id = ".$this->Session->read('Auth.User.group_id')." 
                                                    WHERE pl.partner_id = $partnerId and "." pl.created >= '" . $dt_from . "' and pl.created <= '" . $dt_to . "'".$queryPart);
               
               $partnerLogsCount=$this->Retailer->query("SELECT count(*) as cnt
                                                        FROM partners_log pl
                                                        LEFT JOIN partners p ON pl.partner_id = p.id
                                                        LEFT JOIN retailers r ON p.retailer_id = r.id
                                                        LEFT JOIN vendors_activations va ON pl.vendor_actv_id = va.ref_code
                                                        LEFT JOIN opening_closing oc ON oc.shop_transaction_id = va.shop_transaction_id and oc.shop_id = ".$this->Session->read('Auth.User.id')." and oc.group_id = ".$this->Session->read('Auth.User.group_id')." 
                                                    WHERE pl.partner_id = $partnerId and "." pl.created >= '" . $dt_from . "' and pl.created <= '" . $dt_to . "'");

               
                //print_r($partnerLogs);
               $total_count = $partnerLogsCount[0][0]['cnt'];
               $this->set('total_count',$total_count);
               $this->set('page',$pageNo);
               $this->set('top_tab','home');
               $this->set('side_tab','rechargelist');
               $this->set('transactions',$partnerLogs);
               $this->render('/elements/partner_trans');
        }
        function buyReportPartner($date = null,$page=1){
		if(empty ($_SESSION['Auth']['partner_id'])){
                   $this->redirect("/");
                }
                if(is_array($date)){
			$api = true;
			$params = $date;
			$date = $params['date'];
			$page = $params['page'];
		}
                $grp_id = $_SESSION['Auth']['User']['group_id'];
                $is_partner = $_SESSION['Auth']['is_partner'];
		if($grp_id != RETAILER || $is_partner != 'true' ) $this->render('/shops/partnerPanel');
                
		
		if(empty($date)){
                    $date = date('dmY')."-".date('dmY');
                }
		
                //$limit = " limit " . ($page-1)*PAGE_LIMIT.",".PAGE_LIMIT;

                $dates = explode("-",$date);
                $date_from = $dates[0];
                $date_to = $dates[1];

                $transactions = array();
                if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
                        $date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
                        $date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
                }else{
                        $date_from =  date('Y') . "-" . date('m') . "-" . date('d');
                        $date_to =  date('Y') . "-" . date('m') . "-" . date('d');
                }
		
                $params = array();
                $params['date_from'] = $date_from;
                $params['date_to'] = $date_to ;
                $params['page_no'] =  $page;
                $params['items_per_page'] = PAGE_LIMIT ;
               
                //Instantiation
                App::import('Controller', 'Apis');
                
                //Instantiation
                $apis = new ApisController;
                
                //Load model, components...
                //$apis->constructClasses();

                $transactions = array();
                $total_count = 0;
                $results = $apis->getDistToRetlBalTransfer($params,"JSON");
               // print_r($results);
                if($results['status'] == 'success'){
                        $transactions = $results['data'];
                        $total_count = $results['total_count'];
                }
                $this->set('date_from',$date_from);
                $this->set('date_to',$date_to);
                $this->set('page',$page);
                $this->set('total_count',$total_count);
                $this->set('top_tab','home');
                $this->set('side_tab','transferdetails');//transferdetails discounstructure
		$this->set('transactions',$transactions);
                
                $this->render('buy_report_partner');
               
	}
        function discountStructurePartner($id=0){
            if(empty ($_SESSION['Auth']['partner_id'])){
                   $this->redirect("/");
            }
            $partnerId = $_SESSION['Auth']['partner_id'] ;
            $retailerId = $_SESSION['Auth']['User']['id'];
            $slabId = $_SESSION['Auth']['slab_id'];
            $slabProducts = $this->Retailer->query("SELECT 
                                                        `slabs_products`.`slab_id`,
                                                        `slabs_products`.`product_id`,
                                                        `slabs_products`.`percent`,
                                                        `products`.`name`,
                                                        `products`.`service_id`
                                                    FROM 
                                                        `slabs_products` , `products`
                                                    WHERE
                                                        `slabs_products`.`product_id` = `products`.`id`  and
                                                        `slabs_products`.`slab_id` = $slabId  ");
            //print_r($slabProducts);discounstructure
            $this->set('top_tab','home');
             $this->set('side_tab','discounstructure');
            $this->set('transactions',$slabProducts);
            
        }
	function testModemRequest(){

	}
        function distTopUpRequest(){
            if($this->Session->check('Auth.User.id')){
                $dist_user = $this->General->getUserDataFromId($_SESSION['Auth']['user_id']);
                $this->set('mobile',$dist_user['mobile']);//home
                if($_SESSION['Auth']['User']['group_id'] == DISTRIBUTOR){
                    $top_tab = 'activities';
                }else{
                    $top_tab = 'home';
                }
                $this->set('top_tab',$top_tab);
                $this->render('dist_topup_request');
            }else{
                echo "Authentication Error !!!";
            }
	}
        
        
        function allRetailerTrans($ret_id = 0,$productId=0,$date=null,$page=1,$itemsPerPage=PAGE_LIMIT){
          
            if(empty($date)){
                $date = date('dmY')."-".date('dmY');
            }

            //$limit = " limit " . ($page-1)*PAGE_LIMIT.",".PAGE_LIMIT;

            $dates = explode("-",$date);
            $date_from = $dates[0];
            $date_to = $dates[1];

            $transactions = array();
            if(checkdate(substr($date_from,2,2), substr($date_from,0,2), substr($date_from,4)) && checkdate(substr($date_to,2,2), substr($date_to,0,2), substr($date_to,4))){
                    $date_from =  substr($date_from,4) . "-" . substr($date_from,2,2) . "-" . substr($date_from,0,2);
                    $date_to =  substr($date_to,4) . "-" . substr($date_to,2,2) . "-" . substr($date_to,0,2);
            }else{
                    $date_from =  date('Y') . "-" . date('m') . "-" . date('d');
                    $date_to =  date('Y') . "-" . date('m') . "-" . date('d');
            }
            $this->set('date_from',$date_from);
            $this->set('date_to',$date_to);
            $this->set('page',$page);
            $this->set('ret_id',$ret_id);
            
           //($date,$page=1,$service = null,$date2=null,$itemsPerPage=0,$retailerId=0,$operatorId=0)
            $records = $this->Shop->getLastTransactions($date_from,$page,null,$date_to,$itemsPerPage,$ret_id,$productId);
           
            $products = $this->Retailer->query("SELECT id , name  FROM `products` WHERE to_show = 1 ORDER BY name asc");
            $this->set('productId',$productId);
            $this->set('products',$products);
           
           
           $this->set('transactions',$records['ret']);
           $this->set('total_count',$records['total_count']);
           $this->set('side_tab','discounstructure');
           $this->render('dist_retailer_trans');//$this->autoRender = false;
	}
        function newTemp ($hr){
            //$this->autoRender = false;
            $this->layout = "signal7";
            echo "hello".$hr;
        }

}

?>
